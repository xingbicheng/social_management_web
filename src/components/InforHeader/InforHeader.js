import React,{Component}from 'react';
import {DatePicker,Col,Row,Form,Input,Button,Select} from 'antd';
import { FormItemLayoutPage } from '../../config/config';
import styles from '../../theme/table.less';

const Option = Select.Option;
const FormItem = Form.Item;

export default class InforHeader extends Component{


  render(){
    const formItemLayout = FormItemLayoutPage;
    return (
      <div>
        <Row>
          <Form className={styles.searchBox}>
            <Col span={8}>
              <FormItem
                {...formItemLayout}
                label='标题'
              >
                <Input style={{ width: '100%' }} />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem
                {...formItemLayout}
                label="状态"
              >
                <Select>
                  <Option value='1'>保存</Option>
                  <Option value='2'>未保存</Option>
                </Select>
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem
                {...formItemLayout}
                style={{marginRight:0}}
                className={styles.btnBox}
              >
                <div style={{ width:'100%' }}>
                  <Button >重置</Button>
                  <Button type="primary" icon='search'>查询</Button>
                </div>
              </FormItem>
            </Col>
          </Form>
        </Row>
      </div>
    );
  }
}
