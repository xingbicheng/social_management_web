import React,{ Component } from 'react';
import { DataView } from '@antv/data-set';
import { Chart, Axis, Geom, Tooltip, Facet, View, Legend, Label, Coord } from 'bizcharts';

class TreeMap extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const { dataSource } = this.props;
        const data = {
            children:dataSource || [],
        }
        const dv = new DataView();
        dv.source(data, {
            type: 'hierarchy',
        }).transform({
            field: 'value',
            type: 'hierarchy.treemap',
            tile: 'treemapResquarify',
            as: ['x', 'y'],
        });
        const nodes = dv.getAllNodes();
        nodes.map(node => {
            node.name = node.data.x;
            node.value = node.data.y;
            return node;
        });

        const scale = {
            value:{nice:false},
        }

        const htmlStr = '<li data-index={index}>'
            + '<span style="background-color:{color};" class="g2-tooltip-marker"></span>'
            + '{x}<br/>'
            + '<span style="padding-left: 16px">浏览人数：{y}</span><br/>'
            + '</li>';
        return(
          <Chart  data={nodes} forceFit height={225} scale={scale} >
            <Tooltip showTitle={false} itemTpl={htmlStr} />
            <Geom
              type='polygon'
              position='x*y'
              color='x'
              tooltip={['x*y', (x, y)=>{
                    return {
                        x,
                        y,
                    }
                }]}
              style={{lineWidth:1, stroke:"#fff"}}
            >
              <Label
                content='数据暂无'
                offset={0}
                textStyle={{textBaseline:'middle'}}
                formatter={(val)=>{
                        if(val !== 'root') {
                            return val;
                        }
                    }}
              />
            </Geom>
          </Chart>
        );
    }
}
export default TreeMap;
