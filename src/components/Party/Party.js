import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Cascader, Form, Button, Card, Row, Col, message, Modal } from 'antd';
import ModelFrom from './ModalForm';
import * as moment from 'moment';
import AutoList from '../List/List';
import { changeToArray } from './Mothed';

const FormItem = Form.Item;
const confirm = Modal.confirm;
const modalInitialData = {
  name:0,
  sex:0,
  birthday:0,
  phone:0,
  address:'',
  detailAddress:0,
};

@Form.create()// 经Form.create()包装过的组件会自带this.props.form属性


export default  class PartList extends Component{
  constructor(props){
    super(props);
    this.state = {
      isVisible:false,
      modifyIndex:0,
      peopleList:[],
      selectedData: [],
      handleTitle:'新增',
      modalData:modalInitialData,
      selectedRowKeys:[],
      selectedRows:[],
      pageNation:{
        pageSize : 10, // 一页多少条
        current :1,  // 当前页
        total:0,  // 总条数
        pages:0,  // 一共多少页
      },
    }
  }

  componentWillMount(){
    const {peopleList} = this.props;
    this.setState({peopleList:peopleList||[]});
  }

  componentWillReceiveProps(nextProps){
    const {peopleList} = nextProps;
    this.setState({peopleList:peopleList||this.state.peopleList});
  }

  onChange = (selectedRowKeys,selectedRows) =>{
    this.setState({selectedRowKeys,selectedRows});
  }

  handleOk= (formData) =>{
    const { peopleList, modalData, modifyIndex } = this.state;
    let result = true;
    peopleList.forEach((data)=>{
      if(data.phone === formData.phone){
        result = false;
      }
    });
    if(modalData.name){
      let data;
      for (data in peopleList[modifyIndex]){
        peopleList[modifyIndex][data] = formData[data];
      }
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      message.success('修改成功');
    } else if(result){
      peopleList.unshift(formData);
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      message.success('新增成功');

    } else {
      message.error('不能添加重复人员！');
    }
    this.props.getPartyData(peopleList);
  };

  handleCancel = () =>{
    this.setState({
      isVisible:false,
      modalData:modalInitialData,
    })
  };

  handleAddPeople = () =>{
    this.setState({
      handleTitle:'新增',
      isVisible:true,
      modalData:modalInitialData,
    })
  };

  handleMenuModify=(record)=>{
    const {peopleList} = this.state;
    const index = peopleList.indexOf(record);
    this.setState({
      handleTitle:'编辑',
      isVisible:true,
      modifyIndex:index,
      modalData:{...this.state.peopleList[index]},
    });
  };

  handleMenuDelete=(record)=>{
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const { peopleList } = this.state;
        const index = peopleList.indexOf(record);
        peopleList.splice(index,1);
        this.setState(peopleList);
                        message.success('删除成功');
        this.props.getPartyData(peopleList);
        this.setState({  selectedRowKeys:[]})
      },
      onCancel() {

      },
    });

  };



  handleDelete = () =>{
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const {selectedRowKeys, selectedRows, selectedData } = this.state;// selectedData 为选中的信息在整个数组中的位置
        const { peopleList } = this.state;
        const allIndex = [];
        selectedData.forEach(i=>{
          allIndex.push(peopleList[i]);// allIndex 为选中的信息
        })
        for (let i = 0; i < allIndex.length; i++) {
          for (let j = 0; j < peopleList.length; j++) {
            if (allIndex[i].phone === peopleList[j].phone) {
              peopleList.splice(j, 1);
              continue;
            }
          }
        }
        this.setState({
          peopleList,
          selectedRowKeys:[],
        });
        message.success('删除成功');
        this.props.getPartyData(peopleList);
      },
      onCancel() {

      },
    });
  }

  turnPage = (e)=>{
    const { current } = e;
    this.setState({
      pageNation: {
        current,
      },
    })
  }

  render(){

    const { peopleList, isVisible, modalData, handleTitle, pageNation, selectedRowKeys } = this.state;

    const { address, birthday } = modalData;

    const allAddress = address.split('/');
    if(modalData.address){
      modalData.detailAddress = allAddress[1];
      modalData.address = changeToArray(allAddress[0]);
    }

    let now = new moment();
    now = now.format('YYYY-MM-DD');
    modalData.birthday = moment(birthday||now,'YYYY-MM-DD');

    const hasSelected = selectedRowKeys.length > 0;
    const sexType = ['男', '女'];

    const columns = [{
      title:'编号',
      key:'index',
      dataIndex:'index',
      align:'center',
      render: (text, record, index) => {
        return <span>{index + 1}</span>
      },
    },{
      title: '姓名',
      dataIndex: 'name',
      key:'name',
      align:'center',
    },{
      title: '性别',
      dataIndex: 'sex',
      key:'sex',
      align:'center',
      render(record) {
        return sexType[record];
      },
    },{
      title: '出生年月',
      dataIndex: 'birthday',
      key:'birthday',
      align:'center',
    },{
      title: '电话',
      dataIndex: 'phone',
      key:'phone',
      align:'center',
    },{
      title: '住址',
      dataIndex: 'address',
      key:'address',
      align:'center',
      render:(record)=>{
        const data=record.replace(/^(.{4})(.*)$/,'$1......');
        return data;
      },
    },
    ];
    const pageProps = {
      rowKey: (record) => record.phone,
      dataSource:peopleList,
      columsList:{
        columns,
        menulistArr:[{
          key:'handle-1',
          name:'修改',
          menuclick:this.handleMenuModify,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:this.handleMenuDelete,
        }],
      },
      pageNation,
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          const selectedData = selectedRowKeys.map((item) => {
            return (pageNation.current - 1) * 10 + item;
          })
          this.setState({ selectedRowKeys, selectedData });
        },
      },
    };

    const modalProps = {
      isVisible,
      handleCancel:this.handleCancel,
      handleOK:this.handleOk,
      formInfo:modalData,
      title:`当事人-${handleTitle}`,
    };
    return(
      <div>
        <Row type="flex" justify="end">
          <Col>
            <span style={{ marginRight: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
            </span>
            <span style={{ marginRight: 8 }}>
              {hasSelected? <Button  type='danger' onClick={this.handleDelete} icon='close'>删除当事人</Button>:''}
            </span>
          </Col>
          <Col>
            <Button type="primary" onClick={this.handleAddPeople} icon='plus'>新增当事人</Button>
          </Col>
        </Row>
        <AutoList {...pageProps} turnPage={this.turnPage} />
        <ModelFrom {...modalProps} />
      </div>
    );
  }
}
