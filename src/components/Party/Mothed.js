import { place } from '../../utils/seletLocalData';

const changeToString = (value) => {
  const provinceValue = value[0];
  const  cityValue = value[1];
  const  areaValue = value[2];
  for (let i = 0; i < place.length; i++) {
    if (place[i].value === provinceValue) {
      const province = place[i].label;
      const citys = place[i].children;
      for (let j = 0; j < citys.length; j++) {
        if (cityValue === citys[j].value) {
          const city = citys[j].label
          const areas = citys[j].children;
          for (let k = 0; k < areas.length; k++) {
            if (areaValue === areas[k].value) {
              const area = areas[k].label;
              return (province + city + area);
            }
          }
        }
      }
    }
  }
}

const changeToArray = (defaultPlace) => {
  if(typeof(defaultPlace) === 'number'||typeof(defaultPlace) === 'undefined') return;
  const placeData = [];
  const pr = defaultPlace.slice(0, 3);// 省简写
  for (let i = 0; i < place.length; i++) {
    if (place[i].label.indexOf(pr) > -1) {
      placeData.push(place[i].value);
      const ci = defaultPlace.slice(place[i].label.length, place[i].label.length + 3);
      const citys = place[i].children;
      for (let j = 0; j < citys.length; j++) {
        if (citys[j].label.indexOf(ci) > -1) {
          placeData.push(citys[j].value);
          const ar = defaultPlace.slice(place[i].label.length + citys[j].label.length, place[i].label.length + citys[j].label.length + 2);
          const areas = citys[j].children;
          for (let k = 0; k < areas.length; k++) {
            if (areas[k].label.indexOf(ar) > -1) {
              placeData.push(areas[k].value)
              return placeData;
            }
          }
        }
      }
    }
  }


}

export {
  changeToString,
  changeToArray,
}
