import React,{Component} from 'react';
import { Table } from 'antd';
import DropDown from '../../components/Dropdown/Dropdown'
import styles from './List.less'
import classnames from 'classnames'

const defaultPagination = {
    showTotal: (total) => `共 ${total} 条`,
    showQuickJumper: true,
    showSizeChanger: true,
    hideOnSinglePage: false,
}

class AutoList extends Component{
    constructor(props){
        super(props);
        this.state = {
            columns:[],
        }
    }
    componentDidMount () {
        this.createColums();
    }
    createColums = () => {
        const { columsList: { columns, menulistArr,menulistArrz } } = this.props;
        if ( menulistArr.length > 0 ) {
            const handleOptions = {
                title: '操作',
                key: 'handle',
                width: 100,
                render: (text,record,index)=>{
                    return (
                        <DropDown record={record} index={index} menuOptions={menulistArr} />
                    )
                },
            }
            columns.push(handleOptions);
        }
        if ( menulistArrz.length > 0 ) {
            const handleOptions = {
                title: '更改状态',
                key: 'handleTwo',
                width: 100,
                render: (text,record,index)=>{
                    return (
                        <DropDown record={record} index={index+1} menuOptions={menulistArrz} />
                    )
                },
            }
            columns.push(handleOptions);
        }
        this.setState({
            columns,
        });
    }
    render(){
        const { dataSource, pagination, rowSelection,turnPage,bordered,loading } = this.props;
        const pageNation = {...pagination,...defaultPagination};
        return(
          <Table
            pagination={pageNation}
            rowKey={record => record.id}
            {...this.state}
            bordered={bordered || false}
            dataSource={dataSource}
            rowSelection={rowSelection}
            onChange={turnPage}
            loading={loading}
          />
        );
    }
}
export default AutoList;
