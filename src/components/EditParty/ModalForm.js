import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Cascader, Form, Select, DatePicker, Input, Modal} from 'antd';
import * as moment from 'moment';
import { place } from '../../utils/seletLocalData';
import { changeToString } from './Mothed';
import {deleteSpace} from '../../utils/utils';


const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;
@Form.create()

class ModelFrom extends Component{
    constructor(props){
        super(props);
    }
    returnAccept = () =>{
        this.props.dispatch(routerRedux.push('/commonlist'));
    }
    handleOK = () =>{
      const { handleOK, form } = this.props;
      form.validateFields(
        (err)=>{
          if(!err){
            const formData = form.getFieldsValue();
            let { birthday, address, detailAddress, name } = formData;
            detailAddress = detailAddress.trim();
            formData.name = name.trim();
            formData.birthday = birthday.format("YYYY-MM-DD");
            formData.address = changeToString(address) + '/' +detailAddress;
            handleOK(formData);
            form.resetFields();
          }
        }
      );
    }

    handleCancel = () =>{
      const { handleCancel, form } = this.props;
      form.resetFields();
      handleCancel();
    }

    disabledDate = (current) => {
      return current && current > moment().endOf('day');
    }

    render(){
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const { getFieldDecorator } = this.props.form;
        const { isVisible, formInfo, title } = this.props;
        return(
          <Modal
            title={title}
            visible={isVisible}
            onOk={this.handleOK}
            onCancel={this.handleCancel}
          >
            <Form>
              <FormItem
                {...formItemLayout}
                label="姓名"
              >
                {getFieldDecorator('name', {
                  initialValue:formInfo.name || '',
                  rules: [{
                    required: true, message: '姓名是必填项', whitespace: true,
                  }],
                })(
                  <Input style={{ width: '100%' }} placeholder="请输入姓名" />
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="性别"
              >
                {getFieldDecorator('sex', {
                  initialValue:formInfo.sex || '',
                  rules: [{
                    required: true, message: '性别是必填项',
                  }],
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="请选择性别"
                  >
                    <Option key={0}>男</Option>
                    <Option key={1}>女</Option>
                  </Select>
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="出生年月"
              >
                {getFieldDecorator('birthday', {
                  initialValue:formInfo.birthday || '',
                  rules: [{
                    required: true, message: '出生年月是必填项',
                  }],
                })(
                  <DatePicker style={{ width: '100%' }} placeholder="请选择出生年月"  disabledDate={this.disabledDate} />
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="电话"
              >
                {getFieldDecorator('phone', {
                  initialValue:formInfo.phone || '',
                  rules: [{
                    pattern:/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/,
                    required: true, message: '请输入正确的电话号码',
                  }],
                })(
                  <Input style={{ width: '100%' }} placeholder="请输入手机号码" />
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="住址"
              >
                {getFieldDecorator('address', {
                  initialValue:formInfo.address || '',
                  rules: [{
                    required: true, message: '住址是必填项',
                  }],
                })(
                  <Cascader options={place} placeholder="请选择地址" />
                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label='详细地址'
              >
                {getFieldDecorator('detailAddress', {
                  initialValue:formInfo.detailAddress || '',
                  rules: [{
                    required: true, message: '详细住址是必填项',max:90, whitespace: true,
                  }],
                })(
                  <TextArea placeholder='请输入详细地址，字数不大于90字。' autosize={{minRows:2,maxRows:3}} />
                )}
              </FormItem>
            </Form>
          </Modal>
        );
    }
}
export default ModelFrom;
