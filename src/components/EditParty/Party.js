import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Cascader, Form, Button, Card, Row, Col, message, Modal } from 'antd';
import ModelFrom from './ModalForm';
import * as moment from 'moment';
import AutoList from '../List/List';
import { changeToArray } from './Mothed';

const FormItem = Form.Item;
const confirm = Modal.confirm;
const modalInitialData = {
  name:0,
  sex:0,
  birthday:0,
  phone:0,
  address:'',
  detailAddress:0,
};

@Form.create()// 经Form.create()包装过的组件会自带this.props.form属性


@connect(({ dispute, citizen, loading }) => ({
  dispute,
  citizen,
}))

export default  class EditPartList extends Component{
  constructor(props){
    super(props);
    this.state = {
      isVisible:false,
      modifyIndex:0,
      peopleList:[],
      handleTitle:'新增',
      modalData:modalInitialData,
      selectedRowKeys:[],
      selectedRows:[],
      pageNation:{
        pageSize : 10, // 一页多少条
        current :0,  // 当前页
        total:0,  // 总条数
        pages:0,  // 一共多少页
      },
      id:-1,
    };
  }

  componentWillMount(){
    const {peopleList} = this.props;
    this.setState({peopleList:peopleList||[]});
  }

  componentWillReceiveProps(nextProps){
    const {peopleList} = nextProps;
    this.setState({peopleList:peopleList||this.state.peopleList});
  }

  onChange = (selectedRowKeys,selectedRows) =>{
    this.setState({selectedRowKeys,selectedRows});
  }

  handleOk= (formData) =>{
    const { dispatch } = this.props;
    const { id } = this.state;
    formData.acceptId = this.props.id;
    const { peopleList, modalData, modifyIndex } = this.state;
    let result = true;
    peopleList.forEach((data)=>{
      if(data.phone === formData.phone){
        result = false;
      }
    });
    if(modalData.name){
      formData.id = id;
      let data;
      for (data in peopleList[modifyIndex]){
        peopleList[modifyIndex][data] = formData[data];
      }
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      dispatch({
        type:'dispute/edit',
        payload:formData,
      })
    } else if(result){
      peopleList.unshift(formData);
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      dispatch({
        type:'dispute/addParty',
        payload:formData,
      })
    } else {
      message.error('不能添加重复人员！');
    }
    this.props.getPartyData(peopleList);
  };

  handleCancel = () =>{
    this.setState({
      isVisible:false,
      modalData:modalInitialData,
    })
  };

  handleAddPeople = () =>{
    this.setState({
      handleTitle:'新增',
      isVisible:true,
      modalData:modalInitialData,
    })
  };

  handleMenuModify=(record)=>{
    const {peopleList} = this.state;
    const index = peopleList.indexOf(record);
    this.setState({
      handleTitle:'编辑',
      isVisible:true,
      modifyIndex:index,
      modalData:{...this.state.peopleList[index]},
      id:record.id,
    });
  };

  handleMenuDelete=(record)=>{
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const { peopleList } = this.state;
        if(peopleList.length === 1){
          message.error('当前只有一名当事人，不能删除');
          return;
        }
        const { dispatch } = this.props;
        const index = peopleList.indexOf(record);
        peopleList.splice(index,1);
        this.setState(peopleList);
        dispatch({
          type:'dispute/deleteParty',
          payload:{
            id:record.id,
            acceptId:record.acceptId,
          },
        })
        this.props.getPartyData(peopleList);
        this.setState({  selectedRowKeys:[]})
      },
      onCancel() {

      },
    });
  };

  handleDelete = () =>{
    const { peopleList, selectedRowKeys, selectedRows } = this.state;
    if(peopleList.length === 1){
      message.error('当前只有一名当事人，不能删除');
      return;
    } else if(peopleList.length === selectedRowKeys.length){
      message.error('不能删除所有的当事人');
      return;
    }
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const { dispatch } = this.props;
        const ids = [];
        const allIndex = [];
        selectedRows.forEach(i=>{
          ids.push(i.id);
          allIndex.push(peopleList.indexOf(i));
        });
        let index = 0;
        allIndex.forEach( i =>{
          if(i === peopleList.length-1){
            peopleList.splice(i,1);
          } else{
            peopleList.splice(i-index,1);
            index++;
          }
        });
        dispatch({
          type:'dispute/deleteAllParty',
          payload:{
            ids,
          },
        })
        this.setState({
          peopleList,
          selectedRowKeys:[],
        });
        this.props.getPartyData(peopleList);
      },
      onCancel() {

      },
    });
  }

  render(){
    const { peopleList, isVisible, modalData, handleTitle, pageNation, selectedRowKeys } = this.state;
    const { listLoading } = this.props;
    const { address, birthday } = modalData;
    const allAddress = address.split('/');
    const sexType = ['男', '女'];
    if(modalData.address){
      modalData.detailAddress = allAddress[1];
      modalData.address = changeToArray(allAddress[0]);
    }
    let now = new moment();
    now = now.format('YYYY-MM-DD');
    modalData.birthday = moment(birthday||now,'YYYY-MM-DD');
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [{
      title:'编号',
      key:'index',
      dataIndex:'index',
      align:'center',
      render: (text, record, index) => {
        return <span>{index + 1}</span>
      },
    },{
      title: '姓名',
      dataIndex: 'name',
      key:'name',
      align:'center',
    },{
      title: '性别',
      dataIndex: 'sex',
      key:'sex',
      align:'center',
      render(record) {
        return sexType[record];
      },
    },{
      title: '出生年月',
      dataIndex: 'birthday',
      key:'birthday',
      align:'center',
    },{
      title: '电话',
      dataIndex: 'phone',
      key:'phone',
      align:'center',
    },{
      title: '住址',
      dataIndex: 'address',
      key:'address',
      align:'center',
      render:(record)=>{
        const data=record.replace(/^(.{4})(.*)$/,'$1......');
        return data;
      },
    },
    ];
    const pageProps = {
      rowKey: (record) => record.phone,
      loading:listLoading,
      dataSource:peopleList,
      columsList:{
        columns,
        menulistArr:[{
          key:'handle-1',
          name:'修改',
          menuclick:this.handleMenuModify,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:this.handleMenuDelete,
        }],
      },
      pageNation,
      rowSelection:{
        onChange:this.onChange,
        selectedRowKeys:this.state.selectedRowKeys,
      },
    };

    const modalProps = {
      isVisible,
      handleCancel:this.handleCancel,
      handleOK:this.handleOk,
      formInfo:modalData,
      title:`当事人-${handleTitle}`,
    };
    return(
      <div>
        <Row type="flex" justify="end">
          <Col>
            <span style={{ marginRight: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
            </span>
            <span style={{ marginRight: 8 }}>{hasSelected? <Button type='danger' onClick={this.handleDelete} icon='close'>删除当事人</Button>:''}</span>
          </Col>
          <Col>
            <Button type="primary" onClick={this.handleAddPeople} icon='plus'>新增当事人</Button>
          </Col>
        </Row>
        <AutoList {...pageProps} />
        <ModelFrom {...modalProps} />
      </div>
    );
  }
}
