import React, { Component } from 'react';
import LzEditor from 'react-lz-editor';

export default class Lzeditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseList: [],
    };
    this.receiveHtml = this.receiveHtml.bind(this);
  }

  receiveHtml(content) {
    this.setState({ responseList: [] });
    this.props.getHtml(content);
  }

  render() {
    const policy = '';
    const uploadProps = {
      action: 'http://v0.api.upyun.com/devopee',
      onChange: this.onChange,
      listType: 'picture',
      fileList: this.state.responseList,
      data: (file) => {

      },
      multiple: true,
      beforeUpload: this.beforeUpload,
      showUploadList: true,
    };
    const setting = {
        lang:'zh',
        image:false,
        video:false,
        audio:false,
        color:false,
        pasteNoStyle:true,
    };
    return (
      <LzEditor
        active
        importContent={this.props.htmlContent}
        cbReceiver={this.receiveHtml}
        uploadProps={uploadProps}
        {...setting}
        disabled={this.props.disabled}
      />
    );
  }
}
