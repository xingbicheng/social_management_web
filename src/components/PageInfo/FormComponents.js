import React,{Component} from 'react';
import styles from './index.less';

class FormComponents extends Component{
    render(){
        const { data } = this.props;
        return(
            <div className={styles.infoBox}>
                {
                    data.map((item,index)=>{
                        if(item.type == 'input'){
                            return(
                                <div key={`label-${item.label}`} className={styles.labelBox_intput}>
                                    <p className={styles.value}>
                                        <span className={styles.label}>{item.label}:</span>
                                        {item.value || '未填写'}
                                    </p>
                                </div>
                            )
                        }
                        if(item.type == 'textarea'){
                            return(
                                <div  key={`label-${item.label}`} className={styles.labelBox_textarea}>
                                    <div className={styles.label}>{item.label}:</div>
                                    <div className={styles.value}>{item.value || '未填写'}</div>
                                </div>
                            )
                        }
                    })
                }
            </div>
        );
    }
}
export default FormComponents;