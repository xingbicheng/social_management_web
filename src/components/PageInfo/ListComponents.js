import React,{Component} from 'react';
import { Table,Tabs } from 'antd';

const TabPane = Tabs.TabPane;

class ListComponents extends Component{
    render(){
        const { data } = this.props;
        return(
            <div>
                <Tabs
                    defaultActiveKey="1"
                    style={{ height: 220 }}
                >
                {
                    data.map((item)=>{
                        return(
                        <TabPane tab={item.title} key={`table-${item.title}`}>
                            <Table dataSource={item.dataSource} columns={item.columns} />
                        </TabPane>
                        )
                    })
                }
                </Tabs>
            </div>
        );
    }
}
export default ListComponents;