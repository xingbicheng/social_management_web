import React,{PureComponent} from 'react';
import { Card,Button } from 'antd';
import ListComponents from './ListComponents';
import FormComponents from './FormComponents';
import styles from './index.less';


class PageInfo extends PureComponent{
    constructor(props){
        super(props);
    }
    componentWillMount(){

    }
    createPage =(type,info)=>{
        const returnPage = {
            form:<FormComponents data={info} />,
            list:<ListComponents data={info} />,
        }
        return returnPage[type];
    }
    render(){
        // const { pageProps } = this.props;
        const pageProps = [{
            type:'form',
            title:'基本信息',
            key:'1',
            info:[{
                label:'身份证号',
                value:'513022199502027051',
                type:"input",
            },{
                label:'姓名',
                value:'ff',
                type:"input",
            },{
                label:'文化程度',
                value:'小学',
                type:"input",
            },{
                label:'性别',
                value:'男',
                type:"input",
            },{
                label:'民族',
                value:'汉族',
                type:"input",
            },{
                label:'出生日期',
                value:'2018-07-07',
                type:"input",
            },{
                label:'籍贯',
                value:'福建省泉州市惠安县',
                type:"input",
            },{
                label:'联系方式',
                value:'18784256321',
                type:"input",
            }],
        },{
            type:'form',
            title:'刑满释放人员信息',
            key:'2',
            info:[{
                label:'曾用名',
                value:'小玲玲',
                type:"input",
            },{
                label:'血型',
                value:'AB型',
                type:"input",
            },{
                label:'职业',
                value:'没有',
                type:"input",
            },{
                label:'接受来源',
                value:'没有',
                type:"input",
            },{
                label:'是否重点帮扶对象',
                value:'是',
                type:"input",
            },{
                label:'是否符合低保条件',
                value:'是',
                type:"input",
            },{
                label:'是否重点帮教对象',
                value:'否',
                type:"input",
            },{
                label:'服刑时间',
                value:'2018-07-09',
                type:"input",
            },{
                label:'刑释时间',
                value:'2018-07-09',
                type:"input",
            },{
                label:'服刑原因',
                value:'“为政之要，莫先于用人。”政治路线确定之后，干部就是决定因素。实现中华民族伟大复兴的中国梦，关键在党，关键在人，关键在培养和造就高素质的干部队伍。7月4日，习近平在全国组织工作会议上指出：“贯彻新时代党的组织路线，建设忠诚干净担当的高素质干部队伍是关键，重点是要做好干部培育、选拔、管理、使用工作。”请随“学习中国”小编一起学习。',
                type:"textarea",
            },{
                label:'家庭情况',
                value:'“为政之要，莫先于用人。”政治路线确定之后，干部就是决定因素。实现中华民族伟大复兴的中国梦，关键在党，关键在人，关键在培养和造就高素质的干部队伍。7月4日，习近平在全国组织工作会议上指出：“贯彻新时代党的组织路线，建设忠诚干净担当的高素质干部队伍是关键，重点是要做好干部培育、选拔、管理、使用工作。”请随“学习中国”小编一起学习。',
                type:"textarea",
            },{
                label:'低保落实情况',
                value:'“为政之要，莫先于用人。”政治路线确定之后，干部就是决定因素。实现中华民族伟大复兴的中国梦，关键在党，关键在人，关键在培养和造就高素质的干部队伍。7月4日，习近平在全国组织工作会议上指出：“贯彻新时代党的组织路线，建设忠诚干净担当的高素质干部队伍是关键，重点是要做好干部培育、选拔、管理、使用工作。”请随“学习中国”小编一起学习。',
                type:"textarea",
            },{
                label:'备注',
                value:'“为政之要，莫先于用人。”政治路线确定之后，干部就是决定因素。实现中华民族伟大复兴的中国梦，关键在党，关键在人，关键在培养和造就高素质的干部队伍。7月4日，习近平在全国组织工作会议上指出：“贯彻新时代党的组织路线，建设忠诚干净担当的高素质干部队伍是关键，重点是要做好干部培育、选拔、管理、使用工作。”请随“学习中国”小编一起学习。',
                type:"textarea",
            }],
        },{
            type:'list',
            title:null,
            key:'2',
            info:[{
                title:'帮教责任人',
                columns:[{
                    title: '姓名',
                    dataIndex: 'name',
                    key: 'name',
                }, {
                    title: '年龄',
                    dataIndex: 'age',
                    key: 'age',
                }, {
                    title: '住址',
                    dataIndex: 'address',
                    key: 'address',
                }],
                dataSource:[{
                    key: '1',
                    name: '胡彦斌',
                    age: 32,
                    address: '西湖区湖底公园1号',
                }, {
                    key: '2',
                    name: '胡彦祖',
                    age: 42,
                    address: '西湖区湖底公园1号',
                }],
            },{
                title:'帮教情况',
                columns:[],
                dataSource:[],
            },{
                title:'落实政策情况',
                columns:[],
                dataSource:[],
            }],
        }]
        return(
          <div>
            {
               pageProps.map((item)=>{
                  return (
                    <Card
                      key={`${item.type}-${item.key}`}
                      title={item.title}
                      style={{ marginBottom: 24 }}
                      bordered={false}
                    >
                      {
                          this.createPage(item.type,item.info)
                      }
                    </Card>
                   )
               })
            }
            <div className={styles.btnBox}>
              <div className={styles.saveAndreturn}>
                <Button icon="close">取消</Button>
                <Button icon="save" type="primary">保存</Button>
              </div>
            </div>
          </div>
        );
    }
}
export default PageInfo;
