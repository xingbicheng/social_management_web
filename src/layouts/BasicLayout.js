import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Layout, Icon, message,BackTop,Model } from 'antd';
import DocumentTitle from 'react-document-title';
import { connect } from 'dva';
import { Route, Redirect, Switch, routerRedux } from 'dva/router';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';
import pathToRegexp from 'path-to-regexp';
import { enquireScreen, unenquireScreen } from 'enquire-js';
import GlobalHeader from '../components/GlobalHeader';
import SiderMenu from '../components/SiderMenu';
import UpdatePassword from '../routes/User/UpdatePassword';
import NotFound from '../routes/Exception/404';
import { getRoutes } from '../utils/utils';
import { getMenuData } from '../common/menu';
import logo from '../assets/logo.png';
import { PageHeaderConfig } from '../config/config';


const { Content, Header, Footer } = Layout;

/**
 * 根据菜单取得重定向地址.
 */
const redirectData = [];
const getRedirect = item => {
  if (item && item.children) {
    if (item.children[0] && item.children[0].path) {
      redirectData.push({
        from: `${item.path}`,
        to: `${item.children[0].path}`,
      });
      item.children.forEach(children => {
        getRedirect(children);
      });
    }
  }
};
getMenuData().forEach(getRedirect);

/**
 * 获取面包屑映射
 * @param {Object} menuData 菜单配置
 * @param {Object} routerData 路由配置
 */
const getBreadcrumbNameMap = (menuData, routerData) => {
  const result = {};
  const childResult = {};
  for (const i of menuData) {
    if (!routerData[i.path]) {
      result[i.path] = i;
    }
    if (i.children) {
      Object.assign(childResult, getBreadcrumbNameMap(i.children, routerData));
    }
  }
  return Object.assign({}, routerData, result, childResult);
};

const query = {
  'screen-xs': {
    maxWidth: 575,
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767,
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991,
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199,
  },
  'screen-xl': {
    minWidth: 1200,
  },
};

let isMobile;
enquireScreen(b => {
  isMobile = b;
});

class BasicLayout extends React.PureComponent {
  static childContextTypes = {
    location: PropTypes.object,
    breadcrumbNameMap: PropTypes.object,
  };
  state = {
    isMobile,
  };
  getChildContext() {
    const { location, routerData } = this.props;
    return {
      location,
      breadcrumbNameMap: getBreadcrumbNameMap(getMenuData(), routerData),
    };
  }
  componentWillMount(){
      this.props.dispatch({
          type: 'user/fetchCurrent',
      });
  }
  componentDidMount() {
    this.enquireHandler = enquireScreen(mobile => {
      this.setState({
        isMobile: mobile,
      });
    });
  }
  componentWillUnmount() {
    unenquireScreen(this.enquireHandler);
  }
  getBaseRedirect = () => {
    // According to the url parameter to redirect
    // 这里是重定向的,重定向到 url 的 redirect 参数所示地址
    const urlParams = new URL(window.location.href);

    const redirect = urlParams.searchParams.get('redirect');
    // Remove the parameters in the url
    if (redirect) {
      urlParams.searchParams.delete('redirect');
      window.history.replaceState(null, 'redirect', urlParams.href);
    } else {
      const { routerData } = this.props;
      const authorizedPath = Object.keys(routerData).find(
        item => item !== '/'
      );
      return authorizedPath;
    }
    return redirect;
  };
  getPageTitle() {
    const { routerData, location } = this.props;
    const { pathname } = location;
    let title = PageHeaderConfig.PageHeaderTitle;
    let currRouterData = null;
    // match params path
    Object.keys(routerData).forEach(key => {
      if (pathToRegexp(key).test(pathname)) {
        currRouterData = routerData[key];
      }
    });
    if (currRouterData && currRouterData.name) {
      title = `${currRouterData.name} - ${PageHeaderConfig.PageHeaderTitle}`;
    }
    return title;
  }
  handleMenuCollapse = collapsed => {
    this.props.dispatch({
      type: 'global/changeLayoutCollapsed',
      payload: collapsed,
    });
  };
  handleMenuClick = ({ key }) => {
    if (key === 'logout') {
      this.props.dispatch({
        type: 'login/logout',
      });
    }
    if(key === 'updatepsw'){
      this.props.dispatch({
        type:'user/showModel',
      });
    }
  };
  // 关闭修改密码model
  handleCancel= ()=>{
    this.props.dispatch({
      type:'user/hiddenModel',
    });
  }
  // 提交修改密码表单
  handleOk  =(values)=>{
    let params = {...values};
    delete params.confirm;
    this.props.dispatch({
      type:'user/updatePsw',
      payload:params,
    })
  }

  handleNoticeVisibleChange = visible => {

  };
  render() {
    const bashRedirect = this.getBaseRedirect();
    const {
      currentUser,
      collapsed,
      fetchingNotices,
      routerData,
      match,
      location,
      visible,
      loading,
    } = this.props;
    const updateProps = {
      visible,
      title:'修改密码',
      formInfo:{...currentUser},
      handleCancel:this.handleCancel,
      handleOk :this.handleOk ,
    }
    const layout = (
      <Layout>
        <UpdatePassword {...updateProps} />
        <SiderMenu
          logo={logo}
          // 不带Authorized参数的情况下如果没有权限,会强制跳到403界面
          menuData={getMenuData()}
          collapsed={collapsed}
          location={location}
          isMobile={this.state.isMobile}
          onCollapse={this.handleMenuCollapse}
        />
        <Layout>
          <Header style={{ padding: 0 }}>
            <GlobalHeader
              logo={logo}
              currentUser={currentUser}
              collapsed={collapsed}
              isMobile={this.state.isMobile}
              onCollapse={this.handleMenuCollapse}
              onMenuClick={this.handleMenuClick}
              onNoticeClear={this.handleNoticeClear}
              fetchingNotices={fetchingNotices}
              onNoticeVisibleChange={this.handleNoticeVisibleChange}
            />
          </Header>
          <Content style={{ margin: '24px 24px 0', height: '100%' }}>
            <Switch>
              {redirectData.map(item => (
                <Redirect key={item.from} exact from={item.from} to={item.to} />
              ))}
              {getRoutes(match.path, routerData).map(item => (
                <Route key={item.path} path={item.path} component={item.component} redirectPath="/exception/403"/>
              ))}
              <Redirect exact from="/" to={bashRedirect} />
              <Route render={NotFound} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
    if(JSON.stringify(currentUser) === "{}"){
        return (<Route path="/user/login" />)
    }
    return (
      <DocumentTitle title={this.getPageTitle()}>
        <ContainerQuery query={query}>
          {params => <div className={classNames(params)}>{layout}<BackTop /></div>}
        </ContainerQuery>
      </DocumentTitle>
    );
  }
}

export default connect(({ user, global }) => ({
  currentUser: user.currentUser,
  visible:user.visible,
  collapsed: global.collapsed,
}))(BasicLayout);
