import { dictAdd,dictDel,dictEdit,dictGetTypes,dictList } from '../services/systemSet';
import { message } from 'antd';

export default {
  namespace: 'dictdata',


  state: {
    visible:false,
    types:[],
    dataSource:[],
    currentRow:{
      createUserId: 0,
      id: 1,
      superiorDepartmentId: 0,
      updateUserId: 0,
    },  // 当前选择数据
    pagination : {
      pageSize : 10, // 一页多少条
      current :0,  // 当前页
      total:0,  // 总条数
      pages:0,  // 一共多少页
    },
    searchInfo: {}, // 检索Form表单
    selectedRowKeys:[]
  },
  effects: {
    *add({ payload }, { call, put }) {
      const response = yield call(dictAdd, payload);
      if(response.code === 0){
        yield put({
          type: 'listpage',
          payload:{
            page:1,
            pageSize:10,
          },
        });
        yield put({
          type: 'hiddenModal',
          payload:response,
        });
        message.success('新增成功');
      }
      else if(response.code === 1)
      {
        message.error('该类型键或者键值已经存在,请重新填写');
      }
      else{
        message.error('新增失败');
      }
    },
    *getType({ payload }, { call, put }) {
      const response = yield call(dictGetTypes, payload);
      if(response.code === 0){
        yield put({
          type: 'saveTypes',
          payload:response.data,
        });
      }
    },
    *edit({ payload }, { call, put }) {
      const response = yield call(dictEdit, payload);
      if(response.code === 0){
        yield put({
          type: 'listpage',
          payload:{
            page:1,
            pageSize:10,
          },
        });
        yield put({
          type: 'hiddenModal',
          payload:response,
        });
        message.success('编辑成功');
      }
       else if(response.code === 1)
      {
        message.error('该类型键或者键值已经存在,清重新填写');
      }else{
        message.error('编辑失败');
      }
    },
    *listpage({ payload }, { call, put }) {
      const response = yield call(dictList, payload);
      yield put({
        type: 'save',
        payload:response.data,
      });
    },
    *delete({ payload }, { call, put }) {
      const response = yield call(dictDel, payload);
      if(response.code === 0){
        yield put({
          type: 'save',
          payload:response.data,
        });
        yield put({
          type:'listpage',
          payload:{
            page:1,
            pageSize:10,
          },
        });
        message.success('删除成功');
      }else{
        message.success('删除失败');
      }
    },
  },
  reducers: {
    save(state, action) {
      const { data,pages,pageSize,total,pageNum } = action.payload;
      return {
        ...state,
        dataSource:data,
        pagination:{pages,pageSize,total,current:pageNum},
      };
    },
    showModal(state,action){
      return {
        ...state,
        visible:true,
        currentRow:{...action.payload},
      }
    },
    saveTypes(state,action){
      return {
        ...state,
        types:action.payload,
      }
    },
    hiddenModal(state){
      return {
        ...state,
        visible:false,
        currentRow:{
          createUserId: 0,
          id:1,
          superiorDepartmentId: 0,
          updateUserId: 0,
        },
      }
    },
    clearSearchInfo(state){
      return{
        ...state,
        searchInfo:{},
      }
    },
    setSearchInfo(state,action){
      return{
        ...state,
        searchInfo:{...action.payload},
      }
    },
  },
};
