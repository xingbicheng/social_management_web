import { policyAdd, policyDel,policyGet,policyEdit,policyDelAll, policyList } from '../services/releasePolicy';
import { dictList } from '../services/systemSet'

import { message } from 'antd';

export default {
    namespace: 'releasepolicy',

    state: {
        isVisible:false,
        baseList:[],
        singlePerson:{},
        multiDeleteButtonIsVisable: false,
        selectedRowKeys: [],
        isResettlement:false,
        pagination : {
           pageSize : 10, // 一页多少条
           page :1,  // 当前页
           total:1,  // 总条数
           pages:1,  // 一共多少页
         },
        releasePersonId:0,
        doDispatch:'add',
        degree_education:[],
        rowList:{},
    },
    effects: {
        *addPolicy({ payload }, { call, put, select }) {
            const response = yield call(policyAdd, payload);
            const data=yield select(_=>_.releasepolicy);
            const{page,pageSize}=data.pagination;
            if(response.code === 0){
              yield put({
                  type: 'listpage',
                  payload:{page,pageSize},
              });
              message.success('添加成功');
          }else{
              message.error('添加失败');
          }
            
          },
          *getPerson ({ payload }, { call, put }) {
            const response = yield call(policyGet, {id:payload.id});
            yield put({
              type:'getPersonData',
              payload:response,
      
            })
        },
        *delPolicy({ payload }, { call, put,select }) {
            const response = yield call(policyDel, payload);
            const data=yield select(_=>_.releasepolicy);
            const{page,pageSize}=data.pagination;
            if(response.code === 0){
                yield put({
                    type: 'listpage',
                    payload:{page,pageSize},
                });
                message.success('删除成功');
            
            }else{
                message.error('删除失败');
            }
        },
        *delAllPolicy({ payload }, { call, put,select }) {
            const response = yield call(policyDelAll, payload);
            const data=yield select(_=>_.releasepolicy);
            const{page,pageSize}=data.pagination;
            if(response.code === 0){
            yield put({
                type: 'listpage',
                payload:{page,pageSize},
            });
            message.success('删除成功');
            }else{
                message.error('删除失败');
            }
            yield put({
                type:'updateState',
                payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
            })
        },
        *editPolicy({ payload }, { call, put, select}) {
            const response = yield call(policyEdit, payload);
            const data=yield select(_=>_.releasepolicy);
            const{page,pageSize}=data.pagination;
            if(response.code === 0){
              yield put({
                  type: 'listpage',
                  payload:{page,pageSize},
              });
              message.success('修改成功');
            }else{
                message.error('修改失败');
            }
        },
        *listpage({ payload }, { call, put,select }) {
            if(!payload)
            {   const data=yield select(_=>_.releasepolicy);
                const{pagination:{page,pageSize},releasePersonId}=data;
                payload={page,pageSize,releasePersonId};
            }
                const response = yield call(policyList,{payload});
            yield put({
                type: 'save',
                payload:response.data,
            });
        },
        * getDegree ({ payload }, { call, put }) {
        const response = yield call(dictList,{ type: 'degree_education'})
        yield put({
            type: 'updateState',
            payload: {
                degree_education: response.data.data,
            },
        })
    },
    },
        reducers: {
            save(state, action) {
                const {pages,pageSize,total,pageNum} =action.payload;
                return {
                ...state,
                otherList: action.payload.data,
                pagination:{pages,pageSize,total,current:pageNum},
                };
            },
            updateState (state, { payload }) {
                return {
                    ...state,
                    ...payload,
                }
            },
                getPersonData(state,{payload})
                {
                    return {
                        ...state,
                        singlePerson:payload.data,
                        isResettlement: (payload.data.isResettlement==='1'),
                    }
                },
         },
    };
