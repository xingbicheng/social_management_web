import { message } from 'antd'
import { getArea,  edit, add, addTeam, delTeam, editTeam } from '../services/infoArea'
import { getDepartment } from '../services/infoDepartment'
import { routerRedux } from 'dva/router'
export default {
    namespace: 'infoAreaDetail',
    state: {
       areaDetail: {}, 
       selectedRowKeys: [],
       department: {},
       currentItem: {},
       serviceTeams: [],
       modalVisable: false,
       modalType: 'create',
       departmentId: '',   
       type: '',
       pagination: {},
       saveLoading: false,
       confirmLoading: false,       
    },
    subscriptions: {
        setup ({dispatch, history}) {
            // history.listen((location) => {
                // const matchEdit = pathToRegexp('/gridInfoManage/areaEdit/:id').exec(location.pathname)
                // const matchAdd = pathToRegexp('/gridInfoManage/areaAdd/:id').exec(location.pathname)
                // if (matchEdit) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'getArea',
                //         payload: {
                //             id: matchEdit[1],
                //         },
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             type: 'edit',
                //         },
                //     })
                // }else if (matchAdd) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             departmentId: matchAdd[1],
                //             type: 'add',
                //         },
                //     })
                //     dispatch({
                //         type: 'initBasic',
                //         payload: {
                //             departmentId: matchAdd[1],
                //         },
                //     })
                // }
            // })
        },
    },
    effects: {
        * getArea ({ payload }, { call, put }) {
            const response = yield call(getArea, payload)
            if (response.code === 0) {
                const { departmentId } = response.data
                yield put({
                    type: 'updateState',
                    payload: {
                        areaDetail: response.data,
                        serviceTeams: response.data.serviceTeams,
                        departmentId,
                    },
                })
                const response1 = yield call(getDepartment, {departmentId})
                if (response1.code === 0) {
                    yield put({
                        type: 'updateState',
                        payload: {
                            department: response1.data,
                        },
                    })
                } else {
                    throw "get department failed"
                }
            }else {
                message.error('获取数据失败')
            }
        },
        * save ({ payload }, { call, put }) {
            let { id } = payload;
            const response = yield call (add, payload)
            if (response.code === 0) {
                message.success('添加成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put (routerRedux.push(`/gridInfoManage/area/${id}`));
                
            } else {
                message.error(response.msg)
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * edit ({ payload }, { call, put }) {
            let { departmentId } = payload;
            const response = yield call (edit, payload)
            if (response.code === 0) {
                message.success('编辑成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put (routerRedux.push(`/gridInfoManage/area/${departmentId}`));
            } else {
                message.error('编辑失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * initBasic ({ payload }, { call, put }) {
            const response = yield call(getDepartment, payload)
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        department: response.data,
                    },
                })
            } else {
                throw "get department failed"
            }
        },
        * addTeam ({ payload }, { call, put, select }) {
            const { areaDetail } = yield select(_=>_.infoAreaDetail)
            const response = yield call(addTeam, payload)
            if (response.code === 0 ) {
                yield put ({
                    type: 'getArea',
                    payload: {
                        id: areaDetail.id,
                    },
                })
                message.success('添加成功')
            } else {
                message.error('添加失败')
            }
        },
        * editTeam ({ payload }, { call, put, select }) {
            const response = yield call(editTeam, payload)
            const { areaDetail } = yield select(_=>_.infoAreaDetail)
            if (response.code === 0 ) {
                message.success('编辑成功')
                yield put ({
                    type: 'getArea',
                    payload: {
                        id: areaDetail.id,
                    },
                })
            } else {
                message.error('编辑失败')
            }
        },
        * delTeam ({ payload }, { call, put, select }) {
            const { areaDetail } = yield select(_=>_.infoAreaDetail)
            const response = yield call(delTeam, payload)
            if (response.code === 0 ) {
                message.success('删除成功')
                yield put ({
                    type: 'getArea',
                    payload: {
                        id: areaDetail.id,
                    },
                })
            } else {
                message.error('删除失败')
            }
        },
    },
    reducers: {
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                areaDetail: {}, 
                selectedRowKeys: [],
                department: {},
                currentItem: {},
                serviceTeams: [],
                modalVisable: false,
                modalType: 'create',
                departmentId: '',
                saveLoading: false,
                confirmLoading: false,      
            }
        },
    },
}