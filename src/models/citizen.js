import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { acceptList, acceptDel, typeList, acceptGet, acceptAdd, acceptEdit, acceptPartyDel, acceptPartyAdd, acceptPartyEdit, userList, assignAdd, assignGet, assignDel, assignEdit, acceptDelAll, assignDelAll } from '../services/citizenAPI';
import {
  addInfo,
  deleteFile,
  disputeList, editInfo,
  getByAcceptId,
  getById,
  getFile,
  getType,
  handler,
} from '../services/dispute';
import { departmentListAll } from '../services/department';
import { arrayToTree, constructMenuTreeData, replaceKeyData } from '../utils/utils';

export default {
  namespace: 'citizen',

  state: {
    visible:false,
    dataSource:[],
    dataList:[],
    acceptData:[],
    typeData: [],
    getData: [],
    selectedRowKeys: [],
    currentDepartment: [],
    treeData: [],
    users: [],
    assignData: [],
    defaultData: {},
    allData: {},
    pageData:{
      listParty:[],
    },
    isOk: false,
    currentRow:{
      createUserId: 0,
      id: 0,
      superiorDepartmentId: 0,
      updateUserId: 0,
    },
    fileList:[],
    handleOverData:{
      handlerProcess:'',
      handlerType:'',
      oralAgreement:'',
      fileUrl:'',
      path:'',
    },
    pagination : {
      pageSize : 10, // 一页多少条
      pageNum: 1,
      current :0,  // 当前页
      total:0,  // 总条数
      pages:0,  // 一共多少页
      page: 1,
    },
  },

  effects: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    *list({ payload }, { call, put }) {
      payload.acceptType = "1";
      const response = yield call (acceptList, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    *listSearch({ payload }, { call, put }) {
      payload.acceptType = "1";
      const response = yield call (acceptList, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    *accept({ payload }, { call, put }) {// 正常受理
      yield put({
        type: 'defaultData',
        payload,
      });
    },
    *add({payload},{ call, put, select}){
      const data=yield select(_=>_.citizen);
      yield put({
        type:'addAllData',
        payload,
      });
      let response;
      if(data.allData.id){
        response = yield call(editInfo, data.allData);
      }
      else {
        response =  yield call(addInfo, data.allData);
      }
      if(response.success){
        const { pagination } = data;
        pagination.acceptType = '1';
        const dataList = yield call(acceptList, pagination);
        yield put({
          type:"saveAccept",
          payload:dataList.data,
        })
        message.success('提交成功');
        yield put(routerRedux.push('/information/citizen'));
      }
       else if(response.code === 222)
      {
           message.error("受理期限必须必须是将来的日期,请重新选择受理日期")
      }else{
        console.log(response);
        message.error('提交失败');
      }
    },
    *addAssign({payload},{ call, put, select}){
      const { type } = payload;
      const data = yield select(_=>_.citizen);
      yield put({
        type:'addAllAssignData',
        payload,
      });

      const response =  yield call(assignAdd, data.allData);

      if(response.code === 0){
        const dataList = yield call(acceptGet, { id: data.allData.acceptId });
        yield put({
          type:"search",
          payload:dataList.data,
        })
        message.success('交办成功');
        if (type == 'citizen') {
          yield put(routerRedux.push('/information/citizen'));
        } else {
          yield put(routerRedux.push('/information/dispute'));
        }
      }else{
        message.error('交办失败');
      }
    },

    *addAssignList({payload},{ call, put, select}){
      const data = yield select(_=>_.citizen);
      yield put({
        type:'addAllAssignData',
        payload,
      });
      const response =  yield call(assignAdd, data.allData);
      if(response.code === 0){
        yield put({
          type: 'get',
          payload:{ id: data.allData.acceptId },
        })
        message.success('新增成功');
      }else{
        message.error('新增失败');
      }
    },

    *delAssign ({ payload }, { call, put, select }) {
      const data=yield select(_=>_.citizen);
      const { acceptId } = payload;
      delete payload.acceptId;
      const response = yield call(assignDel, payload);
      if(response.code === 0){
        const dataList = yield call(acceptGet, { id: acceptId });
        yield put({
          type:"search",
          payload:dataList.data,
        })
        message.success('删除成功');
      }else{
        message.error('删除失败');
      }
    },

    *deleteFile({payload},{call, put}){
      const response = yield call(deleteFile,payload);
      if(response.success){
        message.success('删除成功');
      }
    },

    *deleteAssignAll ({ payload }, { call, put }) {
      const { acceptId } = payload;
      delete payload.acceptId;
      const num = payload.ids.length;
      const response = yield call(assignDelAll, payload);
      if(response.code === 0){
        const dataList = yield call(acceptGet, { id: acceptId });
        yield put({
          type:"search",
          payload:dataList.data,
        })
        message.success(`成功删除${num}条`);
      }else{
        message.error('删除失败');
      }
    },

    *editAssign ({ payload }, { call, put }) {
      const { acceptId } = payload;
      const response = yield call(assignEdit, payload);
      if(response.code === 0){
        yield put({
          type: 'get',
          payload: { id: acceptId },
        })
        message.success('编辑成功');
      }else{
        message.error('编辑失败');
      }
    },

    *edit({payload},{ call, put, select}){
      const data = yield select(_=>_.citizen);
      yield put({
        type:'editAllData',
        payload,
      });
      const response =  yield call(acceptEdit, data.allData);
      if(response.code === 0){
        const { pagination } = data;
        const dataList = yield call(acceptList, { ...pagination, acceptType: '1'} );
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('编辑成功');
        yield put(routerRedux.push('/information/citizen'));
      }
      else{
        message.error('编辑失败');
      }
    },

    *delete ({ payload }, { call, put, select }) {
      const data=yield select(_=>_.citizen);
      const response = yield call(acceptDel, payload);
      if(response.code === 0){
        const { pagination } = data;
        const dataList = yield call(acceptList, { ...pagination, acceptType: '1'});
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('删除成功');
      }else{
        message.error('删除失败，只能删除受理中的信息');
      }
    },

    *deleteAcceptAll ({ payload }, { call, put, select }) {
      const data=yield select(_=>_.citizen);
      const num = payload.ids.length;
      const response = yield call(acceptDelAll, payload);
      if(response.code === 0){
        const { pagination } = data;
        const dataList = yield call(acceptList, { ...pagination, acceptType: '1'});
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success(`成功删除${num}条`);
      }else{
        message.error('删除失败');
      }
    },

    *partyDel ({ payload }, { call, put, select }) {
      const response = yield call(acceptPartyDel, payload);
      if(response.code === 0){
        message.success('删除成功');
      }else{
        message.success('删除失败');
      }
    },

    *partyAdd ({ payload }, { call, put, select }) {
      const response = yield call(acceptPartyAdd, payload);
    },

    *partyEdit ({ payload }, { call, put, select }) {
      const response = yield call(acceptPartyEdit, payload);
    },

    *typePage({payload},{call,put}){
      const response = yield call(getType, payload);
      yield put({
        type: 'saveMatterType',
        payload:response.data,
      });
    },

    * get({ payload }, { call, put }) {
      const response = yield call(acceptGet, payload);
      yield put({
        type: 'search',
        payload: response.data,
      });
    },
    * getAssign({ payload }, { call, put }) {
      const response = yield call(assignGet, payload);
      yield put({
        type: 'getAssignData',
        payload: response.data,
      });
    },
    // 交办
    * getDepartmentList({ payload = {} }, { call, put }) {
      const response = yield call(departmentListAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveTreeData',
          payload: response.data,
        });
      }
    },

    * getUser({ payload }, { call, put }) {// 获取部门下的人员信息
      const response = yield call(userList, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveUser',
          payload: response.data.data,
        });
      }
    },

    * searchData({ payload }, { call, put }) {
      payload.page = 1;
      payload.pageSize = 10;
      payload.acceptType = '1';
      const response = yield call(acceptList, payload);
    },
    *getConcluded({payload},{call,put}){
      const {data} = payload;
      const updata = {
        id:data.id,
      };
      const response = yield call(getByAcceptId,updata);
      let codata = {};
      response.data.forEach(i=>{
        if(i.oralAgreement||i.writtenId){
          codata = i;
        }
      })
      if(!codata.writtenId){
        codata.writtenId = 0;
      }else{
        const filerespone = yield call(getFile,{id:codata.writtenId});
        if(filerespone.success){
          codata.path = filerespone.data.path;
        }
      }
      yield put({
        type:'saveConcluded',
        payload:{
          handleOverData:codata,
        },
      });
    },
    *handler({payload},{call,put,select}){
      const data=yield select(_=>_.citizen);
      const {data:upData} = payload;
      const updata = {
        id:upData.id,
      };
      const idResponse = yield call(getByAcceptId,updata);
      if(idResponse.msg === 'OK')
        upData.id = idResponse.data[0].id;
      else{
        message.error('办结失败');
        return;
      }
      const response = yield call(handler,upData);
      if(response.code === 0){
        const { pagination } = data;
        const dataList = yield call(acceptList, { ...pagination, acceptType: '1'});
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('办结成功');
      }else{
        message.error('办结失败');
      }
    },
    *updata({payload},{call, put, select}){

    },
    *getPageData({payload},{call, put}){
      if(payload.id === '-1'){
        yield put({
          type:'setPageData',
          payload:{},
        });
        return;
      }
      const response = yield call(getById,payload);
      if(response.success){
        yield put({
          type:'setPageData',
          payload:response.data,
        });
      }
    },
  },

  reducers: {
    setPageData(state, action) {
      return {
        ...state,
        pageData: action.payload,
      }
    },
    saveAccept(state, action) {
      const { pages, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataList: action.payload,
        pagination: { pages, pageSize, total, current: pageNum },
      }
    },
    saveHandler(state, action) {
      const { pages, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataList: action.payload,
        pagination: { pages, pageSize, total, current: pageNum },
      }
    },
    saveConcluded(state, action){
      const { handleOverData } = action.payload;
      const {handlerType, oralAgreement, handlerProcess, writtenId, path} = handleOverData;
      return{
        ...state,
        handleOverData:{handlerType, oralAgreement, handlerProcess, writtenId, path},
      }
    },
    save(state, action) {
      const { data,pages,pageSize,total,pageNum } = action.payload;
      return {
        ...state,
        dataSource:data,
        pagination:{pages,pageSize,total,current:pageNum},
      };
    },
    saveUser(state, action) {
      return {
        ...state,
        users: action.payload,
      };
    },
    saveTreeData(state, payload) {
      const data = payload.payload;
      const data1 = replaceKeyData(data);
      const treeDataList = constructMenuTreeData(data1);
      return {
        ...state,
        treeData: treeDataList,
      };
    },
    saveMatterType(state,action){
      return {
        ...state,
        typeData: action.payload.data,
      };
    },
    addAllData(state, action) {
      const { allData, pageData } = state;
      const { partyData, moreData } = action.payload;
      allData.listParty = partyData;
      let x;
      for (x in moreData) {
        allData[x] = moreData[x];
      }
      if (pageData.id) {
        allData.id = state.pageData.id;
      }
      allData.acceptType = '1';
      allData.departmentId = '1';
      return {
        ...state,
        allData,
      }
    },
    addAllAssignData(state,action){
      const { allData } = state;
      const { partyData, moreData } = action.payload;
      allData.assigns = partyData;
      let x;
      for(x in moreData){
        allData[x] = moreData[x];
      }
      return{
        ...state,
        allData,
      }
    },
    editAllData(state,action){
      const { allData } = state;
      const { partyData, moreData, editId, editNumber, editStatus } = action.payload;
      allData.listParty = partyData;
      let x;
      for(x in moreData){
        allData[x] = moreData[x];
      }
      allData.id = editId;
      allData.status = editStatus;
      allData.number = editNumber;
      allData.acceptType = '1';
      allData.departmentId = 1;
      return{
        ...state,
        allData,
      }
    },
    search(state, action) {
      return {
        ...state,
        defaultData: action.payload,
      };
    },
    getAssignData(state, action) {
      return {
        ...state,
        assignData: action.payload,
      };
    },
    showModal(state,action){
      return {
        ...state,
        visible:true,
        currentRow:{...action.payload},
      }
    },
    defaultData(state, action) {
      return {
        ...state,
        defaultData: action.payload,
      };
    },
  },
}
