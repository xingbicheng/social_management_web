import { routerRedux } from 'dva/router';
import { message } from 'antd';
import { personDrugAdd, personDrugDel,personDrugGet,personDrugEdit,personDrugDelAll, personDrugList } from '../services/personDrug';
import { personBasegetByIdCard } from '../services/personBase';
import { dictList } from '../services/systemSet'

export default {
    namespace: 'persondrug',
    state: {
         isVisible:false,
         doDispatch:'',
         baseList:[],
         name:'',
         drugList:[],
         singlePerson:{},
         multiDeleteButtonIsVisable: false,
         selectedRowKeys: [],
         pagination : {
           pageSize : 10, // 一页多少条
           page :1,  // 当前页
           total:1,  // 总条数
           pages:1,  // 一共多少页
         },
         degree_education:[],
    },
    effects: {
        *addDrugs({ payload }, { call, put }) {
            const response = yield call(personDrugAdd, payload);
            if(response.code === 0){
              yield put({
                  type: 'listpage',
              });
              yield put({
                  type:'updateState',
                  payload:{addLoading:false},
              })
              message.success('添加成功');
              yield put(routerRedux.push('/person/persondrug'));
                }else{
                    message.error('添加失败');
                }

          },
        *delDrugs({ payload }, { call, put }) {
          const response = yield call(personDrugDel, payload);
          if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            yield put({
                type:'updateState',
                payload:{delLoading:false},
            })
            message.success('删除成功');
            }else{
                message.error('删除失败');
                }
            },
        *delAllDrugs({ payload }, { call, put }) {
            const response = yield call(personDrugDelAll, payload);
            if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('删除成功');
            }else{
                message.error('删除失败');
            }
            yield put({
                type:'updateState',
                payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
            })
            },
          *editDrugs({ payload }, { call, put}) {
            const response = yield call(personDrugEdit, payload);
            if(response.code === 0){
              yield put({
                  type: 'listpage',
              });
              message.success('修改成功');
              yield put(routerRedux.push('/person/persondrug'));
                }else{
                    message.error('修改失败');
                }
          },
          *listpage({ payload }, { call, put,select }) {
            if(!payload)
            {
                const data=yield select(_=>_.persondrug);
                const{pages,pageSize}=data.pagination;
                payload={pages,pageSize};
            }
            const response = yield call(personDrugList, payload);
            if(payload.idCard!==undefined)
            {
                if(response.data.data.length)
                {
                    message.error('该用户已经存在吸毒人员列表,可通过搜索查询到');
                }
                else{
                    yield put({
                        type:'getBase',
                        payload:{idCard:payload.idCard},
                    })
                }
            }
            else{
                yield put({
                    type: 'save',
                    payload:response.data,
                    });
            }
          },
          *getList({ payload }, { call, put }) {
                const response = yield call(personDrugList,payload);
                    message.success('查询成功');
                    yield put({
                        type: 'save',
                        payload:response.data,
                    });
            },
          *getDegree ({ payload }, { call, put }) {
            const response = yield call(dictList,{ type: 'degree_education'})
            yield put({
                type: 'updateState',
                payload: {
                  degree_education: response.data.data,
                },
            })
          },
          *getBase ({ payload }, { call, put }) {
            const response = yield call(personBasegetByIdCard, payload);
            if(response.code === 0&&response.data!==null){
              if(response.data.isDie === '1')
              {
                message.error("此人已死亡,不能继续操作");

              }
              else
              {
                yield put ({
                  type:'getBaseInfo',
                  payload:response.data,
                })
              }

            }else{
                message.error('查询失败,没有此人,请确认您的身份证号输入是否正确');
            }
        },
    },
    reducers: {
        save(state, {payload}) {
            const {pages,pageSize,total,pageNum,data} =payload;
            const newData = data.filter( (item)=> {
                    const arr =item.residence.split(" ");
                    item.residence=arr[0];
                    item.residencedetials=arr[1];
                    return item;
                });
            return {
                ...state,
                drugList: newData,
                pagination:{pages,pageSize,total,current:pageNum},
            };
        },
        searchResult(state, {payload}) {
            const {data} =payload;
            return {
              ...state,
              drugList: data,
            };
          },
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        getBaseInfo(state, { payload })
        {
            return {
                ...state,
                singlePerson:payload,
                name:payload.name,
            }
        },
    },
};
