import { menuTree,addmenu,editmenu,delmenu } from '../services/Permission';
import { constructMenuTreeData2, replaceKeyData } from '../utils/utils';
import { message } from 'antd';
export default {
    namespace: 'menucontroller',
  
    state: {
      dataSource:[],
      pagination : {
        pageSize : 10, // 一页多少条
        current :0,  // 当前页
        total:0,  // 总条数
        pages:0,  // 一共多少页
      },
      modalVisible:false,
      currentRow: {
        name:0,
        route:0,
        parentId:0,
        icon:0,
        sort:0,
      }, 
    },
    effects: {
        *getAllList ({ payload = { } }, { call, put }) {
          const response = yield call(menuTree, payload);
          if(response.code === 0){
              yield put({
                  type:'listAll',
                  payload:response,
              });
          } else {
              message.error(response.msg);
          }
      },
        *addMenu ({ payload = { } }, { call, put }) {
          const response = yield call(addmenu, payload);
          if(response.code === 0){
              yield put({
                  type:'getAllList',
              });
              message.success('添加菜单成功');
          } else {
              message.error(response.msg);
          }
      },
          *editMenu ({ payload = { } }, { call, put }) {
            const response = yield call(editmenu, payload);
            if(response.code === 0){
                yield put({
                    type:'getAllList',
                });
            } else {
                message.error(response.msg);
            }
        },
          *delMenu ({ payload = { } }, { call, put }) {
            const response = yield call(delmenu, payload);
            if(response.code === 0){
                yield put({
                    type:'getAllList',
                });
            } else {
                message.error(response.msg+'无法直接删除');
            }
      },
    
    },
    reducers: {
        listAll(state, action) {
          const { data,pages,pageSize,total,pageNum } = action.payload;
          const menuTree =constructMenuTreeData2(data);
          return {
            ...state,
            dataSource:menuTree,
            pagination:{pages,pageSize,total,current:pageNum},
          };
      },
        showModal(state,action ) {
          return {
            ...state,
            modalVisible:true,
            currentRow:{...action.payload},
          }
        },
        hiddenModal(state,action ) {
          return {
            ...state,
            modalVisible:false,
            currentRow: {
              name:0,
              route:0,
              parentId:0,
              icon:0,
              sort:0,
            },
          }
        },
    },
  };
  