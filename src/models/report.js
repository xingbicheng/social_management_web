import { message } from 'antd';
import { routerRedux } from 'dva/router';
import {
  ReportDetailAdd,
  ReportDetailDel,
  ReportDetailList,
  RiskEvaluationList,
  ReportDetaiEdit,
  RiskEvaluationDel,
  RiskEvaluationEdit,
  RiskEvaluationAdd,
  RiskEvaluationIssued,
  RiskEvaluationSubmit,
  RiskEvaluationDelAll,
} from '../services/risk';
import { dictList } from '../services/systemSet'


export default {
  namespace: 'report',

  state: {
    visible: false,
    dataSource: [],
    detailSource: [],
    type: '',
    headerSource: {
      createTime: '',
      organization: '',
      status: '',
    },
    currentRow: {
      articleId: 0,
      id: 0,
      sort: 0,
      riskEvaluationId: 0,
    },
    pagination: {
      pageSize: 10,
      current: 1,
      total: 1,
      page: 1,
    },
    searchInfo: {},
    risk_details_result:[],
    risk_details_is_archiving:[],
    risk_details_type:[],
    risk_evaluation_status:[],
  },

  effects: {
    * listpage({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationList, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    * add({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationAdd, payload);
      if (response.code === 0) {
        yield put(routerRedux.push('/risk/report'));
        message.success('新增成功');
      } else {
        message.error('新增失败');
      }
    },
    * edit({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationEdit, payload);
      if (response.code === 0) {
        yield put(routerRedux.push('/risk/report'));
        message.success('提交成功');
      } else {
        message.success('提交失败');
      }
    },
    * delete({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationDel, payload);
      if (response.code === 0) {
        yield put({
          type: 'listpage',
          payload: {
            ...response.data,
          },
        });
        message.success('删除成功');
      } else {
        message.success('删除失败');
      }
    },
    * deleteAll({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationDelAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'listpage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('删除成功');
      } else {
        message.success('删除失败');
      }
    },
    * detailPage({ payload }, { call, put }) {
      const response = yield call(ReportDetailList, payload);
      yield put({
        type: 'saveDetail',
        payload: response.data,
      });
    },
    * addDetail({ payload }, { call, put }) {
      const response = yield call(ReportDetailAdd, payload);
      const { riskEvaluationId: id } = payload;
      if (response.code === 0) {
        yield put({
          type: 'detailPage',
          payload: {
            id,
          },
        });
        yield put({
          type: 'hiddenModal',
        });
        message.success('操作成功');
      } else {
        message.error('操作失败');
      }
    },
    * deleteDetail({ payload }, { call, put }) {
      const response = yield call(ReportDetailDel, payload);
      if (response.code === 0) {
        yield put({
          type: 'detailPage',
          payload: {
            id: payload.riskEvaluationId,
          },
        });
        message.success('删除成功');
      } else {
        message.success('删除失败');
      }
    },
    * editDetail({ payload }, { call, put }) {
      const response = yield call(ReportDetaiEdit, payload);
      if (response.code === 0) {
        yield put({
          type: 'detailPage',
          payload: {
            id: payload.riskEvaluationId,
          },
        });
        yield put({
          type: 'hiddenModal',
          payload: response,
        });
        message.success('编辑成功');
      } else {
        message.error('编辑失败');
      }
    },
    * issued({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationIssued, payload);
      if (response.code === 0) {
        yield put({
          type: 'listpage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        yield put(routerRedux.push('/risk/report'));
        message.success('审核成功');
      } else {
        message.error('审核失败');
      }
    },
    * submit({ payload }, { call, put }) {
      const response = yield call(RiskEvaluationSubmit, payload);
      if (response.code === 0) {
        yield put({
          type: 'listpage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        yield put(routerRedux.push('/risk/report'));
        message.success('提交成功');
      } else {
        message.error('提交失败');
      }
    },
    * getRiskArchiving ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'risk_details_is_archiving'})
      yield put({
        type: 'updateState',
        payload: {
          risk_details_is_archiving: response.data.data,
        },
      })
    },
    * getRiskResult ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'risk_details_result'})
      yield put({
        type: 'updateState',
        payload: {
          risk_details_result: response.data.data,
        },
      })
    },
    * getRiskType ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'risk_details_type'})
      yield put({
        type: 'updateState',
        payload: {
          risk_details_type: response.data.data,
        },
      })
    },
    * getRiskStatus ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'risk_evaluation_status'})
      yield put({
        type: 'updateState',
        payload: {
          risk_evaluation_status: response.data.data,
        },
      })
    },

  },

  reducers: {
    save(state, action) {
      const { data, page, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataSource: data,
        pagination: { page, pageSize, total, current: pageNum },
      };
    },
    saveDetail(state, action) {
      const { riskDetailsList, id } = action.payload;
      return {
        ...state,
        headerSource: { ...action.payload },
        detailSource: riskDetailsList,
        id,
      };
    },
    showModal(state, action) {
      return {
        ...state,
        visible: true,
        currentRow: { ...action.payload },
      };
    },
    hiddenModal(state) {
      return {
        ...state,
        visible: false,
      };
    },
    setSearchInfo(state, action) {
      return {
        ...state,
        searchInfo: { ...action.payload },
      };
    },
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    clearAll(state) {
      return {
        ...state,
        detailSource: [],
        searchInfo: {},
      };
    },
    updateForm(state) {
      return {
        ...state,
        headerSource: {},
      };
    },
  },
};
