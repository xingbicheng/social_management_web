import { message } from 'antd';
import { routerRedux } from 'dva/router';

import { personBasegetByIdCard,personBaseList,personBaseGet,personBaseAdd,personBaseEdit } from '../services/personBase';
import { dictList } from '../services/systemSet'
import { createSocket } from 'dgram';

export default {
  namespace: 'personbase',
  state: {
    baseList:[],
    political_outlook:[],
    doDispatch:'',
    isDie:false,
    marital_status:[],
    isVisible:false,
    degree_education:[],
    householdRegisterNature:[],
    singlePerson:{},
    pagination : {
      pageSize : 10, // 一页多少条
      page :1,  // 当前页
      total:1,  // 总条数
      pages:1,  // 一共多少页
    },


  },
  effects: {
    *addBase({ payload }, { call, put }) {
      const response = yield call(personBaseAdd, payload);
      if(response.code === 0){
        yield put({
            type: 'listpage',
        });
        message.success('添加成功');
        yield put(routerRedux.push('/person/personbase'));
    }else{
        message.error(response.msg);
    }

    },

    *getBase({ payload }, { call, put}) {
      const response = yield call(personBaseGet, payload);
      if(response.code === 0){
        yield put({
            type: 'updateState',
            payload:{singlePerson:response.data,doDispatch:'edit',isDie:(response.data.isDie==='1')},
        });
    }
    },
    *editBase({ payload }, { call, put}) {
      const response = yield call(personBaseEdit, payload);
      if(response.code === 0){
        yield put({
            type: 'listpage',
        });
        message.success('修改成功');
         yield put(routerRedux.push('/person/personbase'));
    }else{
        message.error('修改失败');
    }
    },
    * getMarital({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'marital_status'})
      yield put({
          type: 'updateState',
          payload: {
            marital_status: response.data.data,
          },
      })
    },
    * getOutLook ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'political_outlook'})
      yield put({
          type: 'updateState',
          payload: {
            political_outlook: response.data.data,
          },
      })
    },
    * getDegree ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'degree_education'})
      yield put({
          type: 'updateState',
          payload: {
            degree_education: response.data.data,
          },
      })
    },
    * getHouse ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'household_register_nature'})
      yield put({
          type: 'updateState',
          payload: {
            householdRegisterNature: response.data.data,
          },
      })
    },
    *listpage({ payload }, { call, put, select }) {
      console.log(payload);
      if(!payload)
      {
          const data=yield select(_=>_.personbase);
          const{pages,pageSize}=data.pagination;
          payload={pages,pageSize};
      }
        const response = yield call(personBaseList,payload);
        console.log(response)
            yield put({
                type: 'save',
                payload:response.data,
            });
    },
    *getList({ payload }, { call, put }) {
      const response = yield call(personBaseList,payload);
          message.success('查询成功');
          yield put ({
            type:'save',
            payload:response.data,
          })
  },
  },
  reducers: {
    save(state, {payload}) {
      const {page,pageSize,total,pageNum,data} =payload;
      return {
        ...state,
        baseList: data,
        pagination:{page,pageSize,total,current:pageNum},
      };
    },
    searchResult(state, {payload}) {
        const {data} =payload;
        return {
          ...state,
          baseList: data,
        };
      },
    updateState (state, { payload }) {
      return {
          ...state,
          ...payload,
      }
    },
    getBaseInfo(state, { payload })
    {
        return {
            ...state,
            singlePerson:payload,
            name:payload.name,
        }
    },
  },
};
