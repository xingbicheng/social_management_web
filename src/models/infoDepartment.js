import { listAll } from '../services/infoDepartment'
import { getPersonInfo, getHouseInfo, getPersonTypeInfo } from '../services/infoGridBase'
import { dictList } from '../services/systemSet'

export default {
    namespace: 'infoDepartment',
    state: {
       dataSource: [], 
       personInfo: {},
       houseInfo: {},
       personTypeInfo: {},
       name: '',
       houseType: [],
    },
    effects: {
        * listAll ({ payload }, { call, put }) {
            const response = yield call(listAll)
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        dataSource: response.data,
                    },
                })
            }
        },
        * getBaseInfo ({ payload }, { call, put }) {
            const response1 = yield call(getPersonInfo, payload)
            const response2 = yield call(getHouseInfo, payload)
            const response3 = yield call(getPersonTypeInfo, payload)
            if (response1.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personInfo: response1.data,
                    }
                })
            }
            if (response2.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        houseInfo: response2.data,
                    },
                })
            }
            if (response3.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personTypeInfo: response3.data,
                    },
                })
            }
        },
        * initBasic ({ payload }, { call, put }) {
          const response = yield call(dictList,{ type: 'house_type' })
          if (response.code === 0) {
            yield put({
                type: 'updateState',
                payload: {
                    houseType: response.data.data,
                },
            })
          }
        },
    },
    reducers: {
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                dataSource: [], 
                personInfo: {},
                houseInfo: {},
                personTypeInfo: {},
                name: '',
            }
        },
    }
}