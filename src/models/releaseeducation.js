import { message } from 'antd';
import { educationAdd, educationDel,educationGet,educationEdit,educationDelAll, educationList } from '../services/releaseEducation';
import { helperList } from '../services/releaseHelper';
import { dictList } from '../services/systemSet'

export default {
    namespace: 'releaseeducation',

    state: {
         isVisible:false,
         baseList:[],
         singlePerson:{},
         multiDeleteButtonIsVisable: false,
         selectedRowKeys: [],
         pagination : {
           pageSize : 10, // 一页多少条
           page :1,  // 当前页
           total:1,  // 总条数
           pages:1,  // 一共多少页
         },
         releasePersonId:0,
         doDispatch:'add',
         degree_education:[],
         rowList:{},
    },
    effects: {
        *addEdu({ payload }, { call, put }) {
            const response = yield call(educationAdd, payload);
            if(response.code === 0){
              yield put({
                  type: 'listpage',
              });
              message.success('添加成功');
          }else{
              message.error('添加失败');
          }
          },
        *getHelperList({ payload }, { call, put,select }) {
        const data=yield select(_=>_.releaseeducation);
        const{page,pageSize}=data.pagination;
        payload={...payload,page,pageSize};
        const response = yield call(helperList, payload);
            yield put({
            type: 'getHelperName',
            payload:response.data,
            })
        },
        *delEdu({ payload }, { call, put }) {
          const response = yield call(educationDel, payload);
          if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('删除成功'); 
        }else{
            message.error('删除失败');
        }
        
        
      },
      *delAllEdu({ payload }, { call, put }) {
        const response = yield call(educationDelAll, payload);
        if(response.code === 0){
          yield put({
              type: 'listpage',
          });
          message.success('删除成功');
      }else{
          message.error('删除失败');
      }
      yield put({
        type:'updateState',
        payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
      })
      },
        *editEdu({ payload }, { call, put}) {
        const response = yield call(educationEdit, payload);
        if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('修改成功');
        }else{
            message.error('修改失败');
        }
        },
        *listpage({ payload }, { call, put,select }) {
        if(!payload)
        {
            const data=yield select(_=>_.releaseeducation);
            const{pagination:{page,pageSize},releasePersonId}=data;
            payload={pageSize,page,releasePersonId};
        }
            const response = yield call(educationList,payload);
        yield put({
            type: 'save',
            payload:response.data,
        });
        },
        * getDegree ({ payload }, { call, put }) {
        const response = yield call(dictList,{ type: 'degree_education'})
        yield put({
            type: 'updateState',
            payload: {
                degree_education: response.data.data,
            },
        })
        },
    },
    reducers: {
        save(state, action) {
        const {pages,pageSize,total,pageNum} =action.payload;
        return {
            ...state,
            otherList: action.payload.data,
            pagination:{pages,pageSize,total,current:pageNum},
        };
        },
        updateState (state, { payload }) {
        return {
            ...state,
            ...payload,
        }
        },
        getHelperName(state,{payload})
        {
            return {
                ...state,
                helperName:payload.data,
            }
        },
    },
};
