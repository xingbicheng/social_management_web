import { message } from 'antd'
import {list, del } from '../services/infoGrid'
import { getPersonInfo, getHouseInfo, getPersonTypeInfo } from '../services/infoGridBase'
import { dictList } from '../services/systemSet'

export default {
    namespace: 'infoGrid',
    state: {
        dataSource: [],
        history: {},
        districtId: '',
        personInfo: {},
        houseInfo: {},
        personTypeInfo: {},
        name: '',
        houseType: [],
    },
    subscriptions: {
        setup ({dispatch, history}) {
            // history.listen(( location ) => {
            //     const match = pathToRegexp('/gridInfoManage/grid/:id').exec(location.pathname)
            //     if (match) {
            //         dispatch({
            //             type: 'list',
            //             payload: {
            //                 districtId: match[1],
            //             },
            //         })
            //         dispatch({
            //             type: 'updateState',
            //             payload: {
            //                 history,
            //                 districtId: match[1],
            //             },
            //         })
            //     }
            // })
        },
    },
    effects: {
        * list ({ payload }, { call, put}) {
            const response = yield call(list, payload)
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        dataSource: response.data.data,
                    },
                })
            }
        },
        * delCard ({ payload }, { call, put, select }) {
            const response = yield call(del, payload)
            if (response.code === 0) {
                const { districtId } = yield select(_=>_.infoGrid)
                yield put({
                    type: 'list',
                    payload: {
                        districtId,
                    },
                })
                message.success("删除成功")
            } else if (response.code === 1) {
                message.warning("该片区下面包含房屋信息或服务团队，不能删除")
            } else {
                message.error("删除失败")
            }
        },
        * getBaseInfo ({ payload }, { call, put }) {
            const response1 = yield call(getPersonInfo, payload)
            const response2 = yield call(getHouseInfo, payload)
            const response3 = yield call(getPersonTypeInfo, payload)
            if (response1.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personInfo: response1.data,
                    },
                })
            }
            if (response2.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        houseInfo: response2.data,
                    },
                })
            }
            if (response3.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personTypeInfo: response3.data,
                    },
                })
            }
        },
        * initBasic ({ payload }, { call, put }) {
          const response = yield call(dictList,{ type: 'house_type' })
          if (response.code === 0) {
            yield put({
                type: 'updateState',
                payload: {
                    houseType: response.data.data,
                },
            })
          }
        },
    },
    reducers: {
        updateState(state, {payload}) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                personInfo: {},
                houseInfo: {},
                personTypeInfo: {},
                name: '',
            }
        }
    }
}