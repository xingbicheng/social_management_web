import { routerRedux } from 'dva/router';
import { message } from 'antd';
import { releaseAdd, releaseDel,releaseGet,releaseEdit,releaseDelAll, releaseList } from '../services/personRelease';
import { personBasegetByIdCard } from '../services/personBase';
import { dictList } from '../services/systemSet'

export default {
    namespace: 'personrelease',
    state: {
         relList:[],
         flag:'1',
         isTeaching:false,
         isLowInsurance:false,
         isVisible:false,
         baseList:[],
         releasePersonId:0,
         singlePerson:{},
         multiDeleteButtonIsVisable: false,
         selectedRowKeys: [],
         pagination : {
           pageSize : 10, // 一页多少条
           page :1,  // 当前页
           total:1,  // 总条数
           pages:1,  // 一共多少页
         },
         degree_education:[],
    },
    effects: {
        *addRelease({ payload }, { call, put }) {
            const response = yield call(releaseAdd, payload);
            if(response.code === 0){
              yield put({
                  type: 'listpage',
              });
              message.success('添加成功');
              yield put(routerRedux.push('/person/personrelease'));

          }else{
              message.error('添加失败');
          }

          },
        *delRelease({ payload }, { call, put }) {
          const response = yield call(releaseDel, payload);
          if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('删除成功');
            }else{
                message.error('删除失败,因信息已详尽,此人信息不能删除');
            }
        },
        *delAllRelease({ payload }, { call, put }) {
            const response = yield call(releaseDelAll, payload);
            if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('删除成功');
            }else{
                message.error('删除失败,因部分人员信息已详尽,不能一起删除');
            }
            yield put({
                type:'updateState',
                payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
            })
            },
        *editRelease({ payload }, { call, put}) {
        const response = yield call(releaseEdit, payload);
        if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('修改成功');
        }else{
            message.error('修改失败');
        }
        },
        *listpage({ payload }, { call, put,select }) {
        if(!payload)
        {
            const data=yield select(_=>_.personrelease);
            const{pagination:{pages,pageSize},releasePersonId}=data;
            payload={pages,pageSize,releasePersonId};
        }
        const response = yield call(releaseList, payload);
        if(payload.idCard!==undefined)
        {
            if(response.data.data.length)
            {
                message.error('该用户已经存在刑满释放人员列表,可通过搜索查询到');
            }
            else{
                yield put({
                    type:'getBase',
                    payload:{idCard:payload.idCard},
                })
            }
        }
        else{
            yield put({
                type: 'save',
                payload:response.data,
                });
        }
        },
        *getList({ payload }, { call, put }) {
            const response = yield call(releaseList,payload);
                yield put({
                    type: 'searchResult',
                    payload:response.data,

                });
        },
        * getDegree ({ payload }, { call, put }) {
            const response = yield call(dictList,{ type: 'degree_education'})
            yield put({
                type: 'updateState',
                payload: {
                    degree_education: response.data.data,
                },
            })
        },
        *getBase ({ payload }, { call, put }) {
            const response = yield call(personBasegetByIdCard, payload);
            if(response.code === 0&&response.data!==null){
              if(response.data.isDie === '1')
              {
                message.error("此人已死亡,不能继续操作");

              }
              else
              {
                yield put ({
                  type:'getBaseInfo',
                  payload:response.data,
                })
              }
            }else{
                message.error('查询失败,没有此人,请确认您的身份证号输入是否正确');
            }
        },

    },
    reducers: {
          save(state, {payload}) {
            const {pages,pageSize,total,pageNum,data} =payload;
            const {isTeaching,isLowInsurance} =state;
            let info = {};
            if(data.length!==0)
            {
                info = data[0];
            };
            return {
              ...state,
              relList: data,
              releasePersonId: data.id,
              pagination:{pages,pageSize,total,current:pageNum},
              isTeaching:info?(info.isTeaching==='1'):isTeaching,
              isLowInsurance:info?(info.isLowInsurance==='1'):isLowInsurance,
            };
          },
          searchResult(state, {payload}) {
            const {data} =payload;
            return {
              ...state,
              relList: data,
            };
          },
          updateState (state, { payload }) {
                return {
                    ...state,
                    ...payload,
                }
            },
            getBaseInfo(state, { payload })
            {
                return {
                    ...state,
                    baseList:payload,
                    singlePerson:payload,
                    name:payload.name,
                }
            },
    },
};
