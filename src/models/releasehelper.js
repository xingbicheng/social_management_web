import { helperAdd, helperDel,helperGet,helperEdit,helperDelAll, helperList } from '../services/releaseHelper';
import { message } from 'antd';
import { dictList } from '../services/systemSet'

export default {
    namespace: 'releasehelper',
    state: {
         isVisible:false,
         helperList:[],
         multiDeleteButtonIsVisable: false,
         selectedRowKeys: [],
         pagination : {
           pageSize : 10, // 一页多少条
           page :1,  // 当前页
           total:1,  // 总条数
           pages:1,  // 一共多少页
         },
         releasePersonId:0,
         doDispatch:'add',
         rowList:{},
         helper_type:[],
    },
    effects: {
        *addHelper({ payload }, { call, put }) {
            const response = yield call(helperAdd, payload);
            if(response.code === 0){
              yield put({
                  type: 'listpage',
              });
              yield put({
                  type:'releaseeducation/getHelperList',
              })
              message.success('添加成功');
            }else{
                message.error('添加失败');
            }
          },
        *delHelper({ payload }, { call, put }) {
            const response = yield call(helperDel, payload);
            if(response.code === 0){
                yield put({
                    type: 'listpage',
                });
                yield put({
                    type:'releaseeducation/getHelperList',
                })

                message.success('删除成功');
            }
            else if(response.code === 1)
            {
                message.error('因此人已有帮教的情况,所以不能删除');
            }
            else{
                message.error('删除失败');
            }
        },
        *delAllHelper({ payload }, { call, put }) {
            const response = yield call(helperDelAll, payload);
            if(response.code === 0){
            yield put({
                type: 'listpage',
            });
            message.success('删除成功');
           
            }
            else if(response.code === 1)
            {
                message.error('因此人已有帮教的情况,所以不能删除');
            }
            else{
                message.error('删除失败');
            }
            yield put({
                type:'updateState',
                payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
            })
        },
        * initBasicData ({ payload }, { call, put }) {
          const response = yield call(dictList,{ type: 'helper_type'})
          yield put({
              type: 'updateState',
              payload: {
                helper_type: response.data.data,
              },
          })
        },
        *editHelper({ payload }, { call, put}) {
            const response = yield call(helperEdit, payload);
            if(response.code === 0){
                yield put({
                    type: 'listpage',
                });
                message.success('修改成功');
                yield put({
                    type:'releaseeducation/getHelperList',
                })
            }else{
                message.error('修改失败');
            }
        },
        *listpage({ payload }, { call, put,select }) {
            yield put({
                type: 'initBasicData',
            })
            if(!payload)
            {
                const data=yield select(_=>_.releasehelper);
                const{pagination:{page,pageSize},releasePersonId}=data;
                payload={page,pageSize,releasePersonId};
            }
            const response = yield call(helperList,payload);
            yield put({
                type: 'save',
                payload:response.data,
            });
            
        },
    },
        reducers: {
          save(state, action) {
            const {pages,pageSize,total,pageNum} =action.payload;
            return {
              ...state,
              helperList: action.payload.data,
              pagination:{pages,pageSize,total,current:pageNum},
            };
          },
          updateState (state, { payload={} }) {
            return {
                ...state,
                ...payload,
                // helper_type:payload.helper_type,
            }
          },
        },
      };
