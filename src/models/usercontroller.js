import {  listAll, addUser, editUser, resetPassWords, setState, getDataById, } from '../services/usersController';
import { getAllRoles,listAllRoles,getById} from '../services/roleController';
import { departmentListAll } from '../services/department';
import { constructMenuTreeData, replaceKeyData } from '../utils/utils';
import { message } from 'antd';

export default {
  namespace: 'usercontroller',
  state: {
    dataSource: [],
    rolesList:[],
    userRole:[],// 用户拥有的角色
    visible: false,
    treeData: [],//部门树
    pagination: {
      pageSize: 10, // 一页多少条
      current: 0,  // 当前页
      total: 0,  // 总条数
      pages: 0,  // 一共多少页
    },
    currentRow: {
      departmentId: 1,
      username: 0,
      status: '',
      password: 0,
      nickname: '',
      rolename:'',
      roleId:0,
      telephone: '',
      departmentName: '',
    },  // 当前选择数据
    searchInfo: {}, // 检索Form表单
  },
  effects: {
    * getAllList({ payload = {} }, { call, put }) {
      const response = yield call(listAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'listAll',
          payload: response.data,
        });
        yield put({
          type: 'unloading',
        });
      } else {
        message.error(response.msg);
      }
    },
    * getAllRolesList({ payload = {} }, { call, put }) {
        const response = yield call(listAllRoles, payload);
        if (response.code === 0) {
        yield put({
          type: 'saveRolesList',
          payload: response.data,
        });
        // yield put({
        //   type: 'unloading',
        // });
      } else {
        message.error(response.msg);
      }
    },
    * getListById({ payload = {} }, { call, put }) {
      const response = yield call(getDataById, payload);
      if (response.code === 0) {
        yield put({
          type: 'showModal',
          payload: response.data,
        });
      } else {
        message.error(response.msg);
      }
    },
    * getRoleById({ payload = {} }, { call, put }) {
      const response = yield call(getById, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveUserRole',
          payload: response.data,
        });
      } else {
        message.error(response.msg);
      }
    },
    
    * reset({ payload = {} }, { call, put }) {
      const response = yield call(resetPassWords, payload);
      if (response.code === 0) {
        yield put({
          type: 'getAllList',
        });
        message.success('重置密码成功');
      } else {
        message.error(response.msg);
      }
    },
    * getDepartmentList({ payload = {} }, { call, put }) {
      const response = yield call(departmentListAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveTreeData',
          payload: response.data,
        });
      }
    },

    * setstatus({ payload = {} }, { call, put }) {
      let page = payload.pageNum;
      const response = yield call(setState, payload);
      if (response.code === 0) {
        yield put({
          type: 'getAllList',
          payload: {
            page,
            pageSize: 10,
          },
        });
        message.success('更改状态成功');
      } else {
        message.error(response.msg);
      }
    },
    * addNewUsers({ payload = {} }, { call, put }) {
      let role=payload.Roles;
      let id = payload.id;
      payload.sysUserRoles={};
      payload.sysUserRoles.roleIds=[role];
      payload.sysUserRoles.userId=id;
      const response = yield call(addUser, payload);
      if (response.code === 0) {
        yield put({
          type: 'getAllList',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('添加成功');
        yield put({
          type: 'hiddenModal',
          payload: response,
        });
      } else {
        message.error("用户名"+response.msg);
      }
    },
    * edit({ payload = {} }, { call, put }) {
      let page = payload.pageNum;
      let role=payload.Roles;
      let id = payload.id;
      payload.sysUserRoles={};
      payload.sysUserRoles.roleIds=[role];
      payload.sysUserRoles.userId=id;
      const response = yield call(editUser, payload);
      if (response.code === 0) {
        yield put({
          type: 'getAllList',
          payload: {
            page,
            pageSize: 10,
          },
        });
        yield put({
          type: 'hiddenModal',
          payload: response,
        });
        message.success('修改成功');
      } else {
        message.error(response.msg);
      }
    },
  },
  reducers: {
    listAll(state, action) {
      const { data, pages, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataSource: data,
        pagination: { pages, pageSize, total, current: pageNum },
      };

    },
    showModal(state, action) {
      return {
        ...state,
        visible: true,
        currentRow: { ...state.currentRow,...action.payload },
      };
    },
    saveUserRole(state,payload={}){
      let obj={};
      obj.roleId=payload.payload[0].id;
      obj.rolename=payload.payload[0].name;
      return{
        ...state,
        userRole:payload.payload,
        currentRow: {...state.currentRow,...obj},
      }
    },
    saveRolesList(state,payload){
      return{
        ...state,
        rolesList:payload.payload.data,
      }
    },
    saveTreeData(state, payload) {
      const data = payload.payload;
      const data1 = replaceKeyData(data);
      const treeDataList = constructMenuTreeData(data1);
      return {
        ...state,
        treeData: treeDataList,
      };
    },
    hiddenModal(state) {
      return {
        ...state,
        visible: false,
        currentRow: {
          departmentId:1,
          username: 0,
          status: '',
          password: 0,
          nickname: 0,
          roleId:'',
          rolename:'',
          telephone: '0',
          departmentName:'',
        },
      };
    },
    clearSearchInfo(state) {
      return {
        ...state,
        searchInfo: {},
      };
    },
    isloading(state){
      return{
        ...state,
        loading:true,
      }
    },
    unloading(state){
      return{
        ...state,
        loading:false,
      }
    },
    setSearchInfo(state, action) {
      return {
        ...state,
        searchInfo: { ...action.payload },
      };
    },
  },

};
