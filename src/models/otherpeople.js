import { othersAdd,othersDel,othersDelAll,othersEdit,othersGet,othersList,exportExcel,exportExcelAll } from '../services/personOther';
import { message } from 'antd';
import { routerRedux } from 'dva/router';

import { personBasegetByIdCard } from '../services/personBase';
import { dictList } from '../services/systemSet'

export default {
  namespace: 'otherpeople',

  state: {
    visible:false,
    otherList:[],
    doDispatch:'',
    isVisible:false,
    multiDeleteButtonIsVisable: false,
    selectedRowKeys: [],
    singlePerson:{},
    pagination : {
      pageSize : 10, // 一页多少条
      page :1,  // 当前页
      total:1,  // 总条数
      pages:1,  // 一共多少页
    },
    other_person_type:[],
    crowd_type:[],
    degree_education:[],

  },
  effects: {
    *addOthers({ payload }, { call, put }) {
      const response = yield call(othersAdd, payload);
      if(response.code === 0){
        yield put({
            type: 'listpage',
        });
        message.success('添加成功');
        yield put(routerRedux.push('/person/personother'));
    }else{
        message.error(response.msg);
    }

    },
  *delOthers({ payload }, { call, put }) {
    const response = yield call(othersDel, payload);
    if(response.code === 0){
      yield put({
          type: 'listpage',
      });
      message.success('删除成功');
  }else{
      message.error('删除失败');
  }
},
*delAllOthers({ payload }, { call, put }) {
  const response = yield call(othersDelAll, payload);
  if(response.code === 0){
    yield put({
        type: 'listpage',
    });
    message.success('删除成功');
    }else{
        message.error('删除失败');
    }
    yield put({
        type:'updateState',
        payload:{multiDeleteButtonIsVisable: Boolean(0),selectedRowKeys:[]},
    })
},
  * initBasicData ({ payload }, { call, put }) {
    const response = yield call(dictList,{ type: 'other_person_type'})
    yield put({
        type: 'updateState',
        payload: {
          other_person_type: response.data.data,
        },
    })
},
* getCrowdType ({ payload }, { call, put }) {
  const response = yield call(dictList,{ type: 'crowd_type'})
  yield put({
      type: 'updateState',
      payload: {
        crowd_type: response.data.data,
      },
  })
},
* getDegree ({ payload }, { call, put }) {
  const response = yield call(dictList,{ type: 'degree_education'})
  yield put({
      type: 'updateState',
      payload: {
        degree_education: response.data.data,
      },
  })
},
    *editOthers({ payload }, { call, put}) {
      const response = yield call(othersEdit, payload);
      if(response.code === 0){
        yield put({
            type: 'listpage',
        });
        message.success('修改成功');
        yield put(routerRedux.push('/person/personother'));
    }else{
        message.error('修改失败');
    }
    },
    *listpage({ payload }, { call, put,select }) {
        yield put({
            type:'initBasicData',
        })
      if(!payload)
      {
          const data=yield select(_=>_.otherpeople);
          const{pages,pageSize}=data.pagination;
          payload={pages,pageSize};
      }
      const response = yield call(othersList, payload);
      if(payload.idCard!==undefined)
      {
          if(response.data.data.length)
          {
              message.error('该用户已经存在吸毒人员列表,可通过搜索查询到');
          }
          else{
              yield put({
                  type:'getBase',
                  payload:{idCard:payload.idCard},
              })
          }
      }
      else{
          yield put({
              type: 'save',
              payload:response.data,
              });
      }
    },
    *getList({ payload }, { call, put }) {
        const response = yield call(othersList,payload);
            message.success('查询成功');
            yield put({
                type: 'searchResult',
                payload:response.data,
            });
    },
    *getBase ({ payload }, { call, put }) {
      const response = yield call(personBasegetByIdCard, payload);
      if(response.code === 0&&response.data!==null){
        if(response.data.isDie === '1')
        {
          message.error("此人已死亡,不能继续操作");

        }
        else
        {
          yield put ({
            type:'getBaseInfo',
            payload:response.data,
          })
        }

      }else{
          message.error('查询失败,没有此人,请确认您的身份证号输入是否正确');
      }
  },

  },
  reducers: {
    save(state, {payload}) {
      const {pages,pageSize,total,pageNum,data} =payload;
      return {
        ...state,
        otherList: data,
        pagination:{pages,pageSize,total,current:pageNum},
      };
    },
    searchResult(state, {payload}) {
        const {data} =payload;
        return {
          ...state,
          otherList: data,
        };
      },
    updateState (state, { payload }) {
      return {
          ...state,
          ...payload,
      }
    },
    getBaseInfo(state, { payload })
    {
        return {
            ...state,
            singlePerson:payload,
            name:payload.name,
        }
    },
  },
};
