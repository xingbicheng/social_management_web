import { message } from 'antd';
import { routerRedux } from 'dva/router';
import sha1 from 'sha1';
import { queryCurrent,updatePassword } from '../services/user';

export default {
  namespace: 'user',

  state: {
    currentUser: {},
    visible:false,
  },

  effects: {
    * fetchCurrent ({ payload={} }, { put, call }) {
      console.log("enter");
      const { code, data, msg } = yield call(queryCurrent, payload);
      if(code === 401){

        yield put(routerRedux.push('/user/login'));
      }else if (code === 0) {
        yield put({
          type:'saveCurrent',
          payload:data,
        })
      } else {
          message.error(msg);
      }
    },
    * updatePsw ({ payload={} }, { put, call }) {
      payload.oldPassword = sha1(payload.oldPassword);
      payload.newPassword = sha1(payload.oldPassword);
      const { code, data, msg } = yield call(updatePassword, payload);
      if (code === 0) {
        yield put({
          type:'hiddenModel',
          payload:data,
        });
        message.success('修改成功！');
      } else {
          message.error(msg);
      }
  },
  },

  reducers: {
    saveCurrent(state, action){
      return{
          ...state,
          currentUser:{...action.payload},
      }
    },
    hiddenModel(state,action){
      return{
        ...state,
        visible:false,
      }
    },
    showModel(state,action){
      return{
        ...state,
        visible:true,
      }
    },
  },
};
