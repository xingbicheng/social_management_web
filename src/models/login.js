import { message } from 'antd';
import sha1 from 'sha1';
import { routerRedux } from 'dva/router';
import { GetCaptcha,Login,Logout } from "../services/login";
import tokenHandler from '../utils/tokenHandler';
import { getUserTree } from '../services/Permission';


export default {
  namespace: 'login',

  state: {
      captchaImg: '',
      visible: false,
      isFresh:false,
  },
  subscriptions: {

  },
  effects: {
      * login ({ payload={} }, { put, call, select }) {
        payload.password = sha1(payload.password);
        const { code, data, msg } = yield call(Login, payload);
          if (code === 0) {
            tokenHandler.setSessionByKey('token', data.token)
            console.log(data);
            const response = yield call(getUserTree,{});
            if(response.success){
              const { data:menuData } = response;
              tokenHandler.setSessionByKey('menuData',JSON.stringify(menuData));
            }
            //   if (from && from !== '/user/login') {
            //       // yield put(routerRedux.push(from))
            //   } else {
            //       //
            //   }
            message.success("登录成功！");
            yield put(routerRedux.push('/'))
          } else {
              message.error(msg);
          }
      },
      * getcaptcha ({ payload = {} }, { put, call }) {
          const { code,data,msg } = yield call(GetCaptcha);
           if (code === 0) {
              yield put({
                  type: 'updateModalState',
                  payload: {
                      captchaImg: data,
                  },
              })
          } else {
              message.error(msg || '错误！');
          }
      },
      * logout ({ payload = {} }, { put, call }) {
        const { code,data,msg } = yield call(Logout,payload);
        if (code === 0) {
            tokenHandler.removeSessionByKey('token');
            tokenHandler.removeSessionByKey('menuData');
            yield put(routerRedux.push('/user/login'));
            message.success("登出成功！");
        } else {
            message.error(msg || '错误！');
        }
     },
  },

  reducers: {
      updateModalState (state, { payload }) {
          return { ...state, ...payload }
      },
  },
};
