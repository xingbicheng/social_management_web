import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { IsubmitGet, InoticeGet, ReviewPass, ReviewNotPass } from '../services/infoPlatform';
import { dictList } from '../services/systemSet';

export default {
  namespace: 'checkPage',

  state: {
    detailSource: {},
    submitted_infor_category:[],
    submitted_infor_show:[],
  },

  effects: {
    * SubmitDetail({ payload }, { call, put }) {
      const response = yield call(IsubmitGet, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },

    * NoticeDetail({ payload }, { call, put }) {
      const response = yield call(InoticeGet, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },
    * examine({ payload }, { call, put }) {
      const response = yield call(IsubmitGet, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },
    * pass({ payload }, { call, put }) {
      const response = yield call(ReviewPass, payload);
      if (response.code === 0) {
        message.success('操作成功');
        yield put(routerRedux.push('/infoPlatform/submit'));
      } else {
        message.error('操作失败');
      }
    },
    * notPass({ payload }, { call, put }) {
      const response = yield call(ReviewNotPass, payload);
      if (response.code === 0) {
        message.success('操作成功');
        yield put(routerRedux.push('/infoPlatform/submit'));
      } else {
        message.error('操作失败');
      }
    },
    * getSubmitCategory ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'submitted_infor_category'})
      yield put({
        type: 'updateState',
        payload: {
          submitted_infor_category: response.data.data,
        },
      })
    },
    * getSubmitShowHomepage ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'submitted_infor_show'})
      yield put({
        type: 'updateState',
        payload: {
          submitted_infor_show: response.data.data,
        },
      })
    },
  },
  reducers: {
    saveDetail(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
