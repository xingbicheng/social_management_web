import { message } from 'antd';
import { routerRedux } from 'dva/router';
import {
  IsubmitList,
  IsubmitDel,
  IsubmitAdd,
  IsubmitGet,
  IsubmitEdit,
  IsubmitSubmit,
  IsubmitDelAll,
  IsubmitShow,
  IsubmitNotShow,
} from '../services/infoPlatform';
import { constructMenuTreeData, replaceKeyData } from '../utils/utils';
import { departmentListAll } from '../services/department';
import { dictList } from '../services/systemSet';

export default {
  namespace: 'infoSubmit',

  state: {
    hasReceived: false,
    dataSource: [],
    detailSource: {},
    treeData: [],
    currentRow: {
      articleId: 0,
      id: 0,
      sort: 0,
    },
    pagination: {
      pageSize: 10,
      current: 1,
      total: 1,
      page: 1,
    },
    searchInfo: {},
    submitted_infor_category:[],
    submitted_infor_show:[],
    submitted_infor_statues:[],
  },

  effects: {
    * SubmitPage({ payload }, { call, put }) {
      const response = yield call(IsubmitList, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    * add({ payload }, { call, put }) {
      const response = yield call(IsubmitAdd, payload);
      if (response.code === 0) {
        message.success('新增成功');
        yield put(routerRedux.push('/infoPlatform/submit'));
      } else {
        message.error('新增失败');
      }
    },
    * delete({ payload }, { call, put }) {
      const response = yield call(IsubmitDel, payload);
      if (response.code === 0) {
        yield put({
          type: 'SubmitPage',
          payload: {
            ...response.data,
          },
        });
        message.success('删除成功');
      } else {
        message.error('删除失败');
      }
    },
    * deleteAll({ payload }, { call, put }) {
      const response = yield call(IsubmitDelAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'SubmitPage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('删除成功');
      } else {
        message.success('删除失败');
      }
    },
    * SubmitDetail({ payload }, { call, put }) {
      const response = yield call(IsubmitGet, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },
    * update({ payload }, { call, put }) {
      const response = yield call(IsubmitEdit, payload);
      if (response.code === 0) {
        message.success('操作成功');
        yield put(routerRedux.push('/infoPlatform/submit'));
      } else {
        message.error('操作失败');
      }
    },
    * submit({ payload }, { call, put }) {
      const response = yield call(IsubmitSubmit, payload);
      if (response.code === 0) {
        yield put({
          type: 'SubmitPage',
          payload: {
            ...response.data,
          },
        });
        message.success('操作成功');
      } else {
        message.error('操作失败');
      }
    },
    * getDepartmentList({ payload = {} }, { call, put }) {
      const response = yield call(departmentListAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveTreeData',
          payload: response.data,
        });
      }
    },
    * show({ payload }, { call, put }) {
      const response = yield call(IsubmitShow, payload);
      if (response.code === 0) {
        yield put({
          type: 'SubmitPage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('操作成功');
      } else {
        message.error('操作失败');
      }
    },
    * notShow({ payload }, { call, put }) {
      const response = yield call(IsubmitNotShow, payload);
      if (response.code === 0) {
        yield put({
          type: 'SubmitPage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('操作成功');
      } else {
        message.error('操作失败');
      }
    },
    * getSubmitCategory ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'submitted_infor_category'})
      yield put({
        type: 'updateState',
        payload: {
          submitted_infor_category: response.data.data,
        },
      })
    },
    * getSubmitShowHomepage ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'submitted_infor_show'})
      yield put({
        type: 'updateState',
        payload: {
          submitted_infor_show: response.data.data,
        },
      })
    },
    * getSubmitStatues ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'submitted_infor_statues'})
      yield put({
        type: 'updateState',
        payload: {
          submitted_infor_statues: response.data.data,
        },
      })
    },

  },

  reducers: {
    save(state, action) {
      const { data, page, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataSource: data,
        pagination: { page, pageSize, total, current: pageNum },
        currentRow: { ...action.payload },
      };
    },
    saveDetail(state, action) {
      return {
        ...state,
        detailSource: { ...state.detailSource, ...action.payload },
      };
    },
    clearAll(state) {
      return {
        ...state,
        searchInfo: {},
      };
    },
    setSearchInfo(state, action) {
      return {
        ...state,
        searchInfo: { ...action.payload },
      };
    },
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    saveTreeData(state, action) {
      const data = action.payload;
      const datas = replaceKeyData(data);
      const treeDataList = constructMenuTreeData(datas);
      return {
        ...state,
        treeData: treeDataList,
      };
    },
    addDepartment(state, action) {
      const { detailSource } = state;
      detailSource.submittedSectionId = action.payload.submittedSectionId;
      return {
        ...state,
        detailSource,
      };
    },
    updateForm(state) {
      return {
        ...state,
        detailSource: {},
      };
    },
  },
};
