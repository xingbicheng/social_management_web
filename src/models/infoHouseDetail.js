import { message } from 'antd'
import { routerRedux } from 'dva/router'
import { get, edit, editPerson, add, addPerson, delPerson, getIds } from '../services/infoHouse'
import { dictList } from '../services/systemSet'
import { getDepartment } from '../services/infoDepartment'
import { getArea } from '../services/infoArea'
import { getGrid } from '../services/infoGrid'
import { personBasegetByIdCard, judgePersonHaveHouse } from '../services/personBase'

export default {
    namespace: 'infoHouseDetail',
    state: {
       house: {}, 
       selectedRowKeys: [],
       department: {},
       area: {},
       grid: {},
       houseType: [],
       mershId : '',
       persons: [],
       type: '',
       ModalType: 'create',
       ModalVisable: false,
       personBase: {},
       isFindPerson: 'noFind',
       isLocal: 'noFind',
       saveLoading: false,
       searchLoading: false,
       idCard: '',
    },
    subscriptions: {
        setup ({dispatch, history}) {
            history.listen((location) => {
                // const matchEdit = pathToRegexp('/gridInfoManage/houseEdit/:id').exec(location.pathname)
                // const matchAdd = pathToRegexp('/gridInfoManage/houseAdd/:id').exec(location.pathname)
                // if (matchEdit) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'initBasic',
                //     })
                //     dispatch({
                //         type: 'get',
                //         payload: {
                //             id: matchEdit[1],
                //         },
                //     })
                //     dispatch({
                //         type: 'getFathersByhouseId',
                //         payload: {
                //             id: matchEdit[1],
                //         },
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             type: 'edit',
                //         },
                //     })
                // }else if (matchAdd) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'initBasic',
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             mershId: matchAdd[1],
                //             type: 'add',
                //         },
                //     })
                //     dispatch({
                //         type: 'getFathersByMershId',
                //         payload: {
                //             id: matchAdd[1]
                //         },
                //     })
                // }
            })
        },
    },
    effects: {
        * get ({ payload }, { call, put }) {
            const response = yield call(get, payload)
            if (response.code === 0) {
                const persons = response.data.persons
                for (let i = 0; i < persons.length; i++ ) {
                    persons[i] = {...persons[i], ...persons[i].baseInfo, id: persons[i].id}
                }
                yield put({
                    type: 'updateState',
                    payload: {
                        house: response.data,
                        persons,
                        mershId: response.data.mershId,
                    },
                })
            } else {
                message.error('获取数据失败')
            }
        },
        * initBasic ({ payload }, { call, put }) {
            const response1 = yield call(dictList,{ type: 'house_type' })
            const response2 = yield call(dictList,{ type: 'political_outlook' })
            const response3 = yield call(dictList,{ type: 'degree_education' })
            const response4 = yield call(dictList,{ type: 'household_register_nature' })
            const response5 = yield call(dictList,{ type: 'marital_status' })
            if (response1.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        houseType: response1.data.data,
                    },
                })
            }
            if (response2.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        political_outlook: response2.data.data,
                    },
                })
            }
            if (response3.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        degree_education: response3.data.data,
                    },
                })
            }
            if (response4.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        household_register_nature: response4.data.data,
                    },
                })
            }
            if (response5.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        maritalStatus: response5.data.data,
                    },
                })
            }
        },
        * getFathersByhouseId ({payload}, {call, put}) {
            const response2 = yield call(getIds, payload)
            if(response2.code === 0) {
                const response3 = yield call(getDepartment, { id: response2.data.departmentId })
                const response4 = yield call(getArea, { id: response2.data.districtId })
                const response5 = yield call(getGrid, { id: response2.data.mershId })
                if ( response3.code === 0 ) {
                    yield put({
                        type: 'updateState',
                        payload: {
                            department: response3.data,
                        },
                    })
                }
                if ( response4.code === 0 ) {
                    yield put({
                        type: 'updateState',
                        payload: {
                            area: response4.data,
                        },
                    })
                }
                if ( response5.code === 0 ) {
                    yield put({
                        type: 'updateState',
                        payload: {
                            grid: response5.data,
                        },
                    })
                }
            }
        },
        * getFathersByMershId ({ payload }, { call, put }) {
            const respone1 =  yield call(getGrid, payload)
            if (respone1.code === 0) {
                const response2 = yield call(getArea, { id: respone1.data.districtId })
                if (response2.code === 0 ) {
                    const respone3 = yield call(getDepartment, { id: response2.data.departmentId })
                    if (respone3.code === 0 ) {
                        yield put({
                            type: 'updateState',
                            payload: {
                                department: respone3.data,
                                area: response2.data,
                                grid: respone1.data,
                            },
                        })
                    }
                }
            }
        },
        * save ({ payload }, { call, put, select }) {
            const { mershId } = yield select(_=>_.infoHouseDetail)
            const response = yield call (add, payload)
            if (response.code === 0) {
                message.success('添加成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put(routerRedux.push({
                  pathname:  `/gridInfoManage/house/${mershId}`,
                }))
            } else {
                message.error('添加失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * edit ({ payload }, { call, put, select }) {
            const { mershId } = yield select(_=>_.infoHouseDetail)
            const response = yield call (edit, payload)
            if (response.code === 0) {
                message.success('编辑成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put(routerRedux.push({
                  pathname:  `/gridInfoManage/house/${mershId}`,
                }))
            } else {
                message.error('编辑失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * addPerson ({ payload }, { call, put }) {
            const response = yield call (addPerson, payload)
            if (response.code === 0) {
                message.success('添加成功')
            } else {
                message.error('添加失败')
            }
        },
        * editPerson ({ payload }, { call, put }) {
            const response = yield call (editPerson, payload)
            if (response.code === 0) {
                message.success('编辑成功')
            } else {
                message.error('编辑失败')
            }
        },
        * delPerson ({ payload }, { call, put, select }) {
            const response = yield call (delPerson, payload)
            const { house } = yield select(_=>_.infoHouseDetail)
            if (response.code === 0) {
                message.success('删除成功')
                yield put({
                    type: 'get',
                    payload: {
                        id: house.id,
                    },
                })
            } else {
                message.error('删除失败')
            }
        },  
        * judgePersonHaveHouse({payload}, { call, put }) {
          const respone = yield call(judgePersonHaveHouse, payload)
          if (respone.code === 0) {
            if (respone.data === 0) {
              yield put({
                type: 'searchPerson',
                payload,
              })
            } else if (respone.data === 1) {
              message.info('该人员已经录入房屋信息，请输入另外的身份证号')
            }
          }
        },
        * searchPerson ({ payload }, { call, put }) {
            const response = yield call(personBasegetByIdCard, payload) 
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personBase: response.data,
                        isFindPerson: 'Find',
                    },
                })
            } else if (response.code === 1) {
                yield put({
                    type: 'updateState',
                    payload: {
                        personBase: response.data,
                        isFindPerson: 'notFind',
                    },
                })
            } else{
                message.error(response.code);
            }
        },
    },
    reducers: {
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                house: {}, 
                selectedRowKeys: [],
                department: {},
                area: {},
                grid: {},
                houseType: [],
                mershId : '',
                persons: [],
                type: '',
                ModalType: 'create',
                ModalVisable: false,
                personBase: {},
                isFindPerson: 'noFind',
                isLocal: 'noFind',
                saveLoading: false,
                searchLoading: false,
                idCard: '',
            }
        },
    },
}