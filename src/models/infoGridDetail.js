import { message } from 'antd'
import { getGrid, edit, editTeam, add, addTeam, delTeam} from '../services/infoGrid'
import { getArea } from '../services/infoArea'
import { router } from 'sw-toolbox';
import { routerRedux } from 'dva/router';

export default {
    namespace: 'infoGridDetail',
    state: {
       gridDetail: {}, 
       selectedRowKeys: [],
       area: {},
       currentItem: {},
       districtId: '',
       modalVisable: false,
       modalType: 'create',
       serviceTeams: [],
       type: '',
       saveLoading: false,
       modalLoading: false,
    },
    subscriptions: {
        setup ({dispatch, history}) {
            history.listen((location) => {
                // const matchEdit = pathToRegexp('/gridInfoManage/gridEdit/:id').exec(location.pathname)
                // const matchAdd = pathToRegexp('/gridInfoManage/gridAdd/:id').exec(location.pathname)
                // if (matchEdit) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'getGrid',
                //         payload: {
                //             id: matchEdit[1],
                //         }
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             type: 'edit',
                //         }
                //     })
                // }else if (matchAdd) {
                //     dispatch({
                //         type: 'clearState',
                //     })
                //     dispatch({
                //         type: 'updateState',
                //         payload: {
                //             districtId: matchAdd[1],
                //             type: 'add',
                //         },
                //     })
                //     dispatch({
                //         type: 'initBasic',
                //         payload: {
                //             id: matchAdd[1],
                //         },
                //     })
                // }
            })
        },
    },
    effects: {
        * getGrid ({ payload }, { call, put }) {
            const response = yield call(getGrid, payload)
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        gridDetail: response.data,
                        serviceTeams: response.data.serviceTeams,
                        districtId: response.data.districtId,
                    },
                })
                const response1 = yield call(getArea, { id: response.data.districtId})
                if (response1.code === 0) {
                    yield put({
                        type: 'updateState',
                        payload: {
                            area: response1.data,
                        }
                    })
                } else {
                    throw '获取片区失败'
                }
            }else{
                message.error('获取数据失败')
            }
        },
        * save ({ payload }, { call, put }) {
             const {districtId} = payload;
            const response = yield call (add, payload)
            if (response.code === 0) {
                message.success('添加成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put (routerRedux.push(`/gridInfoManage/grid/${districtId}`));
            } else {
                message.error('添加失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * edit ({ payload }, { call, put }) {
            const {districtId} = payload;
            const response = yield call (edit, payload)
            if (response.code === 0) {
                message.success('编辑成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put (routerRedux.push(`/gridInfoManage/grid/${districtId}`));
            } else {
                message.error('编辑失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * initBasic ({ payload }, { call, put }) {
            const response = yield call(getArea, payload)
            if (response.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        area: response.data,
                    },
                })
            } else {
                throw '获取片区失败'
            }
        },
        * addTeam ({ payload }, { call, put, select }) {
            const response = yield call (addTeam, payload)
            const { gridDetail } = yield select(_=>_.infoGridDetail)
            if (response.code === 0) {
                message.success('添加成功')
                yield put({
                    type: 'getGrid',
                    payload: {
                        id: gridDetail.id,
                    },
                })
            } else {
                message.error('添加失败')
            }
        },
        * editTeam ({ payload }, { call, put, select }) {
            const response = yield call (editTeam, payload)
            const { gridDetail } = yield select(_=>_.infoGridDetail)
            if (response.code === 0) {
                message.success('编辑成功')
                yield put({
                    type: 'getGrid',
                    payload: {
                        id: gridDetail.id,
                    },
                })
            } else {
                message.error('编辑失败')
            }
        },
        * delTeam ({ payload }, { call, put, select }) {
            const response = yield call (delTeam, payload)
            const { gridDetail } = yield select(_=>_.infoGridDetail)
            if (response.code === 0) {
                message.success('删除成功')
                yield put({
                    type: 'getGrid',
                    payload: {
                        id: gridDetail.id,
                    },
                })
            } else {
                message.error('删除失败')
            }
        },
    },
    reducers: {
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                gridDetail: {}, 
                selectedRowKeys: [],
                area: {},
                currentItem: {},
                districtId: '',
                modalVisable: false,
                modalType: 'create',
                serviceTeams: [],
                saveLoading: false,
                modalLoading: false,
            }
        },
    }
}