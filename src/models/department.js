import {
  departmentAdd,
  departmentDel,
  departmentEdit,
  departmentGet,
  departmentList,
  departmentListAll,
  exportExcel,
  exportExcelAll,
  departmentDisabled,
} from '../services/department';
import { message } from 'antd';
import { arrayToTree } from '../utils/utils';
import { dictList } from '../services/systemSet';
import { POINT_CONVERSION_COMPRESSED } from 'constants';

export default {
  namespace: 'department',

  state: {
    dataSource: [],
    defaultData: {},
    currentItem: {
      createUserId: 0,
      id: 0,
      superiorDepartmentId: 0,
      updateUserId: 0,
    },// 当前选择数据
    currentDepartment: [],
    pagination: {
      pageSize: 10, // 一页多少条
      current: 1,  // 当前页
    },
    searchInfo: {}, // 检索Form表单
    selectedRowKeys: [],
    multiDeleteButtonIsVisable: false,
    treeData: [],
    department_info_status: [],
    treeSelectedKeys: [],
  },
  effects: {
    * get({ payload }, { call, put }) {
      const response = yield call(departmentGet, payload);
      yield put({
        type: 'getData',
        payload: response.data,
      });
    },
    * create({ payload }, { call, put }) {
      const response = yield call(departmentAdd, payload);
      if (response.code === 0) {
        yield put({
          type: 'ListAll',
        });
        message.success('新增成功');
      } else {
        message.error(response.msg);
      }
    },
    * update({ payload }, { call, put }) {
      const response = yield call(departmentEdit, payload);
      if (response.code === 0) {
        yield put({
          type: 'ListAll',
        });
        message.success('编辑成功');
      }
      else {
        message.error(response.msg);
      }
    },

    * ListAll({ payload = {} }, { call, put }) {
      const response = yield call(departmentListAll, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
      yield put({
        type: 'initBasicData',
      });
    },
    * getList({ payload }, { call, put }) {
      const response = yield call(departmentList, payload);
      yield put({
        type: 'searchSave',
        payload: response.data,
      });
    },
    * delete({ payload }, { call, put }) {
      const response = yield call(departmentDel, payload);
      if (response.code === 0) {
        yield put({
          type: 'save',
          payload: response.data,
        });
        yield put({
          type: 'ListAll',
        });
        message.success('删除成功');
      }
      else if(response.code === -1)
      {
        message.error("删除失败,因为存在下一级部门所以不能删除");
      }
      else if(response.code === 500)
      {
        message.error("此部门因已录入网格信息,不能删除")
      }
      else {
        message.error('删除失败');
      }
    },
    * initBasicData({ payload }, { call, put }) {
      const response = yield call(dictList, { type: 'department_info_status' });
      yield put({
        type: 'updateState',
        payload: {
          department_info_status: response.data.data,
        },
      });
    },
  },
  reducers: {
    updateState(state, { payload }) {

      return {
        ...state,
        ...payload,
      };
    },
    save(state, { payload }) {
      const treeData = arrayToTree(payload, 'id', 'superiorDepartmentId', 'children');
      return {
        ...state,
        dataSource: payload,
        treeData,
        currentDepartment: treeData,
      };
    },
    searchSave(state,{payload}){
      return{
        ...state,
        currentDepartment: payload.data,
      }
    },
    hiddenModal(state) {
      return {
        ...state,
        visible: false,
        currentItem: {
          createUserId: 0,
          id: 0,
          superiorDepartmentId: 0,
          updateUserId: 0,
        },
      };
    },
    clearSearchInfo(state) {
      return {
        ...state,
        searchInfo: {},
      };
    },
    setSearchInfo(state, action) {
      return {
        ...state,
        searchInfo: { ...action.payload },
      };
    },
    updateCurrentItem(state, payload) {
      console.log(payload.payload)
      return {
        ...state,
        currentItem: payload.payload,
      };
    },
    getData(state, action) {
      return {
        ...state,
        defaultData: action.payload,
      };
    },
  },
};
