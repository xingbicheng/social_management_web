import { listAllRoles,getAllRoles,delData,set,addRoles,editRole} from '../services/roleController';
import { menuTree,getDataById } from '../services/Permission';
import { constructMenuTreeData1,replaceKeyData1 } from '../utils/utils';
import { message } from 'antd';

export default {
    namespace: 'rolecontroller',

    state:{
        list: [],
        loading: false,
        modalVisible:false,
        permissionModalVisible:false,
        roleMenuTreeData:[],
        dataSource:[],
        currentUser: {},
        pagination : {
            pageSize : 10, // 一页多少条
            current :1,  // 当前页
            total:0,  // 总条数
            pages:0,  // 一共多少页
          },
        menuTreeData:[],
        currentRow:{
            createTime:36,
            name:0,
            updateTime:0,
            id:1,
        }, 
    },
    effects: {
        *getAllList ({ payload = { } }, { call, put }) {
            const response = yield call(listAllRoles, payload);
            if(response.code === 0){
                yield put({
                    type:'listAll',
                    payload:response.data,
                });
            } else {
                message.error(response.msg);
            }
        },
        *addNewRole ({ payload = { } }, { call, put }) {
            const response = yield call(addRoles, payload);
            if(response.code === 0){
                yield put({
                    type:'getAllList',
                    payload:{
                        page:1,
                        pageSize:10,
                    }
                });
                yield put ({
                    type:'hiddenModal',
                })
                message.success('添加成功');
            } else {
                message.error(response.msg);
            }
        },
        *toDelRole({payload={}}, {call,put }) {
            const response = yield call(delData, payload);
            if(response.code === 0){
                yield put({
                    type:'getAllList',
                    payload:{
                        page:1,
                        pageSize:10,
                    }
                });
            } else {
                message.error(response.msg);
            }
        },
        *getMenuTree({payload={}},{call,put}){
            const response = yield call(menuTree, payload);
            if(response.code === 0){
                yield put({
                    type:'saveMenuTree',
                    payload:response
                });
            }
        },
        *getRoleMenuTree({payload={}},{call,put}){
            const response = yield call(getDataById, payload);
            if(response.code === 0){
                yield put({
                    type:'saveRoleMenuTree',
                    payload:response
                });
            }
        },
        *editRoles({ payload={} },{ call,put }){
            let page=payload.pageNum;
            const response = yield call(editRole, payload);
            if(response.code === 0){
                yield put({
                    type: 'getAllList',
                    payload:{
                      page,
                      pageSize:10,
                    },
                });
                yield put({
                    type: 'hiddenModal',
                    payload:response,
                  });
                message.success('修改成功');
            } else {
                message.error(response.msg);
            }
        },
        *delRoleData ({ payload = { } }, { call, put }) {
            const response = yield call(delData, payload);
            if(response.code === 0){
                yield put({
                    type:'listAll',
                    payload:response.data,
                });
                message.success('删除成功');
            } else {
                message.error(response.msg);
            }
        },
        *setPermission ({ payload = { } }, { call, put }) {
            const response = yield call(set, payload);
            if(response.code === 0){
                yield put({
                    type:'getAllList',
                });
            } else {
                message.error(response.msg);
            }
        },
        *getAllRole ({ payload = { } }, { call, put }) {
            const response = yield call(getAllRoles, payload);
            if(response.code === 0){
                yield put({
                    type:'listAll',
                    payload:response.data,
                });
            } else {
                message.error(response.msg);
            }
        },
    },
  
    reducers: {
        listAll(state,action){
            const { data,pages,pageSize,total,pageNum } = action.payload;
            return {
            ...state,
            dataSource:data,
            pagination:{pages,pageSize,total,current:pageNum},
            };
        },
        saveMenuTree(state,action){
            let { data } =action.payload;
            let data1 = replaceKeyData1(data);
            let treeData = constructMenuTreeData1(data1);
            return{
                ...state,
                menuTreeData:treeData,
            }
        },
        saveRoleMenuTree(state,action){
            let { data } =action.payload;
            let arrdata =[];
            for(let i in data){
                let a =String(data[i].id);
                arrdata.push(a);
            }
            return{
                ...state,
                roleMenuTreeData:arrdata,
            }
        },
        showModal(state, action){
            return{
                ...state,
                modalVisible:true,
                currentRow:{...action.payload},
            }
        },
        showPermissionModal(state, action){
            return{
                ...state,
                permissionModalVisible:true,
                currentRow:{...action.payload},
            }
        },
        hiddenModal(state){
            return {
              ...state,
              modalVisible:false,
              currentRow:{
                createTime:36,
                name:0,
                updateTime:0,
                id:1,
            }, 
            }
        },
        hiddenPermissionModal(state, action){
            return{
                ...state,
                permissionModalVisible:false,
                // currentRow:{
                //     createTime:36,
                //     name:0,
                //     updateTime:0,
                //     id:1,
                // }, 
            }
        },
        updateState (state, { payload }) {
            return {
                ...state,
                roleMenuTreeData:payload.roleMenuTreeData,
            }
        }
    },
};
