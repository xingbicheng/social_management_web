import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { InoticeList, InoticeDel, InoticeAdd, InoticeGet, InoticeEdit, InoticeDelAll } from '../services/infoPlatform';
import { dictList } from '../services/systemSet';

export default {
  namespace: 'infoPlatform',

  state: {
    hasReceived: false,
    handleTitle: '',
    dataSource: [],
    detailSource: {},
    currentRow: {
      articleId: 0,
      id: 0,
      sort: 0,
    },
    pagination: {
      pageSize: 10,
      current: 1,
      total: 1,
      page: 1,
    },
    searchInfo: {},
    infor_notice_statues:[],
    infor_notice_type:[],
  },

  effects: {
    * NoticePage({ payload }, { call, put }) {
      const response = yield call(InoticeList, payload);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    * add({ payload }, { call, put }) {
      const response = yield call(InoticeAdd, payload);
      if (response.code === 0) {
        message.success('新增成功');
        yield put(routerRedux.push('/infoPlatform/notice'));
      } else {
        message.error('新增失败');
      }
    },
    * delete({ payload }, { call, put }) {
      const response = yield call(InoticeDel, payload);
      if (response.code === 0) {
        yield put({
          type: 'NoticePage',
          payload: {
            ...response.data,
          },
        });
        message.success('删除成功');
      } else {
        message.error('删除失败');
      }
    },
    * deleteAll({ payload }, { call, put }) {
      const response = yield call(InoticeDelAll, payload);
      if (response.code === 0) {
        yield put({
          type: 'NoticePage',
          payload: {
            page: 1,
            pageSize: 10,
          },
        });
        message.success('删除成功');
      } else {
        message.error('删除失败');
      }
    },
    * NoticeDetail({ payload }, { call, put }) {
      const response = yield call(InoticeGet, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
      }
    },
    * update({ payload }, { call, put }) {
      const response = yield call(InoticeEdit, payload);
      if (response.code === 0) {
        yield put({
          type: 'saveDetail',
          payload: response.data,
        });
        yield put(routerRedux.push('/infoPlatform/notice'));
        message.success('操作成功');
      } else {
        message.error('操作失败');
      }
    },
    * getNoticeCategory ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'infor_notice_type'})
      yield put({
        type: 'updateState',
        payload: {
          infor_notice_type: response.data.data,
        },
      })
    },
    * getNoticeStatues ({ payload }, { call, put }) {
      const response = yield call(dictList,{ type: 'infor_notice_statues'})
      yield put({
        type: 'updateState',
        payload: {
          infor_notice_statues: response.data.data,
        },
      })
    },
  },

  reducers: {
    save(state, action) {
      const { data, page, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataSource: data,
        pagination: { page, pageSize, total, current: pageNum },
        currentRow: { ...action.payload },
      };
    },
    saveDetail(states, action) {
      const detail = { ...action.payload };
      return {
        ...states,
        detailSource: detail,
      };
    },
    updateTitle(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    clearAll(state) {
      return {
        ...state,
        searchInfo: {},
      };
    },
    setSearchInfo(state, action) {
      return {
        ...state,
        searchInfo: { ...action.payload },
      };
    },
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    updateForm(state) {
      return {
        ...state,
        detailSource: {},
      };
    },
  },
};
