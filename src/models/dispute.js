import {
  addInfo,
  deleteInfo,
  disputeList,
  editInfo,
  getType,
  handler,
  getById,
  addListParty,
  editParty,
  deleteParty,
  getByAcceptId,
  uploadfile,
  deleteFile, deleteAllPart, download, deleteAllInfo,
  getFile,
} from '../services/dispute';
import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { departmentListAll } from '../services/department';
import { arrayToTree } from '../utils/utils';
import { acceptGet, assignAdd, typeList } from '../services/citizenAPI';

export default {
  namespace:'dispute',

  state:{
    dataList:[],  //  主界面列表数据

    allData:{},  //  受理时候的所有数据
    typeData:[], //  选择类型

    pageData:{
      listParty:[],
    },  //  pageForm的数据

    fileList:[],
    handleOverData:{
      handlerProcess:'',
      handlerType:'',
      oralAgreement:'',
      fileUrl:'',
      path:'',
    },

    pagination : {
      pageSize : 10, // 一页多少条
      pageNum: 1,
      current :0,  // 当前页
      total:0,  // 总条数
      pages:0,  // 一共多少页
    },

  },
  effects:{
    *list({payload}, { call, put }) {
      const requstData = payload;
      requstData.acceptType = '0';
      const response = yield call(disputeList, requstData);
      yield put({
        type:"save",
        payload:response.data,
      })
    },
    *add({payload},{ call, put, select}){
      const data=yield select(_=>_.dispute);
      yield put({
        type:'addAllData',
        payload,
      });
      let response;
      if(data.allData.id){
        response = yield call(editInfo, data.allData);
      }
      else {
        response =  yield call(addInfo, data.allData);
      }
      if(response.success){
        const { pagination } = data;
        pagination.acceptType = '0';
        const dataList = yield call(disputeList, pagination);
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('提交成功');
        yield put(routerRedux.push('/information/dispute'));
      }
      else if(response.code === 222)
      {
        message.error('受理期限必须必须是将来的日期,请重新选择受理期限')
      }
      else{
        message.error('提交失败');
      }
    },
    *deleteInfo({payload},{call, put, select}){
      const response = yield call(deleteInfo, payload);
      const data=yield select(_=>_.dispute);
      if(response.success){
        const { pagination } = data;
        pagination.acceptType = '0';
        const dataList = yield call(disputeList, pagination);
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success("删除成功");
      }else{
        message.error("删除失败");
      }
    },
    *deleteAllInfo({payload},{call,put,select}){
      const response = yield call(deleteAllInfo,payload);
      const data=yield select(_=>_.dispute);
      if(response.success){
        const { pagination } = data;
        pagination.acceptType = '0';
        const dataList = yield call(disputeList, pagination);
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('删除成功');
      }else{
        message.error('删除失败');
      }
    },
    *getType({payload},{call,put}){
      const response = yield call(getType, payload);
      yield put({
        type: 'saveMatterType',
        payload:response.data,
      });
    },
    *handler({payload},{call,put,select}){
      const data=yield select(_=>_.dispute);
      const {data:upData} = payload;
      const updata = {
        id:upData.id,
      };
      const idResponse = yield call(getByAcceptId,updata);
      if(idResponse.msg === 'OK')
        upData.id = idResponse.data[0].id;
      else{
        message.error('办结失败');
        return;
      }
      const response = yield call(handler,upData);
      if(response.success){
        const { pagination } = data;
        pagination.acceptType = '0';
        const dataList = yield call(disputeList, pagination)
        yield put({
          type:"save",
          payload:dataList.data,
        })
        message.success('办结成功');
      }else{
        message.error('办结失败');
      }
    },

    //  数据查询
    *typePage({ payload }, { call, put }) {
      const response = yield call(typeList, payload);
      yield put({
        type: 'matterType',
        payload:response.data,
      });
    },

    *getPageData({payload},{call, put}){
      if(payload.id === '-1'){
        yield put({
          type:'setPageData',
          payload:{},
        });
        return;
      }
      const response = yield call(getById,payload);
      if(response.success){
        yield put({
          type:'setPageData',
          payload:response.data,
        });
      }
    },
    *deleteFile({payload},{call, put}){
      const response = yield call(deleteFile,payload);
      if(response.success){
        message.success('删除成功');
      }
    },
    *addParty({payload},{call,put}){
      const { acceptId } = payload;
      const response = yield call(addListParty,payload);
      if(response.success){
        yield put({
          type:'getPageData',
          payload:{id:acceptId},
        });
        message.success('新增成功');
      }
      else message.error('新增失败');
    },
    *edit({payload},{call,put}){
      const { acceptId } = payload;
      const response = yield call(editParty,payload);
      if(response.success){
        yield put({
          type:'getPageData',
          payload:{id:acceptId},
        });
        message.success('修改成功');
      }
      else message.error('新增失败');
    },
    *deleteParty({payload},{call,put}){
      const { acceptId } = payload;
      const response = yield call(deleteParty,payload);
      if(response.msg === 'OK') {
        yield put({
          type:'getPageData',
          payload:{id:acceptId},
        });
        message.success('删除成功');
      }
      else message.error('删除失败');
    },
    *deleteAllParty({payload},{call,put}){
      const response = yield call(deleteAllPart,payload);
      if(response.success){
        message.success('删除成功');
      }else{
        message.error('删除失败');
      }
    },
    *getConcluded({payload},{call,put}){
      const {data} = payload;
      const updata = {
        id:data.id,
      };
      const response = yield call(getByAcceptId,updata);
      let codata = {};
      response.data.forEach(i=>{
        if(i.oralAgreement||i.writtenId){
          codata = i;
        }
      })
      if(!codata.writtenId){
        codata.writtenId = 0;
      }else{
        const filerespone = yield call(getFile,{id:codata.writtenId});
        if(filerespone.success){
          codata.path = filerespone.data.path;
        }
      }
      yield put({
        type:'saveConcluded',
        payload:{
          handleOverData:codata,
        },
      });
    },
  },
  reducers: {
    saveConcluded(state, action){
      const { handleOverData } = action.payload;
      const {handlerType, oralAgreement, handlerProcess, writtenId, path} = handleOverData;
      return{
        ...state,
        handleOverData:{handlerType, oralAgreement, handlerProcess, writtenId, path},
      }
    },

    save(state, action) {
      const { pages, pageSize, total, pageNum } = action.payload;
      return {
        ...state,
        dataList: action.payload,
        pagination: { pages, pageSize, total, current: pageNum },
      }
    },

    addAllData(state, action) {
      const { allData, pageData } = state;
      const { partyData, moreData } = action.payload;
      allData.listParty = partyData;
      let x;
      for (x in moreData) {
        allData[x] = moreData[x];
      }
      if (pageData.id) {
        allData.id = state.pageData.id;
      }
      allData.acceptType = '0';
      allData.departmentId = 1;
      return {
        ...state,
        allData,
      }
    },

    saveMatterType(state, action) {
      return {
        ...state,
        typeData: action.payload.data,
      };
    },

    setPageData(state, action) {
      return {
        ...state,
        pageData: action.payload,
      }
    },
  },
};
