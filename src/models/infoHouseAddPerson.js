import { message } from 'antd'
import { routerRedux } from 'dva/router'
import { get, editPerson, addPerson } from '../services/infoHouse'
import { dictList } from '../services/systemSet'
import { findRecord } from '../utils/utils'
import { personBasegetByIdCard } from '../services/personBase'

export default {
    namespace: 'infoHouseAddPerson',
    state: {
       currentItem: {},
       houseType: [],
       political_outlook: [],
       degree_education: [],
       household_register_nature: [],
       maritalStatus : [],
       houseId: '',
       itemId: '',
       type: '',
       saveLoading: false,
       idCard: '',
    },
    effects: {
        * getCurrent ({ payload }, { call, put }) {
            const response = yield call(get, payload)
            if (response.code === 0) {
                const currentItem = findRecord(response.data.persons, 'id', payload.itemId)
                yield put({
                    type: 'updateState',
                    payload: {
                        currentItem,
                    },
                })
            } else {
                message.error('获取数据失败')
            }
        },
        * searchPerson ({ payload }, { call, put }) {
            const response = yield call(personBasegetByIdCard, payload) 
            const baseInfoFieldsName = ['name', 'idCard','gender', 'nation', 'householdRegisterPlace', 'politicalOutlook', 'degreeEducation', 'householdRegisterNature', 'phone', 'maritalStatus', 'isDie', 'birthday']
            const GridinfoFieldsName = ['isPsychiatricPatients', 'isEsettlementEducation', 'isCommunityDetoxification', 'isCommunityRehabilitation', 'isKeyPetitions', 'isChildren']
            let currentItem = { baseInfo: {} }
            if (response.code === 0) {
                for( let i = 0; i < baseInfoFieldsName.length; i++ ) {
                    currentItem.baseInfo[baseInfoFieldsName[i]] = response.data[baseInfoFieldsName[i]]
                }
                for( let i = 0; i < GridinfoFieldsName.length; i++ ) {
                    currentItem[GridinfoFieldsName[i]] = response.data[GridinfoFieldsName[i]]
                }
                yield put({
                    type: 'updateState',
                    payload: {
                        currentItem,
                    },
                })
            } else if (response.code === 1) {
                response.data = {}
                for( let i = 0; i < baseInfoFieldsName.length; i++ ) {
                    currentItem.baseInfo[baseInfoFieldsName[i]] = response.data[baseInfoFieldsName[i]]
                }
                for( let i = 0; i < GridinfoFieldsName.length; i++ ) {
                    currentItem[GridinfoFieldsName[i]] = response.data[GridinfoFieldsName[i]]
                }
                yield put({
                    type: 'updateState',
                    payload: {
                        currentItem,
                    }
                })
            }
        },
        * initBasic ({ payload }, { call, put }) {
            const response1 = yield call(dictList,{ type: 'house_type' })
            const response2 = yield call(dictList,{ type: 'political_outlook' })
            const response3 = yield call(dictList,{ type: 'degree_education' })
            const response4 = yield call(dictList,{ type: 'household_register_nature' })
            const response5 = yield call(dictList,{ type: 'marital_status' })
            if (response1.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        houseType: response1.data.data,
                    },
                })
            }
            if (response2.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        political_outlook: response2.data.data,
                    },
                })
            }
            if (response3.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        degree_education: response3.data.data,
                    },
                })
            }
            if (response4.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        household_register_nature: response4.data.data,
                    },
                })
            }
            if (response5.code === 0) {
                yield put({
                    type: 'updateState',
                    payload: {
                        maritalStatus: response5.data.data,
                    },
                })
            }
        },
        * addPerson ({ payload }, { call, put, select }) {
          const { houseId } = yield select(_=>_.infoHouseAddPerson)
            const response = yield call (addPerson, payload)
            if (response.code === 0) {
                message.success('添加成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put(routerRedux.push({
                  pathname: `/gridInfoManage/houseEdit/${houseId}`,
                }))
            } else {
                message.error('添加失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
        * editPerson ({ payload }, { call, put, select }) {
            const { houseId } = yield select(_=>_.infoHouseAddPerson)
            const response = yield call (editPerson, payload)
            if (response.code === 0) {
                message.success('编辑成功')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
                yield put(routerRedux.push({
                  pathname: `/gridInfoManage/houseEdit/${houseId}`,
                }))
            } else {
                message.error('编辑失败')
                yield put({
                    type: 'updateState',
                    payload: {
                        saveLoading: false,
                    },
                })
            }
        },
    },
    reducers: {
        updateState (state, { payload }) {
            return {
                ...state,
                ...payload,
            }
        },
        clearState (state, { payload }) {
            return {
                ...state,
                house: {}, 
                selectedRowKeys: [],
                department: {},
                area: {},
                grid: {},
                currentItem: {},
                houseType: [],
                modalVisable: false,
                pageType: 'create',
                mershId : '',
                persons: [],
                saveLoading: false,
            }
        },
    }
}