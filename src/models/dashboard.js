import { message } from 'antd';
import {acceptList, disputeList, houseList} from '../services/dashboard';
import { inoticeList } from '../services/infor';
import { IsubmitList } from '../services/infoPlatform';
import { aceeptInfo } from '../utils/seletLocalData';
import { formmaterData } from '../utils/utils';

export default {
    namespace: 'dashboard',
    state: {
      inoticelist:[],
      isubmittedlist:[],
      acceptlist:[],
      disputelist:[],
      houselist:[],
    },

    effects: {
        *getMantlist({ payload={} },{put,call}){
            const { code,msg,data } = yield call (acceptList, payload);
            if(code === 0){
                const newArr = [];
                data.forEach((item,index)=>{
                    newArr[index] = {
                        y:0,
                        x:0,
                    }
                    newArr[index].y = parseInt(item.personCount);
                    newArr[index].x = item.personType;
                });
                yield put({
                    type: 'saveMantList',
                    payload: newArr,
                });
            }else{
                message.error(msg);
            }
        },
        *getDisputeList({ payload={} },{put,call}){
            const { code,msg,data } = yield call (disputeList, payload);
            if(code === 0){
                const newArr = [];
                data.forEach((item,index)=>{
                    newArr[index] = {
                        y:0,
                        x:0,
                    }
                    newArr[index].y = parseInt(item.total);
                    newArr[index].x = formmaterData('aceeptInfo',item.acceptType);
                });
                yield put({
                    type: 'saveDisputeList',
                    payload: newArr,
                });
            }else{
                message.error(msg);
            }
        },
        *getInoticeList({ payload={} },{put,call}){
            const { code,msg,data } = yield call (inoticeList, payload);
            if(code === 0){
                yield put({
                    type: 'saveInoticeList',
                    payload: data.data,
                });
            }else{
                message.error(msg);
            }
        },
        *getIsubmittedList({ payload={} },{put,call}){
            const { code,msg,data } = yield call (IsubmitList, payload);
            if(code === 0){
                yield put({
                    type: 'saveIsubmittedList',
                    payload: data.data,
                });
            }else{
                message.error(msg);
            }
        },
        *getHouseList({ payload={} },{put,call}){
            const { code,msg,data } = yield call (houseList, payload);
            if(code === 0){
                const newArr = [];
                data.forEach((item,index)=>{
                    newArr[index] = {
                        y:0,
                        x:0,
                    }
                    newArr[index].y = parseInt(item.personTotal);
                    newArr[index].x = item.name;
                });
                yield put({
                    type: 'saveHouseList',
                    payload:newArr,
                });
            }else{
                message.error(msg);
            }
        },
    },

    reducers: {
      saveMantList(state, action){
          return {
              ...state,
              acceptlist: action.payload,
          };
      },
      saveDisputeList(state, action){
          return {
            ...state,
            disputelist: action.payload,
          };
      },
      saveInoticeList(state, action){
          return {
              ...state,
              inoticelist: action.payload,
          };
      },
      saveIsubmittedList(state, action){
          return {
              ...state,
              isubmittedlist: action.payload,
          };
      },
      saveHouseList(state, action){
          return {
              ...state,
              houselist: action.payload,
          };
      },
    },
};
