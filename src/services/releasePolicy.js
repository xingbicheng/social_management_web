import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function policyList(params) {
    return request(createTheURL(Config.API.POLICY, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function policyAdd(params) {
    return request(createTheURL(Config.API.POLICY, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function policyDisabled(params) {
    return request(createTheURL(Config.API.POLICY, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function policyDel(params) {
    return request(createTheURL(Config.API.POLICY, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function policyDelAll(params) {
    return request(createTheURL(Config.API.POLICY, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function policyEdit(params) {
    return request(createTheURL(Config.API.POLICY, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function policyGet(params) {
    return request(createTheURL(Config.API.POLICY, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.POLICY, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.POLICY, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

