import request from '../utils/request/request'

export async function query(code) {
  return request(`/api/${code}`);
}
