import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function addUser(params) {
    return request(createTheURL(Config.API.USER, 'addRolesUser'), {
        method: 'POST',
        body: params,
    });
}

export async function allbrif(params) {
    return request(createTheURL(Config.API.USER, 'allbrif'), {
        method: 'POST',
        body: params,
    });
}

export async function current(params) {
    return request(createTheURL(Config.API.USER, 'current'), {
        method: 'GET',
        body: params,
    });
}

export async function editUser(params) {
    return request(createTheURL(Config.API.USER, 'editUserRoles'), {
        method: 'POST',
        body: params,
    });
}
export async function excel(params) {
    return request(createTheURL(Config.API.USER, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function getDataById(params) {
    return request(createTheURL(Config.API.USER, 'get'), {
        method: 'PUT',
        body: params,
    });
}
export async function setRole(params) {
    return request(createTheURL(Config.API.USER, 'addUserRoles'), {
        method: 'POST',
        body: params,
    });
}


export async function listAll(params) {
    return request(createTheURL(Config.API.USER, 'list'), {
        method: 'POST',
        body: params,
    });
}

export async function listbrif(params) {
    return request(createTheURL(Config.API.USER, 'listbrif'), {
        method: 'POST',
        body: params,
    });
}

export async function resetPassWords(params) {
    return request(createTheURL(Config.API.USER, 'reset'), {
        method: 'PUT',
        body: params,
    });
}
export async function setState(params) {
    return request(createTheURL(Config.API.USER, 'state'), {
        method: 'PUT',
        body: params,
    });
}

export async function updatepsw(params) {
    return request(createTheURL(Config.API.USER, 'updatepsw'), {
        method: 'PUT',
        body: params,
    });
}


