import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";
import config from '../config/config';

export async function othersList(params) {
    return request(createTheURL(Config.API.OTHERS, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function othersAdd(params) {
    return request(createTheURL(Config.API.OTHERS, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function othersDisabled(params) {
    return request(createTheURL(Config.API.OTHERS, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function othersDel(params) {
    return request(createTheURL(Config.API.OTHERS, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function othersDelAll(params) {
    return request(createTheURL(Config.API.OTHERS, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function othersEdit(params) {
    return request(createTheURL(Config.API.OTHERS, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function othersGet(params) {
    return request(createTheURL(Config.API.OTHERS, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.OTHERS, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.OTHERS, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

