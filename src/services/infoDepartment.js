import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function listAll(params) {
    return request(createTheURL(Config.API.DEPARTMENT, 'listAll'), {
        method: 'PUT',
        body: params,
    });
}

export async function getDepartment(params) {
    return request(createTheURL(Config.API.DEPARTMENT, 'get'), {
        method: 'PUT',
        body: params,
    });
}