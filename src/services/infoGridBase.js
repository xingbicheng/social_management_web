import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function getPersonInfo(params) {
    return request(createTheURL(Config.API.STATISTICS, 'person'), {
        method: 'PUT',
        body: params,
    });
}

export async function getHouseInfo(params) {
    return request(createTheURL(Config.API.STATISTICS, 'house'), {
        method: 'PUT',
        body: params,
    });
}

export async function getPersonTypeInfo(params) {
    return request(createTheURL(Config.API.STATISTICS, 'personType'), {
        method: 'PUT',
        body: params,
    });
}