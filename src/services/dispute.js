import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function disputeList(params) {
  return request(createTheURL(Config.API.DISPUTE, 'list'), {
    method: 'PUT',
    body: params,
  });
}

export async function addInfo(params) {
  return request(createTheURL(Config.API.DISPUTE,'add'),{
    method:'POST',
    body:params,
  });
}

export async function deleteInfo(params) {
  return request(createTheURL(Config.API.DISPUTE,'del'),{
    method:'DELETE',
    body:params,
  })
}

export async function deleteAllInfo(params) {
  return request(createTheURL(Config.API.DISPUTE,'delAll'),{
    method:'DELETE',
    body:params,
  })
}

export async function getType(params) {
  return request(createTheURL(Config.API.DATADICT, 'list'), {
    method: 'POST',
    body: params,
  });
}

export async function editInfo(params) {
  return request(createTheURL(Config.API.DISPUTE, 'edit'), {
    method: 'PUT',
    body: params,
  });
}

export async function handler(params) {
  return request(createTheURL(Config.API.ASSIGN, 'add/handler'), {
    method: 'PUT',
    body: params,
  });
}

export async function getByAcceptId(params) {
  return request(createTheURL(Config.API.ASSIGN, 'get'), {
    method: 'PUT',
    body: params,
  });
}

export async function getById(params) {
  return request(createTheURL(Config.API.DISPUTE, 'get'), {
    method: 'PUT',
    body: params,
  });
}

export async function addListParty(params) {
  return request(createTheURL(Config.API.DISPUTE, 'party/add'), {
    method: 'POST',
    body: params,
  });
}

export async function editParty(params) {
  return request(createTheURL(Config.API.DISPUTE, 'party/edit'), {
    method: 'PUT',
    body: params,
  });
}


export async function deleteParty(params) {
  return request(createTheURL(Config.API.DISPUTE, 'party/del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function deleteAllPart(params) {
  return request(createTheURL(Config.API.DISPUTE, 'party/delAll'), {
    method: 'DELETE',
    body: params,
  });
}

export async function uploadfile(params) {
  return request(createTheURL(Config.API.FILE, 'upload'), {
    method: 'POST',
    body: params,
  });
}

export async function deleteFile(params) {
  return request(createTheURL(Config.API.FILE, 'del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function getFile(params) {
  return request(createTheURL(Config.API.FILE, 'get'), {
    method: 'PUT',
    body: params,
  });
}

export async function download(params) {
  return request(createTheURL('','files/2018/07/17/b57ae1ae6b4c68d59000e2cd56cfee7c.zip'),{
    method:'GET',
    //body:params,
  })
}
