import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";
export async function list(params) {
    return request(createTheURL(Config.API.DISTRICT, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function getArea(params) {
    return request(createTheURL(Config.API.DISTRICT, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function setManager(params) {
    return request(createTheURL(Config.API.DISTRICT, 'setManager'), {
        method: 'PUT',
        body: params,
    });
}

export async function edit(params) {
    return request(createTheURL(Config.API.DISTRICT, 'edit'), {
        method: 'PUT',
        body: params,
    });
}


export async function cancelManager(params) {
    return request(createTheURL(Config.API.DISTRICT, 'cancelManager'), {
        method: 'PUT',
        body: params,
    });
}


export async function add(params) {
    return request(createTheURL(Config.API.DISTRICT, 'add'), {
        method: 'POST',
        body: params,
    });
}


export async function addTeam(params) {
    return request(createTheURL(Config.API.DISTRICT, 'addTeam'), {
        method: 'POST',
        body: params,
    });
}

export async function excel(params) {
    return request(createTheURL(Config.API.DISTRICT, 'excel'), {
        method: 'POST',
        body: params,
    });
}


export async function excelAll(params) {
    return request(createTheURL(Config.API.DISTRICT, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

export async function del(params) {
    return request(createTheURL(Config.API.DISTRICT, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function delTeam(params) {
    return request(createTheURL(Config.API.DISTRICT, 'delTeam'), {
        method: 'DELETE',
        body: params,
    });
}

export async function editTeam(params) {
    return request(createTheURL(Config.API.DISTRICT, 'editTeam'), {
        method: 'PUT',
        body: params,
    });
}