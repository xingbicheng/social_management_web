import request from '../utils/request/request';
import Config from "../config/api"; 
import { createTheURL } from "../utils/utils";

 // 查询当前用户
export async function queryCurrent(params) {
    return request(createTheURL(Config.API.USER, 'current'), {
        method: 'GET',
        body: params,
    });
}
export async function updatePassword(params){
    return request(createTheURL(Config.API.USER, 'updatepsw'),{
        method:'PUT',
        body:params,
    });
}
