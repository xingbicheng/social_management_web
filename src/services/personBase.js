import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function personBaseGet(params) {
    return request(createTheURL(Config.API.PERSONBASE, 'get'), {
        method: 'PUT',
        body: params,
    });
}
export async function personBasegetByIdCard(params) {
    return request(createTheURL(Config.API.PERSONBASE, 'getByIdCard'), {
        method: 'PUT',
        body: params,
    });
}
export async function personBaseAdd(params) {
    return request(createTheURL(Config.API.PERSONBASE, 'add'), {
        method: 'POST',
        body: params,
    });
}
export async function personBaseEdit(params) {
    return request(createTheURL(Config.API.PERSONBASE, 'edit'), {
        method: 'PUT',
        body: params,
    });
}
export async function personBaseList(params) {
    return request(createTheURL(Config.API.PERSONBASE, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function judgePersonHaveHouse(params) {
  return request(createTheURL(Config.API.GRID, 'getByIdCard'), {
    method: 'PUT',
    body: params,
  })
}