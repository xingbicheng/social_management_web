import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function list(params) {
    
    return request(createTheURL(Config.API.HOUSE, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function getIds(params) {
    return request(createTheURL(Config.API.HOUSE, 'get/house/address'), {
        method: 'PUT',
        body: params,
    });
}

export async function get(params) {
    return request(createTheURL(Config.API.HOUSE, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function edit(params) {
    return request(createTheURL(Config.API.HOUSE, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function editPerson(params) {
    return request(createTheURL(Config.API.HOUSE, 'editPerson'), {
        method: 'PUT',
        body: params,
    });
}

export async function add(params) {
    return request(createTheURL(Config.API.HOUSE, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function addPerson(params) {
    return request(createTheURL(Config.API.HOUSE, 'addPerson'), {
        method: 'POST',
        body: params,
    });
}

export async function excel(params) {
    return request(createTheURL(Config.API.HOUSE, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function excelAll(params) {
    return request(createTheURL(Config.API.HOUSE, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

export async function del(params) {
    return request(createTheURL(Config.API.HOUSE, 'del'), {
        method: 'DELETE',
        body: params,
    });
}

export async function delPerson(params) {
    return request(createTheURL(Config.API.HOUSE, 'delPerson'), {
        method: 'DELETE',
        body: params,
    });
}