import { createTheURL } from '../utils/utils';
import Config from "../config/api";
import request from '../utils/request/request';

export async function RiskEvaluationList(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'list'), {
    method: 'PUT',
    body: params,
  });   
}
export async function RiskEvaluationAdd(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'add'), {
    method: 'POST',
    body: params,
  });
}
export async function RiskEvaluationDel(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'del'), {
    method: 'DELETE',
    body: params,
  });
}
export async function RiskEvaluationEdit(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'edit'), {
    method: 'PUT',
    body: params,
  });
}
export async function RiskEvaluationIssued(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'issued'), {
    method: 'PUT',
    body: params,
  });
}
export async function RiskEvaluationSubmit(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'submit'), {
    method: 'PUT',
    body: params,
  });
}
export async function ReportDetailAdd(params) {
  return request(createTheURL(Config.API.RISKDETAILS, 'add'), {
    method: 'POST',
    body: params,
  });
}
export async function ReportDetailList(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'get'), {
    method: 'PUT',
    body: params,
  });
}
export async function ReportDetailDel(params) {
  return request(createTheURL(Config.API.RISKDETAILS, 'del'), {
    method: 'DELETE',
    body: params,
  });
}
export async function ReportDetaiEdit(params) {
  return request(createTheURL(Config.API.RISKDETAILS, 'edit'), {
    method: 'PUT',
    body: params,
  });
}
export async function RiskEvaluationDelAll(params) {
  return request(createTheURL(Config.API.RISKEVALUTION, 'delAll'), {
    method: 'DELETE',
    body: params,
  });
}







