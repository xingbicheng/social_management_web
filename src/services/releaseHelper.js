import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function helperList(params) {
    return request(createTheURL(Config.API.HELPER, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function helperAdd(params) {
    return request(createTheURL(Config.API.HELPER, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function helperDisabled(params) {
    return request(createTheURL(Config.API.HELPER, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function helperDel(params) {
    return request(createTheURL(Config.API.HELPER, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function helperDelAll(params) {
    return request(createTheURL(Config.API.HELPER, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function helperEdit(params) {
    return request(createTheURL(Config.API.HELPER, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function helperGet(params) {
    return request(createTheURL(Config.API.HELPER, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.HELPER, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.HELPER, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

