import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function list(params) {
    return request(createTheURL(Config.API.MERSH, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function getGrid(params) {
    return request(createTheURL(Config.API.MERSH, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function setManager(params) {
    return request(createTheURL(Config.API.MERSH, 'setManager'), {
        method: 'PUT',
        body: params,
    });
}

export async function edit(params) {
    return request(createTheURL(Config.API.MERSH, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function editTeam(params) {
    return request(createTheURL(Config.API.MERSH, 'editTeam'), {
        method: 'PUT',
        body: params,
    });
}

export async function cancelManager(params) {
    return request(createTheURL(Config.API.MERSH, 'cancelManager'), {
        method: 'PUT',
        body: params,
    });
}

export async function add(params) {
    return request(createTheURL(Config.API.MERSH, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function addTeam(params) {
    return request(createTheURL(Config.API.MERSH, 'addTeam'), {
        method: 'POST',
        body: params,
    });
}

export async function excel(params) {
    return request(createTheURL(Config.API.MERSH, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function excelAll(params) {
    return request(createTheURL(Config.API.MERSH, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

export async function del(params) {
    return request(createTheURL(Config.API.MERSH, 'del'), {
        method: 'DELETE',
        body: params,
    });
}

export async function delTeam(params) {
    return request(createTheURL(Config.API.MERSH, 'delTeam'), {
        method: 'DELETE',
        body: params,
    });
}