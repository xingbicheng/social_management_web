import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function menuTree(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'menu/tree'), {
        method: 'get',
        body: params,
    });
}
export async function getDataById(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'get'), {
        method: 'get',
        body: params,
    });
}
export async function addmenu(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'add'), {
        method: 'POST',
        body: params,
    });
}
export async function editmenu(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'edit'), {
        method: 'PUT',
        body: params,
    });
}
export async function delmenu(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function getUserTree(params) {
    return request(createTheURL(Config.API.PERMISSIONS, 'current/tree'),{
        method:'GET',
        body:params,
    });
}
