import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from '../utils/utils';

export async function InoticeList(params) {

  return request(createTheURL(Config.API.INOTICE, 'list'), {
    method: 'PUT',
    body: params,
  });
}
export async function InoticeDel(params) {

  return request(createTheURL(Config.API.INOTICE, 'del'), {
    method: 'DELETE',
    body: params,
  });
}
export async function InoticeAdd(params) {

  return request(createTheURL(Config.API.INOTICE, 'add'), {
    method: 'POSt',
    body: params,
  });
}
export async function InoticeGet(params) {

  return request(createTheURL(Config.API.INOTICE, 'get'), {
    method: 'PUT',
    body: params,
  });
}
export async function InoticeEdit(params) {

  return request(createTheURL(Config.API.INOTICE, 'edit'), {
    method: 'PUT',
    body: params,
  });
}
export async function IsubmitList(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'list'), {
    method: 'PUT',
    body: params,
  });
}

export async function IsubmitDel(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'del'), {
    method: 'DELETE',
    body: params,
  });
}
export async function IsubmitAdd(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'add'), {
    method: 'POST',
    body: params,
  });
}
export async function IsubmitGet(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'get'), {
    method: 'PUT',
    body: params,
  });
}
export async function IsubmitEdit(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'edit'), {
    method: 'PUT',
    body: params,
  });
}
export async function ReviewPass(params) {
  return request(createTheURL(Config.API.ISUBMITTEDREVIEW, 'pass'), {
    method: 'PUT',
    body: params,
  });
}
export async function ReviewNotPass(params) {
  return request(createTheURL(Config.API.ISUBMITTEDREVIEW, 'notpass'), {
    method: 'PUT',
    body: params,
  });
}
export async function IsubmitSubmit(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'submit'), {
    method: 'PUT',
    body: params,
  });
}
export async function InoticeDelAll(params) {
  return request(createTheURL(Config.API.INOTICE, 'delAll'), {
    method: 'DELETE',
    body: params,
  });
}
export async function IsubmitDelAll(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'delAll'), {
    method: 'DELETE',
    body: params,
  });
}
export async function IsubmitShow(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'show'), {
    method: 'PUT',
    body: params,
  });
}
export async function IsubmitNotShow(params) {
  return request(createTheURL(Config.API.ISUBMITTED, 'notshow'), {
    method: 'PUT',
    body: params,
  });
}









