import request from '../utils/request/request';
import Config from "../config/api";
import {createTheURL} from "../utils/utils";

export async function acceptList(params) {
  return request(createTheURL(Config.API.ACCEPT, 'list'), {
    method: 'PUT',
    body: params,
  });
}

export async function acceptAdd(params) {
  return request(createTheURL(Config.API.ACCEPT, 'add'), {
    method: 'POST',
    body: params,
  });
}

export async function acceptEdit(params) {
  return request(createTheURL(Config.API.ACCEPT, 'edit'), {
    method: 'PUT',
    body: params,
  });
}

export async function acceptDel(params) {
  return request(createTheURL(Config.API.ACCEPT, 'del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function acceptDelAll(params) {
  return request(createTheURL(Config.API.ACCEPT, 'delAll'), {
    method: 'DELETE',
    body: params,
  });
}

export async function acceptGet(params) {
  return request(createTheURL(Config.API.ACCEPT, 'get'), {
    method: 'put',
    body: params,
  });
}

export async function typeList(params) {
  return request(createTheURL(Config.API.DATADICT, 'list'), {
    method: 'POST',
    body: params,
  });
}

export async function acceptPartyDel(params) {
  return request(createTheURL(Config.API.ACCEPT, 'party/del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function acceptPartyAdd(params) {
  return request(createTheURL(Config.API.ACCEPT, 'party/add'), {
    method: 'POST',
    body: params,
  });
}

export async function acceptPartyEdit(params) {
  return request(createTheURL(Config.API.ACCEPT, 'party/edit'), {
    method: 'PUT',
    body: params,
  });
}

export async function userList(params) {
  return request(createTheURL(Config.API.USER, 'list'), {
    method: 'POST',
    body: params,
  });
}

export async function assignAdd(params) {
  return request(createTheURL(Config.API.ASSIGN, 'add'), {
    method: 'POST',
    body: params,
  });
}

export async function assignGet(params) {
  return request(createTheURL(Config.API.ASSIGN, 'get'), {
    method: 'PUT',
    body: params,
  });
}

export async function assignDel(params) {
  return request(createTheURL(Config.API.ASSIGN, 'del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function assignDelAll(params) {
  return request(createTheURL(Config.API.ASSIGN, 'delAll'), {
    method: 'DELETE',
    body: params,
  });
}

export async function assignEdit(params) {
  return request(createTheURL(Config.API.ASSIGN, 'edit'), {
    method: 'PUT',
    body: params,
  });
}
