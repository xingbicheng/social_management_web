import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function dictList(params) {
  return request(createTheURL(Config.API.DATADICT, 'list'), {
    method: 'POST',
    body: params,
  });
}

export async function dictAdd(params) {
  return request(createTheURL(Config.API.DATADICT, 'add'), {
    method: 'POST',
    body: params,
  });
}

export async function dictDel(params) {
  return request(createTheURL(Config.API.DATADICT, 'del'), {
    method: 'DELETE',
    body: params,
  });
}

export async function dictEdit(params) {
  return request(createTheURL(Config.API.DATADICT, 'edit'), {
    method: 'PUT',
    body: params,
  });
}

export async function dictGet(params) {
  return request(createTheURL(Config.API.DATADICT, 'get'), {
    method: 'PUT',
    body: params,
  });
}
export async function dictGetTypes(params) {
  return request(createTheURL(Config.API.DATADICT, 'getDictTypes'), {
    method: 'PUT',
    body: params,
  });
}


