import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from '../utils/utils';

export async function inoticeList(params) {

  return request(createTheURL(Config.API.INOTICE, 'list'), {
    method: 'PUT',
    body: params,
  });
}
