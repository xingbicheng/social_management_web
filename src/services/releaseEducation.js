import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";
import config from '../config/config';

export async function educationList(params) {
    return request(createTheURL(Config.API.EDUCATION, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function educationAdd(params) {
    return request(createTheURL(Config.API.EDUCATION, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function educationDisabled(params) {
    return request(createTheURL(Config.API.EDUCATION, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function educationDel(params) {
    return request(createTheURL(Config.API.EDUCATION, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function educationDelAll(params) {
    return request(createTheURL(Config.API.EDUCATION, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function educationEdit(params) {
    return request(createTheURL(Config.API.EDUCATION, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function educationGet(params) {
    return request(createTheURL(Config.API.EDUCATION, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.EDUCATION, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.EDUCATION, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

