import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function personDrugList(params) {
    return request(createTheURL(Config.API.DRUG, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function personDrugAdd(params) {
    return request(createTheURL(Config.API.DRUG, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function personDrugDisabled(params) {
    return request(createTheURL(Config.API.DRUG, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function personDrugDel(params) {
    return request(createTheURL(Config.API.DRUG, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function personDrugDelAll(params) {
    return request(createTheURL(Config.API.DRUG, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function personDrugEdit(params) {
    return request(createTheURL(Config.API.DRUG, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function personDrugGet(params) {
    return request(createTheURL(Config.API.DRUG, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.DRUG, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.DRUG, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

