import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";
import config from '../config/config';

export async function releaseList(params) {
    return request(createTheURL(Config.API.RELEASE, 'list'), {
        method: 'PUT',
        body: params,
    });
}

export async function releaseAdd(params) {
    return request(createTheURL(Config.API.RELEASE, 'add'), {
        method: 'POST',
        body: params,
    });
}

export async function releaseDisabled(params) {
    return request(createTheURL(Config.API.RELEASE, 'disabled'), {
        method: 'PUT',
        body: params,
    });
}

export async function releaseDel(params) {
    return request(createTheURL(Config.API.RELEASE, 'del'), {
        method: 'DELETE',
        body: params,
    });
}
export async function releaseDelAll(params) {
    return request(createTheURL(Config.API.RELEASE, 'delAll'), {
        method: 'DELETE',
        body: params,
    });
}

export async function releaseEdit(params) {
    return request(createTheURL(Config.API.RELEASE, 'edit'), {
        method: 'PUT',
        body: params,
    });
}

export async function releaseGet(params) {
    return request(createTheURL(Config.API.RELEASE, 'get'), {
        method: 'PUT',
        body: params,
    });
}

export async function exportExcel(params) {
    return request(createTheURL(Config.API.RELEASE, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function exportExcelAll(params) {
    return request(createTheURL(Config.API.RELEASE, 'excelAll'), {
        method: 'POST',
        body: params,
    });
}

