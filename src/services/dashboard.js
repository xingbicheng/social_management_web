import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";

export async function acceptList(params) {
    return request(createTheURL(Config.API.PERSON, 'count'), {
        method: 'PUT',
        body: params,
    });
}
export async function disputeList(params) {
    return request(createTheURL('/dispute', 'countAcceptType'), {
        method: 'PUT',
        body: params,
    });
}

export async function  houseList(params) {
    return request(createTheURL(Config.API.HOUSECOUNT,'houseList'),{
       method:'PUT',
       body:params,
    });
}
