import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from '../utils/utils';

export async function GetCaptcha(params){
    return request(createTheURL(Config.API.LOGIN, 'getcaptcha'), {
        method: 'GET',
        body: params,
    });
}

export async function Login(params){
    return request(createTheURL(Config.API.LOGIN, 'login'), {
        method: 'POST',
        body: params,
    });
}

export async function Logout(params) {
    return request(createTheURL(Config.API.LOGIN,'/loginOut'),{
       method:'POST',
       body:params,
    });
}
