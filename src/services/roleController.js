import request from '../utils/request/request';
import Config from "../config/api";
import { createTheURL } from "../utils/utils";  

export async function addRoles(params) {
    return request(createTheURL(Config.API.ROLE, 'add'), {
        method: 'POST',
        body: params,
    });
}
export async function allbrif(params) {
    return request(createTheURL(Config.API.ROLE, 'allbrif'), {
        method: 'POST',
        body: params,
    });
}

export async function getAllRoles(params) {
    return request(createTheURL(Config.API.ROLE, 'allrole'), {
        method: 'GET',
        body: params,
    });
}

export async function editRole(params) {
    return request(createTheURL(Config.API.ROLE, 'edit'), {
        method: 'PUT',
        body: params,
    });
}
export async function excel(params) {
    return request(createTheURL(Config.API.ROLE, 'excel'), {
        method: 'POST',
        body: params,
    });
}

export async function getById(params) {
    return request(createTheURL(Config.API.ROLE, 'user/get'), {
        method: 'GET',
        body: params,
    });
}

export async function listAllRoles(params) {
    return request(createTheURL(Config.API.ROLE, 'list'), {
        method: 'POST',
        body: params,
    });
}

export async function listbrif(params) {
    return request(createTheURL(Config.API.ROLE, 'listbrif'), {
        method: 'POST',
        body: params,
    });
}

export async function resetPassWords(params) {
    return request(createTheURL(Config.API.ROLE, 'reset'), {
        method: 'PUT',
        body: params,
    });
}
export async function delData(params) {
    return request(createTheURL(Config.API.ROLE, 'del'), {
        method: 'DELETE',
        body: params,
    });
}

export async function set(params) {
    return request(createTheURL(Config.API.ROLE, 'set'), {
        method: 'PUT',
        body: params,
    });
}


