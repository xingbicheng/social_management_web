import { InputNumber, Modal, Form, Input, DatePicker ,Select,Alert,TreeSelect} from 'antd';
import {FormItemLayoutModal} from "../../config/config";
import {deleteSpace} from "../../utils/utils"

const FormItem = Form.Item;
const TreeNode = TreeSelect.TreeNode;
@Form.create()
class AddForm extends React.Component {

    constructor(props){
        super(props);
    }
    onHandleCreate = () => {
        const { form,handleOk } = this.props;
        form.validateFields((err, values) => {
          if (!err) {
            let list =deleteSpace(values);
            handleOk(list);
          }else{
            return (
                <Alert
                  description="表单验证失败"
                  type="error"
                />
              )
          }
        });
      }
    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        handleCancel();
            }
    render() {
      const formItemLayout = FormItemLayoutModal;  // 通用样式表
      const { modalVisible, form,title,formInfo,addloading } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={modalVisible}
          title={title}
          okText="确定"
          confirmLoading={addloading}
          onCancel={this.onHandleCancel}
          onOk={this.onHandleCreate}
        >
          <Form layout="vertical">
            <FormItem label="角色名称"
            {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue:formInfo.name || '',
                rules: [{ max:8, required: true, message: '角色名称八个字以内!' }],
              })(
                <Input placeholder='角色名称八个字以内' />
              )}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
export default AddForm;