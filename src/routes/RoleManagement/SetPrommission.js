import { InputNumber, Modal, Form, Input, DatePicker ,Select,Alert,TreeSelect} from 'antd';
import {FormItemLayoutModal} from "../../config/config";
import { Tree, Icon } from 'antd';

const TreeNode = Tree.TreeNode;
@Form.create()
class SetPrommission extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            autoExpandParent: false,
            selectedKeys: [],
            checkedKeysData:[],
          }
    }
      onExpand = (expandedKeys) => {
        this.setState({
          expandedKeys,
          autoExpandParent: false,
        });
      }

      onCheck = (checkedKeys) => {
        this.props.dispatch({
            type: 'rolecontroller/updateState',
            payload:{
                roleMenuTreeData:checkedKeys,
            }
        })
        this.setState({
            checkedKeysData:checkedKeys,
        })
      }


    onHandleCreate = () => {
        const { handleOk } = this.props;
        const { id } = this.props.formInfo;
        const { checkedKeysData } =this.state;
         handleOk(checkedKeysData,id);
      }
    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        handleCancel();
        }
    renderTreeNodes = (menuTreeData) => {
        return menuTreeData.map((item) => {
            if (item.children) {
            return (
                <TreeNode icon={<Icon type="dashboard" />} title={item.title} key={item.key} dataRef={item}>
                {this.renderTreeNodes(item.children)}
                </TreeNode>
            );
            }
            return <TreeNode {...item} />;
        });
        }
    render() {
      const { permissionModalVisible,title,menuTreeData,roleMenuTreeData,setloading } = this.props;
      console.log(menuTreeData)
      return (
        <Modal
          visible={permissionModalVisible}
          title={title}
          okText="确定"
          confirmLoading={setloading}
          onCancel={this.onHandleCancel}
          onOk={this.onHandleCreate}
        >
          <Tree
            checkable
            onCheck={this.onCheck}
            checkedKeys={roleMenuTreeData}
        >
            {
                this.renderTreeNodes(menuTreeData)
            }
        </Tree>
        </Modal>
      );
    }
  }
export default SetPrommission;
