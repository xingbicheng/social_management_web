import { Card,Button,message,Popconfirm,Modal } from 'antd';
import React,{Component} from 'react';
import { connect } from 'dva';
import AutoList from '../../components/List/List';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../../theme/table.less';
import AddForm from './AddForm';
import ToolBar from '../../components/ToolBar';
import SetPrommission from './SetPrommission';

const confirm = Modal.confirm;

@connect(({ rolecontroller,loading }) => ({
    rolecontroller,
    loading: loading.effects['rolecontroller/getAllList'],
    addloading:loading.effects['rolecontroller/addNewRole'],
    setloading:loading.effects['rolecontroller/setPermission'],
  }))
class RoleManagement extends Component {
  state = {
    pagination: {},
    title:'xxx',
    formMethod:'addNewRole',
  };

  componentDidMount() {
      this.listpage();
  }
  listpage = ()=>{
    const { rolecontroller:{pagination} } = this.props;
    const { pageNum,pageSize } = pagination;
    const { dispatch } = this.props;
       
    dispatch({
        type:'rolecontroller/getAllList',
        payload:{
            page:pageNum,
            pageSize,
            // ...searchInfo,
        },
    });
  }
  setPermission = (record)=>{
    this.props.dispatch({
        type:'rolecontroller/getRoleMenuTree',
        payload:{
            roleId:record.id
        }
    });
    this.props.dispatch({
        type:'rolecontroller/getMenuTree',
    });
    
    this.props.dispatch({
        type: 'rolecontroller/showPermissionModal',
        payload:{
            ...record,
        }
     });
  } 
  add = () => {
    this.props.dispatch({
      type: 'rolecontroller/showModal',
      payload:{
        name:0,
        id:1,
      }
   });
    this.setState({
      title:'角色管理-新建',
      formMethod:'addNewRole',
    })
  }
  edit = (record)=>{
    const { current } = this.props.rolecontroller.pagination;
    this.props.dispatch({
        type: 'rolecontroller/showModal',
        payload:{
            ...record,
        }
     });
     this.setState({
        title:'角色管理-编辑',
        formMethod:'editRoles',
      })
  }

  delRole=(record)=>{
      let _this=this;
    if(record && record.id){
        confirm({
            title: '删除',
            content: '是否删除所选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                _this.props.dispatch({
                    type:'rolecontroller/toDelRole',
                    payload:{
                        id:record.id
                      }
                })
            },
        });
    }
  }
  turnPage = (e)=>{
    const {current,pageSize} = e;
    const { dispatch,searchInfo } = this.props;
    dispatch({
        type:'rolecontroller/getAllList',
        payload:{
            ...searchInfo,
            page:current,
            pageSize,
        },
    });
  }
  handleCancel = () => {
    this.props.dispatch({
      type:'rolecontroller/hiddenModal',
  });
  }
  handleOk = (value) =>{
    const { dispatch } = this.props;
    const { formMethod } = this.state;
    const { currentRow } = this.props.rolecontroller;
    const { current } = this.props.rolecontroller.pagination;
    dispatch({
      type:`rolecontroller/${formMethod}`,
      payload:{
        id:currentRow.id,
        ...value,
        pageNum:current,
        visible:1
      },
    })
  }
  permissionhandleOk= (checkedKeys,id) =>{
      this.props.dispatch({
        type:`rolecontroller/setPermission`,
        payload:{
            permissionIds:checkedKeys,
            roleId:id,
        },
      })
      this.props.dispatch({
        type:'rolecontroller/hiddenPermissionModal',
    });
  }
  permissionhandleCancel=()=>{
    this.props.dispatch({
        type:'rolecontroller/hiddenPermissionModal',
    });
  }
  render() {
    const { rolecontroller:{ dataSource,currentRow,menuTreeData,permissionModalVisible,roleMenuTreeData },loading,addloading,setloading }=this.props;
    const { title } =this.state;
    const { rolecontroller:{ modalVisible,pagination} } = this.props;
    const pageProps = {
        dataSource,
        loading,
        columsList:{
            columns:[{
                title:'#',
                key:'index',
                dataIndex:'index',
                align:'center',
                render: (text,record, index) => {
                    return <span>{index + 1}</span>
                },
            },{
                title: '角色名称',
                key: 'rolename',
                dataIndex:'name',
            },{
                title: '创建时间',
                key: 'createTime',
                dataIndex:'createTime',
            },
            {
                title: '修改时间',
                key: 'updateTime',
                dataIndex:'updateTime',
            },    
            ],
            menulistArr:[{
                key:'handle-4',
                name:'修改角色',
                menuclick:this.edit,
                },{
                    key:'handle-5',
                    name:'删除角色',
                    menuclick:this.delRole,
                },{
                    key:'handle-6',
                    name:'设置权限',
                    menuclick:this.setPermission,
            }],
           
        },
        pagination,
    }
    const modalProps = {
        modalVisible,
        addloading,
        title,
        handleOk:this.handleOk,
        handleCancel:this.handleCancel,
        formInfo:currentRow,
      }
      const modalProps1 = {
        dispatch:this.props.dispatch,
        roleMenuTreeData,
        setloading,
        permissionModalVisible,
        title:'设置权限',
        handleOk:this.permissionhandleOk,
        handleCancel:this. permissionhandleCancel,
        formInfo:currentRow,
        menuTreeData,
      }
      const BtnList = [{
        icon:'plus',
        name:'新增',
        type:'',
        key:'1',
        click:this.add,
    },]
    return (
        <PageHeaderLayout>
        <Card bordered={false}>
        {/* <SearchForm {...searchProps}  /> */}
            <div style={{ marginBottom: 16 }}>
                 <ToolBar BtnList={BtnList} />
                <AddForm {...modalProps} />
                <SetPrommission {...modalProps1} />
              </div>
              <AutoList {...pageProps} turnPage={this.turnPage} />
        </Card>
        </PageHeaderLayout>
    );
  }
}
export default RoleManagement;
