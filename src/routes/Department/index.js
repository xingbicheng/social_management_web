import React,{Component} from 'react';
import {  Card, Modal, Form, Row ,Col, Button, Tree,Icon, message  } from 'antd';
import AutoList from '../../components/List/List'
import ModalForm from './Modal';
import { connect } from 'dva';

import FontAwesome from 'react-fontawesome';
import ToolBar from '../../components/ToolBar';
import SearchForm from './SearchForm';
import { findChildren, findRecord }  from '../../utils/utils'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const confirm = Modal.confirm
const TreeNode = Tree.TreeNode
@Form.create()
@connect(({ department, loading }) => ({
  department,
  loading: loading.effects['department/ListAll'],
}))

class DepartmentList extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible: false,
      departmentId:0,
      modalType: 'create',
      addType: 'add',
      type: '',
      BtnList : [{
        icon:'plus',
        name:'新增第一级',
        type:'',
        key:'1',
        click:this.createDepartmentFirst,
      },
        {
          icon:'home',
          name:'顶部',
          type:'',
          key:'3',
          click:this.home,
        }, {
          icon:'plus',
          name:'下一级',
          type:'',
          key:'2',
          ishow:'none',
          click:this.createDepartment,
        },]
    }
  }
  componentWillMount(){
    this.props.dispatch({
      type:'department/ListAll',
    })
  }

  delRow=(record)=>{
    confirm({
      title: '删除',
      content: '确定删除此项？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        this.props.dispatch({
          type: 'department/delete',
          payload: {
            id: record.id,
          },
        })
      },
      onCancel() {

      },
    });
  }

  editRow=(record)=>{
    this.props.dispatch({
      type:'department/updateCurrentItem',
      payload: record,
    })
    this.setState({
      modalVisible:true,
      modalType:'update',
      type: 'edit',
    });
  }

  search = (values)=>{
    const {dispatch} = this.props;
    dispatch({
      type:'department/setSearchInfo',
      payload:{...values},
    });
    dispatch({
      type:'department/getList',
      payload:{
        page:1,
        pageSize:10,
        ...values,
      }
    })
  }

  restSearch = () => {
    const {dispatch} = this.props;
    dispatch({
      type:'department/clearSearchInfo',
    });

    dispatch({
      type:'department/ListAll',
    });
  }

  renderTreeNodes = (data) => {
    return data.map((item) => {
      if (item.children) {
        return (

          <TreeNode icon={<Icon type="folder-add" />} title={item.name} key={item.id}  dataRef={item}>
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return<TreeNode icon={<Icon type="folder-open" />} title={item.name} key={item.id} dataRef={item} />
    });
  }

  createDepartment = () => {
    const { dispatch, department: { defaultData } } = this.props
    if (JSON.stringify(defaultData) !== '{}' && defaultData.status === '2') {
      message.error('停用的部门不能添加下一级');
    } else {
      this.setState({
        modalVisible: true,
        modalType: 'create',
        addType: 'add',
        type: 'add',
      })
    }
  }
  createDepartmentFirst = () => {
    const { dispatch } = this.props;
    this.setState({
      modalVisible: true,
      modalType: 'create',
      addType: 'addFirst',
      type: 'addFirst',
    })
  }
  home = () => {
    this.props.dispatch({
      type:'department/ListAll',
    })
    this.setState({
      BtnList:[{
        icon:'plus',
        name:'新增第一级',
        type:'',
        key:'1',
        click:this.createDepartmentFirst,
      },
        {
          icon:'home',
          name:'顶部',
          type:'',
          key:'3',
          click:this.home,
        }]
    })
  }
  render = () => {
    const { modalVisible, modalType, addType, type } = this.state;
    const { dispatch,loading } =this.props;
    const { selectedRowKeys, pagination, dataSource, currentItem, searchInfo, multiDeleteButtonIsVisable, currentDepartment, treeData, department_info_status, treeSelectedKeys } =this.props.department;
    // currentItem.superiorDepartmentId=selectedRowKeys[0];
    console.log(currentDepartment);
   
    const listProps={
      dataSource: currentDepartment,
      loading,
      pagination: {
        ...pagination,
        onChange (page, pageSize) {
          dispatch({
            type: 'department/updateState',
            payload: {
              pagination: {
                pageSize,
                current: page,
              },
            },
          })
        },
      },
      columsList: {
        columns: [
          {
            title: '部门名称',
            dataIndex: 'name',
            key: 'name',
          },{
            title: '上级部门',
            dataIndex: 'superiorDepartmentId',
            key: 'superiorDepartmentId',
            render: (text, record) => {
              const { dataSource } = this.props.department

              const result = findRecord(dataSource, 'id', record.superiorDepartmentId)
              return (
                <span>{result.name}</span>
              )
            },
          },{
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render: (text, record) => {
              if(record.status==='1'){
                return (
                  <span>启用</span>
                )
              }
              else {
                return(
                  <span>停用</span>
                )
              }
            }
          }
        ],
        menulistArr : [{
          key:'del',
          name:'删除',
          menuclick:this.delRow,
        },{
          key:'edit',
          name:'编辑',
          menuclick:this.editRow,
        }],
      },
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          this.props.dispatch({
            type: 'department/updateState',
            payload: {
              selectedRowKeys,
              multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
            },
          })

        },
      },
    }
    const modalProps = {
      dataSource,
      addType,
      type,
      departmentId:this.state.departmentId,
      item: currentItem,
      visible: modalVisible,
      maskClosable: false,
      department_info_status,
      title: `${modalType === 'create' ? '部门-新增' : '部门-编辑'}`,
      wrapClassName: 'vertical-center-modal',
      onOk: (data) => {
        this.setState({ modalVisible: false })
        if ( addType === 'add') {
          dispatch({
            type: `department/${modalType}`,
            payload: data,
          })
        } else if (addType === 'addFirst') {
          data = { ...data, superiorDepartmentId: 0}
          dispatch({
            type: `department/${modalType}`,
            payload: data,
          })
        }
        dispatch({
          type: 'department/updateState',
          payload: {

          },
        })
      },
      onCancel: () => {
        this.setState({ modalVisible: false })
      },
    }

    const searchFormProps = {
      searchInfo,
      restForm:this.restSearch,
      search:this.search,
    }

    const treeProps = {
      showIcon: true,

      onSelect: (selectedKeys, e) => {
        this.setState({
          departmentId:selectedKeys[0],
        })
        dispatch({
          type:'department/get',
          payload: {
            "id": selectedKeys[0],
          },
        });
        const currentDepartment = findChildren(treeData, 'id', selectedKeys[0], 'children');
        this.setState({
          BtnList:[{
            icon:'plus',
            name:'新增第一级',
            type:'',
            key:'1',
            click:this.createDepartmentFirst,
          },
            {
              icon:'home',
              name:'顶部',
              type:'',
              key:'3',
              click:this.home,
            }, {
              icon:'plus',
              name:'下一级',
              type:'',
              key:'2',
              ishow:'block',
              click:this.createDepartment,
            },]
        })
        dispatch({
          type: 'department/updateState',
          payload: {
            currentDepartment,
            currentItem:{
              createUserId: 0,
              id: 0,
              superiorDepartmentId: selectedKeys[0],
              updateUserId: 0,
            },
            treeSelectedKeys: selectedKeys,
          },
        })
      },
    }

    return(
      <PageHeaderLayout>
        <Card title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
          {/* <FontAwesome
          name="angle-double-up"
          style={{fontSize:'27px'}}
          /> */}
          {/* <Button><i className="iconfont">ASDASD</i></Button> */}

          <SearchForm {...searchFormProps}  />
        </Card>
        <Card bordered={false}>
          <Row type="flex" justify="end">
            <ToolBar BtnList={this.state.BtnList} />
          </Row>
          <Row>
            <Col span="6">
              <Card title="部门树" style={{ overflow:'hidden' }} >
                <Tree {...treeProps}>
                  {
                    this.renderTreeNodes(treeData)
                  }
                </Tree>
              </Card>
            </Col>
            <Col span="1" />
            <Col span="17">
              <AutoList {...listProps} />
            </Col>
          </Row>
          <ModalForm {...modalProps} />
        </Card>
      </PageHeaderLayout>
    );
  }
}

export default DepartmentList;
