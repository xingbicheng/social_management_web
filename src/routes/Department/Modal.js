import React , {Component} from 'react';
import { connect } from 'dva';
import { Button, Form, Select, Card, Input, Modal, Row } from 'antd';
import { SelectProps } from '../../utils/utils'

const FormItem = Form.Item;
const Option = Select.Option;
@Form.create()
@connect(({ department, loading }) => ({
  department,
  loading: loading.effects['department/get'],
}))

class ModelFrom extends Component{
  constructor(props){
    super(props);
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { getFieldDecorator, getFieldsValue } = this.props.form;
    const { item, visible, title, onOk, onCancel, department_info_status, dataSource, addType,departmentId, type }= this.props;
    const { defaultData } = this.props.department;
    const statusOptions = department_info_status.map(current => {
      return <Option key={current.k}>{current.val}</Option>
    })
    const departmentOptions = dataSource.map((current) => {
      return <Option key={current.id}>{current.name}</Option>
    })
    let isDisabled = false;
    if (type === 'add') {
      if (JSON.stringify(defaultData) !== '{}' && defaultData.status === '2') {
        isDisabled = true;
      }
    }
    return(
      <Modal
        title={title}
        visible={visible}
        onOk={() => {
          const { form } =this.props;
          let data = getFieldsValue()
          data = { ...item, ...data}
          onOk(data)
          form.resetFields();
        }}
        onCancel={()=>{
          const { form } =this.props;
          onCancel();
          form.resetFields();
        }}
        okButtonProps={{ disabled: isDisabled }}
      >
        <Form>
          <FormItem
            {...formItemLayout}
            label="部门名称"
          >
            {getFieldDecorator('name', {
              initialValue: item.name || '',
              rules: [{
                required: true, message: '部门名称是必填项',whitespace:true,
              }],
            })(
              <Input placeholder='请输入部门名称' disabled={isDisabled} />
            )}
          </FormItem>
          {
            (addType === 'add') && (
            <FormItem
              {...formItemLayout}
              label="上级部门"
            >
              {getFieldDecorator('superiorDepartmentId', {
                initialValue: `${item.superiorDepartmentId || '' }` ,
                rules: [{
                  required: true, message: '上级部门是必填项',
                }],
              })(
                <Select style={{ width: '100%' }} {...SelectProps} disabled={isDisabled}>{departmentOptions}</Select>
              )}
            </FormItem>
)}
          <FormItem
            {...formItemLayout}
            label="状态"
          >
            {getFieldDecorator('status', {
              initialValue: item.status || '',
              rules: [{
                required: true, message: '状态是必选项',
              }],
            })(
              <Select style={{ width: '100%' }} disabled={isDisabled} >
                {
                  statusOptions
                }
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="备注"
          >
            {getFieldDecorator('memo', {
              initialValue: item.memo || '',
              rules: [{
                max:400, message:'备注不能超过400字',
              }],
            })(
              <Input.TextArea  placeholder='备注不超过400个字符'  style={{ width: '100%'}} autosize={{minRows: 4, maxRows: 10 }} disabled={isDisabled} />
            )}
          </FormItem>
        </Form>

      </Modal>
    );
  }
}
export default ModelFrom;
