import React, { Component } from 'react';
import { Form, Input, Button, Select, Row, Col, Alert } from 'antd';
import { FormItemLayoutPage,SearchFormLayout, SearchBtnLayout } from '../../config/config';
import styles from '../../theme/table.less';

const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
class SearchForm extends Component {
  constructor(props) {
    super(props);
  }

  searchList = () => {
    const { form, search } = this.props;
    form.validateFields((err, values) => {
        for (const valuesName in values) {
          if (values.hasOwnProperty(valuesName)) {
            if (!values[valuesName]) {
              delete values[valuesName];
            }
          }
        }
      if(JSON.stringify(values) != "{}"){
        search(values);
      }
        
       else {
        return (
          <Alert
            description="表单验证失败"
            type="error"
          />
        );
      }
    });
  };
  restSearchForm = () => {
    const { form, restForm } = this.props;
    form.resetFields();
    restForm();
  };

  render() {
    const { searchInfo, form: { getFieldDecorator } } = this.props;
    const formItemLayout = FormItemLayoutPage;  // 通用样式表
    return (
      <Row>
        <Form className={styles.searchBox}>
          <Row>
          <Col {...SearchFormLayout}>
            <FormItem
              {...formItemLayout}
              label="部门名称"
            >
              {getFieldDecorator('name', {
                initialValue: searchInfo.name || '',
                rules: [],
              })(
                <Input placeholder='请输入部门名称' style={{ width: '100%' }}/>,
              )}
            </FormItem>
          </Col>
          <Col {...SearchFormLayout}>
            <FormItem
              {...formItemLayout}
              label="部门缩写"
            >
              {getFieldDecorator('abridge', {
                initialValue: searchInfo.abridge || '',
                rules: [],
              })(
                <Input placeholder='请输入部门缩写' style={{ width: '100%' }}/>,
              )}
            </FormItem>
          </Col>
          {/* <Col {...SearchFormLayout}>
            <FormItem
              {...formItemLayout}
              label="是否启用"
            >
              {getFieldDecorator('status', {
                initialValue: searchInfo.status || null,
                rules: [],
              })(
                <Select  style={{ width: '100%' }}>
                  <Option value="1">启用</Option>
                  <Option value="2">停用</Option>
                </Select>,
              )}
            </FormItem>
          </Col> */}
          </Row>
          <Row>
          <Col {...SearchBtnLayout}>
            <div
              style={{ margin: 0 }}
              className={styles.btnBox}
            >
              <Button onClick={this.restSearchForm}>重置</Button>
              <Button onClick={this.searchList} type="primary" icon="search">搜索</Button>
            </div>
          </Col>
          </Row>
        </Form>
      </Row>
    );
  }
}

export default SearchForm;
