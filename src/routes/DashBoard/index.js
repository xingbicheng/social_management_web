import React,{Component,Fragment} from 'react';
import { connect } from 'dva';
import { Card,Icon,Row,Col,Divider } from 'antd';
import { routerRedux } from 'dva/router';
import { Link } from 'react-router-dom';
import {
    ChartCard,
    Bar,
    Pie,
} from 'components/Charts';
import styles from './index.less';
import { ISUBMITTED,INOTICE } from '../../utils/emenus';
import { formatterTime } from '../../utils/utils';

@connect(({ dashboard,loading }) => ({
   dashboard,
   loading: loading.effects['dashboard/getMantlist'],
}))
class DashBoard extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        const { dispatch } = this.props;
        const now = new Date();
        const startTime = now.setDate(now.getDate()-30);
        dispatch({
            type:"dashboard/getMantlist",
        });
        dispatch({
            type:"dashboard/getDisputeList",
            payload:{
                startTime,
                endTime:(new Date()).getTime(),
            },
        });
        dispatch({
            type:"dashboard/getInoticeList",
            payload:{
                pageSize:6,
                state:INOTICE.RELEASE,
            },
        });
        dispatch({
            type:"dashboard/getIsubmittedList",
            payload:{
                pageSize:6,
                state:ISUBMITTED.RELEASE,
            },
        });
        dispatch({
            type:'dashboard/getHouseList',
            payload:{
                departmentId:"1",
            },
        });
    }
    createDisputeCount=()=>{
        const { disputelist } = this.props.dashboard;
        const newArr = [];
        disputelist.forEach((item,index)=>{
            newArr[index] = {
                y:0,
                x:0,
            }
            newArr[index].y = parseInt(item.personCount);
            newArr[index].x = item.personType;
        });
        return(
          <div style={{marginTop:'30px'}}>
            <Pie
              hasLegend
              subTitle="受理统计"
              total={() => <span>{newArr.reduce((pre, now) =>parseInt(now.y) + pre, 0)}起</span>}
              data={newArr}
              valueFormat={value => <span>{value}起</span>}
              height={150}
              lineWidth={4}
            />
          </div>
        );
    }

    createNewsList=(newslist,type)=>{
        if(newslist.length<1){
            return(
              <div className={styles.newsalert}>暂无消息</div>
            )
        }else{
            return(
                newslist.map((child)=>{
                    return(
                      <div key={child.id} className={styles.newsBox} >
                        <div className={styles.newsTitle} onClick={()=>this.WacthSubmitDtail(child,type)} >
                          <Icon type="caret-right" style={{paddingRight:'2px'}} />
                          {child.title}
                        </div>
                        <div>{formatterTime('date',child.releaseTime || child.submittedTime)}</div>
                      </div>
                    )
                })
            )
        }
    }

    createNewsCount=()=>{
        const { inoticelist,isubmittedlist } = this.props.dashboard;
        return(
          <div>
            <div style={{height:'225px'}}>
              <Divider orientation="left">通知通报</Divider>
              {
                  this.createNewsList(inoticelist,'NoticeDetail')
              }
            </div>
            <Divider orientation="left">报送信息</Divider>
            {
               this.createNewsList(isubmittedlist,'SubmitDetail')
            }
          </div>
            )
    }
    WacthSubmitDtail = (record,type) => {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch(routerRedux.push({
        pathname: `/infoPlatform/check/${id}/${type}`,
      }));
    };
    WatchNoticeDetail = (record,type) => {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch(routerRedux.push(`/infoPlatform/check/${id}${type}`));
    };
    render(){
        const { loading } = this.props;
        const { acceptlist,disputelist } = this.props.dashboard;
        let { houselist } = this.props.dashboard;
        if(houselist.length>10){
          houselist.slice(10);
        }
        return(
          <Fragment>
            <Row gutter={24} style={{marginBottom:'20px'}}>
              <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                <ChartCard
                  bordered={false}
                  title={<Link to="/person/persondrug">人员统计</Link>}
                  loading={loading}
                  contentHeight={300}
                  style={{marginBottom:'20px'}}
                >
                  <div style={{marginTop:'10px'}}>
                    <Pie
                      hasLegend
                      subTitle="人员统计"
                      total={() => <span>{acceptlist.reduce((pre, now) => parseInt(now.y) + pre, 0)}人</span>}
                      data={acceptlist}
                      valueFormat={value => <span>{value}人</span>}
                      height={225}
                      lineWidth={4}
                    />
                  </div>
                </ChartCard>
                <ChartCard
                  bordered={false}
                  title={<Link to="/information/citizen">受理统计</Link>}
                  loading={loading}
                  contentHeight={150}
                >
                  <Pie
                    hasLegend
                    subTitle="受理统计"
                    total={() => <span>{disputelist.reduce((pre, now) => parseInt(now.y) + pre, 0)}人</span>}
                    data={disputelist}
                    valueFormat={value => <span>{value}人</span>}
                    height={150}
                    lineWidth={4}
                  />
                  <div style={{marginTop:'10px'}} />
                </ChartCard>
              </Col>
              <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                <ChartCard
                  bordered={false}
                  title={<Link to="/infoPlatform/notice">消息通知</Link>}
                  loading={loading}
                  contentHeight={532}
                >
                  <div className={styles.newsListBox}>
                    {
                       this.createNewsCount()
                    }
                  </div>
                </ChartCard>
              </Col>
            </Row>
            <Card
              loading={loading}
              bordered={false}
              style={{ padding: 0,marginBottom:'20px' }}
              title={<Link to="/gridInfoManage/department">网格信息统计表</Link>}
            >
              <Row>
                <Col xl={17} lg={12} md={12} sm={24} xs={24}>
                  <div className={styles.salesBar}>
                    <Bar height={295} data={houselist} />
                  </div>
                </Col>
                <Col offset={1} xl={6} lg={12} md={12} sm={24} xs={24}>
                  <div className={styles.salesRank}>
                    <ul className={styles.rankingList}>
                      {houselist.map((item, i) => (
                        <li key={item.x}>
                          <span className={i < 3 ? styles.active : ''}>{i + 1}</span>
                          <span className={styles.houseName} >{item.x}</span>
                          <span>{item.y}人</span>
                        </li>
                      ))}
                    </ul>
                  </div>
                </Col>
              </Row>
            </Card>
          </Fragment>
        );
    }
}
export default DashBoard;



