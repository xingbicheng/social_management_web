import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button,Form, Select, Card, Input, Row, Col, DatePicker, Radio, Modal, message} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import PartList from '../../components/Party/Party';
import * as moment from 'moment';
import EditPartList from '../../components/EditParty/Party';
import styles from './index.less';


const FormItem = Form.Item;
const {Option} = Select;
const RadioGroup = Radio.Group;
const { TextArea } = Input;
const confirm = Modal.confirm;

@Form.create()
@connect(({ dispute, loading }) => ({
    dispute,
    addLoading: loading.effects['dispute/add'],
    loading:loading.effects['dispute/getPageData'],
}))

class PageForm extends Component{
    constructor(props){
        super(props);
        this.state = {
          partyData:[],
          cardLoading:true,
        }
    }

    componentWillMount = () => {
      const { dispatch } = this.props;
      dispatch({
        type: 'dispute/getType',
        payload:{
          "page": 1,
          "pageSize": 30,
          "type": "accept_dispute_matter_type",
        },
      })
      const pathToRegexp = require('path-to-regexp');
      const match = pathToRegexp('/information/pageForm/:id').exec(this.props.location.pathname); // match[1]
      if(match[1] == -1){
        dispatch({
          type:'dispute/setPageData',
          payload:{},
        });
        this.setState({
          partyData:[],
        })
      }
      else{
        dispatch({
          type: 'dispute/getPageData',
          payload:{
            id:match[1],
          },
        });
        const {dispute:{pageData}} = this.props;
        this.setState({
          partyData:pageData.listParty,
        })
      }

    }

    getPartyData = (partyData)=>{
       this.setState({
         partyData,
         cardLoading:false,
       });
    }

    returnAccept = () =>{
      confirm({
        title: '返回',
        content: '是否返回主界面？',
        cancelText:'取消',
        okText:'确定',
        onOk: () => {
          this.props.dispatch(routerRedux.push('/information/dispute'));
        },
        onCancel() {

        },
      });

    }

    disabledDate =  current => {
      return current && current < moment().endOf('day');
    }

    handleConfirm = () =>{
      const { form } = this.props;
      form.validateFields(
        (err)=>{
          if(!err){
            const {dispatch,dispute:{pageData}} = this.props;
            const {partyData} = this.state;
            const moreData  = form.getFieldsValue();
            moreData.handleDeadline = moreData.handleDeadline.format("YYYY-MM-DD hh:mm:ss");
            const pathToRegexp = require('path-to-regexp');
            const match = pathToRegexp('/information/pageForm/:id').exec(this.props.location.pathname); // match[1]
            if(match[1] == -1){
              if(!partyData.length){
                message.error('请添加当事人');
                return;
              }
            }else if(!pageData.listParty.length){
              message.error('请添加当事人');
              return;
            }
            confirm({
              title: '保存',
              content: '是否保存当前信息？',
              okText:'确定',
              cancelText:'取消',
              onOk() {
                dispatch({
                  type:'dispute/add',
                  payload:{
                    moreData,
                    partyData,
                  },
                });
              },
              onCancel() {},
            });

            this.setState({
              partyData:[],
            });
          }
        }
      );
    }
    render(){
      const {cardLoading} = this.state;
      const {getFieldDecorator} = this.props.form;
      const {dispute:{typeData, pageData}, addLoading, loading} = this.props;
      const pathToRegexp = require('path-to-regexp');
      const match = pathToRegexp('/information/pageForm/:id').exec(this.props.location.pathname); // match[1]
      const { handleDeadline } = pageData;
      let now = new moment();
      now.add(1,'d');
      now = now.format('YYYY-MM-DD HH:mm:ss');
      pageData.handleDeadline = moment(handleDeadline||now,'YYYY-MM-DD HH:mm:ss');
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 6 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 18 },
        },
      };
      const editPartyListProps = {
        listLoading:loading,
        getPartyData:this.getPartyData,
        peopleList:pageData.listParty,
        id:match[1],
      }
      const matterType = typeData.map(d => <Option key={d.k}>{d.val}</Option>);
      const editPartylist = <EditPartList {...editPartyListProps}  />;
      const partyList = <PartList getPartyData={this.getPartyData} />;
      return(
        <div>
          <Card bordered={false} title='当事人' style={{marginTop:'10px'}} >
          { match[1] == -1? partyList:editPartylist }
          </Card>
          <Card bordered={false} title='纠纷矛盾' style={{marginTop:'10px'}} loading={loading&&cardLoading}>
            <Row>
              <Col span={8}>
                <FormItem
                  {...formItemLayout}
                  label='类别'
                >
                  {getFieldDecorator('matterType',{
                          initialValue:pageData.matterType || '',
                          rules:[{
                            required:true,message: '类别是必填项',
                          }],
                        })(
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                          >
                            {matterType}
                          </Select>
                        )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...formItemLayout}
                  label='受理期限'
                >
                  {getFieldDecorator('handleDeadline',{
                      initialValue:pageData.handleDeadline || '',
                      rules:[{
                        required:true,message: '受理期限是必填项',
                      }],
                    })(
                      <DatePicker showTime disabledDate={this.disabledDate} format="YYYY-MM-DD HH:mm:ss" />
                    )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem>
                  {getFieldDecorator('level',{
                    initialValue:pageData.level || '',
                    rules:[{
                      required:true, message: '受理类型是必选项',
                    }],
                  })(
                    <RadioGroup name="radiogroup" style={{marginLeft:'10%', marginTop:'1%'}}>
                      <Radio value='0'>一般事项</Radio>
                      <Radio value='1'>重大事项</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <FormItem
                  labelCol={{span:2}}
                  wrapperCol={{span:22}}
                  label='内容'
                >
                  {getFieldDecorator('memo',{
                          initialValue:pageData.memo || '',
                          rules:[{
                            required:true,
                            whitespace:true,
                            message: '请输入不少于50字并且不大于400字',min:50,max:400,
                          }],
                        })(
                          <TextArea placeholder='请输入不少于50字并且不大于400字' autosize={{minRows:3,maxRows:5}} />
                        )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button icon="close" onClick={this.returnAccept} >返回</Button>
                  <Button icon="save" onClick={this.handleConfirm} loading={addLoading} type="primary">保存</Button>
                </div>
              </div>
            </Row>
          </Card>
        </div>
        );
    }
}
export default PageForm;
