import React,{ Component } from 'react';
import { connect } from 'dva';
import 'antd/dist/antd.css';
import { Table, Badge, Menu, Dropdown, Icon, Button} from 'antd';
import AutoList from '../../components/List/List';
import { matterStatus, matterType } from '../../utils/seletLocalData';
import {MATTERLEVEN} from '../../utils/emenus';

@connect(({ dispute, loading }) => ({
  dispute,
  loading: loading.effects['dispute/list'],
  deleteLoading:loading.effects['dispute/deleteInfo'],
  concludedLoading:loading.effects['dispute/handler'],
}))
class NestedTable extends Component{
   expandedRowRender = (record,index) => {
     const {data} = this.props;
     const columns = [
      { title: '姓名', dataIndex: 'name', key: 'name' },
      { title: '性别', dataIndex: 'sex', key: 'sex' },
      { title: '出生年月', dataIndex:'birthday', key: 'birthday'},
      { title: '住址', dataIndex: 'address', key: 'address' },
    ];

     const listParty = data[index].listParty;
    return (
      <Table
        columns={columns}
        dataSource={listParty}
        pagination={false}
      />
    );
   };

  turnPage = (e)=>{
    const {current,pageSize} = e;
    const { dispatch, searchData } = this.props;
    dispatch({
      type:'dispute/list',
      payload:{
        page:current,
        pageSize,
        ...searchData,
      },
    });
  }

  render(){
    const { data, handleMenuModify, handleMenuDelete, handleMenuConcluded, handleMenuHandlover, loading,pagination} = this.props;
    const columns = [
      { title: '编号', dataIndex: 'number', key: 'number' },
      { title: '状态', dataIndex: 'status', key: 'status',render:(record) =>{
        for(let i = 0;i < 5;i++){
          if(matterStatus[i].key === record){
            return matterStatus[i].value;
          }
        }
      } },
      { title: '受理时间', dataIndex: 'createTime', key: 'handleDate' },
      { title: '事项类型', dataIndex: 'matterType', key: 'matterType',render:(record)=>{
        for(let i = 0;i < 24;i++){
          if(matterType[i].k === record){
            return matterType[i].val;
          }
        }
        }},
      { title: '级别', dataIndex: 'level', key: 'level',render:(record) =>{
        if(record === MATTERLEVEN.NORMAL){
          return '一般事项';
        }else if(record === MATTERLEVEN.IMPORTANT){
        return '重大事项';
        }
      }},
      { title: '办理期限', dataIndex: 'handleDeadline',key: 'handleDeadline'},
    ];
    const pageProps = {
      dataSource:data,
      columsList:{
        columns,
        menulistArr:[{
          key:'handle-1',
          name:'编辑',
          menuclick:handleMenuModify,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:handleMenuDelete,
        },{
          key:'handle-3',
          name:'交办',
          menuclick:handleMenuHandlover,
        },{
          key:'handle-4',
          name:'办结',
          menuclick:handleMenuConcluded,
        }],
      },
      pagination,
      rowSelection:this.props.rowSelection,
      expandedRowRender: this.expandedRowRender,
      loading,
    }
    return (
      <AutoList {...pageProps} turnPage={this.turnPage} />
    );
  }
}


export default NestedTable;
