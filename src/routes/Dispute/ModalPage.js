import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Button,Form, Card, Input, Row, Icon, Table, Col, Modal, Tree } from 'antd';
import AutoList from '../../components/List/List'

const confirm = Modal.confirm
const TreeNode = Tree.TreeNode

@connect(({ department, loading }) => ({
  department,
}))
class ModalPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      department: [],
    }
  }

  renderTreeNodes = (data) => {
    return data.map((item) => {
      if (item.children) {
        return (
          <TreeNode title={item.name} key={item.id} dataRef={item} >
            {this.renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode title={item.name} key={item.id} dataRef={item} />
    });
  }

  parseTreeJson = (treeNodes, id) => {
    if (!treeNodes || !treeNodes.length) return;

    for (var i = 0, len = treeNodes.length; i < len; i++) {

      var childs = treeNodes[i].children;
      if (treeNodes[i].id == id) {
        const treeNode = Object.assign({}, treeNodes[i])
        delete treeNode.children;
        this.setState({department: [treeNode]})
        break;
      }

      if(childs && childs.length > 0){
        this.parseTreeJson(childs, id);
      }
    }
  };

  render() {
    const { isVisible, handleOk, handleCancel, dispatch, treeData, defaultData } = this.props;
    const { selectedRowKeys, pagination, dataSource, currentItem, searchInfo, multiDeleteButtonIsVisable, currentDepartment, department_info_status, treeSelectedKeys } =this.props.department;
    const treeProps = {
      showIcon: true,
      onSelect: (selectedKeys, e) => {
        this.parseTreeJson(treeData, selectedKeys[0]);
      },
    }
    const { department } = this.state;
    const listProps={
      dataSource: department,
      pagination: {
        ...pagination,
        onChange (page, pageSize) {
          dispatch({
            type: 'department/updateState',
            payload: {
              pagination: {
                pageSize,
                current: page,
              },
            },
          })
        },
      },
      columsList: {
        columns: [
          {
            title: '用户名',
            dataIndex: 'userId',
            key: 'userId',
          },
          {
            title: '姓名',
            dataIndex: 'userName',
            key: 'userName',
          },
          {
            title: '部门名称',
            dataIndex: 'name',
            key: 'name',
          },
        ],
        menulistArr : [{
          key:'del',
          name:'删除',
          menuclick:this.delRow,
        },{
          key:'edit',
          name:'编辑',
          menuclick:this.editRow,
        }],
      },
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          this.props.dispatch({
            type: 'department/updateState',
            payload: {
              selectedRowKeys,
              multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
            },
          })
        },
      },
    }
    return (
      <Modal onOk={handleOk} onCancel={handleCancel}   visible={isVisible} title="单位/责任人" width='60%' >
        <Row>
          <Col span={8}>
            <Card title="部门树">
              <Tree {...treeProps}>
                {
                  this.renderTreeNodes(treeData)
                }
              </Tree>
            </Card>
          </Col>
          <Col span={1} />
          <Col  span={15}>
            <AutoList {...listProps} />
          </Col>
        </Row>
      </Modal>
      )
  }
}

export default ModalPage;
