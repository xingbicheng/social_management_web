import React , {Component} from 'react';
import { connect } from 'dva';
import {  Card, Row, Col, Form, Input, Button, Select } from 'antd';
import { FormItemLayoutPage,SearchFormLayout,SearchBtnLayout } from '../../config/config';
import styles from '../../theme/table.less';
import { statusType } from '../../utils/seletLocalData'

const FormItem = Form.Item;
const Option = Select.Option;
@Form.create()

@connect(({ dispute, loading }) => ({
  dispute,
  loading: loading.effects['dispute/list'],
}))
class DisputeSearch extends Component{
  constructor(props) {
    super(props);
    this.state={
      isLoading:false,
    }
  }

  componentWillMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'dispute/getType',
      payload:{
        "page": 1,
        "pageSize": 30,
        "type": "accept_dispute_matter_type",
      },
    })
  }

  handleSearch = () => {
    const { form } = this.props;
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let value = 0;
        for (value in values) {
          if(values[value] === '' || values[value] === undefined) {
            delete values[value];
          }
        }
        this.props.getSearchData(values);
      }
    });
  }

  restSearchForm = () => {
    this.props.form.resetFields();
    this.props.getSearchData({});
  }

  render() {
    const { isLoading } = this.state;
    const { dispute:{typeData}, loading} = this.props;
    const matterType = typeData.map(d => <Option key={d.k}>{d.val}</Option>);
    const status = statusType.map(d => <Option key={d.key}>{d.value}</Option>);
    const { getFieldDecorator } = this.props.form;
    return (
      <Card bordered={false} title="快速查询">
        <Row gutter={24}>
          <Form className={styles.searchBox}>
            <Row>
              <Col {...SearchFormLayout}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="编号"
                >
                  {getFieldDecorator('number', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input style={{ width: '100%' }} placeholder="请输入编号" />,
                    )}
                </FormItem>
              </Col>
              <Col {...SearchFormLayout}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="当事人"
                >
                  {getFieldDecorator('name', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input style={{ width: '100%' }} placeholder="请输入姓名" />,
                    )}
                </FormItem>
              </Col>
              <Col {...SearchFormLayout}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="事件类别"
                >
                  {getFieldDecorator('matterType', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Select placeholder="请选择事项类别">
                        {matterType}
                      </Select>
                    )}
                </FormItem>
              </Col>
              <Col {...SearchFormLayout}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="状态"
                >
                  {getFieldDecorator('status', {
                    rules: [{
                      validator: this.validateToNextPassword,
                    }],
                  })(
                    <Select placeholder="请选择事项状态">
                      {status}
                    </Select>
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row >
              <Col {...SearchBtnLayout}>
                <div
                  style={{ margin: 0 }}
                  className={styles.btnBox}
                >
                  <Button onClick={this.restSearchForm}>重置</Button>
                  <Button onClick={this.handleSearch} type="primary" icon="search" loading={loading&&isLoading}>搜索</Button>
                </div>
              </Col>
            </Row>
          </Form>
        </Row>
      </Card>
    )
  }
}

export default DisputeSearch;
