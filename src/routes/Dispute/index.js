import React,{Component} from 'react';
import { connect } from 'dva';
import { Button,Form, Row, Col, Card , Modal ,Dropdown ,Icon ,message} from 'antd';
import * as moment from 'moment';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { routerRedux } from 'dva/router';
import ToolBar from '../../components/ToolBar';
import NestedTable from './NestedTable';
import Concluded from './Concluded';
import DisputeSearch from './DisputeSearch';

const confirm = Modal.confirm;
const FormItem = Form.Item;
@Form.create()
@connect(({ dispute, loading }) => ({
  dispute,
  loading: loading.effects['dispute/list'],
  deleteLoading:loading.effects['dispute/deleteInfo'],
  concludedLoading:loading.effects['dispute/handler'],
}))
class Index extends Component{
    constructor(props) {
        super(props);
        this.state = {
          selectedRowKeys: [],
          selectedRows:[],
          searchData: {},
          visible:false,
          concludedData:{},
          editable:false,
          handleOverData:{},
        }
    }

    componentWillMount(){
        const { dispatch } = this.props;
        const { pagination } = this.props.dispute;
        const { pageNum,pageSize } = pagination;
        dispatch({
            type: 'dispute/list',
            payload:{
              page:pageNum,
              pageSize,
            },
        });

    }

    onSelectChange = (selectedRowKeys,selectedRows) =>{
      this.setState({
        selectedRowKeys,
        selectedRows,
      });
    }

    getSearchData = (searchData) => {// 获取用户要搜索的信息
      const { dispatch,dispute:{pagination} } = this.props;
      const { page,pageSize } = pagination;
      this.setState({searchData});
      dispatch({
        type: 'dispute/list',
        payload:{
          ...searchData,
          page,
          pageSize,
        },
      });
    }

    handleDelete = ()=>{
      const {selectedRows} = this.state;
      const rows = selectedRows.filter(i => i.status !== '0');
      if(rows.length){
        message.error('包含有不在受理中的事件');
        return;
      }
      confirm({
        title: '删除',
        content: '是否删除选中信息？',
        cancelText:'取消',
        okText:'确定',
        onOk: () => {
          const { dispatch } = this.props;
          const ids=[];
          selectedRows.forEach(i=>{
            ids.push(i.id);
          })
          dispatch({
            type:'dispute/deleteAllInfo',
            payload:{
              ids,
            },
          })
          this.setState({
            selectedRowKeys:[],
          })
        },
        onCancel() {

        },
      });
    };

    handleMenuModify = (record, index)=>{
      const {status} = record;
      if(status === '0'){
        confirm({
          title: '编辑',
          content: '是否修改当前信息？',
          cancelText:'取消',
          okText:'确定',
          onOk: () => {
            this.props.dispatch(routerRedux.push(`/information/pageForm/${record.id}`));
            this.setState({
              selectedRowKeys:[],
            })
          },
          onCancel() {

          },
        });
      }
      else{
        message.error('该事件无法编辑');
      }
    }

    handleMenuDelete = (record)=>{
      if(record.status !== '0'){
        message.error('不在受理中的事件无法删除');
        return;
      }
      confirm({
        title: '删除',
        content: '是否删除当前信息？',
        cancelText:'取消',
        okText:'确定',
        onOk: () => {
          const { dispatch } = this.props;
          dispatch({
            type:'dispute/deleteInfo',
            payload:{
              id:record.id,
            },
          })
        },
        onCancel() {

        },
      });
    }

    handleMenuHandlover=(record,index)=>{
      const { id } = record;
      const { dispatch } = this.props;
      const nowTime = new moment();
      if (record.status != 0 && record.status != 1) {
        message.error('只有受理和待办的事件才可交办')
      } else if(moment(record.handleDeadline).isBefore(nowTime)){
        message.error('办理期限已过，无法进行交办');
      } else if (record.status == 0) {
        dispatch(routerRedux.push({
          pathname: `/information/handover/dispute/${id}`,
        }))
      } else if (record.status == 1) {
        dispatch(routerRedux.push({
          pathname: `/information/handoverEdit/dispute/${id}`,
        }))
      }
    }

    handleMenuConcluded = (record)=>{
      const { dispatch } = this.props;
      const { status } = record;
      if(status === '1'||status === '2'){
        this.setState({
          concludedData:record,
          visible:true,
          editable:false,
        })
        dispatch({
          type:'dispute/saveConcluded',
          payload:{
            handleOverData:{},
          },
        })
      } else if(status === '3'){
        this.setState({
          concludedData:record,
          visible:true,
          editable:true,
        })
        dispatch({
          type:'dispute/getConcluded',
          payload:{
            data:record,
          },
        })
      } else if(status === '0'){
        message.error('受理中的信息无法办结');
      } else if(status === '4'){
        message.error('已经批示的事件无法办结');
      }
    }

    checkConcludedOk = (data) => {
      const that = this;
      if (JSON.stringify(data) === '{}') {
        this.handleConcludedOK(data);
      } else {
        confirm({
          title: '办结',
          content: '是否要保存当前操作',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            that.handleConcludedOK(data);
          },
          onCancel() {

          },
        });
      }
    }

    handleConcludedOK = (data)=>{
      const { dispatch } = this.props;
      const { concludedData,editable } = this.state;
      if(editable){
        this.setState({
          visible:false,
        })
        return;
      }
      data.number = concludedData.number;
      data.acceptId = concludedData.id;
      data.handleDeadline = concludedData.handleDeadline;
      dispatch({
        type:'dispute/handler',
        payload:{
          data,
        },
      });

      this.setState({
        visible:false,
      })
    }

    handleConcludedCancle = ()=>{
      this.setState({visible: false});
    }

    handleFileDelete=(id)=>{
      const {dispatch} = this.props;
      dispatch({
        type:'dispute/deleteFile',
        payload:{
          id,
        },
      })
    }

    acceptForm = ()=>{
      this.props.dispatch(routerRedux.push('/information/pageForm/-1'));
    }

    turnPage = (e)=>{
      const {current,pageSize} = e;
      const { dispatch } = this.props;
      dispatch({
        type:'dispute/list',
        payload:{
          page:current,
          pageSize,
        },
      });
    };

    handleUpload = (file) => {
      const param = new FormData();
      const { dispatch } = this.props;
      param.append('file', file.file);
      dispatch({
        type: 'dispute/uploadfile',
        payload: param,
      });
    }

    download =()=>{
      const { dispute:{handleOverData},dispatch } = this.props;
      dispatch({
        type:'dispute/downloadFile',
        payload:{
          id:handleOverData.writtenId,
        },
      })
    }

    render(){
        const {selectedRowKeys, visible, concludedData, editable} = this.state;
        const hasSelected = selectedRowKeys.length > 0;
        const {dispute:{dataList:{data},pagination,handleOverData},loading, deleteLoading, concludedLoading} = this.props;
        const rowSelection ={
          selectedRowKeys,
          onChange:this.onSelectChange,
        };
        const nestedTableprops = {
          rowSelection,
          data,
          handleMenuModify:this.handleMenuModify,
          handleMenuDelete:this.handleMenuDelete,
          handleMenuConcluded:this.handleMenuConcluded,
          handleMenuHandlover:this.handleMenuHandlover,
          turnPage:this.turnPage,
          loading:loading||deleteLoading||concludedLoading,
          pagination,
        };
        const BtnList = [{
          icon: 'plus',
          name: '受理',
          type: '',
          key: '1',
          click: this.acceptForm,
        }];
        const concludedprops = {
          editable,
          visible,
          concludedData,
          confirmLoading:concludedLoading,
          handleOverData,
          handleOk:this.checkConcludedOk,
          handleCancel:this.handleConcludedCancle,
          handleFileDelete:this.handleFileDelete,
          download:this.download,
        }

        return(
          <PageHeaderLayout>
            <DisputeSearch getSearchData={this.getSearchData} />
            <Card bordered={false} style={{marginTop:20}}>
              <Row type="flex" justify="end" >
                <Col>
                  <span style={{ marginRight: 8 }}>
                    {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
                  </span>
                  {hasSelected? <Button type='danger'  onClick={this.handleDelete} loading={deleteLoading} icon='close'>删除</Button>:''}
                </Col>
                <Col>
                  <ToolBar BtnList={BtnList} />
                </Col>
              </Row>
              <NestedTable {...nestedTableprops} searchData={this.state.searchData} />
            </Card>
            <Concluded {...concludedprops} />
          </PageHeaderLayout>
        );
    }
}

export default Index;
