import React, { Component } from 'react';
import * as moment from 'moment';
import { Select, DatePicker, Row,Cascader, Col,Form, Card, Input } from 'antd';
import { gender, nations,place } from '../../utils/seletLocalData';
import {changeToString,changeToArray,getNowFormatDate} from '../../utils/utils'
import {phoneRegular} from '../../utils/regular'


const FormItem = Form.Item;
const {Option} = Select;

@Form.create()
class BaseInfoForm extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  chekForm() {
    this.props.form.validateFields((err) => {
        if (!err) {
          const data = this.props.form.getFieldsValue();
          data.householdRegisterPlace=changeToString(data.householdRegisterPlace);
          data.birthday=getNowFormatDate(data.birthday);
          this.props.submitAllForm(data);
        }
      },
    );
  }
  disabledDate=(current)=>{
    return current > moment().endOf('day');
  }
  handleName=(rule, value, callback)=>
  {
    if(!rule.pattern.test(value))
    {
      callback('请输入中文或者英文');
    }
    if(value.length>rule.max)
    {
      callback(`请不要超过${rule.max}`);
    }
    callback()
  }
  render() {
    const data = this.props;
    const {selectTion,loading}=data;
    const dateFormat = 'YYYY-MM-DD';
    const nationsOptions=nations.map((current)=>{
      return <Option key={current.name}>{current.name}</Option>
    })
    const genderOptions = gender.map(current => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const educationOptions = selectTion.map(current => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
      xs: { span: 24 },
      sm: { span: 7 },
      },
      wrapperCol: {
      xs: { span: 24 },
      sm: { span: 17 },
      },
      };
    return (
      <div>
        <Card  loading={loading} title="基本信息" bordered={false} style={{ marginBottom: '20px' }}>
          <Form>
            <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
              <Col span={10}>
                <FormItem {...formItemLayout} label="身份证号">
                  {getFieldDecorator('idCard', {
                    initialValue: data.idCard || '',
                    rules:[{
                      required:true,
                    }],
                  })(
                    <Input disabled />,
                  )}
                </FormItem>
              </Col>
              <Col offset={2} span={10}>
                <FormItem {...formItemLayout} label="姓名">
                  {getFieldDecorator('name', {
                    initialValue: data.name || '',
                    rules: [{
                      required: true,
                      validator:this.handleName,
                      max:60,
                      pattern:/^[a-zA-Z\u4e00-\u9fa5]+$/,
                    }],
                  })(
                    <Input  />
                  )}
                </FormItem>

              </Col>
            </Row>
            <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
              <Col span={10}>
                <FormItem {...formItemLayout} label="文化程度">
                  {getFieldDecorator('degreeEducation', {
                    initialValue: data.degreeEducation,
                    rules: [{
                      required: true,
                      message: '请选择文化程度',
                    }],
                  })(
                    <Select >
                      {educationOptions}
                    </Select>,
                  )}
                </FormItem>
              </Col>
              <Col offset={2} span={10}>
                <FormItem {...formItemLayout} label="性别">
                  {getFieldDecorator('gender', {
                    initialValue: data.gender || '',
                    rules: [{
                      required: true,
                      message: '请选择性别',
                    }],
                  })(
                    <Select>
                      {genderOptions}
                    </Select>,
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
              <Col span={10}>
                <FormItem {...formItemLayout} label="民族">
                  {getFieldDecorator('nation', {
                    initialValue: data.nation || '',
                    rules: [{
                      required: true,
                      message: '请选择民族',
                    }],
                  })(
                    <Select
                      showSearch
                      placeholder='请选择您要查询的民族'
                    >
                      {nationsOptions}
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col offset={2} span={10}>
                <FormItem {...formItemLayout} label="出生日期">
                  {getFieldDecorator('birthday', {
                    initialValue: moment(data.birthday||moment(), dateFormat),
                    rules: [{
                      required: true,
                      message: '请选择出生日期',
                    }],
                  })(
                    <DatePicker  disabledDate={this.disabledDate} style={{ width: '100%' }} />,
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
              <Col span={10}>
                <FormItem {...formItemLayout} label="籍贯">
                  {getFieldDecorator('householdRegisterPlace', {
                    initialValue: changeToArray(data.householdRegisterPlace )|| [],
                    rules: [{
                      required: true,
                      message: '请输入籍贯',
                    }],
                  })(
                    <Cascader  options={place} placeholder="选择地址" />

                  )}
              </FormItem>
              </Col>
              <Col offset={2} span={10}>
                <FormItem {...formItemLayout} label="联系方式">
                  {getFieldDecorator('phone', {
                    initialValue: data.phone || '',
                    rules: [{
                      required: true,
                      message: '请填写正确的电话号码',
                      pattern: phoneRegular,
                    }],
                  })(
                    <Input />
                  )}
                </FormItem>
              </Col>
            </Row>
          </Form>
        </Card>
      </div>);

  }
}
export default BaseInfoForm;
