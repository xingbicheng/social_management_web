import React , {Component} from 'react';
import { Row,Col, Modal, Form,message, Input} from 'antd';
import {idCardRegular} from '../../utils/regular'

const FormItem = Form.Item;
const {Search} = Input;

@Form.create()
class PersonModal extends Component{
    componentDidMount(){
      this.props.parentprops.onRef(this)
    }
    handleCancel=()=>
    {
      this.props.parentprops.handleCancel();
      this.props.form.resetFields();
    }
    handleOk=()=>
    {
    this.props.form.validateFields((err) => {
      if (!err) {
              if(this.props.form.getFieldValue('name'))
              {
                const idCard=this.props.form.getFieldValue('idCard');
                this.props.parentprops.handleOk(idCard);
                this.props.parentprops.handleCancel();
              }
              else
              {
                message.error('请点击右侧搜寻名称');
              };
            }
          },
        );
    }
    chekIdCard=()=>
    {
      this.props.form.validateFields((err) => {
        if (!err) {
      const idCard=this.props.form.getFieldValue('idCard');
      this.props.parentprops.seachIdCard(idCard);
        }})
    }
render(){
    const formItemLayout = {
    labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
    },
    wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
    },
    };
const { getFieldDecorator } = this.props.form;
const {title,name,isVisible} = this.props.parentprops
return(
  <Modal
    title={title}
    visible={isVisible}
    onOk={this.handleOk}
    onCancel={this.handleCancel}
  >
    <Form >
      <FormItem
        {...formItemLayout}
        label="身份证号"
      >
        <Row type="flex" justify="center" >
          <Col span={24}>
            {getFieldDecorator('idCard', {
              rules: [{
              required: true,pattern:idCardRegular, message: '请填写正确的身份证号码',
              }],
              })(
                <Search placeholder="请填写您的身份证号码" enterButton onSearch={this.chekIdCard} />
          )}
          </Col>
        </Row>
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="姓名"
      >
        <Row type="flex" justify="center" >
          <Col span={24}>
            {getFieldDecorator('name', {
             initialValue:name,
        })(
          <Input disabled style={{ width:'100%' }} />
        )}
          </Col>
        </Row>
      </FormItem>
    </Form>
  </Modal>
);
}
}
export default PersonModal;

