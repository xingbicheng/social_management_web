import React , {Component} from 'react';
import {  Modal, Form, Select, Input} from 'antd';
import { quarters } from '../../../../utils/seletLocalData';

const FormItem = Form.Item;
const {Option} = Select;
const { TextArea } = Input;

@Form.create()
class EduModal extends Component{
    constructor(props){
        super(props);
        this.state =
        {
        }
      }
        componentDidMount(){
          this.props.parentprops.onRef(this)
        }
        handleCancel=()=>
        {
          this.props.parentprops.handleCancel();
          this.props.form.resetFields();
        }
        handleOk=()=>
        {
        this.props.form.validateFields((err) => {
          if (!err) {
                let data =this.props.form.getFieldsValue();
                const {id}=this.props.parentprops.rowList
                data={...data,id};
                this.props.parentprops.handleOk(data);
                this.props.parentprops.handleCancel();
                this.props.form.resetFields();
                }
              },
            );
        }
        handleYear=(rule, value, callback)=>
        {
          const myDate= new Date();
          const year = myDate.getFullYear();
          if(value-''>year)
          {
            callback('年份不能大于今年,请重新填写');
          }
          if(!rule.pattern.test(value))
          {
            callback('请输入正确年份')
          }
          callback()
        }
render(){
  const {helperName}=this.props;
  const{rowList}=this.props.parentprops;
    const quartersOptions = quarters.map((current) => {
        return <Option key={current.name}>{current.name}</Option>;
      });
      const helperNameOptions = helperName.map((current) => {
        return <Option key={current.name}>{current.name}</Option>;
      });
      const formItemLayout = {
        labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
        },
        wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
        },
        };
const { getFieldDecorator } = this.props.form;
return(
  <Modal
    title="新增帮教情况"
    visible={this.props.parentprops.isVisible}
    onOk={this.handleOk}
    onCancel={this.handleCancel}
  >

    <Form >
      <FormItem
        {...formItemLayout}
        label="帮教人姓名"
      >
        {getFieldDecorator('teacherName',
         {
          initialValue:rowList.teacherName ||'',
        rules: [{
            required:true,
            message: '请选择帮教人姓名',
        }],
        })(
          <Select>
            {helperNameOptions}
          </Select>
        )}
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="年度"
      >
        {getFieldDecorator('year', {
          initialValue:rowList.year || '',
        rules: [{
        required: true,
        validator:this.handleYear,
        pattern:/^[0-9]{4}$/,
        }],
        })(
          <Input placeholder='请输入四位数的年度,例如:2012' style={{ width: '100%' }} />
        )}
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="季度"
      >
        {getFieldDecorator('quarter', {
          initialValue:rowList.quarter || '一季度',
        rules: [{
        required: true, message: '请选择季度',
        }],
        })(
          <Select>
            {quartersOptions}
          </Select>

        )}
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="帮教详情"
      >
        {getFieldDecorator('content', {
          initialValue:rowList.content || '',
        rules: [{
        required: true,
        whitespace:true,
        message: '请填写帮教详情,不能超过200字',max:200,
        }],
        })(
          <TextArea  placeholder='请填写帮教情况,不要超过200字' style={{ width: '100%' }} maxLength='200' rows={4} />
        )}
      </FormItem>
      
      <FormItem
        {...formItemLayout}
        label="备注"
      >
        {getFieldDecorator('memo',
         {
          initialValue:rowList.memo || '',
        rules: [{
            max:200,
            whitespace:true,
            message: '请不要输入纯空格,并且不能超过200字',
        }],
        })(
          <TextArea placeholder='请不要超过200字' rows={4} maxLength="200" style={{ width: '100%' }} />
        )}
      </FormItem>
    </Form>
  </Modal>
);
}
}
export default EduModal;

