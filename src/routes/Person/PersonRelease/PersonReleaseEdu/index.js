import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import {  Card,Row,Form,Button,Modal,Tabs,Col } from 'antd';
import { connect } from 'dva';
import AutoList from '../../../../components/List/List';
import ModalForm from './EduModal';
import ToolBar from '../../../../components/ToolBar';

const {TabPane} = Tabs;

const {confirm} = Modal;

@Form.create()
@connect(({ releaseeducation, loading }) => ({
    releaseeducation,
    loading: loading.effects['releaseeducation/listpage'],
    delLoading:loading.effects['releaseeducation/delAllEdu'],
}))
class ReleaseEdu extends Component{
    constructor(props){
        super(props);
        this.state = {
            number:'',
            parentprops :{
                isVisible:this.props.releaseeducation.isVisible,
                rowList:{},
                doDispatch:'add',
            },
        }
    }
    
    componentWillMount(){
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseEdu/:id/:idCard').exec(this.props.history.location.pathname);
        const { releaseeducation:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'releaseeducation/listpage',
            payload:{
                page,
                pageSize,
                releasePersonId:match[1],
            },
        });
        this.props.dispatch({
            type:'releaseeducation/getHelperList',
            payload:{
                releasePersonId:match[1],
            },
        });
        
        this.props.dispatch({
            type:'releaseeducation/updateState',
            payload:{ releasePersonId:match[1]},
        })
    }
    componentWillReceiveProps(nextprops)
    {
        const {rowList,isVisible}=nextprops.releaseeducation;
        this.setState({
            parentprops: {
                ...this.state.parentprops,
                isVisible,
                rowList,
            },
           
    })
    
    }
    onRef =(ref) => {
        this.ModalForm = ref
    }
    handleCancel=()=>
    {
        this.props.dispatch({
            type:'releaseeducation/updateState',
            payload:{isVisible:false,rowList:{}},

        })
    }
    
    handleOk=(data)=>{
        const {releasePersonId}=this.props.releaseeducation;
        let type='releaseeducation/addEdu'
        const {doDispatch} =this.props.releaseeducation;
        if(doDispatch==='edit')
        {
            type='releaseeducation/editEdu';
        }
        this.props.dispatch({
            type,
            payload:{...data,releasePersonId},
        })
    }
    addData =()=>{
        const {helperName}=this.props.releaseeducation;
        if(helperName.length===0)
        {
            confirm({
                title: '提示',
                content: '请您先添加帮教责任人',
                okText:'确定',
                cancelText:'取消',
            });
        }
        else{
            this.props.dispatch({
                type:'releaseeducation/updateState',
                payload:{isVisible:true,doDispatch:'add'},
            })
        }
       
    }
    turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch } = this.props;
        dispatch({
            type:'releaseeducation/listpage',
            payload:{
                page:current,
                pageSize,
            },
        });
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const data=record;
                data.releasePersonId=than.props.releasePersonId;
                than.setState({
                    parentprops: {
                        ...than.state.parentprops,
                        doDispatch:'edit',
                    },
            })
            than.props.dispatch({
                    type:'releaseeducation/updateState',
                    payload:{rowList:{...record},
                    doDispatch:'edit',
                    isVisible:true},
                });
                },
            });

        }
    }
    // 删除
    delete=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'releaseeducation/delEdu',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    deleteSome=()=>
    {
        const idList=this.props.releaseeducation.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'releaseeducation/delAllEdu',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });
        
    }
    callback=(key)=>{
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseEdu/:id/:idCard').exec(this.props.history.location.pathname);
        let pathname=`/person/releaseInfo/edit/${match[1]}/${match[2]}`
        switch(key)
        {
          case '2':pathname=`/person/releaseHelper/${match[1]}/${match[2]}`;break;
          case '4':pathname=`/person/releasePolicy/${match[1]}/${match[2]}`;break;
        default:break;
        }
        this.props.dispatch(routerRedux.push({   
          pathname,
      }))
      }
      back=()=>
    {
      const than=this.props;
      confirm({
        title: '返回',
        content: '是否返回到上一页面？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          than.dispatch(routerRedux.push('/person/personrelease'));
        },
    });
    }
    render(){
        const {onRef,handleOk,handleCancel}=this;
        const parentprops={...this.state.parentprops,onRef,handleOk,handleCancel}
        const {helperName}=this.props.releaseeducation;
        const { releaseeducation:{otherList,selectedRowKeys,pagination,multiDeleteButtonIsVisable },delLoading }= this.props;
        const { dispatch,loading } =this.props;
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }];
        const pageProps = {
            dataSource:otherList,
            loading,
            columsList:{
                columns:[ {
                    title: '年度',
                    key: 'year',
                    dataIndex:'year',
                },{
                    title : '季度',
                    key : 'quarter',
                    dataIndex : 'quarter',
                    
                },
               {
                    title: '帮教人姓名',
                    key: 'teacherName',
                    dataIndex:'teacherName',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },
                
                {
                    title : '详情',
                    key : 'content',
                    dataIndex : 'content',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
                {
                    title : '备注',
                    key : 'memo',
                    dataIndex : 'memo',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
               
                ],
                menulistArr:[{
                    key:'handle-1',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-2',
                    name:'删除',
                    menuclick:this.delete,
                }],
            },
            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'releaseeducation/listpage',
                    payload: {
                        pageSize,  
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    number:selectedRows.length,
                })
                  this.props.dispatch({
                    type: 'releaseeducation/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },
        }
        
        return(
          <Card style={{position:'relative'}}>
            <Button style={{position:'absolute',zIndex:'222',right:'5%'}} onClick={this.back}>返回</Button>
            <Row>
              <Col span={24}>
                <Tabs defaultActiveKey='3' onChange={this.callback}>
                  <TabPane tab="刑满释放人员信息" key="1" />
                  {(this.state.releasePersonId!==-1)&&
                  (
                  <TabPane  tab="帮教责任人" key="2" />
)}
                  {(this.state.releasePersonId!==-1)&&(
                    <TabPane tab="帮教情况" key="3">
                      <Card bordered={false} style={{width:'100%'}}>
                        <Row type="flex" justify="end">
                          <Col>
                            <span style={{ marginRight: 8 }}>
                              {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                            </span>
                            {
                                multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                                } 
                          </Col>
                          <Col>
                            <ToolBar BtnList={BtnList} />
                          </Col>
                        </Row>
                        <AutoList {...pageProps}   turnPage={this.turnPage}/>
                        <ModalForm parentprops={parentprops}  helperName={helperName||[]} />
                      </Card>
                    </TabPane>)}
                  {(this.state.releasePersonId!==-1)&&(
                    <TabPane tab="落实政策情况" key="4" />
                )}
                </Tabs>
              </Col>
              <Col />
            </Row>
          </Card>
         
        );
    }
}
export default ReleaseEdu;

