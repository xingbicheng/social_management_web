import React , {Component} from 'react';
import { Modal, Form,Select,Input} from 'antd';
import {phoneRegular} from '../../../../utils/regular'

const FormItem = Form.Item;
const {Option} = Select;
const {TextArea} =Input;
@Form.create()
class ReleaseModal extends Component{
    constructor(props){
        super(props);
        this.state =
        {
        }
      }
        componentDidMount(){
          this.props.parentprops.onRef(this)
        }
        handleCancel=()=>
        {
          this.props.parentprops.handleCancel();
          this.props.form.resetFields();
        }
        handleOk=()=>
        {
        this.props.form.validateFields((err) => {
          if (!err) {
              let data =this.props.form.getFieldsValue();
              const {id}=this.props.parentprops.rowList
              data={...data,id};
                this.props.parentprops.handleOk(data);
                this.props.parentprops.handleCancel();
                this.props.form.resetFields();
                }
              },
            );
        }
render(){
    const {parentprops:{rowList,isVisible},helper_type}=this.props
    const { getFieldDecorator } = this.props.form;
    const TypeOptions=helper_type.map((current)=>{
    return <Option key={current.k}>{current.val}</Option>
    })
    const formItemLayout = {
    labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
    },
    wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
    },
    };
   
return(
  <Modal
    title="新增帮教责任人"
    visible={isVisible}
    onOk={this.handleOk}
    onCancel={this.handleCancel}
  >

    <Form >
      <FormItem
        {...formItemLayout}
        label="姓名"
      >
        {getFieldDecorator('name', {
          initialValue:rowList.name || '',
        rules: [{
        required: true, message: '请填写姓名',
        max:15,
        whitespace:true,
        // pattern:/^[a-zA-Z\u4e00-\u9fa5]+$/,
        }],
        })(
          <Input  placeholder='请输入帮教责任人姓名,不要超过15字' maxLength='15'   style={{ width: '100%' }} />
        )}
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="联系方式"
      >
        {getFieldDecorator('phone', {
          initialValue:rowList.phone || '',
        rules: [{
        required: true, message: '请填写手机号',
        pattern:phoneRegular,
        }],
        })(
          <Input  placeholder='请输入您的手机号码' style={{ width: '100%' }} />

        )}
      </FormItem>
      <FormItem
        {...formItemLayout}
        label="类型"
      >
        {getFieldDecorator('helperType', {
          initialValue:rowList.helperType || '0',
        rules: [{
        required: true, message: '请选择帮教责任人的类型',
        }],
        })(
          <Select>
            {TypeOptions}
          </Select>

        )}
      </FormItem>
      
      <FormItem
        {...formItemLayout}
        label="备注"
      >
        {getFieldDecorator('memo',
         {
          initialValue:rowList.memo || '',
        rules: [{
            max:200,
            message: '请不要输入纯空格信息,同时不能超过200字',
            whitespace:true,
        }],
        })(
          <TextArea  placeholder='请不要超过200字' style={{ width: '100%' }} maxLength='200' rows={5} />
        )}
      </FormItem>
    </Form>
  </Modal>
);
}
}
export default ReleaseModal;

