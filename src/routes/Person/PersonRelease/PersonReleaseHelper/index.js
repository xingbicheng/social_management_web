import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import {  Card,Row,Form,Button,Modal,Tabs,Col } from 'antd';
import { connect } from 'dva';
import AutoList from '../../../../components/List/List';
import ModalForm from './ReleaseModal';
import ToolBar from '../../../../components/ToolBar';
import {findRecord} from '../../../../utils/utils'

const {confirm} = Modal;
const {TabPane} = Tabs;

@Form.create()
@connect(({ releasehelper, loading }) => ({
    releasehelper,
    loading: loading.effects['releasehelper/listpage'],
    delLoading: loading.effects['releasehelper/delAllHelper'],

}))
class ReleaseHelper extends Component{
    constructor(props){
        super(props);
        this.state = {
            number:'',
            parentprops :{
                isVisible:false,
                rowList:{},
                doDispatch:'add',
            },
        }
    }
    
    componentWillMount(){
        const {pathname}=this.props.history.location;
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseHelper/:id/:idCard').exec(pathname);
        const { releasehelper:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'releasehelper/updateState',
            payload: {releasePersonId:match[1]},
        })
        this.props.dispatch({
            type:'releasehelper/listpage',
            payload:{page,pageSize,releasePersonId:match[1]},
        });
    }
    componentWillReceiveProps(nextprops)
    { 
        const {releasehelper:{isVisible,rowList}} =nextprops;
        this.setState({
            parentprops: {
                ...this.state.parentprops,
                isVisible,
                rowList,
            },
    })
    }
    onRef =(ref) => {
        this.ModalForm = ref
    }
    handleCancel=()=>
    {
        this.props.dispatch({
            type:'releasehelper/updateState',
            payload:{isVisible:false},

        })
    }
    
    handleOk=(data)=>{
        const {releasePersonId}=this.props.releasehelper;
        let type='releasehelper/addHelper'
        const {doDispatch} =this.props.releasehelper;
        if(doDispatch==='edit')
        {
            type='releasehelper/editHelper';
        }
        this.props.dispatch({
            type,
            payload:{...data,releasePersonId},
        })
    }
    addData =()=>{
        this.props.dispatch({
            type:'releasehelper/updateState',
            payload:{rowList:{},isVisible:true,doDispatch:'add'},
        });
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const data=record;
                data.releasePersonId=than.state.releasePersonId;
                than.setState({
                    parentprops: {
                        ...than.state.parentprops,
                        doDispatch:'edit',
                    },
            })
            than.props.dispatch({
                    type:'releasehelper/updateState',
                    payload:{rowList:{...record},
                    doDispatch:'edit',
                    isVisible:true},
                });


                },
            });

        }
    }
    back=()=>
    {
      const than=this.props;
      confirm({
        title: '返回',
        content: '是否返回到上一页面？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          than.dispatch(routerRedux.push('/person/personrelease'));
        },
    });
    }
    // 删除
    delete=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'releasehelper/delHelper',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    callback=(key)=>{
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseHelper/:id/:idCard').exec(this.props.history.location.pathname);
        let pathname=`/person/releaseInfo/edit/${match[1]}/${match[2]}`
        switch(key)
        {
          case '3':pathname=`/person/releaseEdu/${match[1]}/${match[2]}`;break;
          case '4':pathname=`/person/releasePolicy/${match[1]}/${match[2]}`;break;
        default:break;
        }
        this.props.dispatch(routerRedux.push({   
          pathname,
      }))
      }
    deleteSome=()=>
    {
        const idList=this.props.releasehelper.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'releasehelper/delAllHelper',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });

    }
    turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch } = this.props;
        dispatch({
            type:'releasehelper/listpage',
            payload:{
                page:current,
                pageSize,
            },
        });
    }
    render(){
        const {onRef,handleOk,handleCancel}=this;
        const parentprops={...this.state.parentprops,onRef,handleOk,handleCancel}
        const {loading,delLoading}=this.props;
        const { releasehelper:{helperList,selectedRowKeys,pagination,multiDeleteButtonIsVisable,helper_type } }= this.props;
        const { dispatch } =this.props;
          const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        const pageProps = {
            dataSource:helperList||[],
            loading,
            columsList:{
                columns:[{
                    title: '姓名',
                    key: 'name',
                    dataIndex:'name',
                },
                {
                    title : '联系方式',
                    key : 'phone',
                    dataIndex : 'phone',
                    render:(record)=>{
                        const data=record.replace(/^(.{3})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },

                },
                {
                    title: '类型',
                    key: 'helperType',
                    dataIndex:'helperType',
                    render:(record,)=>{
                        const data= findRecord(this.props.releasehelper.helper_type,'k',record);
                        return data.val;
                     },
                }, {
                    title : '备注',
                    key : 'memo',
                    dataIndex : 'memo',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },

                },

                ],
                menulistArr:[{
                    key:'handle-1',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-2',
                    name:'删除',
                    menuclick:this.delete,
                }],
            },
            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'releasehelper/listpage',
                    payload: {
                        pageSize,
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                    this.setState({
                        number:selectedRows.length,
                    })
                  this.props.dispatch({
                    type: 'releasehelper/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },
        }
        return(
          <Card style={{position:'relative'}}>
            <Button style={{position:'absolute',zIndex:'222',right:'5%'}} onClick={this.back}>返回</Button>
            <Row>
              <Col span={24}>
                <Tabs defaultActiveKey='2' onChange={this.callback}>
                  <TabPane tab="刑满释放人员信息" key="1" />
                  {(this.state.releasePersonId!==-1)&&
                  (
                  <TabPane  tab="帮教责任人" key="2">
                    <Card bordered={false} style={{width:'100%'}}>
                      <Row type="flex" justify="end">
                        <Col>
                          <span style={{ marginRight: 8 }}>
                            {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                          </span>
                          {
                             multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                        } 
                        </Col>
                        <Col>
                          <ToolBar BtnList={BtnList} />
                        </Col>
                      </Row>
                      <AutoList {...pageProps}   turnPage={this.turnPage} />
                      <ModalForm parentprops={parentprops} helper_type={this.props.releasehelper.helper_type||[]} />
                    </Card>
                  </TabPane>)}
                  {(this.state.releasePersonId!==-1)&&(
                    <TabPane tab="帮教情况" key="3" />
                    )}
                  {(this.state.releasePersonId!==-1)&&( 
                    <TabPane tab="落实政策情况" key="4" />
                )}
                </Tabs>
              </Col>
              <Col />
            </Row>
          </Card>
        );
    }
}
export default ReleaseHelper;

