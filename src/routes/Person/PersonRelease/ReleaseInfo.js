import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import * as moment from 'moment';
import { Button, Row,Col,Tabs, Modal, Form,message, Select, DatePicker, Card, Input } from 'antd';
import { connect } from 'dva';
import BaseForm from '../BaseInfoForm'
import {getNowFormatDate,deleteSpace} from '../../../utils/utils'
import {bloods} from '../../../utils/seletLocalData'
import styles from '../../../components/PageInfo/index.less';


const FormItem = Form.Item;
const {TabPane} = Tabs;
const {confirm} = Modal;
const {TextArea} =Input;
const Option =Select.Option;
const myDate=new Date();
const dateNow=myDate.toLocaleDateString();
@Form.create()
@connect(({ personrelease,loading }) => ({
    personrelease,
    editLoading:loading.effects['personrelease/editRelease'],
    addLoading:loading.effects['personrelease/addRelease'],
    loading: loading.effects['personrelease/getList'],
}))

class ReleaseInfo extends Component{
    constructor() {
        super();
        this.state = {
          message:'确定后将不能在本页面进行修改,需返回上以页面',
        };
      }
      componentDidMount()
      {
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseInfo/:type/:id/:idCard').exec(this.props.history.location.pathname);
        this.props.dispatch({
            type:'personrelease/getBase',
            payload:{
                idCard:match[3],
            },
        });
        this.props.dispatch({
          type:'personrelease/updateState',
          payload:{
            doDispatch:match[1],
          },
      });
        if(match[1]==='edit')
        {
          this.props.dispatch({
            type:'personrelease/getList',
            payload:{
                idCard:match[3],
            },
            
        });
        this.setState({
          message:'确定后可在本页面进行修改',
        })
        }
      this.props.dispatch({
        type:'personrelease/getDegree',
      });  
      }
      onRef =(ref) => {
        this.BaseForm = ref
    }
      submitForm=()=>
      {
        const than = this;
        confirm({
          title: '确定',
          content: than.state.message,
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.form.validateFields((err) => {
              if (!err) {
                  than.BaseForm.chekForm();
                    }
                    else{
                      message.error('请检查必填项是否填写正确');
                    }
                  },
                );
          },
      });
      }
      isTeachingSelect=(value)=>
      {
        if(value==='1')
        {
          this.props.dispatch({type:'personrelease/updateState',
          payload:{isTeaching:true}})
        }
        else
        {
          this.props.dispatch({type:'personrelease/updateState',
          payload:{isTeaching:false}})
        }

      }
      isLowInsuranceSelect=(value)=>
      {
        if(value==='1')
        {
          this.props.dispatch({type:'personrelease/updateState',
          payload:{isLowInsurance:true}})
        }
        else
        {
          this.props.dispatch({type:'personrelease/updateState',
          payload:{isLowInsurance:false}})
        }
      }
      handlesentenceDate=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const firstDrugSeizure= getFieldValue('releaseDate');
        if(value>firstDrugSeizure)
        {
           callback('服刑时间不能晚于刑释时间')
        }
        callback()
      }
      handlereleaseDate=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const firstDrug= getFieldValue('sentenceDate');
        if(value<firstDrug)
        {
           callback('刑释时间不能早于服刑时间')
        }
        callback()
      }
      callback=(key)=>{
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releaseInfo/:type/:id/:idCard').exec(this.props.history.location.pathname);
        let pathname=`/person/releaseHelper/${match[2]}/${match[3]}`
        switch(key)
        {
          case '3':pathname=`/person/releaseEdu/${match[2]}/${match[3]}`;break;
          case '4':pathname=`/person/releasePolicy/${match[2]}/${match[3]}`;break;
        default:break;
        }
        this.props.dispatch(routerRedux.push({   
          pathname,
      }))
      }
   
    submitAllForm=(data)=>
    {
      const {singlePerson,relList,doDispatch}=this.props.personrelease;
      
      let dataList =this.props.form.getFieldsValue();
      dataList={...relList[0]||{},...dataList};
      singlePerson.deathTiem=singlePerson.isDie?getNowFormatDate(singlePerson.deathTiem):null;
      const baseInfo={...singlePerson,...data};
      let formData={...dataList,baseInfo};
      formData.releaseDate=getNowFormatDate(formData.firstDrug);
      formData.sentenceDate=getNowFormatDate(formData.firstDrugSeizure);
      formData.idCard=data.idCard;
      formData=deleteSpace(formData);
      if(relList.length!==0)
      {
        formData.birthday=getNowFormatDate(formData.birthday);
      }
      let type='personrelease/addRelease';
      
      if(doDispatch==='edit')
      {
        type='personrelease/editRelease';
      }
      this.props.dispatch({
        type,
        payload:{
          ...formData,
        },
      })
    }
      back=()=>
      {
        const than=this.props;
        confirm({
          title: '返回',
          content: '是否返回到上一页面？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.dispatch(routerRedux.push('/person/personrelease'));
          },
      });
      }
      render() {
        const baseInfoProps=
        {
          onRef :this.onRef,
          submitAllForm:this.submitAllForm,
        };
        const dateFormat = 'YYYY-MM-DD';
        const {editLoading,addLoading}=this.props;
        const {singlePerson,relList,doDispatch,degree_education,isTeaching,isLowInsurance} =this.props.personrelease;
        const state=relList[0]||{};
        const bloodsOption=bloods.map((current)=>{
          return <Option key={current.name}>{current.name}</Option>
        });
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 15 },
          },
          };
          const textAreaLayout = {
            labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            },
            wrapperCol: {
            xs: { span: 24 },
            sm: { span: 19 },
            },
            };
        return (
          
          <div>
            <Card style={{position:'relative'}}>
              <Button style={{position:'absolute',zIndex:'222',right:'5%'}} onClick={this.back}>返回</Button>
              <Row>
                <Col span={24}>
                  <Tabs defaultActiveKey='1'  onChange={this.callback}>
                    <TabPane tab="刑满释放人员信息" key="1">
                      <BaseForm {...baseInfoProps} {...singlePerson}  selectTion={degree_education} /> 
                      <Card title="刑满释放人员信息" bordered={false} style={{marginBottom:'20px'}}>
                        <Form >
                          <Row  type="flex" justify="center" style={{marginTop:'20px'}}>  
                            <Col span={10}> 
                              <FormItem {...formItemLayout} label="曽用名">
                                {getFieldDecorator('beforeName', {
                                  initialValue:state.beforeName||'',
                                  rules:[{
                                    // required:true,
                                    message:'请正确填写曾用名',
                                    max:40,
                                    whitespace:true,
                                    // pattern:/^[a-zA-Z\u4e00-\u9fa5]+$/,
                                  }],
                                })(
                                  <Input maxLength="200" placeholder='请输入曾用名,不要超过40个字'  />
                                )}
                              </FormItem>
                            </Col>
                            <Col offset={2} span={10}> 
                              <FormItem {...formItemLayout} label="血型">
                                {getFieldDecorator('blood',{
                                  initialValue:state.blood||'A型',
                                  rules:[{
                                    required:true,
                                    message:'请选择血型',
                                  }],
                                })(
                                  <Select >
                                    {bloodsOption}
                                  </Select>
                              )}
                              </FormItem>
                            </Col>
                          </Row>
                          <Row  type="flex" justify="center"  style={{marginTop:'20px'}}> 
                            <Col span={10}> 
                              <FormItem {...formItemLayout} label="职业">
                                {getFieldDecorator('occupation', {
                                initialValue:state.occupation||'',
                                rules: [{
                                  required:true,
                                  message: '请填写职业',
                                  max:200,
                                  whitespace:true,
                                  // pattern:/^[a-zA-Z\u4e00-\u9fa5]+$/,
                                  }],
                                })(
                                  <Input maxLength="200"  placeholder='请输入您的职业,不要超过200个字'  />
                                )}
                              </FormItem>
                            </Col> 
                            <Col offset={2} span={10}> 
                              <FormItem {...formItemLayout} label="接受来源">
                                {getFieldDecorator('receiveSource', {
                                  initialValue:state.receiveSource||'',
                                  rules:[{
                                    required:true,
                                    message:'请填写接受来源',
                                    max:200,
                                    whitespace:true,
                                  }],
                                })(
                                  <Input placeholder="请填写收入来源" maxLength="200"  />
                                )}
                              </FormItem>
                            </Col>
                          </Row>
                          <Row  type="flex" justify="center" style={{marginTop:'20px'}}>  
                            <Col span={10}> 
                              <FormItem {...formItemLayout} label="重点帮扶对象">
                                {getFieldDecorator('isImportant', {
                                  initialValue:state.isImportant||'0',
                                  rules:[{
                                    required:true,
                                    message:'请选择是否为重点帮扶对象',
                                  }],
                                })(
                                  <Select style={{ width:'100%' }}>
                                    <Option value="1">是</Option>
                                    <Option value="0">否</Option>
                                  </Select>
                                )}
                              </FormItem>
                            </Col> 
                            <Col offset={2} span={10}> 
                              <FormItem {...formItemLayout} label="符合低保条件">
                                {getFieldDecorator('isLowInsurance', {
                                  initialValue:state.isLowInsurance||'0',
                                  rules:[{
                                    required:true,
                                    message:'请选择是否符合低保条件',
                                  }],
                                })(
                                  <Select  onSelect={this.isLowInsuranceSelect} style={{ width:'100%' }}>
                                    <Option value="1">是</Option>
                                    <Option value="0">否</Option>
                                  </Select>
                                )}
                              </FormItem>
                            </Col>
                          </Row>
                          <Row  type="flex" justify="center"  style={{marginTop:'20px'}}> 
                            <Col span={10}>  
                              <FormItem {...formItemLayout} label="重点帮教对象">
                                {getFieldDecorator('isTeaching', {
                                initialValue:state.isTeaching||'0',
                                rules:[{
                                  required:true,
                                  message:'请选择是否为帮教对象',
                                }],
                                  })(
                                    <Select   onSelect={this.isTeachingSelect} style={{ width:'100%' }}>
                                      <Option value="1">是</Option>
                                      <Option value="0">否</Option>
                                    </Select>
                                )}
                              </FormItem>
                            </Col>
                            <Col offset={2} span={10}>  
                              {isTeaching&&(
                                <FormItem {...formItemLayout} label="帮教对象类型">
                                    {getFieldDecorator('teachingType', {
                                      initialValue:state.teachingType||'',
                                      rules:[{
                                        required:true,
                                        message:'请填写重点帮教对象类型',
                                        max:200,
                                      }],
                                        })(
                                          <Input  />
                                      )}
                                </FormItem>
          )   }
                            </Col>
                          </Row>
                        
                          <Row  type="flex" justify="center"  style={{marginTop:'20px'}}>  
                            <Col span={10}> 
                              <FormItem {...formItemLayout} label="服刑时间">
                                {getFieldDecorator('sentenceDate',{
                                  initialValue:moment(state.sentenceDate||dateNow,dateFormat),
                                  rules:[{
                                    required:true,
                                    validator:this.handlesentenceDate,
                                  }],
                                })(
                                  <DatePicker style={{width:'100%'}} />
                              )}
                              </FormItem>
                            </Col> 
                            <Col offset={2} span={10}> 
                              <FormItem {...formItemLayout} label="刑释时间">
                                {getFieldDecorator('releaseDate', {
                                  initialValue:moment(state.releaseDate||dateNow,dateFormat),
                                  rules:[{
                                    required:true,
                                    validator:this.handlereleaseDate,
                                  }],
                                })(
                                  <DatePicker  style={{width:'100%'}} />
                                )}
                              </FormItem>
                            </Col>
                          
                          </Row>
                          <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                            <Col span={23}> 
                              <FormItem {...textAreaLayout} label="服刑原因">
                                {getFieldDecorator('sentenceReason', {
                                  initialValue:state.sentenceReason||'',
                                  rules: [{
                                    max:200,
                                    required:true,
                                    message:'请填写服刑原因,且不能超出200字',
                                    whitespace:true,
                                    }],
                                  })(
                                    <TextArea maxLength="200"  placeholder='请输入服刑原因,不要超过两百字'  rows={4} />
                                  )}
                              </FormItem>
                            </Col>
                          </Row>
                          <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                            <Col span={23}> 
                              <FormItem {...textAreaLayout} label="家庭情况">
                                {getFieldDecorator('familySituation', {
                                  initialValue:state.familySituation||'',
                                  rules: [{
                                    max:200,
                                    required:true,
                                    whitespace:true,
                                    message:'请填写家庭情况,且不能超出200字',
                                    }],
                                  })(
                                    <TextArea maxLength="200"  placeholder="请输入家庭情况,不要超过200字"  rows={4}  />
                                  )}
                              </FormItem>
                            </Col>
                          </Row>
                          {isLowInsurance&&(
                          <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                            <Col span={23}> 
                              <FormItem {...textAreaLayout} label="低保落实情况">
                                {getFieldDecorator('implementationLowInsurance',{
                                  initialValue:state.implementationLowInsurance||'',
                                  rules:[{
                                    message:'请填写低保落实情况,不能超过200字',
                                    max:200,
                                    required:true,
                                  }],
                                })(
                                  <TextArea  placeholder="请填写低保落实情况,且请不要超过200字" rows={4} style={{ width: '100%' }} />
                              )}
                              </FormItem>
                            </Col>
                          </Row>) }
                          <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                            <Col span={23}> 
                              <FormItem  {...textAreaLayout} label="备注">
                                {getFieldDecorator('memo', {
                                  initialValue:state.memo||'',
                                  rules: [{
                                    max:200,
                                    whitespace:true,
                                    message:'不能超出200字',
                                    }],
                                  })(
                                    <TextArea maxLength="200" placeholder="请不要超过200字"  rows={4} style={{ width: '100%' }} />
                                  )}
                              </FormItem>
                            </Col>
                          </Row>
                        </Form>
                        <Row  type="flex" justify="center">
                          <div className={styles.btnBox}>
                            <div className={styles.saveAndreturn}>
                              <Button onClick={this.submitForm} loading={(doDispatch==='edit')?editLoading:addLoading} icon="save" type="primary">保存</Button>
                            </div>
                          </div>
                        </Row>
                      </Card>
                    </TabPane>
                    {(doDispatch==='edit')&&
                    (
                    <TabPane  tab="帮教责任人" key="2" />)}
                    {(doDispatch==='edit')&&(
                    <TabPane tab="帮教情况" key="3" />)}
                    {(doDispatch==='edit')&&( 
                    <TabPane tab="落实政策情况" key="4" />)}
                  </Tabs>
                </Col>
                <Col />
              </Row>
            </Card>
          
          </div>
        );
    }
}
export default ReleaseInfo;

