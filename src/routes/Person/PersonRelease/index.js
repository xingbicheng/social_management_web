import React,{Component} from 'react';
import {  Card,Row,Form,Button,Modal,Col } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/List/List';
import ModalForm from '../PersonModal';
import SearchForm from './ReleaseSearch';
import ToolBar from '../../../components/ToolBar';

const {confirm} = Modal;

@Form.create()
@connect(({ personrelease, loading }) => ({
    personrelease,
    loading: loading.effects['personrelease/listpage'],
    searchLoading: loading.effects['personrelease/getList'],
    delLoading: loading.effects['personrelease/delAllRelease'],

}))
class ReleasePeople extends Component{
    constructor(props){
        super(props);
        this.state = {
            parentprops :{
                isVisible:false,
                name:'',
                title:'新增刑满释放人员',
            },
        }
    }
    componentWillMount(){
        const { personrelease:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        localStorage.setItem('flag','1');
        this.props.dispatch({
            type:'personrelease/listpage',
            payload:{
                page,
                pageSize,
            },
        });
        
    }
    componentWillReceiveProps(nextprops)
    {
        this.setState({
            parentprops: {
                ...this.state.parentprops,
                isVisible: nextprops.personrelease.isVisible,
                name:nextprops.personrelease.name,
            },
    })
    
    }
    onRef =(ref) => {
        this.ModalForm = ref
    }
    handleCancel=()=>
    {
        this.props.dispatch({
            type:'personrelease/updateState',
            payload:{isVisible:false,name:''},

        })
    }
  
    seachIdCard=(idCard)=>
    {
        this.props.dispatch({
            type:'personrelease/listpage',
            payload:{
                idCard,
            },
        }); 
    }
    handleOk=(idCard)=>{
    this.props.dispatch({
        type:'personrelease/updateState',
        payload:{relList:[],releasePersonId:-1},
    })
    this.props.dispatch(routerRedux.push({   
        pathname: `/person/releaseInfo/add/-1/${idCard}`,
    }))
    }
    addData =()=>{
        this.props.dispatch({
            type:'personrelease/updateState',
            payload:{isVisible:true,name:'',isTeaching:false},
        })
    }
    handleSearch=(data)=>{
        const {dispatch} = this.props;
        const { personrelease:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'personrelease/getList',
            payload:{...data,page,pageSize},
        });
    }
    restSearch =() => {
        const {dispatch} = this.props;
        const { personrelease:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'personrelease/listpage',
            payload:{
                page,
                pageSize,
            },
        });
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const {idCard,id}= record;
                
                than.props.dispatch(routerRedux.push({   
                    pathname: `/person/releaseInfo/edit/${id}/${idCard}`,
                })) 
                },
            });
        }
    }
    // 删除
    delete=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'personrelease/delRelease',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    deleteSome=()=>
    {
        const idList=this.props.personrelease.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'personrelease/delAllRelease',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });    
    }
    turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch } = this.props;
        dispatch({
            type:'personrelease/listpage',
            payload:{
                page:current,
                pageSize,
            },
        });
    }
    render(){
        const {onRef,handleOk,seachIdCard,handleCancel,handleSearch,restSearch}=this;
        const searchProps={handleSearch,restSearch}
        const parentprops={...this.state.parentprops,onRef,handleOk,seachIdCard,handleCancel}
        const { personrelease:{relList,selectedRowKeys,pagination,multiDeleteButtonIsVisable },loading,searchLoading,delLoading }= this.props;
        const { dispatch } =this.props;;
        const pageProps = {
            dataSource:relList,
            loading,
            columsList:{
                columns:[{
                    title: '姓名',
                    key: 'name',
                    dataIndex:'name',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },
                {
                    title : '曾用名',
                    key : 'beforeName',
                    dataIndex : 'beforeName',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
                {
                    title: '性别',
                    key: 'gender',
                    dataIndex:'gender',
                    render:(record)=>
                    {
                        if(record==='0')
                        {
                            return '女';
                        }
                        else return '男';
                    },
                }, {
                    title : '身份证号',
                    key : 'idCard',
                    dataIndex : 'idCard',
                    render:(record)=>{
                        const data=record.replace(/^(.{4})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },
                    
                },
                {
                    title : '是否低保条件',
                    key : 'isLowInsurance',
                    dataIndex : 'isLowInsurance',
                    render:(record)=>{
                        if(record==='1')
                        {
                            return "是"
                        }
                        return "否"
                    },
                },
                {
                    title : '是否重点帮扶对象',
                    key : 'isImportant',
                    dataIndex : 'isImportant',
                    render:(record)=>{
                        if(record==='1')
                        {
                            return "是"
                        }
                        else{
                            return "否"
                        }
                       
                    },
                    
                },
                {
                    title : '是否重点帮教对象',
                    key : 'isTeaching',
                    dataIndex : 'isTeaching',
                    render:(record)=>{
                        if(record==='1')
                        {
                            return "是"
                        }
                        return "否"
                    },
                    
                },
                ],
                menulistArr:[
                    {
                    key:'handle-2',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-3',
                    name:'删除',
                    menuclick:this.delete,
                },
                ],
            },
            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'personrelease/listpage',
                    payload: {
                        pageSize,  
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                    this.setState({
                        number:selectedRows.length,
                    })
                  this.props.dispatch({
                    type: 'personrelease/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },
        }
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        return(
          <PageHeaderLayout>
            <Card title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
              <SearchForm loading={searchLoading} searchProps={searchProps} />
            </Card>
            <Card bordered={false} style={{width:'100%'}}>
              <Row type="flex" justify="center" />
              <Row type="flex" justify="end">
                <Col>
                  <span style={{ marginRight: 8 }}>
                    {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                  </span>
                  {
                multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                } 
                </Col>
                <Col>
                  <ToolBar BtnList={BtnList} />
                </Col>
              </Row>
              <AutoList {...pageProps}  turnPage={this.turnPage} />
              <ModalForm parentprops={parentprops} />
            </Card>
          </PageHeaderLayout>
        );
    }
}
export default ReleasePeople;

