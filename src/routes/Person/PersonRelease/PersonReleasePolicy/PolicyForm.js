import React,{Component} from 'react';
import { Button, Row,Col, Modal, Form,message, Select, DatePicker, Card, Input } from 'antd';
import { connect } from 'dva';
import * as moment from 'moment';
import {getNowFormatDate} from '../../../../utils/utils'

const FormItem = Form.Item;
const {confirm} = Modal;
const {TextArea} =Input;
const {Option} =Select;
@Form.create()
@connect(({ releasepolicy,loading }) => ({
  releasepolicy,
    loading: loading.effects['releasepolicy/getList'],
}))
class PolicyForm extends Component{
    constructor() {
        super();
        this.state = {
          type:0,
          releasePersonId:0,
        };
      }
      componentDidMount()
      {
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/policyForm/:id/:id').exec(this.props.location.pathname);
        if(match[1]!=='-1')
        {
          this.props.dispatch({
            type:'releasepolicy/getPerson',
            payload:{
                id:match[1],
            },
        });
        }
        else{
          this.props.dispatch({
            type:'releasepolicy/updateState',
            payload:{singlePerson:{}},
          })
        }
        this.setState({
          releasePersonId:match[2],
        })
        this.setState({
          type:match[1],
        })
      }
      submitForm=()=>
      {
        const than = this;
        confirm({
          title: '确定',
          content: '确定后将不能在本页面进行修改,需返回上以页面',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.form.validateFields((err) => {
              if (!err) {
                  let type='releasepolicy/addOthers';
                  if(than.state.type!=='-1')
                  {
                    type='releasepolicy/editOthers';
                  }
                  let dataList =than.props.form.getFieldsValue();
                  dataList={...dataList,implementationPolicyDate:getNowFormatDate(dataList.implementationPolicyDate),releasePersonId:than.state.releasePersonId,id:than.state.type}
                  than.props.dispatch({
                    type,
                    payload:{
                      ...dataList,
                    },
                  })
                  than.props.history.goBack();
                    }
                    else{
                      message.error('请检查必填项是否填写正确');
                    }
                  },
                );
          },
      });
      }
      disabledDate=(current)=>{
        return current > moment().endOf('day');
      }
      change=(value)=>
      {
        if(value==='1')
        {
          this.props.dispatch({
            type:'releasepolicy/updateState',
            payload:{isResettlement:true},       
          })
        }
        else
        {
          this.props.dispatch({
            type:'releasepolicy/updateState',
            payload:{isResettlement:false},    
          })
        }

      }
      back=()=>
      {
        const than=this;
        confirm({
          title: '返回',
          content: '是否返回到上一页面？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.history.goBack();
          },
      });
      }
      render() {
        const dateFormat = 'YYYY-MM-DD';
        const {singlePerson,isResettlement} =this.props.releasepolicy;
        const state=singlePerson||{};
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 17 },
          },
          };
          const textAreaLayout = {
            labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            },
            wrapperCol: {
            xs: { span: 24 },
            sm: { span: 19 },
            },
            };
        return (
          <div>
            <Card title="落实政策信息" bordered={false} style={{marginBottom:'20px'}}>
              <Form >
                <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                  <Col span={10}> 
                    <FormItem {...formItemLayout} label="落实政策时间">
                      {getFieldDecorator('implementationPolicyDate', {
                    initialValue:moment(state.implementationPolicyDate||moment(),dateFormat),
                    rules:[{
                      required: true,
                    }],
                    })(
                      <DatePicker  disabledDate={this.disabledDate} style={{ width: '100%' }} />,
                    )}
                    </FormItem>
                  </Col>
                  <Col offset={2}span={10}>
                    <FormItem
                      {...formItemLayout}
                      label="是否安置"
                    >
                      {getFieldDecorator('isResettlement', {
                  initialValue:state.isResettlement || '否',
                rules: [{
                required: true, message: '请选择是否安置',
                }],
                })(
                  <Select onSelect={this.change} style={{ width:'100%' }}>
                    <Option value="1">是</Option>
                    <Option value="0">否</Option>
                  </Select>
              )}
                    </FormItem>
                  </Col>
                </Row>
                {(isResettlement)&&(
                <FormItem
                  {...textAreaLayout}
                  label="安置情况"
                >
                  {getFieldDecorator('resettlement', {
                  initialValue:state.resettlement || '',
                rules: [{
                required: true, message: '请填写安置情况',
                max:200,
                whitespace:true,
                }],
                })(
                  <TextArea placeholder='请填写安置情况,不要超过200字' maxLength="200" style={{ width: '100%' }} rows={4}  />
                )}
                </FormItem>)}
                {(isResettlement)&&(
                <FormItem
                  {...textAreaLayout}
                  label="安置方式"
                >
                  {getFieldDecorator('resettlementMode', {
                  initialValue:state.resettlementMode || '',
                rules: [{
                required: true, message: '请填写安置方式',max:200,whitespace:true,

                }],
                })(
                  <TextArea placeholder='请填写安置方式,不要超过200字' maxLength="200"  style={{ width: '100%' }} rows={4} />
                )}
                </FormItem>)}
           
                <FormItem
                  {...textAreaLayout}
                  label="回归技能培训"
                >
                  {getFieldDecorator('afterTrainingSkills', {
                initialValue:state.afterTrainingSkills || '',
              rules: [{
              required: true,max:200,
              whitespace:true,
              message: '请填写回归技能培训,不能超过200字',
              }],
              })(
                <TextArea placeholder='请填写回归后的技能培训,不要超过200字' maxLength="200" style={{ width: '100%' }} rows={4} />
              )}
                </FormItem>
                <FormItem
                  {...textAreaLayout}
                  label="从业情况"
                >
                  {getFieldDecorator('employmentSituation', {
                initialValue:state.employmentSituation || '',
              rules: [{
              required: true,max:200,
              whitespace:true,
              message: '请填写从业情况,不能超过200字',
              }],
              })(
                <TextArea placeholder='请填写从业情况,不要超过200字' maxLength="200" style={{ width: '100%' }} rows={4} />
      
              )}
                </FormItem>
        
                <FormItem
                  {...textAreaLayout}
                  label="现掌握技能"
                >
                  {getFieldDecorator('nowSkills', {
                initialValue:state.nowSkills || '',
              rules: [{
              required: true,max:200,
              whitespace:true,
              message: '请填写现掌握技能,不能超过200字',
              }],
              })(
                <TextArea placeholder='请填写现掌握技能,不要超过200字' maxLength="200"  style={{ width: '100%' }} rows={4} />
      
              )}
                </FormItem>
                <FormItem
                  {...textAreaLayout}
                  label="监狱技能"
                >
                  {getFieldDecorator('prisonSkills', {
                initialValue:state.prisonSkills || '',
              rules: [{
              required: true,max:200,whitespace:true, message: '请填写监狱技能,不能超过200字',
              }],
              })(
                <TextArea maxLength="200" placeholder='请填写在监狱获得的技能,不要超过200字' style={{ width: '100%' }} rows={4} />
      
              )}
                </FormItem>
                <FormItem
                  {...textAreaLayout}
                  label="备注"
                >
                  {getFieldDecorator('memo',
               {
                initialValue:state.memo || '',
              rules: [{
                  max:200,
                  message: '请不要超过200字',
                  whitespace:true,
              }],
              })(
                <TextArea placeholder='请不要超过200字' maxLength="200" style={{ width: '100%' }} rows={4}  />
              )}
                </FormItem>
              </Form>
              <Row type="flex" justify="center">
                <Col span="6">
                  <Button  onClick={this.back}>返回</Button>
                </Col>
                <Col>
                  <Button type="primary" onClick={this.submitForm}>确认</Button>
                </Col>
              </Row>
            </Card>
          </div>
        );
    }
}
export default PolicyForm;

