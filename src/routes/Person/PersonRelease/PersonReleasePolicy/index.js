import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import {  Card,Row,Form,Tabs,Button,Modal,Col } from 'antd';
import { connect } from 'dva';
import AutoList from '../../../../components/List/List';
import ToolBar from '../../../../components/ToolBar';

const {confirm} = Modal;
const {TabPane} = Tabs;
@Form.create()
@connect(({ releasepolicy, loading }) => ({
    releasepolicy,
    loading: loading.effects['releasepolicy/listpage'],
    delLoading: loading.effects['releasepolicy/delAllPolicy'],

}))
class ReleasePolicy extends Component{
    constructor(props){
        super(props);
        this.state = {
            number:'',
        }
    }
    componentWillMount(){
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releasePolicy/:id/:idCard').exec(this.props.history.location.pathname);
        const { releasepolicy:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'releasepolicy/updateState',
            payload: {releasePersonId:match[1]},
        })
        this.props.dispatch({
            type:'releasepolicy/listpage',
            payload:{
                page,
                pageSize,
                releasePersonId:match[1],
            },
        });
        
        
    }
    back=()=>
    {
      const than=this.props;
      confirm({
        title: '返回',
        content: '是否返回到上一页面？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          than.dispatch(routerRedux.push('/person/personrelease'));
        },
    });
    }
    addData =()=>{
        const {releasePersonId}=this.props.releasepolicy;
        this.props.dispatch(routerRedux.push({   
            pathname: `/person/policyForm/-1/${releasePersonId}`,
        }))
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const {id}=record;
                const {releasePersonId}=than.props.releasepolicy;
                than.props.dispatch(routerRedux.push({   
                    pathname: `/person/policyForm/${id}/${releasePersonId}`,
                }))    
                },
            });

        }
    }
    // 删除
    delete=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'releasepolicy/delPolicy',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    deleteSome=()=>
    {
        const idList=this.props.releasepolicy.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'releasepolicy/delAllPolicy',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });     
    }
    callback=(key)=>{
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/releasePolicy/:id/:idCard').exec(this.props.history.location.pathname);
        let pathname=`/person/releaseInfo/edit/${match[1]}/${match[2]}`
        switch(key)
        {
          case '3':pathname=`/person/releaseEdu/${match[1]}/${match[2]}`;break;
          case '2':pathname=`/person/releaseHelper/${match[1]}/${match[2]}`;break;
        default:break;
        }
        this.props.dispatch(routerRedux.push({   
          pathname,
      }))
      }
      turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch } = this.props;
        dispatch({
            type:'releasepolicy/listpage',
            payload:{
                page:current,
                pageSize,
            },
        });
    }
    render(){
        const { releasepolicy:{otherList,selectedRowKeys,pagination,multiDeleteButtonIsVisable },delLoading }= this.props;
        const { dispatch,loading } =this.props;
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        const pageProps = {
            dataSource:otherList,
            loading,
            columsList:{
                columns:[
                    {
                        title: '落实政策时间',
                        key: 'implementationPolicyDate',
                        dataIndex:'implementationPolicyDate',
                    },{
                    title: '回归技能',
                    key: 'afterTrainingSkills',
                    dataIndex:'afterTrainingSkills',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },
                {
                    title : '从业情况',
                    key : 'employmentSituation',
                    dataIndex : 'employmentSituation',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
                 {
                    title : '现掌握何种技能',
                    key : 'nowSkills',
                    dataIndex : 'nowSkills',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
                {
                    title : '监狱技能',
                    key : 'prisonSkills',
                    dataIndex : 'prisonSkills',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                    
                },
                {
                    title : '是否安置',
                    key : 'isResettlement',
                    dataIndex : 'isResettlement',
                    render:(record)=>{
                        if(record==='1')
                        {
                            return '是';
                        }
                        else {
                            return '否';
                        }
                    },
                    
                },
               
                ],
                menulistArr:[{
                    key:'handle-1',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-2',
                    name:'删除',
                    menuclick:this.delete,
                }],
            },
            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'releasepolicy/listpage',
                    payload: {
                        pageSize,  
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                    this.setState({
                        number:selectedRows.length,
                    })
                  this.props.dispatch({
                    type: 'releasepolicy/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },
        }
        
        return(
          <Card style={{position:'relative'}}>
            <Button style={{position:'absolute',zIndex:'222',right:'5%'}} onClick={this.back}>返回</Button>
            <Row>
              <Col span={24}>
                <Tabs defaultActiveKey='4'  onChange={this.callback}>
                  <TabPane tab="刑满释放人员信息" key="1" />
                  {(this.state.releasePersonId!==-1)&&
                  (
                  <TabPane  tab="帮教责任人" key="2" />)}
                  {(this.state.releasePersonId!==-1)&&(
                    <TabPane tab="帮教情况" key="3" />)}
                  {(this.state.releasePersonId!==-1)&&(
                    <TabPane tab="落实政策情况" key="4">
                      <Card bordered={false} style={{width:'100%'}}>
                        <Row type="flex" justify="end">
                          <Col>
                            <span style={{ marginRight: 8 }}>
                              {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                            </span>
                            {
                                multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                                } 
                          </Col>
                          <Col>
                            <ToolBar BtnList={BtnList} />
                          </Col>
                        </Row>
                        <AutoList {...pageProps}  turnPage={this.turnPage}  />
                      </Card>
                    </TabPane> )}
                </Tabs>
              </Col>
              <Col />
            </Row>
          </Card>
        
        );
    }
}
export default ReleasePolicy;

