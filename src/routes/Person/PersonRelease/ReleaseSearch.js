import React , {Component} from 'react';
import {Row,message, Col, Select, Form, Input, Button } from 'antd';
import styles from '../../../theme/table.less';
import {SearchBtnLayout,FormItemLayoutPage } from '../../../config/config';

const FormItem = Form.Item;

const {Option} = Select;

@Form.create()

class ReleaseSearch extends Component{
  constructor(props) {
    super(props);
    this.state={
    }
  }
  searchInfo=()=>
  {
    this.props.form.validateFields((err) => {
      if (!err) {
      const data=this.props.form.getFieldsValue();
      const {handleSearch}=this.props.searchProps;
      let item=0;
      for(item in data) {
		if(data[item] === '' || data[item] === undefined) {	
	    delete data[item];
      }
    }
    
      handleSearch(data);
    
  }})
  }
  resetDara=()=>
  {
    const {restSearch}=this.props.searchProps;
    this.props.form.resetFields();
    restSearch();
  }
  render() {
    const {loading} =this.props;
    const searchFormLayout={
      xs:{span:24},
      sm:{span:12},
      md:{span:12},
      lg:{span:12},
      xxl:{span:8},
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form className={styles.searchBox}>
          <Row type="flex" justify="left">
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="姓名"
              >
                {getFieldDecorator('name', {
                  rules:[{whitespace:true, message:'请不要输入纯空格'}],
                  })(
                    <Input placeholder='请输入您要查询的姓名' />
                  )}
              </FormItem>
            </Col>
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="身份证号"
              >
                {getFieldDecorator('idCard', {
                rules: [{pattern:/^[0-9a-zA-Z]+$/, message: '只能填写数字或字母',
                 }],
                })(
                  <Input placeholder='请输入您要查询的身份证号' style={{ width:'100%' }} />
                )}
              </FormItem>
            </Col>
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="是否低保"
              >
                {getFieldDecorator('isLowInsurance', {
                  })(
                    <Select placeholder='请选择是否低保' style={{ width:'100%' }}>
                      <Option value="1">是</Option>
                      <Option value="0">否</Option>
                    </Select>
                  )}
              </FormItem>
            </Col> 
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="是否重点帮扶"
              >
                {getFieldDecorator('isImportant', {
                  })(
                    <Select placeholder='选择是否重点帮扶对象' style={{ width:'100%' }}>
                      <Option value="1">是</Option>
                      <Option value="0">否</Option>
                    </Select>
                  )}
              </FormItem>
            </Col>
          </Row>
          <Row >
            <Col {...SearchBtnLayout}>
              <div
                style={{ margin: 0 }}
                className={styles.btnBox}
              >
                <Button onClick={this.resetDara}>重置</Button>
                <Button onClick={this.searchInfo} loading={loading}  type="primary" icon="search">搜索</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

export default ReleaseSearch
