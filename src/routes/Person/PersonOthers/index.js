import React,{Component} from 'react';
import {  Card,Row,Form,Button,Modal,Col } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/List/List';
import ModalForm from '../PersonModal';
import ToolBar from '../../../components/ToolBar';
import {crowdtype} from '../../../utils/seletLocalData'
import SearchForm from './OtherSearch';
import {findRecord} from '../../../utils/utils'

const {confirm} = Modal;
@Form.create()
@connect(({ otherpeople, loading }) => ({
    otherpeople,
    loading: loading.effects['otherpeople/listpage'],
    delLoading:loading.effects['otherpeople/delAllOthers'],
    searchLoading: loading.effects['otherpeople/getList'],
}))
class OtherPeople extends Component{
    constructor(props){
        super(props);
        this.state = {
            number:'',
            parentprops :{
                isVisible:false,
                name:'',
                title:'新增其他人员',
            },
        }
    }
    
    componentWillMount(){
        const { otherpeople:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'otherpeople/listpage',
            payload:{
                page,
                pageSize,
            },
        });
        
    }
    componentWillReceiveProps(nextprops)
    {
        this.setState({
            parentprops: {
                ...this.state.parentprops,
                isVisible: nextprops.otherpeople.isVisible,
                name:nextprops.otherpeople.name,
            },
    })
    
    }
    onRef =(ref) => {
        this.ModalForm = ref
    }
    handleSearch=(data)=>{
        const {dispatch} = this.props;
        const { otherpeople:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'otherpeople/getList',
            payload:{...data,page,pageSize},
        });
        }
    restSearch =() => {
        const {dispatch} = this.props;
        const { otherpeople:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'otherpeople/listpage',
            payload:{
                page,
                pageSize,
            },
        });
    
    }
    handleCancel=()=>
    {
        this.props.dispatch({
            type:'otherpeople/updateState',
            payload:{isVisible:false,name:''},

        })
    }
    
    seachIdCard=(idCard)=>
    {
        this.props.dispatch({
            type:'otherpeople/listpage',
            payload:{
                idCard,
            },
        });
        
    }
    handleOk=(idCard)=>{
        this.props.dispatch({
            type:'otherpeople/updateState',
            payload:{otherList:[]},
        })
        this.props.dispatch(routerRedux.push({   
            pathname: `/person/otherInfo/add/${idCard}`,
        }))
    }
    addData =()=>{
        this.props.dispatch({
            type:'otherpeople/updateState',
            payload:{isVisible:true,name:''},
        })
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    const {idCard}= record;
                    than.props.dispatch(routerRedux.push({   
                        pathname: `/person/otherInfo/edit/${idCard}`,
                    }))
                    
                },
            });

        }
    }
    deleteRow=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'otherpeople/delOthers',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    deleteSome=()=>
    {
        const idList=this.props.otherpeople.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'otherpeople/delAllOthers',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });
        
    }
    turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch } = this.props;
        dispatch({
            type:'otherpeople/listpage',
            payload:{
                page:current,
                pageSize,
            },
        });
    }

    render(){
        const {onRef,handleOk,seachIdCard,handleCancel,handleSearch,restSearch}=this;
        const searchProps={handleSearch,restSearch}
        const parentprops={...this.state.parentprops,onRef,handleOk,seachIdCard,handleCancel}
        const {other_person_type}=this.props.otherpeople;
        const { otherpeople:{otherList,selectedRowKeys,pagination,multiDeleteButtonIsVisable },loading,searchLoading,delLoading }= this.props;
        const { dispatch } =this.props;
        const pageProps = {
            dataSource:otherList,
            loading,
            columsList:{
                columns:[
                {
                    title: '姓名',
                    key: 'name',
                    dataIndex:'name',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },{
                    title: '性别',
                    key: 'gender',
                    dataIndex:'gender',
                    render:(record)=>
                    {
                        if(record==='0')
                        {
                            return '女';
                        }
                        else return '男';
                    },
                }, {
                    title : '身份证号',
                    key : 'idCard',
                    dataIndex : 'idCard',
                    render:(record)=>{
                        const data=record.replace(/^(.{4})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },
                },
                {
                    title : '人群类型',
                    key : 'crowdType',
                    dataIndex : 'crowdType',
                    render:(record)=>{
                       const data= findRecord(crowdtype,'k',record);
                       return data.val;
                    },
                },
                {
                    title : '人员类型',
                    key : 'otherPersonType',
                    dataIndex : 'otherPersonType',
                    render:(record)=>{
                        const data= findRecord(this.props.otherpeople.other_person_type,'k',record);
                        return data.val;
                     },
                },
                {
                    title : '区域',
                    key : 'area',
                    dataIndex : 'area',
                    render:(record)=>{
                        let data=record;
                        if(record)
                        {
                             data=record.replace(/^(.{6})(.*)$/,'$1...');
                        }
                       
                        return data;
                    },
                    
                },
                ],
                menulistArr:[
                    {
                    key:'handle-2',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-3',
                    name:'删除',
                    menuclick:this.deleteRow,
                },
                ],
            },
            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'otherpeople/listpage',
                    payload: {
                        pageSize,  
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                    this.setState({
                        number:selectedRows.length,
                    })
                  this.props.dispatch({
                    type: 'otherpeople/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },
        }
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        return(
           
          <PageHeaderLayout>
            <Card title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
              <SearchForm loading={searchLoading}  searchProps={searchProps} other_person_type={other_person_type} />
            </Card>
            <Card bordered={false} style={{width:'100%'}}>
              <Row type="flex" justify="center" /> 
              <Row type="flex" justify="end">
                <Col>
                  <span style={{ marginRight: 8 }}>
                    {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                  </span>
                  {
                        multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                    } 
                </Col>
                <Col>
                  <ToolBar BtnList={BtnList} />
                </Col>
              </Row>
              <AutoList {...pageProps}  turnPage={this.turnPage}  />
              <ModalForm parentprops={parentprops} />
            </Card>
          </PageHeaderLayout>
        );
    }
}
export default OtherPeople;

