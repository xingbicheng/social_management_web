import React,{Component} from 'react';
import { Button,Select, Row,Col, Modal, Form, Card, Input,message } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {crowdtype} from '../../../utils/seletLocalData'
import BaseForm from '../BaseInfoForm'
import styles from '../../../components/PageInfo/index.less';
import {deleteSpace,getNowFormatDate} from '../../../utils/utils'


const FormItem = Form.Item;
const {Option} = Select;
const {confirm} = Modal;
const { TextArea } = Input;

@Form.create()
@connect(({ otherpeople,loading }) => ({
  otherpeople,
    loading: loading.effects['otherpeople/getList'],
    editLoading:loading.effects['otherpeople/editOthers'],
    addLoading:loading.effects['otherpeople/addOthers'],
}))
class OtherInfo extends Component{
    constructor() {
        super();
        this.state = {
        };
      }
      componentDidMount()
      {
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/otherInfo/:type/:idCard').exec(this.props.history.location.pathname)
        this.props.dispatch({
            type:'otherpeople/getBase',
            payload:{
                idCard:match[2],
            },
        });
        this.props.dispatch({
          type:'otherpeople/updateState',
          payload:{
            doDispatch:match[1],
          },
      });
        this.props.dispatch({
          type:'otherpeople/initBasicData',
        });
        this.props.dispatch({
          type:'otherpeople/getDegree',
      });
      }
      onRef =(ref) => {
        this.BaseForm = ref
    }
      submitForm=()=>
      {
        const than = this;
        confirm({
          title: '确定',
          content: '是否确定？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.form.validateFields((err) => {
              if (!err) {
                  than.BaseForm.chekForm();
                    }
                    else{
                      message.error('请检查必填项是否填写正确');
                    }
                  },
                );
          
          },
      });
      }

    submitAllForm=(data)=>
    {
      let dataList =this.props.form.getFieldsValue();
      const {singlePerson,otherList,doDispatch}=this.props.otherpeople;
      singlePerson.deathTiem=singlePerson.isDie?getNowFormatDate(singlePerson.deathTiem):null;
      const baseInfo={...singlePerson,...data};
      dataList={...otherList[0]||{},...dataList};
      let formData={...dataList,baseInfo,idCard:data.idCard};
      formData=deleteSpace(formData);
      let type='otherpeople/addOthers';
      if(doDispatch==='edit')
      {
        type='otherpeople/editOthers';
      }
      this.props.dispatch({
        type,
        payload:{
          ...formData,
        },
      })
    }
      back=()=>
      {
        const than=this.props;
        confirm({
          title: '返回',
          content: '是否返回到上一页面？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.dispatch(routerRedux.push('/person/personother'));
          },
      });
      }
      render() {
        const { otherpeople:{singlePerson,doDispatch,otherList,other_person_type,degree_education},editLoading,addLoading }= this.props;
        const list=otherList[0]||{};
        const baseInfoProps=
        {
          onRef :this.onRef,
          submitAllForm:this.submitAllForm,
        };
        const crowdOptions=crowdtype.map((current)=>{
          return <Option key={current.k}>{current.val}</Option>
        })
        const typeOptions = other_person_type.map(current => {
          return <Option key={current.k}>{current.val}</Option>
        })
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 17 },
          },
          };
          const textAreaLayout = {
            labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            },
            wrapperCol: {
            xs: { span: 24 },
            sm: { span: 19 },
            },
            };
        return (
          <div>
            <BaseForm {...baseInfoProps}{...singlePerson}  selectTion={degree_education} /> 
            <Card title="其他人员信息" bordered={false} style={{marginBottom:'20px'}}>
              <Form >
                <Row type="flex" justify="center"  style={{marginTop:'20px'}}>
                  <Col span={10}> 
                    <FormItem {...formItemLayout} label="人群类型">
                      {getFieldDecorator('crowdType', {
                      initialValue:list.crowdType||'1',
                      rules: [{
                        required:true,
                        message:'请选择人群类型',
                        }],
                      })(
                        <Select >
                          {crowdOptions}
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}> 
                    <FormItem {...formItemLayout} label="人员类型">
                      {getFieldDecorator('otherPersonType', {
                      initialValue:list.otherPersonType||'0',
                      rules: [{
                        required:true,
                        message:'请选择人员类型',
                        }],
                      })(
                        <Select style={{ width: '100%' }} >
                          {
                            typeOptions
                            }
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                </Row>
                <Row  type="flex" justify="center" style={{marginTop:'20px'}} >   
                  <Col  span={24}>  
                    <FormItem {...textAreaLayout} label="区域">
                      {getFieldDecorator('area', {
                      initialValue:list.area,
                      rules:[{
                        max:200,
                        message:'不能超出200字',
                        whitespace:true,
                      }],
                      })(
                        <TextArea maxLength="200" placeholder='请输入您所在区域,不超过200字' autosize={{minRows:3,maxRows:5}} />
                      )}
                    </FormItem>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <FormItem {...textAreaLayout} label="备注">
                      {getFieldDecorator('memo', {
                      initialValue:list.memo||'',
                      rules: [{
                        max:200,
                        message:'不能超出200字',
                        whitespace:true,
                        }],
                      })(
                        <TextArea maxLength="200" placeholder="请不要超过200字" autosize={{minRows:3,maxRows:5}} />
                      )}
                    </FormItem>
                  </Col>
                </Row>
               
                
              </Form>
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button  onClick={this.back}>返回</Button>
                  <Button onClick={this.submitForm} loading={(doDispatch==='edit')?editLoading:addLoading} icon="save" type="primary">保存</Button>
                </div>
              </div>
            </Card>
          </div>
        );
    }
}
export default OtherInfo;

