import React , {Component} from 'react';
import {Row,message, Col, Select, Form, Input, Button } from 'antd';
import {crowdtype} from '../../../utils/seletLocalData'
import styles from '../../../theme/table.less';
import {SearchBtnLayout,FormItemLayoutPage } from '../../../config/config';

const FormItem = Form.Item;

const {Option} = Select;

@Form.create()

class OtherSearch extends Component{
  constructor(props) {
    super(props);
    this.state={
    }
  }
  searchInfo=()=>
  {
     this.props.form.validateFields((err) => {
      if (!err) {
      const data=this.props.form.getFieldsValue();
      const {handleSearch}=this.props.searchProps;
      for(const item in data) {
	  	if(data[item] === '' || data[item] === undefined) {	
	    delete data[item];
      }
    }
    
      handleSearch(data);
  }
})
  }
  resetDara=()=>
  {
    const {restSearch}=this.props.searchProps;
    this.props.form.resetFields();
    restSearch();

  }
  render() {
    const {loading}=this.props;
    const searchFormLayout={
      xs:{span:24},
      sm:{span:12},
      md:{span:12},
      lg:{span:12},
      xxl:{span:8},
    }
    const {other_person_type}=this.props
    const crowdOptions=crowdtype.map((current)=>{
      return <Option key={current.k}>{current.val}</Option>
    })
    const typeOptions = other_person_type.map(current => {
      return <Option key={current.k}>{current.val}</Option>
    })
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className={styles.searchBox}>
        <Row>
          <Col  {...searchFormLayout} >
            <FormItem
              {...FormItemLayoutPage}
              style={{width: '100%'}}
              label="姓名"
            >
              {getFieldDecorator('name', {
                rules:[{ whitespace:true,
                  message:'请不要输入纯空格'}],
                })(
                  <Input placeholder='请输入您要查询的姓名' />
                )}
            </FormItem>
          </Col>
          <Col  {...searchFormLayout} >
            <FormItem
              {...FormItemLayoutPage}
              style={{width: '100%'}}
              label="身份证号"
            >
              {getFieldDecorator('idCard', {
                rules: [{pattern:/^[0-9a-zA-Z]+$/, message: '只能填写数字或字母',
                }],
                })(
                  <Input placeholder='请输入您要查询的身份证号'  style={{ width:'100%' }} />
              )}
            </FormItem>
          </Col>
          <Col   {...searchFormLayout}>
            <FormItem
              {...FormItemLayoutPage}
               
              style={{width: '100%'}}
              label="人群类型"
            >
              {getFieldDecorator('crowdType', {
                })(
                  <Select placeholder='请选择人群类型'>
                    {crowdOptions}
                  </Select>
                )}
            </FormItem>
          </Col>
          <Col {...searchFormLayout}>
            <FormItem
              {...FormItemLayoutPage}
                
              style={{width: '100%'}}
              label="人员类型"
            >
              {getFieldDecorator('otherPersonType', {
                })(
                  <Select placeholder='请选择人员类型'>
                    {typeOptions}
                  </Select>
                )}
            </FormItem>
          </Col>
        </Row>
        <Row >
          <Col {...SearchBtnLayout}>
            <div
              style={{ margin: 0 }}
              className={styles.btnBox}
            >
              <Button onClick={this.resetDara}>重置</Button>
              <Button onClick={this.searchInfo} loading={loading}  type="primary" icon="search">搜索</Button>
            </div>
          </Col>
        </Row>          
      </Form>
    )
  }
}

export default OtherSearch
