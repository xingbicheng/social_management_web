import React,{Component} from 'react';
import * as moment from 'moment';
import { Button, Row,Col, Modal,Cascader, Form,message,Select, DatePicker, Card, Input } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {phoneRegular,idCardRegular} from '../../../utils/regular'
import { gender, nations,place } from '../../../utils/seletLocalData';
import {getNowFormatDate,changeToString,changeToArray,deleteSpace} from '../../../utils/utils'
import styles from '../../../components/PageInfo/index.less';

const FormItem = Form.Item;
const {confirm} = Modal;
const {TextArea} =Input;
const {Option} = Select;

@Form.create()
@connect(({ personbase,loading }) => ({
    personbase,
    loading: loading.effects['personbase/getList'],
    baseLoading:loading.effects['personbase/getBase'],
    editLoading:loading.effects['personbase/editBase'],
    addLoading:loading.effects['personbase/addBase'],
}))
class BaseInfo extends Component{
    constructor() {
        super();
        this.state =
        {
          birthday:'',
          dieday:'',
          sex:'',
        };
      }
      componentDidMount()
      {
        const {dispatch} = this.props;
        dispatch({
          type:'personbase/getMarital',
        });
        dispatch({
          type:'personbase/getOutLook',
        });
        dispatch({
          type:'personbase/getDegree',
        });
        dispatch({
          type:'personbase/getHouse',
        });
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/baseInfo/:id').exec(this.props.history.location.pathname)
        if(match[1]!=='-1')
        {
          this.props.dispatch({
            type:'personbase/getBase',
            payload:{id:match[1]},
          })
        }
      }
       onRef=(ref)=>
      {
        this.BaseForm = ref
      }

      handleBack=()=>
      {
        const than=this.props;
        confirm({
          title: '返回',
          content: '是否返回到上一页面？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.dispatch(routerRedux.push('/person/personbase'));
          },
        });
      }
      submitForm=()=>
      {
        const than = this;
        confirm({
          title: '确定',
          content: '是否确定？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.form.validateFields((err) => {
              if (!err) {
                  than.submitAllForm();
                    }
                    else{
                      message.error('请检查必填项是否填写正确');
                    }
                  },
                );
          },
      });
      }
      submitAllForm=()=>
      {
        const { personbase:{singlePerson,isDie}}= this.props;
          let dataList =this.props.form.getFieldsValue();
          const {doDispatch} = this.props.personbase;
          const {birthday,deathTiem,householdRegisterPlace}=dataList;
          dataList.birthday=getNowFormatDate(birthday);
          dataList.householdRegisterPlace=changeToString(householdRegisterPlace)
          dataList.deathTiem=isDie?getNowFormatDate(deathTiem):null;
          dataList=deleteSpace(dataList);
          dataList.id=singlePerson.id;
          let type='personbase/addBase';
          if(doDispatch==='edit')
          {
            type='personbase/editBase';
          }
          this.props.dispatch({
            type,
            payload:{
              ...dataList,
            },
          })
      }
      handleIdCard=(rule, value, callback)=>
      {
        if(!rule.pattern.test(value))
        {
          callback('请输入正确的身份证号码');
        }
        else{
          const idCard =this.props.form.getFieldValue('idCard');
          let birthday ="";
          let sex  = 0;
          if(idCard.length===15)
          {
            birthday=`19${idCard.substring(6,8)}-${idCard.substring(8,10)}-${idCard.substring(10,12)}`;
            sex=(idCard[14]%2 === 0)?'0':'1';
          }
          else{
            birthday=`${idCard.substring(6,10)}-${idCard.substring(10,12)}-${idCard.substring(12,14)}`;
            sex=(idCard[16]%2 === 0)?'0':'1';
          }
          this.setState({
            birthday,
            sex,
          })
        }
        callback();

      }

      handleName=(rule, value, callback)=>
      {
        if(!rule.pattern.test(value))
        {
          callback('请输入中文或者英文');
        }
        if(value.length>rule.max)
        {
          callback(`请不要超过${rule.max}`);
        }
        callback()
      }
      isDie=(value)=>
      {
        if(value==='1')
        {
          this.props.dispatch({type:'personbase/updateState',
          payload:{isDie:true}})
        }
        else
        {
          this.props.dispatch({type:'personbase/updateState',
          payload:{isDie:false}})
        }

      }
      onChange = (field, value) => {
        this.setState({
          [field]: value,
        });
      }
      onDieChange = (value) => {
        this.onChange('dieday', value);
      }
      onbirthdayChange = (value) => {
        this.onChange('birthday', value);
      }
      disabledBirthdayDate=(birthday)=>{
          return birthday.valueOf()>=moment().endOf('day').valueOf();

      }
      disabledDieDate = (dieday) =>
      {
          return dieday>=moment().endOf('day');
      }
      handlebirthday=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const deathTiem= getFieldValue('deathTiem');
        if(value>deathTiem)
        {
           callback('出生日期不能晚于死亡日期')
        }
        callback()
      }
      handledieday=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const birthday= getFieldValue('birthday');
        if(value<birthday)
        {
           callback('死亡日期不能早与出生日期')
        }
        callback()
      }
      render() {
        const dateFormat = 'YYYY-MM-DD';
        const {birthday,sex} = this.state;
        const { personbase:{isDie,doDispatch,singlePerson,householdRegisterNature,degree_education,marital_status,political_outlook },loading,baseLoading,addLoading,editLoading }= this.props;
        const data =singlePerson||{};
        const nationsOptions=nations.map((current)=>{
          return <Option key={current.name}>{current.name}</Option>
        })
        const houseOptions=householdRegisterNature.map((current)=>{
          return <Option key={current.k}>{current.val}</Option>
        })
        const genderOptions = gender.map(current => {
          return <Option key={current.k}>{current.val}</Option>;
        });
        const eduOptions=degree_education.map((current)=>{
          return <Option key={current.k}>{current.val}</Option>
        })
        const MaritalOptions=marital_status.map((current)=>{
          return <Option key={current.k}>{current.val}</Option>
        })
        const politicalOptions=political_outlook.map((current)=>{
          return <Option key={current.k}>{current.val}</Option>
        })
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 17 },
          },
          };
          const textAreaLayout = {
            labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            },
            wrapperCol: {
            xs: { span: 24 },
            sm: { span: 19 },
            },
            };
        return (
          <Card>
            <Card  loading={loading} title="基本信息" bordered={false} style={{ marginBottom: '20px' }}>
              <Form>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="姓名">
                      {getFieldDecorator('name', {
                      initialValue: data.name || '',
                      rules: [{
                      required: true,
                      validator:this.handleName,
                      max:60,
                      pattern:/^[a-zA-Z\u4e00-\u9fa5]+$/,
                    }],
                  })(
                    <Input  />
                  )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="身份证号">
                      {getFieldDecorator('idCard', {
                      initialValue: data.idCard || '',
                      rules:[{
                        required: true,pattern:idCardRegular,
                        validator:this.handleIdCard,
                      }],
                    })(
                      <Input />,
                    )}
                    </FormItem>

                  </Col>
                </Row>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="性别">
                      {getFieldDecorator('gender', {
                    initialValue: sex || data.gender,
                    rules: [{
                      required: true,
                      message: '请选择性别',
                    }],
                  })(
                    <Select>
                      {genderOptions}
                    </Select>,
                  )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="出生日期">
                      {getFieldDecorator('birthday', {
                        initialValue: moment(birthday||data.birthday||moment(), dateFormat),
                        rules: [{
                          required: true,
                          validator:this.handlebirthday,
                        }],
                      })(
                        <DatePicker onChange={this.onbirthdayChange} disabledDate={this.disabledBirthdayDate} style={{ width: '100%' }} />,
                      )}
                    </FormItem>
                  </Col>
                </Row>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="民族">
                      {getFieldDecorator('nation', {
                        initialValue: data.nation || '',
                        rules: [{
                          required: true,
                          message: '请选择民族',
                        }],
                      })(
                        <Select
                          showSearch
                          placeholder='请选择您要查询的民族'
                        >
                          {nationsOptions}
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="文化程度">
                      {getFieldDecorator('degreeEducation', {
                    initialValue: data.degreeEducation,
                    rules: [{
                      required: true,
                      message: '请选择文化程度',
                    }],
                  })(
                    <Select >
                      {eduOptions}
                    </Select>,
                  )}
                    </FormItem>
                  </Col>
                </Row>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="户口性质">
                      {getFieldDecorator('householdRegisterNature', {
                    initialValue: data.householdRegisterNature||'0',
                    rules: [{
                      required: true,
                      message: '请选择户口性质',
                    }],
                  })(
                    <Select >
                      {houseOptions}
                    </Select>
                  )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="政治面貌">
                      {getFieldDecorator('politicalOutlook'||'0', {
                    initialValue: data.politicalOutlook,
                    rules: [{
                      required: true,
                      message: '请选择政治面貌',
                    }],
                  })(
                    <Select >
                      {politicalOptions}
                    </Select>
                  )}
                    </FormItem>

                  </Col>
                </Row>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="婚姻状况">
                      {getFieldDecorator('maritalStatus', {
                    initialValue: data.maritalStatus||'',
                    rules: [{
                      required: true,
                      message: '请选择婚姻状况',
                    }],
                  })(
                    <Select >
                      {MaritalOptions}
                    </Select>
                  )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="籍贯">
                      {getFieldDecorator('householdRegisterPlace', {
                        initialValue: changeToArray(data.householdRegisterPlace )|| [],
                        rules: [{
                          required: true,
                          message: '请输入籍贯',
                        }],
                      })(
                        <Cascader  options={place} placeholder="选择地址" />

                      )}
                    </FormItem>

                  </Col>
                </Row>
                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10} >
                    <FormItem {...formItemLayout} label="联系方式">
                      {getFieldDecorator('phone', {
                        initialValue: data.phone || '',
                        rules: [{
                          required: true,
                          message: '请填写正确的电话号码',
                          pattern: phoneRegular,
                        }],
                      })(
                        <Input />
                      )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>

                    {(doDispatch==='edit')&&(   <FormItem {...formItemLayout} label="是否死亡">
                      {getFieldDecorator('isDie', {
                      initialValue: data.isDie || '0',
                      rules: [{
                      required: true,
                      message: '请选择是否死亡',
                    }],
                  })(
                    <Select onSelect={this.isDie} style={{ width:'100%' }}>
                      <Option value="1">是</Option>
                      <Option value="0">否</Option>
                    </Select>
                  )}
                    </FormItem>)}
                  </Col>
                </Row>

                <Row type="flex" justify="center" style={{ marginTop: '20px' }}>
                  <Col span={10}>
                    {isDie&&(
                    <FormItem {...formItemLayout} label="死亡时间">
                      {getFieldDecorator('deathTiem', {
                        initialValue: moment(data.deathTiem||moment(), dateFormat),
                        rules: [{
                          required: true,
                          validator:this.handledieday,
                        }],
                      })(
                        <DatePicker  onChange={this.onDieChange}  disabledDate={this.disabledDieDate} style={{ width: '100%' }} />,
                      )}
                    </FormItem>
                  )}
                  </Col>
                  <Col offset={2} span={10} />
                </Row>
                <Row type="flex"justify="center" style={{marginTop:'20px'}}>
                  <Col span={24}>
                    <FormItem {...textAreaLayout} label="备注">
                      {getFieldDecorator('memo', {
                        initialValue:data.memo||'',
                        rules: [{
                          max:200,
                          whitespace:true,
                          message:'请不要输入纯空格,不并且输入内容不能超出200字',
                          }],
                        })(
                          <TextArea placeholder="请不要超过200字"   maxLength="200" autosize={{minRows:3,maxRows:5}} />
                        )}
                    </FormItem>
                  </Col>
                </Row>
              </Form>
            </Card>
            <div className={styles.btnBox}>
              <div className={styles.saveAndreturn}>
                <Button  onClick={this.handleBack}>返回</Button>
                <Button onClick={this.submitForm} loading={(doDispatch==='edit')?editLoading:addLoading}  icon="save" type="primary">保存</Button>
              </div>
            </div>
          </Card>
        );
    }
}
export default BaseInfo;

