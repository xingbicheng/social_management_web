import React,{Component} from 'react';
import {  Card,Form,Row,Col,Modal } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/List/List';
import ToolBar from '../../../components/ToolBar';
 import SearchForm from './BaseSearch';

const {confirm} = Modal;
@Form.create()
@connect(({ personbase, loading }) => ({
    personbase,
    searchLoading: loading.effects['personbase/getList'],
    loading: loading.effects['personbase/listpage'],
}))
class PersonBase extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    componentWillMount(){
        const { personbase:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'personbase/listpage',
            payload:{
                page,
                pageSize,
            },
        });
    }
    addData =()=>{
        this.props.dispatch({
            type:'personbase/updateState',
            payload:{singlePerson:{},doDispatch:'add',isDie:false},
        })
        this.props.dispatch(routerRedux.push({
            pathname: `/person/baseInfo/-1`,
        }))


    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否修改所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const {id}= record;
                than.props.dispatch(routerRedux.push({
                    pathname: `/person/baseInfo/${id}`,
                }))
                },
            });
        }
    }
    handleSearch=(data)=>{
        const {dispatch,personbase:pagination} = this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'personbase/getList',
            payload:{...data,page,pageSize},
        });
        }
        restSearch =() => {
            const {dispatch,personbase:{pagination}} = this.props;
            const { page,pageSize } = pagination;
            dispatch({
                type:'personbase/listpage',
                payload:{
                    page,
                    pageSize,
                },
            });
        }
        turnPage = (e)=>{
            const {current,pageSize} = e;
            const { dispatch } = this.props;
            dispatch({
                type:'personbase/listpage',
                payload:{
                    page:current,
                    pageSize,
                },
            });
        }
    render(){
        const {handleSearch,restSearch}=this;
        console.log(this.props);
        const { personbase:{baseList,pagination } ,loading,searchLoading}= this.props;
        const searchProps={handleSearch,restSearch}
        const { dispatch } =this.props;
        const pageProps = {
             dataSource:baseList,
             loading,
            columsList:{
                columns:[
                    {
                        title:'#',
                        key:'index',
                        dataIndex:'index',
                        align:'center',
                        render: (text, record, index) => {
                            return <span>{index + 1}</span>
                        },
                    },
                    {
                    title: '姓名',
                    key: 'name',
                    dataIndex:'name',
                },{
                    title: '性别',
                    key: 'gender',
                    dataIndex:'gender',
                    render:(record)=>
                    {
                        if(record==='0')
                        {
                            return '女';
                        }
                        else return '男';
                    },
                }, {
                    title : '身份证号',
                    key : 'idCard',
                    dataIndex : 'idCard',
                    render:(record)=>{
                        const data=record.replace(/^(.{4})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },
                },
                {
                    title : '户籍所在地',
                    key : 'householdRegisterPlace',
                    dataIndex : 'householdRegisterPlace',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },
                {
                    title : '出生日期',
                    key : 'birthday',
                    dataIndex : 'birthday',

                },
                {
                    title : '民族',
                    key : 'nation',
                    dataIndex : 'nation',

                },
                ],
                menulistArr:[
                    {
                    key:'handle-2',
                    name:'修改',
                    menuclick:this.update,
                },
                ],
            },

            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'personbase/listpage',
                    payload: {
                        pageSize,
                        page,
                    },
                  })
                },
              },
        }
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        return(
          <PageHeaderLayout>
            <Card  title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
              <SearchForm  loading={searchLoading} searchProps={searchProps} />
            </Card>
            <Card bordered={false} style={{width:'100%'}}>
              <Row type="flex" justify="end">
                <Col>
                  <ToolBar BtnList={BtnList} />
                </Col>
              </Row>
              <AutoList {...pageProps}  turnPage={this.turnPage}   />
            </Card>
          </PageHeaderLayout>
        );
    }
}
export default PersonBase;

