import React,{Component} from 'react';
import * as moment from 'moment';
import { Button, Row,Col, Modal,Cascader, Form,message,Select, DatePicker, Card, Input } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import BaseForm from '../BaseInfoForm'
import {getNowFormatDate,changeToString,changeToArray,deleteSpace} from '../../../utils/utils'
import {physicalHealths,place} from '../../../utils/seletLocalData'
import styles from '../../../components/PageInfo/index.less';

const FormItem = Form.Item;
const {confirm} = Modal;
const {TextArea} =Input;
const {Option} = Select;

@Form.create()
@connect(({ persondrug,loading }) => ({
    persondrug,
    loading: loading.effects['persondrug/getList'],
    baseLoading:loading.effects['persondrug/getBase'],
    editLoading:loading.effects['persondrug/editDrugs'],
    addLoading:loading.effects['persondrug/addDrugs'],
}))
class DrugInfo extends Component{
    constructor() {
        super();
        this.state =
        {
        };
      }
      componentDidMount()
      {
        const pathToRegexp = require('path-to-regexp');
        const match = pathToRegexp('/person/druginfo/:type/:idCard').exec(this.props.history.location.pathname)
        this.props.dispatch({
            type:'persondrug/getBase',
            payload:{
                idCard:match[2],
            },
        });
        this.props.dispatch({
          type:'persondrug/updateState',
          payload:{
            doDispatch:match[1],
          },
        });
        if(match[1]==='edit')
        {
          
          this.props.dispatch({
            type:'persondrug/getList',
            payload:{
                idCard:match[2],
            },
        });
        }
        this.props.dispatch({
          type:'persondrug/getDegree',
          payload:{type:'degree_education'},
        });
      }
       onRef=(ref)=>
      {
        this.BaseForm = ref
      }
      submitForm=()=>
      {
        const than = this;
        confirm({
          title: '确定',
          content: '是否确定？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.props.form.validateFields((err) => {
              if (!err) {
                  than.BaseForm.chekForm();
                    }
                    else{
                      message.error('请检查必填项是否填写正确');  
                    }
                  },
                );
          },
      });
      }
      disabledDate=(current)=>{
        return current && current > moment().startOf('day');
      }
      handlefirstDrug=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const firstDrugSeizure= getFieldValue('firstDrugSeizure');
        if(value>firstDrugSeizure)
        {
           callback('初次吸毒时间不能晚于被抓时间')
        }
        callback()
      }
      handlefirstDrugSeizure=(rule, value, callback)=>
      {
        const { getFieldValue } = this.props.form
        const firstDrug= getFieldValue('firstDrug');
        if(value<firstDrug)
        {
           callback('初次吸毒被抓时间不能早于初次吸毒时间')
        }
        callback()
      }
      handleBack=()=>
      {
        const than=this.props;
        confirm({
          title: '返回',
          content: '是否返回到上一页面？',
          okText:'确定',
          cancelText:'取消',
          onOk() {
            than.dispatch(routerRedux.push('/person/persondrug'));
          },
        });
      }
     
      submitAllForm=(data)=>
      {
        let dataList =this.props.form.getFieldsValue();
        const {residence,residencedetials} =dataList;
        dataList.residence = `${changeToString(residence)} ${residencedetials}`;
        const {singlePerson,drugList,doDispatch}=this.props.persondrug;
        singlePerson.deathTiem=singlePerson.isDie?getNowFormatDate(singlePerson.deathTiem):null;
        let formData={};
        dataList={...drugList[0]||{},...dataList};
        const {firstDrug,firstDrugSeizure,collectionDate}=formData;
        formData={...dataList,idCard:singlePerson.idCard,baseInfo:{...singlePerson,...data},firstDrug:getNowFormatDate(firstDrug),firstDrugSeizure:getNowFormatDate(firstDrugSeizure),collectionDate:getNowFormatDate(collectionDate)};
        formData=deleteSpace(formData);
        if(drugList.length!==0)
        {
          formData.birthday=getNowFormatDate(formData.birthday);
        }
        let type='persondrug/addDrugs';
        if(doDispatch==='edit')
        {
          type='persondrug/editDrugs';
        }
        this.props.dispatch({
          type,
          payload:{
            ...formData,
          },
        })
      }
      render() {
        const baseInfoProps=
          {
            onRef :this.onRef,
            submitAllForm:this.submitAllForm,
          };
        const { persondrug:{singlePerson,degree_education,drugList,doDispatch },loading,baseLoading,addLoading,editLoading }= this.props;
        const list={...drugList[0]};
        const healthOptions=physicalHealths.map((current)=>{
          return <Option key={current.name}>{current.name}</Option>
        })
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 17 },
          },
          };
          const textAreaLayout = {
          labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
          },
          wrapperCol: {
          xs: { span: 24 },
          sm: { span: 19 },
          },
          };
        return (
          <div>
            <BaseForm {...baseInfoProps} {...singlePerson}loading={baseLoading}selectTion={degree_education} />
            <Card loading={loading} title="吸毒人员信息">
              <Form >
                <Row  type="flex" justify="center"  style={{marginTop:'20px'}}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="初次吸毒日期">
                      {getFieldDecorator('firstDrug', {
                         initialValue:moment(list.firstDrug||moment()),
                         rules:[{
                           required:true,
                           validator:this.handlefirstDrug,
                         }],
                      })(
                        <DatePicker disabledDate={this.disabledDate} style={{width:'100%'}} />
                      )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10}>
                    <FormItem {...formItemLayout} label="吸毒查获日期">
                      {getFieldDecorator('firstDrugSeizure',{
                        initialValue:moment(list.firstDrugSeizure||moment()),
                        rules:[{
                          required:true,
                          validator:this.handlefirstDrugSeizure,
                        }],
                      })(
                        <DatePicker disabledDate={this.disabledDate} style={{width:'100%'}} />
                    )}
                    </FormItem>
                  </Col>
                </Row>
                <Row  type="flex" justify="center" style={{marginTop:'20px'}}>
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="采集单位">
                      {getFieldDecorator('collectionUnit', {
                      initialValue:list.collectionUnit||'',
                      rules:[{
                        required:true,
                        whitespace:true,
                        message:'请填写采集单位',
                      }],

                        })(
                          <Input  placeholder="请填写采集单位" />
                      )}
                    </FormItem>
                  </Col>
                  <Col  offset={2} span={10}>
                    <FormItem {...formItemLayout} label="身体状况">
                      {getFieldDecorator('physicalHealth', {
                      initialValue:list.physicalHealth||'健康',
                      rules: [{
                        required: true,
                        message: '请输入姓名',
                      }],
                    })(
                      <Select>
                        {healthOptions}
                      </Select>
                    )}
                    </FormItem>
                  </Col>

                </Row>
                <Row  type="flex" justify="center" style={{marginTop:'20px'}} >
                  <Col span={10}>
                    <FormItem {...formItemLayout} label="现居住地址">
                      {getFieldDecorator('residence', {
                      initialValue:changeToArray(list.residence)||[],
                      rules:[{  required:true}],
                      })(
                        <Cascader options={place} placeholder="请选择您的居住地址" />
                      )}
                    </FormItem>
                  </Col>
                  <Col offset={2} span={10} />
                </Row>
                <Row type="flex" justify="center">
                  <Col span={24}>
                    <FormItem {...textAreaLayout} label="现居住详细地址">
                      {getFieldDecorator('residencedetials', {
                        initialValue:list.residencedetials,
                        rules:[{ 
                          required:true,
                          whitespace:true,
                          message:'请填写详细地址',
                          max:200}],
                        })(
                          <TextArea  placeholder="请填写您现居住的详细地址,请不要超过200字"  maxLength="200" autosize={{minRows:3,maxRows:5}} />
                        )}
                    </FormItem>
                  </Col>
                </Row>
                <Row type="flex"justify="center" style={{marginTop:'20px'}}>
                  <Col span={24}>
                    <FormItem {...textAreaLayout} label="备注">
                      {getFieldDecorator('memo', {
                        initialValue:list.memo||'',
                        rules: [{
                          max:200,
                          whitespace:true,
                          message:'请不要输入纯空格,不并且输入内容不能超出200字',
                          }],
                        })(
                          <TextArea placeholder="请不要超过200字"   maxLength="200" autosize={{minRows:3,maxRows:5}} />
                        )}
                    </FormItem>
                  </Col>
                </Row>
              </Form>
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button  onClick={this.handleBack}>返回</Button>
                  <Button onClick={this.submitForm} loading={(doDispatch==='edit')?editLoading:addLoading} icon="save" type="primary">保存</Button>
                </div>
              </div>
            </Card>
          </div>
        );
    }
}
export default DrugInfo;

