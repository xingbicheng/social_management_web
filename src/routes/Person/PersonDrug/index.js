import React,{Component} from 'react';
import {  Card,Form,Row,Col,Button,Modal } from 'antd';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/List/List';
import ModalForm from '../PersonModal';
import ToolBar from '../../../components/ToolBar';
import SearchForm from './DrugSearch';

const {confirm} = Modal;
@Form.create()
@connect(({ persondrug, loading }) => ({
    persondrug,
    searchLoading: loading.effects['persondrug/getList'],
    loading: loading.effects['persondrug/listpage'],
    delLoading: loading.effects['persondrug/delAllDrugs'],
}))
class DrugPerson extends Component{
    constructor(props){
        super(props);
        this.state = {
            number:'',
            parentprops :{
                isVisible:false,
                name:'',
                title:'新增吸毒人员',
            },
        }
    }
    componentWillMount(){
        const { persondrug:{pagination} }= this.props;
        const { page,pageSize } = pagination;
        this.props.dispatch({
            type:'persondrug/listpage',
            payload:{
                page,
                pageSize,
            },
        });
    }
    componentWillReceiveProps(nextprops)
    {
        this.setState({
            parentprops: {
                ...this.state.parentprops,
                isVisible: nextprops.persondrug.isVisible,
                name:nextprops.persondrug.name,
            },
    })

    }

    onRef =(ref) => {
        this.ModalForm = ref
    }
    handleCancel=()=>
    {
        this.props.dispatch({
            type:'persondrug/updateState',
            payload:{isVisible:false,name:''},
        })
    }
    seachIdCard=(idCard)=>
    {
        this.props.dispatch({
            type:'persondrug/listpage',
            payload:{
                idCard,
            },
        });
    }
    handleOk=(idCard)=>{
    this.props.dispatch({
        type:'persondrug/updateState',
        payload:{drugList:[]},
    })
    this.props.dispatch(routerRedux.push({
        pathname: `/person/druginfo/add/${idCard}`,
    }))
    }
    addData =()=>{
        this.props.dispatch({
            type:'persondrug/updateState',
            payload:{isVisible:true,name:''},
        })
    }
    update=(record)=>{
        if(record && record.id){
            const than = this;
            confirm({
                title: '修改',
                content: '是否所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                const {idCard}= record;
                than.props.dispatch(routerRedux.push({
                    pathname: `/person/druginfo/edit/${idCard}`,
                }))
                },
            });
        }
    }

    delete=(record)=>{
        const than = this;
        if( record.id){
            confirm({
                title: '删除',
                content: '是否删除所选信息？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'persondrug/delDrugs',
                        payload:{
                            id:record.id,
                        },
                    });
                },
            });
        }
    }
    deleteSome=()=>
    {
        const idList=this.props.persondrug.selectedRowKeys;
        const than=this;
        confirm({
            title: '删除',
            content: '是否删除所勾选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                than.props.dispatch({
                    type:'persondrug/delAllDrugs',
                    payload:{
                        ids:idList,
                    },
                });
            },
        });

    }
    handleSearch=(data)=>{
        const {dispatch,persondrug:pagination} = this.props;
        const { page,pageSize } = pagination;
        dispatch({
            type:'persondrug/getList',
            payload:{...data,page,pageSize},
        });
        }
        restSearch =() => {
            const {dispatch,persondrug:{pagination}} = this.props;
            const { page,pageSize } = pagination;
            dispatch({
                type:'persondrug/listpage',
                payload:{
                    page,
                    pageSize,
                },
            });
        }
        turnPage = (e)=>{
            const {current,pageSize} = e;
            const { dispatch } = this.props;
            dispatch({
                type:'persondrug/listpage',
                payload:{
                    page:current,
                    pageSize,
                },
            });
        }
    render(){
        const {onRef,handleOk,seachIdCard,handleCancel,handleSearch,restSearch}=this;
        const parentprops={...this.state.parentprops,onRef,handleOk,seachIdCard,handleCancel}
        const { persondrug:{drugList,selectedRowKeys,pagination,multiDeleteButtonIsVisable } ,loading,delLoading,searchLoading}= this.props;
        const searchProps={handleSearch,restSearch}
        const { dispatch } =this.props;
        const pageProps = {
             dataSource:drugList,
             loading,
            columsList:{
                columns:[
                    {
                    title: '姓名',
                    key: 'name',
                    dataIndex:'name',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },{
                    title: '性别',
                    key: 'gender',
                    dataIndex:'gender',
                    render:(record)=>
                    {
                        if(record==='0')
                        {
                            return '女';
                        }
                        else return '男';
                    },
                }, {
                    title : '身份证号',
                    key : 'idCard',
                    dataIndex : 'idCard',
                    render:(record)=>{
                        const data=record.replace(/^(.{4})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },
                },
                {
                    title : '户籍所在地',
                    key : 'householdRegisterPlace',
                    dataIndex : 'householdRegisterPlace',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },
                },
                {
                    title : '出生日期',
                    key : 'birthday',
                    dataIndex : 'birthday',

                },
                {
                    title : '民族',
                    key : 'nation',
                    dataIndex : 'nation',
                    render:(record)=>{
                        const data=record.replace(/^(.{6})(.*)$/,'$1****');
                        return data;
                    },

                },
                {
                    title : '现居住地',
                    key : 'residence',
                    dataIndex : 'residence',
                    render:(record)=>{
                        let data = record.replace(/\s+/g,'');
                        data=record.replace(/^(.{6})(.*)$/,'$1...');
                        return data;
                    },

                },
                ],
                menulistArr:[
                    {
                    key:'handle-2',
                    name:'修改',
                    menuclick:this.update,
                },{
                    key:'handle-3',
                    name:'删除',
                    menuclick:this.delete,
                },
                ],
            },

            pagination: {
                ...pagination,
                onChange (page, pageSize) {
                dispatch({
                    type: 'persondrug/listpage',
                    payload: {
                        pageSize,
                        page,
                    },
                  })
                },
              },
              rowSelection: {
                selectedRowKeys,
                onChange: (selectedRowKeys, selectedRows) => {
                  this.setState({
                      number:selectedRows.length,
                  })
                  this.props.dispatch({
                    type: 'persondrug/updateState',
                    payload: {
                      selectedRowKeys,
                      multiDeleteButtonIsVisable: Boolean(selectedRowKeys.length),
                    },
                  })
                },
              },

        }
        const BtnList = [{
            icon: 'plus',
            name: '新增',
            type: '',
            key: '1',
            click: this.addData,
          }]
        return(
          <PageHeaderLayout>
            <Card  title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
              <SearchForm  loading={searchLoading} searchProps={searchProps} />
            </Card>
            <Card bordered={false} style={{width:'100%'}}>
              <Row type="flex" justify="end">
                <Col>
                  <span style={{ marginRight: 8 }}>
                    {multiDeleteButtonIsVisable&&`当前选中${this.state.number}条`}
                  </span>
                  {
                    multiDeleteButtonIsVisable && <Button loading={delLoading} type='danger'  icon="close"   onClick={this.deleteSome}>删除</Button>
                    }
                </Col>
                <Col>
                  <ToolBar BtnList={BtnList} />
                </Col>
              </Row>
              <AutoList {...pageProps} turnPage={this.turnPage} />
              <ModalForm parentprops={parentprops}    />
            </Card>
          </PageHeaderLayout>
        );
    }
}
export default DrugPerson;

