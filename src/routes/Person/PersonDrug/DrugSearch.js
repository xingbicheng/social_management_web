import React , {Component} from 'react';
import {  Row,message, Col, Select, Form, Input, Button } from 'antd';
import {nations} from '../../../utils/seletLocalData'
import styles from '../../../theme/table.less';
import {SearchBtnLayout,FormItemLayoutPage } from '../../../config/config';

const FormItem = Form.Item;

const Option = Select.Option;

@Form.create()

class DrugSearch extends Component{
  searchInfo=()=>
  {
    this.props.form.validateFields((err) => {
      if (!err) {
      const data=this.props.form.getFieldsValue();
      const {handleSearch}=this.props.searchProps;
      let item=0;
      for(item in data) {
		  if(data[item] === '' || data[item] === undefined) {	
	    delete data[item];
      }
    }
      handleSearch(data);
  }
  })
  }
  resetDara=()=>
  {
    const {restSearch}=this.props.searchProps;
    this.props.form.resetFields();
    restSearch();
  }
  render() {
    const {loading}=this.props;
    const searchFormLayout={
      xs:{span:24},
      sm:{span:12},
      md:{span:12},
      lg:{span:12},
      xxl:{span:8},
    }
    const nationsOptions=nations.map((current)=>{
      return <Option key={current.name}>{current.name}</Option>
    })
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form className={styles.searchBox}>
          <Row type="flex" justify="left">
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="姓名"
              >
                {getFieldDecorator('name', {
                  rules:[{
                    whitespace:true,
                    message:'请不要输入纯空格',
                  }],
                  })(
                    <Input placeholder='请输入您要查询的姓名' />
                  )}
              </FormItem>
            </Col>
            <Col  {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="身份证号"
              >
                {getFieldDecorator('idCard', {
                    rules:[{
                      pattern:/^[0-9a-zA-Z]+$/,
                      whitespace:true,
                      message:'只能填写数字或字母',
                    }] })(
                      <Input placeholder='请输入您要查询的身份证号' style={{ width:'100%' }} />
                )}
              </FormItem>
            </Col>
            <Col {...searchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                style={{width: '100%'}}
                label="民族"
              >
                {getFieldDecorator('nation', {
                    })(
                      <Select
                        showSearch
                        placeholder='请选择您要查询的民族' 
                      >
                        {nationsOptions}
                      </Select>
                    )}
              </FormItem>
            </Col>
          </Row>
          <Row >
            <Col {...SearchBtnLayout}>
              <div
                style={{ margin: 0 }}
                className={styles.btnBox}
              >
                <Button onClick={this.resetDara}>重置</Button>
                <Button onClick={this.searchInfo} loading={loading} type="primary" icon="search">搜索</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}
export default DrugSearch
