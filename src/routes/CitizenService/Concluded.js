import React , {Component} from 'react';
import { Modal, Form, Input, Select, Radio, Upload, message,Icon, Button  } from 'antd';
import tokenHandler from '../../utils/tokenHandler';
import { connect } from 'dva';
import Config from "../../config/api";
import { createTheURL } from '../../utils/utils';  


const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const InputGroup = Input.Group;
const { TextArea } = Input;
const Dragger = Upload.Dragger;

@Form.create()
@connect(({ dispute, loading }) => ({
  dispute,
  loading: loading.effects['citizen/list'],
}))
export default class Concluded extends Component{
  constructor(props){
    super(props);
    this.state = {
      agreementType:'0',
      fileList:[],
    }
  }

  componentWillMount(){
    const { form,handleOverData } = this.props;
    this.setState({
      agreementType:handleOverData.agreementType,
    });
    form.resetFields();
  }

  onChange = (e)=>{
    this.setState({
      agreementType:e.target.value,
    })
  }

  onRemove = (file)=>{
    const { id }= file.response.data;
    const { handleFileDelete } = this.props;
    handleFileDelete(id);
  }

  handleOk = ()=>{
    // 处理数据
    const { form } = this.props;
    const {handleOk, concludedData, editable} = this.props;
    if(editable){
      handleOk({});
      form.resetFields();
    }else {
      form.validateFields(
        (err)=>{
          if(!err){
            const data  = form.getFieldsValue();
            const {agreementType} = this.state;
            data.id = concludedData.id;
            data.acceptId = concludedData.acceptId;
            if(agreementType === '1'){
              data.writtenId = data.oralAgreement.file.response.data.id;
              data.oralAgreement = 'document';
            }
            handleOk(data);
            form.resetFields();
          }
        }
      );
    }
  };

  handleCancel = ()=>{
    const { form, handleCancel } = this.props;
    form.resetFields();
    handleCancel();
  }
  // download =()=>{
  //   const { handleOverData,dispatch } = this.props;
  //   dispatch({
  //     type:'dispute/downloadFile',
  //     payload:handleOverData.path,
  //   })
  // }

  createUpload = ()=>{
    const { handleOverData } = this.props;
    const place = handleOverData.path.slice(4);
    const request = createTheURL('',place);
    // return <a herf=`${basic}`>123</a>; //  `/information/handoverEdit/dispute/${id}`
    return <a target='_blank' rel="noopener noreferrer" href={request} >点击查看附件</a>;
  }

  render(){
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const bFormItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
    const { getFieldDecorator } = this.props.form;
    const { concludedData, editable, handleOverData, download } = this.props;
    const {agreementType, fileList} = this.state;
    const processPlaceholder = '根据_' +
      '_综合工作中心交办要求，我单位工作人员_' +
      '于何时展开调查取证,何时何地主持调解(多次调解分别' +
      '写明调解时间) ,经法制宣传和政策教育，当事人何时达成了调解协议。' +
      '(或经___ 次调解 ,双方分歧较大 ,调解未果)。'
    const resultPlaceholder = '当事人达成的口头或书面协议内容。或调解未成功，给出的意见和人建议。';
    const actionAddress = createTheURL(Config.API.FILE,'upload');
    const draggerProps = {
      disabled:editable,
      action:actionAddress,
      data:{
        token:tokenHandler.getSessionByKey('token'),
      },
      multiple:false,
      onRemove:this.onRemove,
      onChange(info) {
        const { status } = info.file;
        if (status !== 'uploading') {
        }
        if (status === 'done') {
          message.success('文件上传成功');
        } else if (status === 'error') {
          message.error('文件上传失败');
        }
      },
    };
    let upload = (
      <Dragger {...draggerProps}>
        <p className="ant-upload-drag-icon">
          <Icon type="inbox" />
        </p>
        <p className="ant-upload-text">单击或拖动文件到此区域进行上传</p>
        <p className="ant-upload-hint">支持单个或批量上传。严格禁止上传公司数据或其他禁止文件</p>
      </Dragger>
    );
    // 再次出现时文件的后台文件数据。
    if(editable&&handleOverData.path){
      upload = this.createUpload();
    }
    const textarea = <TextArea placeholder={resultPlaceholder} autosize={{minRows:3,maxRows:5}} disabled={editable} />;
    return(
      <Modal
        visible={this.props.visible}
        title="办结"
        wrapClassName="vertical-center-modal"
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem
            {...formItemLayout}
            style={{width: '100%'}}
            label="编号"
          >
            {getFieldDecorator('number', {
            })(
              <p>{concludedData.number || ''}</p>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            style={{width: '100%'}}
            label="办理期限"
          >
            {getFieldDecorator('handleDeadline', {
            })(
              <p>{concludedData.handleDeadline || ''}</p>
            )}
          </FormItem>
          <FormItem
            {...bFormItemLayout}
            style={{width: '100%'}}
            label="办理过程"
          >
            {getFieldDecorator('handlerProcess', {
              initialValue:handleOverData.handlerProcess || '',
              rules: [{ required: true, message: '请填写处理过程' }],
            })(
              <TextArea placeholder={processPlaceholder} autosize={{minRows:3,maxRows:5}} disabled={editable} />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            style={{width: '100%'}}
            label="办理结果"
          >
            {getFieldDecorator('handlerType', {
              initialValue:handleOverData.handlerType || '',
              rules: [{ required: true, message: '请填写办理结果类型' }],
            })(
              <RadioGroup name="radiogroup" onChange={this.onChange} disabled={editable} >
                <Radio value='0'>口头协议</Radio>
                <Radio value='1'>书面协议</Radio>
              </RadioGroup>
            )}
          </FormItem>
          <FormItem
            {...bFormItemLayout}
            style={{width: '100%'}}
            label="协议内容"
          >
            {getFieldDecorator('oralAgreement', {
              initialValue:handleOverData.oralAgreement || '',
              rules: [{ required: true, message: '请填写协议内容' }],
            })(
              agreementType === '0'? textarea:upload
            )}
          </FormItem>
        </Form>
      </Modal>
    );

  }
}
