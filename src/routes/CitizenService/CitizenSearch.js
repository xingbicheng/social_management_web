import React , {Component} from 'react';
import { connect } from 'dva';
import {  Card, Row, Col, Form, Input, Button, Select } from 'antd';
import { FormItemLayoutPage,SearchFormLayout,SearchBtnLayout } from '../../config/config';
import styles from '../../theme/table.less'
import { statusType } from '../../utils/seletLocalData'


const FormItem = Form.Item;
const Option = Select.Option;
@Form.create()

@connect(({ citizen, loading }) => ({// 把组件connect以下
  citizen,
  loading: loading.effects['citizen/listSearch'],
}))
class CitizenSearch extends Component{
  constructor(props) {
    super(props);
  }

  componentWillMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'citizen/typePage',
      payload:{
        "type": "accept_service_matter_type",
      },
    })
  }

  handleSearch = () => {
    const { form } = this.props;
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let value = 0;
        for (value in values) {
          if(values[value] === '' || values[value] === undefined) {
            delete values[value];
          }
        }
        this.props.getSearchData(values);
      }
    });
  }

  handleReset = () => {
    this.props.form.resetFields();
    this.handleSearch();
  }

  render() {
    const { citizen:{ typeData }, loading} = this.props;
    const matterType = typeData.map(d => <Option key={d.k}>{d.val}</Option>);
    const statusOption = statusType.map(d => <Option key={d.key}>{d.value}</Option>);
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Card bordered={false} title="快速查询">
          <Row gutter={24}>
            <Form className={styles.searchBox}>
              <Row>
                <Col {...SearchFormLayout}>
                  <FormItem
                    {...FormItemLayoutPage}
                    label="编号"
                  >
                    {getFieldDecorator('number', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input type="input" placeholder="请输入编号" />
                    )}
                  </FormItem>
                </Col>
                <Col {...SearchFormLayout}>
                  <FormItem
                    {...FormItemLayoutPage}
                    label="当事人"
                  >
                    {getFieldDecorator('name', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input type="input" placeholder="请输入姓名" />
                    )}
                  </FormItem>
                </Col>
                <Col {...SearchFormLayout}>
                  <FormItem
                    {...FormItemLayoutPage}
                    label="事件类别"
                  >
                    {getFieldDecorator('matterType', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Select placeholder="请选择事项类别">
                        {matterType}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col {...SearchFormLayout}>
                  <FormItem
                    {...FormItemLayoutPage}
                    label="状态"
                  >
                    {getFieldDecorator('status', {
                      rules: [{
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Select placeholder="请选择事项状态">
                        {statusOption}
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row >
                <Col {...SearchBtnLayout}>
                  <div
                    style={{ margin: 0 }}
                    className={styles.btnBox}
                  >
                    <Button onClick={this.handleReset}>重置</Button>
                    <Button onClick={this.handleSearch} type="primary" icon="search" loading={loading}>搜索</Button>
                  </div>
                </Col>
              </Row>
            </Form>
          </Row>
        </Card>
      </div>
    )
  }
}

export default CitizenSearch
