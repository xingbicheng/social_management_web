import React , {Component} from 'react';
import { Form, Button, Row, Col, message, Modal } from 'antd';
import ToolBar from '../../components/ToolBar';
import ModelFrom from './ModalForm';
import * as moment from 'moment';
import AutoList from '../../components/List/List';

const confirm = Modal.confirm;
const modalInitialData = {
  name:0,
  sex:0,
  birthday:0,
  phone:0,
  address:0,
  detailAddress:0,
};

@Form.create()// 经Form.create()包装过的组件会自带this.props.form属性

export default  class AddHandover extends Component{
  constructor(props){
    super(props);
    this.state = {
      isVisible:false,
      modifyIndex:0,
      peopleList:[],
      handleTitle:'新增',
      type: '',
      modalData:modalInitialData,
      selectedRowKeys:[],
      pageNation:{
        pageSize : 10, // 一页多少条
        current :0,  // 当前页
        total:0,  // 总条数
        pages:0,  // 一共多少页
      },
    }
  }

  componentWillMount(){
    const {peopleList} = this.props;
    this.setState({peopleList:peopleList||[]});
  }

  onChange = (selectedRowKeys) =>{
    this.setState({selectedRowKeys});
  }

  handleOk= (formData) =>{
    const { peopleList, modifyIndex } = this.state;
    let result = true;
    peopleList.forEach((data)=>{
      if( data.userId === formData.userId ){
        result = false;
      }
    });
    if(this.state.type === 'edit'){
      let data;
      for (data in peopleList[modifyIndex]){
        peopleList[modifyIndex][data] = formData[data];
      }
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      message.success('修改成功');
      this.setState({type: ''})
    } else if(result){

      peopleList.push(formData);
      this.setState({
        peopleList,
        isVisible:false,
        modalData:modalInitialData,
      })
      message.success('新增成功');

    } else {
      message.error('不能添加重复人员！');
    }
    this.props.getPartyData(peopleList);
  };

  handleCancel = () =>{
    this.setState({
      isVisible:false,
      modalData:modalInitialData,
    })
  };

  handleAddPeople = () =>{
    this.setState({
      isVisible:true,
      modalData:modalInitialData,
    })
  };

  handleMenuModify=(record, index)=>{
    this.setState({
      handleTitle:'编辑',
      isVisible:true,
      modifyIndex:index,
      modalData:{...this.state.peopleList[index]},
      type: 'edit',
    });
  };

  handleMenuDelete=(record,index)=>{
    const that = this;
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const { peopleList } = this.state;
        peopleList.splice(index,1);
        that.setState(peopleList);
        that.setState({selectedRowKeys:[]})
        message.success('删除成功');
        that.props.getPartyData(peopleList);
      },
      onCancel() {

      },
    });

  };

  changeState = () => {
    this.setState({type: ''});
  }



  handleDelete = () =>{
    confirm({
      title: '删除',
      content: '确定删除？',
      cancelText:'取消',
      okText:'确定',
      onOk: () => {
        const {selectedRowKeys} = this.state;
        const { peopleList } = this.state;
        let index = 0;
        selectedRowKeys.forEach( i =>{
          if(i === peopleList.length-1){
            peopleList.splice(i,1);
          } else{
            peopleList.splice(i-index,1);
            index++;
          }
        });
        this.setState({
          peopleList,
          selectedRowKeys:[],
        });
        message.success('删除成功');
        this.props.getPartyData(peopleList);
      },
      onCancel() {

      },
    });
  }

  render(){

    const { peopleList, isVisible, modalData, handleTitle, pageNation, selectedRowKeys } = this.state;
    let isDisabled = peopleList.length >= 1 ? true: false;
    const { treeData } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [{
      title: '单位名称',
      dataIndex: 'departmentName',
      key:'departmentName',
      align:'center',
    },{
      title: '单位角色',
      dataIndex: 'departmentRole',
      key:'departmentRole',
      align:'center',
    },{
      title: '责任人',
      dataIndex: 'userName',
      key:'userName',
      align:'center',
    },
    ];
    const pageProps = {
      dataSource:peopleList,
      columsList:{
        columns,
        menulistArr:[{
          key:'handle-1',
          name:'修改',
          menuclick:this.handleMenuModify,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:this.handleMenuDelete,
        }],
      },
      pageNation,
      rowSelection:{
        onChange:this.onChange,
        selectedRowKeys:this.state.selectedRowKeys,
      },
    };
    const modalProps = {
      isVisible,
      handleCancel:this.handleCancel,
      handleOK:this.handleOk,
      baseData:modalData,
      treeData,
      title:`当事人-${handleTitle}`,
      changeState: this.changeState,
    };
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.handleAddPeople,
      disabled: isDisabled,
    }];
    return(
      <div>
        <Row type="flex" justify="end" >
          <Col>
            <span style={{ marginRight: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
            </span>
            {hasSelected? <Button type='danger' onClick={this.handleDelete} icon="close">删除</Button>:''}
          </Col>
          <Col>
            <ToolBar BtnList={BtnList} />
          </Col>
        </Row>
        <AutoList {...pageProps} />
        <ModelFrom {...modalProps} />
      </div>
    );
  }
}
