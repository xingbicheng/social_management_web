import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import * as moment from 'moment';
import { Button,Form, Select, Card, Input, Row, Icon, DatePicker, Col, Modal, message } from 'antd';
import { connect } from 'dva';
import ToolBar from '../../components/ToolBar';
import AutoList from '../../components/List/List';
import ModelFrom from './ModalForm';
import styles from './index.less';
import { findRecord } from '../../utils/utils';


const confirm = Modal.confirm;
const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
@connect(({ citizen, loading }) => ({
  citizen,
  loading: loading.effects['citizen/get'],
  loadingAdd: loading.effects['citizen/addAssignList'],
  loadingDel: loading.effects['citizen/delAssign'],
  loadingDelAll: loading.effects['citizen/deleteAssignAll'],
  loadingEdit: loading.effects['citizen/editAssign'],
}))
class DataDictIndex extends Component{
  constructor(props){
    super(props);
    this.state = {
      selectedData: [],
      selectedRowKeys: [], // Check here to configure the default column
      isVisible: false,
      listData: [],
      type: '',
      modalType: '',
      acceptId: 0,
      editId: 0,
    }
  }
  componentWillMount(){
    const { dispatch } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handoverEdit/:type/:id').exec(this.props.history.location.pathname)
    dispatch({
      type:'citizen/get',
      payload:{
        "id": match[2],
      },
    });
    dispatch({
      type:'citizen/getDepartmentList',
      payload:{},
    });
  }

  handleAddPeople = () =>{
    const { citizen:{ defaultData }} = this.props;
    this.setState({
      isVisible:true,
      type: 'add',
      modalType: '新增',
      acceptId: defaultData.id,
      listData: [],
    })
  };

  deepCopy = (obj) => {
    if (typeof obj !== "object") {
      return;
    }
    const str = JSON.stringify(obj);
    return JSON.parse(str);
  }

  handleOk= (formData) =>{
    const { citizen:{ defaultData }, dispatch, form } = this.props;
    const { type, acceptId, editId } = this.state;
    let handleDeadline;
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        handleDeadline = values.handleDeadline.format('YYYY-MM-DD HH:MM:SS');
      }
    });
    if (type === 'add') {
      dispatch({
        type:'citizen/addAssignList',
        payload:{
          partyData: [formData],
          moreData: {
            "handleDeadline": handleDeadline,
            "acceptId": acceptId,
          },
        },
      });
    } else if (type === 'edit') {
      dispatch({
        type:'citizen/editAssign',
        payload:{
          "handleDeadline": handleDeadline,
          "acceptId": acceptId,
          "id": editId,
          ...formData,
        },
      });
    }

    this.setState({
      isVisible:false,
    })
  };

  handleDelete = () => {
    const { selectedData } = this.state;
    const { citizen:{ defaultData }, dispatch} = this.props;
    confirm({
      title: '删除',
      content: '是否删除选中信息？',
      okText:'确定',
      cancelText:'取消',
      onOk() {
        const ids = [];
        for (let i = 0; i < selectedData.length; i++) {
          ids.push(selectedData[i].id);
        }
        dispatch({// 删除
          type: 'citizen/deleteAssignAll',
          payload: {
            "ids": ids,
            "acceptId": defaultData.id,
          },
        })
      },
      onCancel() {

      },
    });
this.handleReload();
  }

  handleReload = () => {
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
      });
    }, 2000);
  }

  handleConfirm = () => {
    const { dispatch, citizen:{ defaultData } } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handoverEdit/:type/:id').exec(this.props.history.location.pathname)
    if (!defaultData.listAssign) {
      message.error('责任人不得为空');
    } else {
      confirm({
        title: '保存',
        content: '是否保存为当前状态？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          if (match[1] == 'citizen') {
            dispatch(routerRedux.push('/information/citizen'));
          } else {
            dispatch(routerRedux.push('/information/dispute'));
          }
        },
        onCancel() {},
      });
    }
  }

  delete = (record) => {
    const { dispatch } = this.props;
    dispatch({
      type:'citizen/delAssign',
      payload:{
        "acceptId": record.acceptId,
        "id": record.id,
      },
    });
    this.setState({ selectedRowKeys: [] });
  }

  edit = (record) => {
    const { citizen:{ defaultData }} = this.props;
    const { listAssign } = defaultData;
    for (let i = 0; i < listAssign.length; i++) {
      if (listAssign[i].id === record.id) {
        const { dispatch } = this.props;
        dispatch({
          type: 'citizen/getUser',
          payload: {
            departmentId: record.departmentId,
          },
        })
        this.setState({
          listData: listAssign[i],
          editId: record.id,
          acceptId: record.acceptId,
          isVisible: true,
          type: 'edit',
          modalType: '编辑',
        });
        break;
      }
    }
  }

  handleCancel = () => {
    this.setState({isVisible:false})
  }

  cancelOperating = () => {
    const { dispatch, citizen:{ defaultData } } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handoverEdit/:type/:id').exec(this.props.history.location.pathname)
    if (!defaultData.listAssign) {
      message.error('责任人不得为空');
    } else {
      confirm({
        title: '返回',
        content: '是否要返回主界面？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          if (match[1] == 'citizen') {
            dispatch(routerRedux.push('/information/citizen'));
          } else {
            dispatch(routerRedux.push('/information/dispute'));
          }
        },
        onCancel() {},
      });
    }
  }

  changeState = () => {

  }

  render(){
    const { citizen:{ defaultData, treeData }, loadingDel, loading, loadingAdd, loadingEdit, loadingDelAll } = this.props;
    const { handleDeadline, listAssign } = defaultData;
    defaultData.handleDeadline = moment(handleDeadline);
    const nowTime = new moment().format("YYYY-MM-DD hh:mm:ss");
    if (listAssign) {
      var isDisabled = listAssign.length >= 1 ? true: false;
    }
    const formItemLayout = {
      number: {// 编号
        labelCol: {
          xs: { span: 24 },
          sm: { span: 2 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 20 },
        },
      },
      handleDeadline: {// 办理期限
        labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 19 },
        },
      },
      type: {
        labelCol: {// 类型
          xs: { span: 24 },
          sm: { span: 1 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 21 },
        },
      },
    };
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    const { modalType } = this.state;
    const { selectedRowKeys, isVisible } = this.state;
    const pageProps = {
      dataSource: listAssign,
      columsList:{
        columns: [
          { title: '单位名称', dataIndex: 'departmentName', key: 'departmentName' },
          { title: '单位角色', dataIndex: 'departmentRole', key: 'departmentRole' },
          { title: '责任人', dataIndex: 'userName', key: 'userName' ,
            render(reocrd){
            const userName = reocrd;
              return userName;
            } },
        ],
        menulistArr:[{
          key:'handle-1',
          name:'编辑',
          menuclick:this.edit,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:this.delete,
        }],
      },
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          const selectedData = selectedRows;
          this.setState({ selectedRowKeys, selectedData });
        },
      },
      loading:loading || loadingAdd || loadingDel || loadingDelAll || loadingEdit,
    }

    const copyData = this.deepCopy(this.state.listData);

    const modalProps = {
      baseData: copyData,
      isVisible,
      handleCancel:this.handleCancel,
      handleOK:this.handleOk,
      treeData,
      title:`责任人-${modalType}`,
      changeState: this.changeState,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.handleAddPeople,
      loading: loadingAdd,
      disabled: isDisabled,
    }];

    return(
      <div>
        <Card bordered={false}>
          <Row>
            <Form>
              <Col span={12}>
                <FormItem
                  {...formItemLayout.number}
                  label="编号"
                >
                  {getFieldDecorator('number', {
                    initialValue: defaultData.number || '',
                    rules: [{ required: true }],
                  })(
                    <Input disabled />
                  )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem
                  {...formItemLayout.handleDeadline}
                  style={{width: '100%'}}
                  label="办理期限"
                >
                  {getFieldDecorator('handleDeadline', {
                    initialValue:defaultData.handleDeadline || '',
                    rules: [{ required: true }],
                  })(
                    <DatePicker style={{width: '100%'}} disabled />
                  )}
                </FormItem>
              </Col>
            </Form>
          </Row>
        </Card>
        <Card bordered={false} title='责任人' style={{marginTop:'10px'}}>
          <Row type="flex" justify="end" >
            <Col>
              <span style={{ marginRight: 8 }}>
                {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
              </span>
              {hasSelected? <Button type='danger' onClick={this.handleDelete} loading={loadingDelAll} icon="close">删除</Button>:''}
            </Col>
            <Col>
              <ToolBar BtnList={BtnList} />
            </Col>
          </Row>
          <AutoList {...pageProps} />
          <ModelFrom {...modalProps} />
          <div className={styles.btnBox}>
            <div className={styles.saveAndreturn}>
              <Button icon="close" onClick={this.cancelOperating} >返回</Button>
              <Button icon="save" type="primary"   onClick={this.handleConfirm}>保存</Button>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}
export default DataDictIndex;
