import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Cascader, Form, Select, DatePicker, Input, Modal, TreeSelect} from 'antd';


const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;
@Form.create()
@connect(({ citizen, loading }) => ({
  citizen,
  loading: loading.effects['citizen/typePage'],
}))
class ModelFrom extends Component{
  constructor(props){
    super(props);
    this.state = {
      departmentName: '',
      userName: '',
    }
  }
  handleOK = () =>{
    const { handleOK, form, baseData } = this.props;
    const { departmentName, userName } = this.state;
    form.validateFields(
      (err)=>{
        if(!err){
          const formData = form.getFieldsValue();
          let { departmentRole } = formData;
          formData.departmentRole = departmentRole.trim();
          formData.departmentName = departmentName ? departmentName: baseData.departmentName;
          formData.userName = userName ? userName: baseData.userName;
          formData.userId = formData.userId.key;
          formData.departmentId = parseInt(formData.departmentId);
          handleOK(formData);
          form.resetFields();
        }
      }
    );
  }

  departmentChange = (value, label) => {
    this.setState({departmentName: label[0]});
    const { dispatch } = this.props;
    dispatch({
      type: 'citizen/getUser',
      payload: {
        departmentId: value,
      },
    })
    this.props.form.setFields({// 重置
      userId: {
        value: undefined,
      },
    });
  }

  userChange = (value) => {
    this.setState({userName: value.label});
  }

  handleCancel = () =>{
    const { handleCancel, form, changeState } = this.props;
    changeState();
    form.resetFields();
    handleCancel();
  }

  render(){
    const { citizen:{ users }} = this.props;
    const OptionData = users.map((item) => {
      return(<Option value={item.id} key={item.id}>{item.username}</Option>)
    })
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const { getFieldDecorator } = this.props.form;
    const { isVisible, baseData, title, treeData } = this.props;
    return(
      <Modal
        title={title}
        visible={isVisible}
        onOk={this.handleOK}
        onCancel={this.handleCancel}
      >
        <Form>
          <FormItem
            {...formItemLayout}
            label="类型"
          >
            {getFieldDecorator('assignType', {
              initialValue:baseData.assignType || '',
              rules: [{
                required: true, message: '类型是必选项',
              }],
            })(
              <Select
                placeholder="请选择类型"
                showSearch
                style={{ width: '100%' }}
              >
                <Option value="0">调处单位</Option>
                <Option value="1">协处单位</Option>
              </Select>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="部门"
          >
            {getFieldDecorator('departmentId', {
              initialValue: baseData.departmentId ? String(baseData.departmentId): '',
              rules: [{
                required: true, message: '部门是必填项',
              }],
            })(
              <TreeSelect
                placeholder="请选择部门"
                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                treeData={treeData}
                onChange={this.departmentChange}
              />
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="责任人"
          >
            {getFieldDecorator('userId', {
              initialValue: baseData.userId ? { "key": baseData.userId, "label": baseData.userName }: '',
              rules: [{
                required: true, message: '责任人必选',
              }],
            })(
              <Select
                placeholder="请选择责任人"
                showSearch
                labelInValue
                style={{ width: '100%' }}
                onChange={this.userChange}
              >
                {OptionData}
              </Select>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="单位角色"
          >
            {getFieldDecorator('departmentRole', {
              initialValue:baseData.departmentRole || '',
              rules: [{
                required: true, message: '单位角色是必填项', whitespace: true,
              }],
            })(
              <Input style={{ width: '100%' }} placeholder="请填写单位角色" />
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}
export default ModelFrom;
