import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import {  Card,Form,Modal,Dropdown,Button,Icon,Row,Col,Table, message } from 'antd';
import { connect } from 'dva';
import * as moment from 'moment';
import Concluded from './Concluded';
import ToolBar from '../../components/ToolBar';
import AutoList from '../../components/List/List';
import { matterStatus, matterType } from '../../utils/seletLocalData';
import { MATTERLEVEN } from '../../utils/emenus';

const confirm = Modal.confirm;

@Form.create()
@connect(({ citizen, loading }) => ({
  citizen,
  loading: loading.effects['citizen/list'],
  loadingDel: loading.effects['citizen/delete'],
  loadingDelAll: loading.effects['citizen/deleteAcceptAll'],
  concludedLoading:loading.effects['citizen/handler'],
}))
class DataDictIndex extends Component{
  constructor(props){
    super(props);
    this.state = {
      selectedData: [],
      selectedRowKeys: [], // Check here to configure the default column
      concludedData:{},
      handleOverData: {},
      editable:false,
      visible:false,
    }
  }
  componentWillMount(){
    this.listpage();
  }
  listpage = ()=>{
    const { dispatch,citizen:{pagination} } = this.props;
    const { searchData } = this.props;
    const { pageNum,pageSize } = pagination;
    dispatch({
      type:'citizen/list',
      payload:{
        page:pageNum,
        pageSize,
        searchData,
      },
    });
  }
  handleDeleteAll = () => {
    const { dispatch } = this.props;
    const {selectedData} = this.state;
    const rows = selectedData.filter(i => i.status !== '0');
    if(rows.length){
      message.error('包含有不在受理中的事件');
      return;
    }
    confirm({
      title: '删除',
      content: '是否删除选中信息？',
      okText:'确定',
      cancelText:'取消',
      onOk() {
        const ids = [];
        for (let i = 0; i < selectedData.length; i++) {
          ids.push(selectedData[i].id);
        }
        dispatch({// 删除
          type: 'citizen/deleteAcceptAll',
          payload: {
            "ids": ids,
          },
        })
      },
      onCancel() {

      },
    });
    this.handleReload();
  }

  handleReload = () => {
    this.setState({
      selectedRowKeys: [],
    });
  }

  handleHandover = (record) => {
    const { id } = record;
    const { dispatch } = this.props;
    const nowTime = new moment();
    if (record.status != 0 && record.status != 1) {
      message.error('只有受理和待办的事件才可交办')
    } else if(moment(record.handleDeadline).isBefore(nowTime)){
      message.error('办理期限已过，无法进行交办');
    } else if (record.status == 0) {
        dispatch(routerRedux.push({
          pathname: `/information/handover/citizen/${id}`,
        }))
      } else if (record.status == 1) {
        dispatch(routerRedux.push({
          pathname: `/information/handoverEdit/citizen/${id}`,
        }))
      }
  }

  handleConcluded = (record)=>{
    const { dispatch } = this.props;
    const { status } = record;
    if(status === '1'||status === '2'){
      this.setState({
        concludedData:record,
        visible:true,
        editable:false,
      })
      dispatch({
        type:'citizen/saveConcluded',
        payload:{
          handleOverData:{},
        },
      })
    } else if(status === '3'){
      this.setState({
        concludedData:record,
        visible:true,
        editable:true,
      })
      dispatch({
        type:'citizen/getConcluded',
        payload:{
          data:record,
        },
      })
    } else if(status === '0'){
      message.error('受理中的信息无法办结');
    } else if(status === '4'){
      message.error('已经批示的事件无法办结');
    }
  }

  checkConcludedOk = (data) => {
    const that = this;
    if (JSON.stringify(data) === '{}') {
      this.handleConcludedOK(data);
    } else {
      confirm({
        title: '办结',
        content: '是否要保存当前操作',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          that.handleConcludedOK(data);
        },
        onCancel() {

        },
      });
    }
  }

  handleConcludedOK = (data)=>{
    const { dispatch } = this.props;
    const { concludedData,editable } = this.state;
    if(editable){
      this.setState({
        visible:false,
      })
      return;
    }
    data.number = concludedData.number;
    data.acceptId = concludedData.id;
    data.handleDeadline = concludedData.handleDeadline;
    dispatch({
      type:'citizen/handler',
      payload:{
        data,
      },
    });

    this.setState({
      visible:false,
    })
  }

  handleConcludedCancle = ()=>{
    this.setState({visible: false});
  }

  returnAccept = () => {
    const { dispatch } = this.props;
    dispatch({
      type:'citizen/accept',
      payload:{
      },
    });
    this.props.dispatch(routerRedux.push(`/information/accept/-1`));
  }

  add =()=>{
    const { dispatch } = this.props;
    dispatch({
      type: 'dictdata/showModal',
      payload:{
        createUserId: 0,
        id: 0,
        superiorDepartmentId: 0,
        updateUserId: 0,
      },
    });
  }

  // 编辑
  handleEdit=(record)=>{
    const { dispatch } = this.props;
    if (record.status != 0) {
      message.error('只有受理中的信息可以编辑')
    } else {
      confirm({
        title: '编辑',
        content: '是否修改当前信息？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          dispatch(routerRedux.push(`/information/accept/${record.id}`));
        },
        onCancel() {},
      });
    }
  }
  // 删除
  handleDelete=(record)=>{
    const than = this;
    if(record.status !== '0'){
      message.error('不在受理中的事件无法删除');

    } else {
      confirm({
        title: '删除',
        content: '是否删除当前信息？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          than.props.dispatch({
            type:'citizen/delete',
            payload:{
              id:record.id,
            },
          });
          than.setState({ selectedRowKeys: []})
        },
        onCancel() {},
      });
    }
  }

  download =()=>{
    const { citizen:{handleOverData},dispatch } = this.props;
    dispatch({
      type:'citizen/downloadFile',
      payload:{
        id:handleOverData.writtenId,
      },
    })
  }

  handleFileDelete=(id)=>{
    const {dispatch} = this.props;
    dispatch({
      type:'citizen/deleteFile',
      payload:{
        id,
      },
    })
  }

  turnPage = (e)=>{
    const {current,pageSize} = e;
    const { dispatch, searchData } = this.props;
    dispatch({
      type:'citizen/list',
      payload:{
        page:current,
        pageSize,
        ...searchData,
      },
    });
  }

  handleCancel = () =>{
    this.props.dispatch({
      type:'dictdata/hiddenModal',
    });
  }

  restSearch = () => {
    const {dispatch} = this.props;
    dispatch({
      type:'dictdata/clearSearchInfo',
    });
    dispatch({
      type:'dictdata/listpage',
      payload:{
        page:1,
        pageSize:10,
      },
    });
  }

  search = (values)=>{
    const {dispatch} = this.props;
    dispatch({
      type:'dictdata/setSearchInfo',
      payload:{...values},
    });
    dispatch({
      type:'dictdata/listpage',
      payload:{
        page:1,
        pageSize:10,
        ...values,
      },
    })
  }

  expandedRowRender = (record) => {
    const columns = [
      { title: '姓名', dataIndex: 'name', key: 'name' },
      { title: '性别', dataIndex: 'sex', key: 'sex' },
      { title: '出生年月', dataIndex: 'birthday', key: 'birthday' },
      { title: '电话', dataIndex: 'phone', key: 'phone' },
      {
        title: '住址',
        dataIndex: 'address',
        key: 'address',
      },
    ];

    const { citizen:{dataSource} } = this.props;
    for (let i = 0; i < dataSource.length; i++) {
      if (dataSource[i].number == record.number) {
        var listPartyData = dataSource[i].listParty;
        break;
      }
    }

    return (
      <Table
        columns={columns}
        dataSource={listPartyData}
        pagination={false}
      />
    );
  };

  render(){
    const { citizen:{dataSource, pagination, handleOverData}, loadingDel, loading, concludedLoading, loadingDelAll } = this.props;
    const { selectedRowKeys, visible, concludedData, editable } = this.state;
    const pageProps = {
      dataSource,
      columsList:{
        columns:[
          { title: '编号', dataIndex: 'number', key: 'number' },
          { title: '状态', dataIndex: 'status', key: 'status',
            render:(record) =>{
              for(let i = 0;i < 5;i++){
                if(matterStatus[i].key === record){
                  return matterStatus[i].value;
                }
              }
            },
          },
          { title: '受理时间', dataIndex: 'createTime', key: 'createTime' },
          { title: '事项类别', dataIndex: 'matterType', key: 'matterType',
            render: (record) => {
              for(let i = 24;i < 29;i++){
                if(matterType[i].k === record){
                  return matterType[i].val;
                }
              }
            },
          },
          { title: '级别', dataIndex: 'level', key: 'level', render:(record) =>{
              if(record === MATTERLEVEN.NORMAL){
                return '一般事项';
              }else if(record === MATTERLEVEN.IMPORTANT){
                return '重大事项';
              }
            } },
          { title: '办理期限', key: 'handleDeadline', dataIndex: 'handleDeadline' },
        ],
        menulistArr:[{
          key:'handle-1',
          name:'编辑',
          menuclick:this.handleEdit,
        },{
          key:'handle-2',
          name:'删除',
          menuclick:this.handleDelete,
        },{
          key:'handle-3',
          name:'交办',
          menuclick:this.handleHandover,
        },{
          key:'handle-4',
          name:'办结',
          menuclick:this.handleConcluded,
        }],
      },
      pagination,
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          const selectedData = selectedRows;
          this.setState({ selectedRowKeys, selectedData });
        },
      },
      expandedRowRender: this.expandedRowRender,
      loading:loading || loadingDel || loadingDelAll || concludedLoading,
    }
    const hasSelected = selectedRowKeys.length > 0;
    const BtnList = [{
      icon: 'plus',
      name: '受理',
      type: '',
      key: '1',
      click: this.returnAccept,
    }];
    const concludedprops = {
      editable,
      visible,
      concludedData,
      confirmLoading:concludedLoading,
      handleOverData,
      handleOk:this.checkConcludedOk,
      handleCancel:this.handleConcludedCancle,
      handleFileDelete:this.handleFileDelete,
      download:this.download,
    }

    return(
      <Card bordered={false} style={{marginTop: 20}}>
        <Row type="flex" justify="end" >
          <Col>
            <span style={{ marginRight: 8 }}>
              {hasSelected ? `当前选中 ${selectedRowKeys.length} 条` : ''}
            </span>
            {hasSelected? <Button type='danger' onClick={this.handleDeleteAll} loading={loadingDelAll} icon="close">删除</Button>:''}
          </Col>
          <Col>
            <ToolBar BtnList={BtnList} />
          </Col>
        </Row>
        <AutoList {...pageProps} turnPage={this.turnPage} />
        <Concluded {...concludedprops} />
      </Card>
    );
  }
}
export default DataDictIndex;
