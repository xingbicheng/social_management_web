import React , {Component} from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import CitizenSearch from './CitizenSearch'
import NestedTable from './NestedTable'

@connect(({ citizen, loading }) => ({
  citizen,
  loading: loading.effects['citizen/get'],
}))

class Index extends Component{
  constructor(props){
    super(props);
    this.state = {
      allData: [],
      isList: false,
      searchData: {},
    }
  }

  getSearchData = (searchData) => {// 获取用户要搜索的信息
    const { dispatch } = this.props;
    const { citizen:{pagination} }= this.props;
    const { page,pageSize } = pagination;
    this.setState({searchData});
    dispatch({
      type:'citizen/listSearch',
      payload:{...searchData,page,pageSize},
    });
  }

  render(){
    return(
      <PageHeaderLayout>
        <CitizenSearch getSearchData={this.getSearchData} />
        <NestedTable searchData={this.state.searchData} />
      </PageHeaderLayout>
    );
  }
}

export default Index;
