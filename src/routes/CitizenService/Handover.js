import React,{Component} from 'react';
import { routerRedux } from 'dva/router';
import * as moment from 'moment';
import { Button,Form, Select, Card, Input, Row, DatePicker, Col, Modal, message } from 'antd';
import { connect } from 'dva';
import AddHandover from './AddHandover';
import styles from './index.less';


const confirm = Modal.confirm;
const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
@connect(({ citizen, loading }) => ({
  citizen,
  loadingAdd: loading.effects['citizen/addAssign'],
}))
class DataDictIndex extends Component{
  constructor(props){
    super(props);
    this.state = {
      isVisible: false,
      partyData: [],
    }
  }
  componentWillMount(){
    const { dispatch } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handover/:type/:id').exec(this.props.history.location.pathname)
    dispatch({
      type:'citizen/get',
      payload:{
        "id": match[2],
      },
    });
    dispatch({
      type:'citizen/getDepartmentList',
      payload:{},
    });
  }

  listpage = ()=>{
    const { dispatch,citizen:{pagination} } = this.props;
    const { pageNum,pageSize } = pagination;
    dispatch({
      type:'citizen/list',
      payload:{
        page:pageNum,
        pageSize,
      },
    });
  }

  // handleSave = () => {
  //   const that = this;
  //   confirm({
  //     title: '保存',
  //     content: '是否提交当前数据？',
  //     okText:'确定',
  //     cancelText:'取消',
  //     onOk() {
  //       that.handleConfirm();
  //     },
  //     onCancel() { },
  //   });
  // }
  handleConfirm = () => {// 表单验证
    const that = this;
    const { form } = this.props;
    const { dispatch } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handover/:type/:id').exec(this.props.history.location.pathname)
    form.validateFields(
      (err)=>{
        if(!err){
          const {partyData} = this.state;
          const { citizen:{ defaultData }} = this.props;
          const moreData  = form.getFieldsValue();
          moreData.handleDeadline = moreData.handleDeadline.format("YYYY-MM-DD hh:mm:ss");
          delete moreData.number;
          moreData.acceptId = defaultData.id;
          if (partyData.length === 0) {
            message.error('请添加责任人')
          } else {
            confirm({
              title: '保存',
              content: '是否提交当前数据？',
              okText:'确定',
              cancelText:'取消',
              onOk() {
                dispatch({
                  type:'citizen/addAssign',
                  payload:{
                    moreData,
                    partyData,
                    type: match[1],
                  },
                })
              },
              onCancel() { },
            });
          }
        }
      }
    );
  }

  add =()=>{
    const { dispatch } = this.props;
    this.setState({
      formMethod:'add',
      handleTitle:'新增',
    });
    dispatch({
      type: 'dictdata/showModal',
      payload:{
        createUserId: 0,
        id: 0,
        superiorDepartmentId: 0,
        updateUserId: 0,
      },
    });
  }

  // 编辑
  edit=(record)=>{
    this.props.dispatch({
      type:'citizen/getEditData',
      payload:{
        ...record,
      },
    });
    this.props.dispatch(routerRedux.push('/information/accept'));
  }
  // 删除
  delete=(record)=>{
    const than = this;
    if(record && record.id){
      confirm({
        title: '删除',
        content: '是否删除当前信息？',
        okText:'确定',
        cancelText:'取消',
        onOk() {
          than.props.dispatch({
            type:'citizen/delete',
            payload:{
              id:record.id,
            },
          });
        },
        onCancel() {

        },
      });

    }
  }

  turnPage = (e)=>{
    const {current,pageSize} = e;
    const { dispatch } = this.props;
    dispatch({
      type:'citizen/list',
      payload:{
        page:current,
        pageSize,
      },
    });
  }

  handleOk = () =>{
    this.setState({isVisible: false});
  }

  handleCancel = () =>{
    this.setState({isVisible: false});
  }

  cancelOperating = () => {
    const { dispatch } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/information/handover/:type/:id').exec(this.props.history.location.pathname)
    confirm({
      title: '返回',
      content: '是否返回主界面？',
      okText:'确定',
      cancelText:'取消',
      onOk() {
        if (match[1] == 'citizen') {
          dispatch(routerRedux.push('/information/citizen'));
        } else {
          dispatch(routerRedux.push('/information/dispute'));
        }
      },
      onCancel() {},
    });
  }

  restSearch = () => {
    const {dispatch} = this.props;
    dispatch({
      type:'dictdata/clearSearchInfo',
    });
    dispatch({
      type:'dictdata/listpage',
      payload:{
        page:1,
        pageSize:10,
      },
    });
  }

  search = (values)=>{
    const {dispatch} = this.props;
    dispatch({
      type:'dictdata/setSearchInfo',
      payload:{...values},
    });
    dispatch({
      type:'dictdata/listpage',
      payload:{
        page:1,
        pageSize:10,
        ...values,
      },
    })
  }

  getPartyData = (partyData)=>{// 获取当事人信息
    this.setState({partyData});
  }

  render(){
    const { citizen:{ defaultData, treeData }, loadingAdd} = this.props;
    const { handleDeadline } = defaultData;
    defaultData.handleDeadline = moment(handleDeadline);
    const formItemLayout = {
      number: {// 编号
        labelCol: {
          xs: { span: 24 },
          sm: { span: 2 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 20 },
        },
      },
      handleDeadline: {// 办理期限
        labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 19 },
        },
      },
      type: {
        labelCol: {// 类型
          xs: { span: 24 },
          sm: { span: 1 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 21 },
        },
      },
    };
    const { getFieldDecorator } = this.props.form;

    return(
      <div>
        <Card bordered={false}>
          <Row>
            <Form>
              <Col span={12}>
                <FormItem
                  {...formItemLayout.number}
                  label="编号"
                >
                  {getFieldDecorator('number', {
                              initialValue: defaultData.number || '',
                              rules: [{ required: true }],
                            })(
                              <Input disabled />
                            )}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem
                  {...formItemLayout.handleDeadline}
                  style={{width: '100%'}}
                  label="办理期限"
                >
                  {getFieldDecorator('handleDeadline', {
                              initialValue:defaultData.handleDeadline || '',
                              rules: [{ required: true }],
                            })(
                              <DatePicker style={{width: '100%'}} disabled />
                            )}
                </FormItem>
              </Col>
            </Form>
          </Row>
        </Card>
        <Card bordered={false} title="责任人">
          <AddHandover  getPartyData={this.getPartyData} treeData={treeData} />
          <div className={styles.btnBox}>
            <div className={styles.saveAndreturn}>
              <Button icon="close" onClick={this.cancelOperating} >返回</Button>
              <Button icon="save" type="primary"   onClick={this.handleConfirm} loading={loadingAdd}  >保存</Button>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}
export default DataDictIndex;
