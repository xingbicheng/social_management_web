import React,{Component} from 'react';
import { Form,Input,Modal,Alert,Popover,Progress } from 'antd';
import {FormItemLayoutModal} from "../../config/config";
import styles from './Register.less';

const FormItem = Form.Item;
const passwordStatusMap = {
    ok: <div className={styles.success}>强度：强</div>,
    pass: <div className={styles.warning}>强度：中</div>,
    poor: <div className={styles.error}>强度：太短</div>,
};
const passwordProgressMap = {
    ok: 'success',
    pass: 'normal',
    poor: 'exception',
};

@Form.create()
class UpdatePassword extends Component{
    constructor(props){
        super(props);
        this.state = {
            count: 0,
            confirmDirty: false,
            visible: false,
            help: '',
            prefix: '86',
        }
    }
    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        this.setState({
          visible:false,
        });
        handleCancel();
    }
    
    onHandleOk = ()=>{
      const { form,handleOk } = this.props;
      form.validateFields((err, values) => {
          if(!err){
              handleOk(values);
          }else{
              return (
                <Alert
                  description="表单验证失败"
                  type="error"
                />
              )
          }
      });
  };
  getPasswordStatus = () => {
    const { form } = this.props;
    const value = form.getFieldValue('newPassword');
    if (value && value.length > 9) {
      return 'ok';
    }
    if (value && value.length > 5) {
      return 'pass';
    }
    return 'poor';
};
    checkPassword = (rule, value, callback) => {
      const regExp = /^[a-zA-Z]\w{5,17}$/;
      if (!value) {
        this.setState({
          help: '请输入密码！',
          visible: !!value,
        });
        callback('error');
      } else {
        this.setState({
          help: '',
        });
        if (!this.state.visible) {
          this.setState({
            visible: !!value,
          });
        }

        if (value.length < 6) {
          callback('error');
        } else {
          if(!regExp.test(value)){
              callback('error');
          }
          const { form } = this.props;
          if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
          }
          callback();
        }
      }
  };
    
    checkConfirm = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('newPassword')) {
          callback('两次输入的密码不匹配!');
        } else {
          callback();
        }
    };
    
    renderPasswordProgress = () => {
      const { form } = this.props;
      const value = form.getFieldValue('newPassword');
      const passwordStatus = this.getPasswordStatus();
      return value && value.length ? (
        <div className={styles[`progress-${passwordStatus}`]}>
          <Progress
            status={passwordProgressMap[passwordStatus]}
            className={styles.progress}
            strokeWidth={6}
            percent={value.length * 10 > 100 ? 100 : value.length * 10}
            showInfo={false}
          />
        </div>
      ) : null;
  };
  
    render(){
        const { visible,title,formInfo,form:{getFieldDecorator} } = this.props;
        const formItemLayout = FormItemLayoutModal;  // 通用样式表
        return(
          <Modal
            title={title}
            visible={visible}
            onOk={this.onHandleOk}
            onCancel={this.onHandleCancel}
          >
            <Form>
              <FormItem
                {...formItemLayout}
                label="用户名"
              >
                <p>{formInfo.nickname}</p>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="旧密码"
              >
                {getFieldDecorator('oldPassword', {
                        rules: [{
                            required: true,
                            message:'请输入旧密码',
                        }],
                })(<Input type="password" placeholder="请输入旧密码" />)}
              </FormItem>
              <FormItem
                help={this.state.help}
                {...formItemLayout}
                label="新密码"
              >
                <Popover
                  content={
                    <div style={{ padding: '4px 0' }}>
                      {passwordStatusMap[this.getPasswordStatus()]}
                      {this.renderPasswordProgress()}
                      <div style={{ marginTop: 10 }}>
                            请至少输入 6 个字符，且由字母开头。请不要使用容易被猜到的密码。
                      </div>
                    </div>
                    }
                  overlayStyle={{ width: 240 }}
                  placement="right"
                  visible={this.state.visible}
                >
                  {getFieldDecorator('newPassword', {
                        rules: [{
                            required: true,
                        },
                        {
                            validator: this.checkPassword,
                        },
                        ],
                    })(<Input type="password" placeholder="至少6位密码，且字母开头，区分大小写" />)}
                </Popover>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="确认密码"
              >
                {getFieldDecorator('confirm', {
                    rules: [
                        {
                        required: true,
                        message: '请确认密码！',
                        },
                        {
                        validator: this.checkConfirm,
                        },
                    ],
                    })(<Input type="password" placeholder="确认密码" />)}
              </FormItem>
            </Form>
          </Modal>
        );
    }
}
export default UpdatePassword;

