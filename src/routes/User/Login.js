import React, { Component } from 'react';
import { connect } from 'dva';
import Vcode from 'components/Vcode';
import { Alert, Icon, Input, message, Row, Col, Form, Button } from 'antd';
import styles from './Login.less';

const FormItem = Form.Item;


@Form.create()
@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects['login/login'],
}))
export default class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    }
  }
  onTabChange = type => {
    this.setState({ type });
  };

  onRef = (ref) => {
    this.child = ref
  }


  onGetCaptcha = (e) => {
    localStorage.setItem('captcha', e.toLocaleUpperCase());
  };


  handleSubmit = (event) => {
    event.preventDefault();
    const { dispatch, form } = this.props;
    form.validateFieldsAndScroll((err, values) => {
      const captcha = localStorage.getItem('captcha');
      if(!values.captcha){
        message.warning('验证码为必填项！');
        form.resetFields('captcha');
        this.child.onClick();
        return;
      }
      if (captcha !== values.captcha.toLocaleUpperCase()) {
        message.warning('验证码错误！');
        form.resetFields('captcha');
        this.child.onClick();
        return;
      }
      if (!err) {
        dispatch({
          type: 'login/login',
          payload: {
            ...values,
            captcha: '123456',
          },
        });
        this.child.onClick();
        form.resetFields();
      }
    });
  };
  
  renderMessage = content => {
    return <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />;
  };
  render() {
    const { submitting } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className={styles.main} >
        <div className={styles.inputBox}>
          <Form onSubmit={this.handleSubmit}>
            <FormItem>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: '请填写账号！！！' }],
              })(
                <Input size="large" placeholder='请输入账号' prefix={<Icon type="user" className={styles.prefixIcon} />} />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: '请填写密码！！！！' }],
              })(
                <Input type="password" size="large" placeholder='请输入密码' prefix={<Icon type="lock" className={styles.prefixIcon} />} />
              )}
            </FormItem>
            <Row gutter={8}>
              <Col span={14}>
                <FormItem>
                  {getFieldDecorator('captcha', {
                    rules: [{ required: true, message: '验证码必填！！！' }],
                  })(
                    <Input size="large" placeholder='验证码' prefix={<Icon type="mail" className={styles.prefixIcon} />} />
                  )}
                </FormItem>
              </Col>
              <Col span={10}>
                <Vcode className={styles.getCaptcha} onChange={this.onGetCaptcha} onRef={this.onRef} />
              </Col>
            </Row>
            <FormItem>
              <Button htmlType="submit" size="large" className={styles.submit} type="primary" loading={submitting}>登录</Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}
