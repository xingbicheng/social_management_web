import React, {Component} from 'react'
import pathToRegexp  from 'path-to-regexp'
import PropsTypes from 'prop-types'
import { Col, Row, Form, Button, Card, Input, Modal, Select } from 'antd'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout'
import AutoList from '../../../components/List/List'
import { gender } from '../../../utils/seletLocalData'
import AddModal from './addModal.js'
import styles from '../index.less'

const FormItem = Form.Item
const colLayout = {
    span: 8,
}
const FormItemLaylout = {
    labelCol: {
        span: 5,
    },
    wrapperCol: {
        span: 18,
    },
}

@Form.create()
@connect(({ infoAreaDetail, loading }) => ({
  infoAreaDetail,
  loading: loading.models.infoAreaDetail,
}))
export default class AreaDetail extends Component {
    componentDidMount () {
      const { location, dispatch } = this.props
      const matchEdit = pathToRegexp('/gridInfoManage/areaEdit/:id').exec(location.pathname)
                const matchAdd = pathToRegexp('/gridInfoManage/areaAdd/:id').exec(location.pathname)
                if (matchEdit) {
                    dispatch({
                        type: 'infoAreaDetail/clearState',
                    })
                    dispatch({
                        type: 'infoAreaDetail/getArea',
                        payload: {
                            id: matchEdit[1],
                        },
                    })
                    dispatch({
                        type: 'infoAreaDetail/updateState',
                        payload: {
                            type: 'edit',
                        },
                    })
                }else if (matchAdd) {
                    dispatch({
                        type: 'infoAreaDetail/clearState',
                    })
                    dispatch({
                        type: 'infoAreaDetail/updateState',
                        payload: {
                            departmentId: matchAdd[1],
                            type: 'add',
                        },
                    })
                    dispatch({
                        type: 'infoAreaDetail/initBasic',
                        payload: {
                            id: matchAdd[1],
                        },
                    })
                }
    }
    save = () => {
        const { getFieldsValue, validateFields, resetFields } = this.props.form
        validateFields((errors, values) => {
            if (errors) {
                return
            } else {
                const values = getFieldsValue()
                const { departmentId, type, areaDetail  } = this.props.infoAreaDetail
                this.props.dispatch({
                    type: 'infoAreaDetail/updateState',
                    payload: {
                        saveLoading: true,
                    },
                })
                if (type === 'add') {
                    this.props.dispatch({
                        type: 'infoAreaDetail/save',
                        payload: {
                            departmentId,
                            ...values,
                            id:departmentId
                        },
                    })
                } else if (type === 'edit') {
                    this.props.dispatch({
                        type: 'infoAreaDetail/edit',
                        payload: {
                            ...areaDetail,
                            departmentId,
                            ...values,
                        },
                    })
                    // this.props.dispatch(routerRedux.push({
                    //     pathname: `/gridInfoManage/area/${departmentId}`,
                    // }))
                }
                // this.props.dispatch({
                //     type: 'infoAreaDetail/clearState',
                // })

            }
        })

        // resetFields()
    }
    create = () => {
        this.props.dispatch({
            type: 'infoAreaDetail/updateState',
            payload: {
                modalVisable: true,
                modalType: 'create',
                currentItem: {},
                confirmLoading: false,
            },
        })
    }
    delRow = (record) => {
        const { areaDetail } = this.props.infoAreaDetail
        Modal.confirm({
          title: '删除',
          content: '确定删除此项?',
          cancelText:'取消',
          okText:'确定',
          onOk: () => {
            this.props.dispatch({
              type: 'infoAreaDetail/delTeam',
              payload: {
                id: record.id,
              },
            })
          },
          onCancel() {

          },
        });
    }
    editRow = (record) => {
        this.props.dispatch({
            type: 'infoAreaDetail/updateState',
            payload: {
                modalVisable: true,
                modalType: 'edit',
                currentItem: record,
                confirmLoading: false,
            },
        })
    }
    handleCancle = () => {
        Modal.confirm({
            content: '你确定要取消当前操作吗? 这样除了片区服务团队信息以外的新增或者修改的信息将不会被保存',
            onOk: () => {
                this.props.history.goBack()
            },
            okText: '确定',
            cancelText: '取消',
        })
    }
    render () {
        const { getFieldDecorator }  = this.props.form
        const { areaDetail, selectedRowKeys, department, serviceTeams, currentItem, modalVisable, modalType, type, pagination, saveLoading, confirmLoading } = this.props.infoAreaDetail
        const listProps = {
            dataSource: serviceTeams,
            loading: this.props.loading,
            // pagination: {
            //   ...pagination,
            //   onChange (page, pageSize) {
            //     dispatch({
            //       type: 'infoAreaDetail/updateState',
            //       payload: {
            //         pagination: {
            //           pageSize,
            //           current: page,
            //         }
            //       }
            //     })
            //   }
            // },
            columsList: {
              columns: [
                {
                  title: '序号',
                  key: 'name1',
                  render: (text, record, index) => {
                    return <span>{index + 1}</span>
                  },
                },{
                  title: '名称',
                  dataIndex: 'branch',
                  key: 'branch',
                },{
                  title: '姓名',
                  dataIndex: 'name',
                  key: 'name',
                },{
                  title: '性别',
                  dataIndex: 'sex',
                  key: 'sex',
                },{
                  title: '年龄',
                  dataIndex: 'age',
                  key: 'age',
                },{
                    title: '职务',
                    dataIndex: 'position',
                    key: 'position',
                },{
                    title: '联系电话',
                    dataIndex: 'phone',
                    key: 'phone',
                },
              ],
              menulistArr : [{
                key:'del',
                name:'删除',
                menuclick:this.delRow,
              },{
                key:'edit',
                name:'编辑',
                menuclick:this.editRow,
              }],
            },
            // rowSelection: {
            //   selectedRowKeys,
            //   onChange: (selectedRowKeys, selectedRows) => {
            //     this.props.dispatch({
            //       type: 'infoAreaDetail/updateState',
            //       payload: {
            //         selectedRowKeys,
            //         currentItem: selectedRows,
            //       },
            //     })
            //   },
            // },
        }
        const ModalProps = {
            options: {
                gender,
            },
            currentItem,
            visible: modalVisable,
            confirmLoading,
            title: modalType === 'create' ? '添加-片区服务团队' : '编辑-片区服务团队',
            handleOk: (values) => {
                this.props.dispatch({
                    type: 'infoAreaDetail/updateState',
                    payload: {
                        confirmLoading: true,
                    },
                })
                if ( modalType === 'create') {
                    this.props.dispatch({
                        type: 'infoAreaDetail/addTeam',
                        payload: {
                            districtId: areaDetail.id,
                            team: values,
                        },
                    })
                } else if (modalType === 'edit') {
                    values = { ...currentItem, ...values }
                    this.props.dispatch({
                        type: 'infoAreaDetail/editTeam',
                        payload: {
                            districtId: areaDetail.id,
                            team: values,
                        },
                    })
                }
                this.props.dispatch({
                    type: 'infoAreaDetail/updateState',
                    payload: {
                        modalVisable: false,
                    },
                })
            },
            onCancel: () => {
                this.props.dispatch({
                    type: 'infoAreaDetail/updateState',
                    payload: {
                        modalVisable: false,
                        currentItem: {},
                    },
                })
            },
        }
        const createButton = (<Button type="primary" onClick={this.create} icon="plus">添加</Button>)
        return (
          <PageHeaderLayout>
            <Card bordered={false} title="片区录入">
              <Form>
                <Row>
                  <Col {...colLayout}>
                    <FormItem {...FormItemLaylout} label="所在部门">
                      {
                          getFieldDecorator('department', {
                              initialValue: department.name || '',
                              rules: [{ required: true, message: "所在部门是必填项", whitespace: true }],
                          })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout}>
                    <FormItem {...FormItemLaylout} label="片区名称">
                      {
                          getFieldDecorator('name', {
                              initialValue: areaDetail.name || '',
                              rules: [{ required: true, message: "片区名称是必填项", whitespace: true }],
                          })(<Input placeholder="请输入不超过30个字" />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout}>
                    <FormItem {...FormItemLaylout} label="片区编号">
                      {
                          getFieldDecorator('number', {
                              initialValue: areaDetail.number || '',
                              rules: [{ whitespace: true }],
                          })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                </Row>
              </Form>
              {
                (type === "edit") &&
                <Card title="片区服务团队" extra={createButton}>
                    <AutoList {...listProps}/>
                </Card>
              }
              <AddModal {...ModalProps} />
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button icon="close" onClick={this.handleCancle}>返回</Button>
                  <Button icon="save" type="primary" onClick={this.save} loading={saveLoading}>保存</Button>
                </div>
              </div>

            </Card>
          </PageHeaderLayout>
        )
    }
}

AreaDetail.propsTypes = {

}
