import React from 'react'
import { Col, Row, Form, Modal, Input, Select } from 'antd'
import PropTypes from 'prop-types'

const FormItem = Form.Item
const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 17,
    } ,
}
const colLayout = {
    span: 24,
}
const TextArea = Input.TextArea
const Option = Select.Option

class AddModal extends React.Component {
    handleOkClick = () => {
        const { getFieldsValue, validateFields, resetFields } = this.props.form
        const { handleOk } = this.props
        validateFields((errors, values) => {
            if (errors) {
                return 
            } else {
                const values = getFieldsValue()
                resetFields()
                handleOk(values)
            }
        })
    }
    handleCancel = () => {
        const { resetFields } = this.props.form
        const { onCancel } = this.props
        resetFields()
        onCancel()
    }
    render () {
        const { currentItem, options, modalLoading } = this.props
        const { getFieldDecorator } = this.props.form
        const sexOptions = options.gender.map( current => {
            return <Option key={current.val}>{current.val}</Option>
        })
        return (
          <Modal {...this.props} onOk={this.handleOkClick} onCancel={this.handleCancel}>
            <Row>
              <Col {...colLayout} > 
                <FormItem {...formItemLayout} label="名称">
                  {
                      getFieldDecorator("branch", {
                          initialValue: currentItem['branch'] || '',
                          rules: [{ required: true, message: "名称是必填项", whitespace: true }],
                      })(<Input placeholder="请输入名称" />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout} > 
                <FormItem {...formItemLayout} label="姓名">
                  {
                      getFieldDecorator("name", {
                          initialValue: currentItem['name'] || '',
                          rules: [{ required: true, message: "姓名是必填项", whitespace: true }],
                      })(<Input placeholder="请输入姓名" />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout} > 
                <FormItem {...formItemLayout} label="性别">
                  {
                      getFieldDecorator("sex", {
                          initialValue: currentItem['sex'] || '',
                          rules: [{ required: true, message: "性别是必填项" }]
                      })(<Select style={{ width: "100%"}}>{sexOptions}</Select>)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout} > 
                <FormItem {...formItemLayout} label="年龄">
                  {
                      getFieldDecorator("age", {
                          initialValue: currentItem['age'] || '',
                          rules: [{ required: true,  message: "年龄是必填项", whitespace: true }, { pattern: /^(1[89]|[2-9][0-9])$/, message: '年龄必须大于等于18小于100'}],
                      })(<Input placeholder="请输入年龄" />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout}>
                <FormItem {...formItemLayout} label="职务">
                  {
                      getFieldDecorator("position", {
                          initialValue: currentItem['position'] || '',
                          rules: [{ required: true,  message: "职务是必填项", whitespace: true }],
                      })(<Input placeholder="请输职务" />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout} > 
                <FormItem {...formItemLayout} label="手机号码">
                  {
                      getFieldDecorator("phone", {
                          initialValue: currentItem['phone'] || '',
                          rules: [{ required: true, pattern: /^1[3|4|5|7|8][0-9]{9}$/, message: "请填入正确的手机号码", whitespace: true }],
                      })(<Input placeholder="请输入手机号码" />)
                  }
                </FormItem>
              </Col>
            </Row>
          </Modal>
        )
    }
}

export default Form.create()(AddModal)