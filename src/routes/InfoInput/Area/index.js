import React from 'react'
import { Col, Row, Modal, Card } from 'antd'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import pathToRegexp  from 'path-to-regexp'
import BasicInfo from '../basicInfo'
import Cards from '../createCards'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';


@connect(({ infoArea, loading }) => ({
  infoArea,
  loading: loading.effects['infoArea/list'],
}))
export default class Area extends React.Component {
    componentWillMount () {
      const { location, dispatch } = this.props;
      const match = pathToRegexp('/gridInfoManage/area/:id').exec(location.pathname);
      dispatch({
          type: 'infoArea/list',
          payload: {
              departmentId: match[1],
          },
      });
      dispatch({
          type: 'infoArea/updateState',
          payload: {
              departmentId: match[1],
          },
      });
      dispatch({
        type: 'infoArea/initBasic',
      })
    }
    render () {
        const { dataSource, departmentId, personInfo, houseInfo, personTypeInfo, name, houseType } = this.props.infoArea
        const CardsProps = {
          houseType,
          dataSource,
          showedArray: ['片区/name', '编号/number'],
          actiontypes: [ 'edit/编辑', 'delete/删除', 'right/网格'],
          isShowPreButton: true,
          isShowAddButton: true,
          addClick: () => {
              this.props.dispatch(routerRedux.push({
                  pathname: `/gridInfoManage/areaAdd/${departmentId}`,
              }))
          },
          preClick: () => {
              this.props.dispatch(routerRedux.push({
                  pathname: `/gridInfoManage/department/`,
              }))
          },
          handleClick: (current) => {
              this.props.dispatch({
                  type: 'infoArea/getBaseInfo',
                  payload: {
                      districtId: current.id,
                  },
              })
              this.props.dispatch({
                  type: 'infoArea/updateState',
                  payload: {
                      name: current.name,
                  },
              })
            },
            onClick: (value, type) => {
                switch (type) {
                    case 'right':
                        this.props.dispatch(routerRedux.push({
                            pathname: `/gridInfoManage/grid/${value.id}`,
                        }))
                        break;
                    case 'edit':
                        this.props.dispatch(routerRedux.push({
                            pathname: `/gridInfoManage/areaEdit/${value.id}`,
                        }))
                        break;
                    case 'delete':
                        Modal.confirm({
                            title: '删除',
                            content: '确认删除这这个片区吗',
                            onOk: () => {
                                this.props.dispatch({
                                    type: 'infoArea/delCard',
                                    payload: {
                                        id: value.id,
                                    },
                                })
                            },
                        })
                        break;
                    default:    
                        break;
                }
            },
        }
        const BasicInfoProps = {
            personInfo: personInfo || {}, 
            houseInfo: houseInfo || {}, 
            personTypeInfo: personTypeInfo || {},
            name,
        }
        return (
          <PageHeaderLayout>
            <Card loading={this.props.loading}>
              <Row>
                <Col span={17}> <Cards {...CardsProps} /> </Col>
                <Col span={7}> <BasicInfo {...BasicInfoProps} /> </Col>
              </Row>
            </Card>
          </PageHeaderLayout>
        )
    }
}
