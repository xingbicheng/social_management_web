import React from 'react'
import PropTypes from 'prop-types'
import { Col, Row, Form, Card, Input } from 'antd'
import styles from './index.less'

const FormItem = Form.Item
const formItemLayout = {
    labelCol: {
        span: 9,
    },
    wrapperCol: {
        span: 15,
    },
}
class BasicInfo extends React.Component {
    create = (array = [], unit, cols, dataSource = {}) => {    
        dataSource = dataSource || {}
        const spans = 24 / cols
        const { getFieldDecorator } = this.props.form
        const changedArray = array.map( current => {
            const result = current.split('/')
            return {
                label: result[0],
                id: result[1],
            }
        })
        const result = changedArray.map( (current, index) => {
            return (
              <Col span={spans} key={index}> 
                <FormItem label={current.label} {...formItemLayout}>
                  {
                      getFieldDecorator(current.id, {
                          initialValue: dataSource[current.id] || '0',
                      })(<Input addonAfter={unit} disabled />)
                  }
                </FormItem>
              </Col>
            )
        })
        return <Row>{result}</Row>
    }
    render () {
        const { getFieldDecorator } = this.props.form
        let { personInfo, houseInfo, personTypeInfo, name } = this.props
        personTypeInfo.politicalOutlook  = personInfo.politicalOutlook
        const people = ['男/man', '女/girl']
        const house = ['住宅/residential', '公房/common', '出租房/rental', '临时房/temporary']
        const other = ['精神病人/psychiatricPatients', '留守儿童/children', '安置帮教对象/esettlementEducation', '重点信访/keyPetitions', '社区戒毒/communityDetoxification', '社区康复/communityRehabilitation', '党员/politicalOutlook']
        return (
          <Form>
            <Card className={styles.CardHeader}><div className={styles.title}>{name}基本情况</div></Card>
            <Card title="总人口数">
              <Row>
                <Col span={24}>
                  <FormItem label="共" {...formItemLayout}>
                    {
                        getFieldDecorator('total', {
                            initialValue: personInfo.total || '0',
                        })(<Input addonAfter="人" disabled />)
                    }
                  </FormItem>
                </Col>
              </Row>
              {this.create(people, '人', 2, personInfo)}
            </Card>
            <Card title="总房屋数">
              <Row>
                <Col span={24}>
                  <FormItem label="共" {...formItemLayout}>
                    {
                        getFieldDecorator('total', {
                            initialValue: houseInfo.total || '0',
                        })(<Input addonAfter="间" disabled />)
                    }
                  </FormItem>
                </Col>
              </Row>
              {this.create(house, '间', 2, houseInfo)}
            </Card>
            <Card title="人员类型">
              {this.create(other, '人', 1, personTypeInfo)}    
            </Card>
          </Form>
        )
    }
}

BasicInfo.propTypes = {

}

export default Form.create()(BasicInfo)
