import React from 'react'
import PropTypes from 'prop-types'
import { Col, Row, Card, Icon, Button } from 'antd'
import styles from './index.less'

class Cards extends React.Component {

    handleMenuClick = (value, type) => {
        const { onClick } = this.props
        onClick(value, type)
    }

    createCards = (dataSource, array, actiontypes, houseType ) => {
        const changedArray = array.map( current => {
            const result = current.split('/')
            return {
                label: result[0],
                id: result[1],
            }
        })
        const changedActiontypes = actiontypes.map( current => {
            const result = current.split('/')
            return {
                type: result[0],
                text: result[1],
            }
        })
        const result = dataSource.map((current, index) => {
          if (current.status === 2) {
            return 
          }
          current.houseType = houseType[current.houseType];
            const actions = changedActiontypes.map((value, index) => {
                return <Icon type={value.type} onClick={(e) => { e.stopPropagation();this.handleMenuClick(current, value.type); }} key={index} style={{fontSize:'12px'}}>{value.text}</Icon>
            })
            return (
              <Col span={8} className={styles.Cards} onClick={ () => { this.props.handleClick(current) }} key={index} title="单击卡片显示基本情况">
                <Card actions={actions} style={{ margin: "10px"}} hoverable={true}>
                  {
                      changedArray.map((value, index) => (<p className={styles.CardsText} key= {index}>{value.label}:&nbsp;{current[value.id]}</p>))
                  }
                </Card>
              </Col>
            )
        })
        return result
    }
    render () {
        const { dataSource, showedArray, actiontypes, addClick, preClick, isShowPreButton, isShowAddButton, houseType } = this.props
        return (
          <Card>
            <Row type="flex" justify="end">
              {
                isShowPreButton &&
                <Col style={{marginLeft: '5px', marginBottom: '5px'}}>
                    <Button icon="left" type="primary" onClick={preClick}>上一级</Button>
                </Col>
              }
              {
                isShowAddButton &&
                <Col style={{marginLeft: '5px', marginBottom: '5px'}}>
                    <Button icon="plus" type="primary" onClick={addClick}>添加</Button>
                </Col>
              }
            </Row>
            <Row>
              {
                this.createCards(dataSource, showedArray, actiontypes, houseType)
              }
            </Row>
            {
                dataSource.length === 0 &&
                <div className={styles.empty}>暂无数据</div>
            }
          </Card>
        )
    }
}

Cards.propTypes = {

}

export default Cards
