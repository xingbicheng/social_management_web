import React from 'react'
import { Col, Row, Card, message } from 'antd'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import BasicInfo from '../basicInfo'
import Cards from '../createCards'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

@connect(({ infoDepartment, loading }) => ({
    infoDepartment,
    loading: loading.effects['infoDepartment/listAll'],
}))
export default  class Department extends React.Component {
    componentDidMount () {
      const { dispatch } = this.props
      dispatch({
          type: 'infoDepartment/clearState',
      })
      dispatch({
          type: 'infoDepartment/listAll',
      })
      dispatch({
        type: 'infoDepartment/initBasic',
      })
    }
    render () {
        const { dataSource, personInfo, houseInfo, personTypeInfo, name, houseType } = this.props.infoDepartment
        const CardsProps = {   
          houseType,
          dataSource,
          showedArray: ['部门/name', '缩写/abridge'],
          actiontypes: ['right/片区'],
          isShowPreButton: false,
          isShowAddButton: false,
          onClick: (value, type) => {
            if (value.status === '2') {
              message.info('此部门已停用，不能查看片区')
              return 
            }
            this.props.dispatch(routerRedux.push({   
                pathname: `/gridInfoManage/area/${value.id}`,
            }))
          },
          handleClick: (current) => {
            this.props.dispatch({
                type: 'infoDepartment/getBaseInfo',
                payload: {
                    departmentId: current.id,
                },
            })
            this.props.dispatch({
                type: 'infoDepartment/updateState',
                payload: {
                    name: current.name,
                },
            })
          },
        }
        const BasicInfoProps = {
            personInfo: personInfo || {}, 
            houseInfo: houseInfo || {}, 
            personTypeInfo: personTypeInfo || {},
            name,
        }
        return (
          <PageHeaderLayout >
            <Card loading={this.props.loading} bordered={false}>
              <Row> 
                <Col span={17}> <Cards {...CardsProps} /> </Col>
                <Col span={7}> <BasicInfo {...BasicInfoProps} /> </Col>
              </Row>
            </Card>
          </PageHeaderLayout>
        )
    }
}