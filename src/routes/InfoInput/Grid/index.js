import React from 'react'
import pathToRegexp  from 'path-to-regexp'
import { Col, Row, Modal, Card } from 'antd'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import BasicInfo from '../basicInfo'
import Cards from '../createCards'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

@connect(({ infoGrid, loading }) => ({
  infoGrid,
  loading: loading.effects['infoGrid/list'],
}))
export default class Gird extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            
        }
    }
    componentDidMount () {
      const { dispatch, location } = this.props
      dispatch({
          type: 'infoGrid/clearState',
      })
      const match = pathToRegexp('/gridInfoManage/grid/:id').exec(location.pathname)
      if (match) {
        dispatch({
            type: 'infoGrid/list',
            payload: {
                districtId: match[1],
            },
        })
        dispatch({
            type: 'infoGrid/updateState',
            payload: {
                history,
                districtId: match[1],
            },
        })
        dispatch({
          type: 'infoGrid/initBasic',
        })
      }
    }
    render () {
        const { dataSource, districtId, personInfo, houseInfo, personTypeInfo, name, houseType } = this.props.infoGrid
        const CardsProps = {
          houseType,
          dataSource,
          showedArray: ['网格/name', '编号/number'],
          actiontypes: ['edit/编辑', 'delete/删除', 'right/房屋'],
          isShowPreButton: true,
          isShowAddButton: true,
          onClick: (value, type) => {
              switch (type) {
                  case 'right':
                      this.props.dispatch(routerRedux.push({
                          pathname: `/gridInfoManage/house/${value.id}`,
                      }))
                      break;
                  case 'edit':
                      this.props.dispatch(routerRedux.push({
                          pathname: `/gridInfoManage/gridEdit/${value.id}`,
                          state: {
                          },
                      }))
                      break;
                  case 'delete':
                      Modal.confirm({
                          title: '删除',
                          content: '确认删除这这个网格吗',
                          onOk: () => {
                              this.props.dispatch({
                                  type: 'infoGrid/delCard',
                                  payload: {
                                      id: value.id,
                                  },
                              })
                          },
                      })
                      break;
                  default:
                      break;
              }
            },
            addClick: () => {
                this.props.dispatch(routerRedux.push({
                    pathname: `/gridInfoManage/gridAdd/${districtId}`,
                }))
            },
            preClick: () => {
                this.props.history.goBack();
            },
            handleClick: (current) => {
                this.props.dispatch({
                    type: 'infoGrid/getBaseInfo',
                    payload: {
                        mershId: current.id,
                    },
                })
                this.props.dispatch({
                    type: 'infoGrid/updateState',
                    payload: {
                        name: current.name,
                    },
                })
            },
        }
        const BasicInfoProps = {
            personInfo: personInfo || {}, 
            houseInfo: houseInfo || {}, 
            personTypeInfo: personTypeInfo || {},
            name,
        }
        return (
          <PageHeaderLayout>
            <Card loading={this.props.loading}>
              <Row>
                <Col span={17}> <Cards {...CardsProps} /> </Col>
                <Col span={7}> <BasicInfo {...BasicInfoProps} /> </Col>
              </Row>
            </Card>
          </PageHeaderLayout>
        )
    }
}