import React from 'react'
import pathToRegexp  from 'path-to-regexp'
import PropsTypes from 'prop-types'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import { Col, Row, Form, Button, Card, Input, Modal } from 'antd'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout'
import AutoList from '../../../components/List/List'
import { gender } from '../../../utils/seletLocalData'
import AddModal from '../Area/addModal.js'
import styles from '../index.less'

const FormItem = Form.Item
const colLayout = {
    span: 8,
}
const FormItemLaylout = {
    labelCol: {
        span: 5,
    },
    wrapperCol: {
        span: 18,
    },
}

@Form.create()
@connect(({ infoGridDetail, loading }) => ({
  infoGridDetail,
  loading: loading.models.infoGridDetail,
}))
export default class GirdDetail extends React.Component {
    componentWillMount () {
      const { location, dispatch } = this.props
      const matchEdit = pathToRegexp('/gridInfoManage/gridEdit/:id').exec(location.pathname)
      const matchAdd = pathToRegexp('/gridInfoManage/gridAdd/:id').exec(location.pathname)
      if (matchEdit) {
          dispatch({
              type: 'infoGridDetail/clearState',
          })
          dispatch({
              type: 'infoGridDetail/getGrid',
              payload: {
                  id: matchEdit[1],
              },
          })
          dispatch({
              type: 'infoGridDetail/updateState',
              payload: {
                  type: 'edit',
              },
          })
      }else if (matchAdd) {
          dispatch({
              type: 'infoGridDetail/clearState',
          })
          dispatch({
              type: 'infoGridDetail/updateState',
              payload: {
                  districtId: matchAdd[1],
                  type: 'add',
              },
          })
          dispatch({
              type: 'infoGridDetail/initBasic',
              payload: {
                  id: matchAdd[1],
              },
          })
        }
    }
    save = () => {
        const { getFieldsValue, validateFields } = this.props.form
        validateFields((error, values) => {
            if (error) {
                return 
            } else {
                this.props.dispatch({
                    type: 'infoGridDetail/updateState',
                    payload: {
                        saveLoading: true,
                    },
                })
                const values = getFieldsValue()
                const { districtId, gridDetail, type } = this.props.infoGridDetail
                if (type === 'add') {
                    this.props.dispatch({
                        type: 'infoGridDetail/save',
                        payload: {
                            districtId,
                            ...values,
                        },
                    })
                } else if (type === 'edit') {
                    this.props.dispatch({
                        type: 'infoGridDetail/edit',
                        payload: {
                            ...gridDetail,
                            districtId,
                            ...values,
                        },
                    })
                }
                // resetFields()
            }
        })

    }
    create = () => {
        this.props.dispatch({
            type: 'infoGridDetail/updateState',
            payload: {
                modalVisable: true,
                modalType: 'create',
                currentItem: {},
                confirmLoading: false,
            },
        })
    }
    delRow = (record) => {
        Modal.confirm({
          title: '删除',
          content: '确定删除此项？',
          cancelText:'取消',
          okText:'确定',
          onOk: () => {
            this.props.dispatch({
                type: 'infoGridDetail/delTeam',
                payload: {
                  id: record.id,
                },
            })
          },
          onCancel() {
            
          },
        });
    }
    editRow = (record) => {
        this.props.dispatch({
            type: 'infoGridDetail/updateState',
            payload: {
                modalVisable: true,
                modalType: 'edit',
                currentItem: record,
                confirmLoading: false,
            },
        })
    }
    handleCancle = () => {
        Modal.confirm({
            content: '你确定要取消当前操作吗? 这样除了网格服务团队信息以外的新增或者修改的信息将不会被保存',
            onOk: () => {
                this.props.history.goBack()
            },
            okText: '确定',
            cancelText: '取消',
        })
    }
    render () {
    const { getFieldDecorator }  = this.props.form
    const { selectedRowKeys, gridDetail, area, currentItem, modalVisable, modalType, serviceTeams ,type, saveLoading, confirmLoading } = this.props.infoGridDetail
    const listProps={
        dataSource: serviceTeams,
        loading: this.props.loading,
        // pagination: {
        //   ...pagination,
        //   onChange (page, pageSize) {
        //     dispatch({
        //       type: 'department/updateState',
        //       payload: {
        //         pagination: {
        //           pageSize,  
        //           current: page,   
        //         }
        //       }
        //     })
        //   }
        // },
        columsList: {
          columns: [
            {
                title: '序号',
                key: 'name1',
                render: (text, record, index) => {
                    return <span>{index + 1}</span>
                },
            },{
                title: '名称',
                dataIndex: 'branch',
                key: 'branch',
            },{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
            },{
                title: '性别',
                dataIndex: 'sex',
                key: 'sex',
            },{
                title: '年龄',
                dataIndex: 'age',
                key: 'age',
            },{
                title: '职务',
                dataIndex: 'position',
                key: 'position',
            },{
                title: '联系电话',
                dataIndex: 'phone',
                key: 'phone',
              },
          ],
          menulistArr : [{
            key:'del',
            name:'删除',
            menuclick:this.delRow,
          },{
            key:'edit',
            name:'编辑',
            menuclick:this.editRow,
          }],
        },
        // rowSelection: {
        //   selectedRowKeys,
        //   onChange: (selectedRowKeys, selectedRows) => {
        //     this.props.dispatch({
        //       type: 'infoGridDetail/updateState',
        //       payload: {
        //         selectedRowKeys,
        //         currentItem: selectedRows,
        //       },
        //     })
        //   },
        // },
    }
    const ModalProps = {
        options: {
            gender,
        },
        currentItem,
        visible: modalVisable,
        confirmLoading,
        title: modalType === 'create' ? '添加-网格服务团队' : '编辑-网格服务团队',
        handleOk: (values) => {
            this.props.dispatch({
                type: 'infoGridDetail/updateState',
                payload: {
                    confirmLoading: true,
                },
            })
            if ( modalType === 'create') {
                this.props.dispatch({
                    type: 'infoGridDetail/addTeam',
                    payload: {
                        mershId: gridDetail.id,
                        team: values,
                    },
                })
            } else if (modalType === 'edit') {
                values = { ...currentItem, ...values }
                this.props.dispatch({
                    type: 'infoGridDetail/editTeam',
                    payload: {
                        mershId: gridDetail.id,
                        team: values,
                    },
                })
            }
            this.props.dispatch({
                type: 'infoGridDetail/updateState',
                payload: {
                    modalVisable: false,
                },
            })
        },
        onCancel: () => {
            this.props.dispatch({
                type: 'infoGridDetail/updateState',
                payload: {
                    modalVisable: false,
                    currentItem: {},
                },
            })
        },
    }
    const createButton = (<Button type="primary" onClick={this.create} icon="plus">添加</Button>)
    return (
      <PageHeaderLayout>
        <Card title="网格录入">
          <Form>
            <Row>
              <Col {...colLayout}> 
                <FormItem {...FormItemLaylout} label="所在片区">
                  {
                      getFieldDecorator('department', {
                          initialValue: area.name || '',
                          rules: [{ required: true, message: "所在片区是必填项", whitespace: true }],
                      })(<Input disabled />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout}>
                <FormItem {...FormItemLaylout} label="网格名称">
                  {
                      getFieldDecorator('name', {
                          initialValue: gridDetail.name || '',
                          rules: [{ required: true, message: "网格名称是必填项", whitespace: true }],
                      })(<Input />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout}>
                <FormItem {...FormItemLaylout} label="网格编号">
                  {
                      getFieldDecorator('number', {
                          initialValue: gridDetail.number || '',
                          rules: [{ whitespace: true }]
                      })(<Input disabled />)
                  }
                </FormItem>
              </Col>
            </Row>
          </Form>
          {
            (type === 'edit') &&
            <Card title="网格服务团队" extra={createButton}>
                <AutoList {...listProps}/>
            </Card>
          }
          <div className={styles.btnBox}>
            <div className={styles.saveAndreturn}>
              <Button icon="close" onClick={this.handleCancle}>返回</Button>
              <Button icon="save" type="primary" onClick={this.save} loading={saveLoading}>保存</Button>
            </div>
          </div> 
          <AddModal {...ModalProps} />

        </Card>
      </PageHeaderLayout>
    )
    }
}

GirdDetail.propsTypes = {

}