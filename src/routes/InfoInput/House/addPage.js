import pathToRegexp  from 'path-to-regexp'
import React from 'react'
import { Col, Row, Form, Modal, Input, Select, DatePicker, Card ,Button, Radio, Cascader } from 'antd'
import PropTypes from 'prop-types'
import moment from 'moment'
import { connect } from 'dva';
import { gender, nations, place } from '../../../utils/seletLocalData'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout'
import styles from '../index.less'
import { changeToArray, changeToString } from '../../../utils/utils';

const FormItem = Form.Item
const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 17,
    } ,
}
const formItemLayout1 = {
    labelCol: {
        span: 10,
    },
    wrapperCol: {
        span: 14,
    },
}
const colLayout = {
    span: 8,
}
const Option = Select.Option
const RadioGroup = Radio.Group;

@Form.create()
@connect(({ infoHouseAddPerson, loading }) => ({
  infoHouseAddPerson,
  loading: loading.effects['infoHouseAddPerson/searchPerson'] || loading.effects['infoHouseAddPerson/getCurrent'],
}))
export default class AddPage extends React.Component {
    componentDidMount () {
      const { location, dispatch } = this.props
      const matchAdd = pathToRegexp('/gridInfoManage/houseAddPerson/:id/:idCard').exec(location.pathname)
      const matchEdit = pathToRegexp('/gridInfoManage/houseEditPerson/:houseId/:itemId/:personId').exec(location.pathname)
      if (matchAdd) {
          dispatch({
              type: 'infoHouseAddPerson/clearState',
          })
          dispatch({
              type: 'infoHouseAddPerson/updateState',
              payload: {
                  houseId: matchAdd[1],
                  type: 'addPerson',
                  idCard: matchAdd[2],
              },
          })
          dispatch({
              type: 'infoHouseAddPerson/searchPerson',
              payload: {
                  idCard: matchAdd[2],
              },
          })
          dispatch({
              type: 'infoHouseAddPerson/initBasic',
          })
      } else if (matchEdit) {
          dispatch({
              type: 'infoHouseAddPerson/clearState',
          })
          dispatch({
              type: 'infoHouseAddPerson/updateState',
              payload: {
                  houseId: matchEdit[1],
                  type: 'editPerson',
              },
          })
          dispatch({
              type: 'infoHouseAddPerson/getCurrent',
              payload: {
                  id: matchEdit[1],
                  itemId: matchEdit[2],
              },
          })
          dispatch({
              type: 'infoHouseAddPerson/initBasic',
          })
      }
    }
    handleOkClick = () => {
        const { getFieldsValue, validateFields } = this.props.form
      const { location } = this.props
        this.props.dispatch({
            type: 'infoHouseAddPerson/updateState',
            payload: {
                saveLoading: true,
            },
        })
        const baseInfoFieldsName = ['name', 'idCard', 'gender', 'nation', 'householdRegisterPlace', 'politicalOutlook', 'degreeEducation', 'householdRegisterNature', 'phone', 'maritalStatus', 'isDie', 'birthday']
        const GridinfoFieldsName = ['isPsychiatricPatients', 'isEsettlementEducation', 'isCommunityDetoxification', 'isCommunityRehabilitation', 'isKeyPetitions', 'isChildren']
        const { houseId, type } = this.props.infoHouseAddPerson
        validateFields((errors, values) => {
            if (errors) {
                return
            } else {
                const baseInfo = getFieldsValue(baseInfoFieldsName)
                const GridInfo = getFieldsValue(GridinfoFieldsName)
              baseInfo.householdRegisterPlace = changeToString(baseInfo.householdRegisterPlace);
                if (type === 'addPerson') {
                    this.props.dispatch({
                        type: 'infoHouseAddPerson/addPerson',
                        payload: {
                            houseId,
                            person: {
                                baseInfo,
                                ...GridInfo,
                                idCard: baseInfo.idCard,
                            },
                        },
                    })
                } else if (type === 'editPerson') {
                  console.log(baseInfo, GridInfo)
                  const matchEdit = pathToRegexp('/gridInfoManage/houseEditPerson/:houseId/:itemId/:personId').exec(location.pathname)
                  const id = matchEdit[2],
                        personId = matchEdit[3];
                  baseInfo.id = personId;
                  console.log(baseInfo);
                    this.props.dispatch({
                        type: 'infoHouseAddPerson/editPerson',
                        payload: {
                            houseId,
                            person: {
                                baseInfo,
                                ...GridInfo,
                                idCard: baseInfo.idCard,
                              'id': id,
                              'houseId': houseId,
                            },
                        },
                    })
                }
            }
        })
    }
    handleCancle = () => {
        Modal.confirm({
            content: '你确定要取消当前操作吗? 这样你新增或修改的信息将不会被保存',
            onOk: () => {
                this.props.history.goBack()
            },
            okText: '确定',
            cancelText: '取消',
        })
    }
    render () {
        const { political_outlook, degree_education, household_register_nature, currentItem, maritalStatus, saveLoading, idCard } = this.props.infoHouseAddPerson
        const { getFieldDecorator } = this.props.form
        const sexOptions = gender.map( current => {
            return <Option key={current.k}>{current.val}</Option>
        })
        const politicalOutlookOptions = political_outlook.map( current => {
            return <Option key={current.k}>{current.val}</Option>
        })
        const degreeEducationOptions = degree_education.map( current => {
            return <Option key={current.k}>{current.val}</Option>
        })
        const householdRegisterNatureOptions = household_register_nature.map( current => {
            return <Option key={current.k}>{current.val}</Option>
        })
        const maritalStatusOptions = maritalStatus.map( current => {
            return <Option key={current.k}>{current.val}</Option>
        })
        const nationOptions = nations.map( current => {
            return <Option key={current.name}>{current.name}</Option>
        })
        const options = [{label: '是', value: '1'}, {label: '否', value: '0'}]
        return (
          <PageHeaderLayout>
            <Card loading={this.props.loading}>
              <Card bordered={false} title="基本信息">
                <Row>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="姓名">
                      {
                          getFieldDecorator("name", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['name'] || '',
                              rules: [{ required: true, message: "姓名是必填项", whitespace: true }],
                          })(<Input />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="性别">
                      {
                          getFieldDecorator("gender", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['gender'] || '',
                              rules: [{ required: true, message: "性别是必填项" }],
                          })(<Select style={{ width: "100%" }}>{sexOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="出生年月">
                      {
                          getFieldDecorator("birthday", {
                              initialValue: moment(currentItem.baseInfo && currentItem.baseInfo['birthday']) || moment(),
                              rules: [{ required: true, message: "出生年月是必填项" }]
                          })(<DatePicker style={{ width: "100%" }} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="民族">
                      {
                          getFieldDecorator("nation", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['nation'] || '',
                              rules: [{ required: true,  message: "民族是必填项且必须为整数" }],
                          })(<Select style={{ width: "100%" }}>{nationOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="身份证号">
                      {
                          getFieldDecorator("idCard", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['idCard'] || idCard || '',
                              rules: [{ required: true, pattern:  /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: "请填入正确的身份证号码", whitespace: true }],
                          })(<Input />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout}>
                    <FormItem {...formItemLayout} label="政治面貌">
                      {
                          getFieldDecorator("politicalOutlook", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['politicalOutlook'] || '',
                              rules: [{ required: true, message: "政治面貌是必填项" }],
                          })(<Select style={{ width: "100%" }}>{politicalOutlookOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="文化程度">
                      {
                          getFieldDecorator("degreeEducation", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['degreeEducation'] || '',
                              rules: [{ required: true, message: "文化程度是必填项" }],
                          })(<Select style={{ width: "100%" }}>{degreeEducationOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="户口性质">
                      {
                          getFieldDecorator("householdRegisterNature", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['householdRegisterNature'] || '',
                              rules: [{ required: true, message: "户口性质是必填项" }],
                          })(<Select style={{ width: "100%" }}>{householdRegisterNatureOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="联系方式">
                      {
                          getFieldDecorator("phone", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['phone'] || '',
                              rules: [{ required: true, pattern: /^1[3|4|5|7|8][0-9]{9}$/, message: "请填入正确的电话号码", whitespace: true }],
                          })(<Input />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="婚姻状况">
                      {
                          getFieldDecorator("maritalStatus", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['maritalStatus'] || '',
                              rules: [{ required: true,  message: "婚姻状况是必填项" }],
                          })(<Select style={{ width: "100%" }}>{maritalStatusOptions}</Select>)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="户籍信息">
                      {
                          getFieldDecorator("householdRegisterPlace", {
                              initialValue: changeToArray(currentItem.baseInfo && currentItem.baseInfo['householdRegisterPlace'] || '') || [],
                              rules: [{ required: true, message: "户籍信息是必填项" }],
                          })(  <Cascader options={place} style={{width: '100%'}} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout} label="是否死亡">
                      {
                          getFieldDecorator("isDie", {
                              initialValue: currentItem.baseInfo && currentItem.baseInfo['isDie'] || '',
                              rules: [{ required: true, message: "是否死亡是必填项" }],
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                </Row>
              </Card>
              <Card title="其他">
                <Row>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label=" 是否是精神病人矫正对象">
                      {
                          getFieldDecorator("isPsychiatricPatients", {
                              initialValue: currentItem['isPsychiatricPatients'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label="是否是安置帮教对象">
                      {
                          getFieldDecorator("isEsettlementEducation", {
                              initialValue: currentItem['isEsettlementEducation'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label="是否是社区戒毒对象">
                      {
                          getFieldDecorator("isCommunityDetoxification", {
                              initialValue: currentItem['isCommunityDetoxification'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label="是否是社区康复对象">
                      {
                          getFieldDecorator("isCommunityRehabilitation", {
                              initialValue: currentItem['isCommunityRehabilitation'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label="是否是重点信访人员">
                      {
                          getFieldDecorator("isKeyPetitions", {
                              initialValue: currentItem['isKeyPetitions'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayout} >
                    <FormItem {...formItemLayout1} label="是否是留守儿童">
                      {
                          getFieldDecorator("isChildren", {
                              initialValue: currentItem['isChildren'] || '',
                          })(<RadioGroup options={options} />)
                      }
                    </FormItem>
                  </Col>
                </Row>
                <div className={styles.btnBox}>
                  <div className={styles.saveAndreturn}>
                    <Button icon="close" onClick={this.handleCancle}>返回</Button>
                    <Button icon="save" type="primary"onClick={this.handleOkClick} loading={saveLoading}>保存</Button>
                  </div>
                </div>
              </Card>
            </Card>
          </PageHeaderLayout>
        )
    }
}
