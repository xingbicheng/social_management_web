import React from 'react'
import { Col, Row, Form, Modal, Input } from 'antd'
import PropTypes from 'prop-types'
import styles from '../index.less'

const FormItem = Form.Item
const formItemLayout = {
    labelCol: {
        span: 6,
    },
    wrapperCol: {
        span: 17,
    } ,
}
const colLayout = {
    span: 24,
}
const Search = Input.Search
@Form.create()

class AddModal extends React.Component {
    handleOkClick = () => {
        const { getFieldsValue, validateFieldsAndScroll } = this.props.form
        const { handleOk } = this.props
        validateFieldsAndScroll((errors, values) => {
            if (!errors) {
                const values = getFieldsValue()
                handleOk(values)
            }
        });
    }
    handleCancel = () => {
        const { resetFields } = this.props.form
        const { onCancel } = this.props
        resetFields()
        onCancel()
    }
    handleSearch = () => {
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const { onSerach } = this.props;
                onSerach(values.idCard)
            }
        });
    }
    render () {
        const { getFieldDecorator } = this.props.form
        const { isFindPerson, personBase, isLocal } = this.props
        return (
          <Modal {...this.props} onOk={this.handleOkClick} onCancel={this.handleCancel}>
            <Row>
              <Col {...colLayout}>
                <Form>
                  <FormItem {...formItemLayout} label="身份证">
                    {
                      getFieldDecorator('idCard', {
                          initialValue: '',
                          rules: [{ required: true, pattern:  /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/, message: "请填入正确的身份证号码" }],
                      })(<Search style={{ width: '100%' }} enterButton onSearch={this.handleSearch} placeholder="请输入身份证号" />)
                  }
                  </FormItem>
                </Form>
              </Col>
              <Col {...colLayout}> 
                {
                    isFindPerson === 'notFind' &&
                    <span className={styles.houseDetailSpan}>未找到此人的身份信息，点击<a onClick={this.props.goAdd}>添加</a>,去添加身份信息</span>
                }
                {
                    isFindPerson === 'Find' &&
                    <span className={styles.houseDetailSpan}>已找到身份证号<strong>{personBase.idCard}</strong>名为<strong>{personBase.name}</strong>的身份信息点击<a onClick={this.props.goAdd}>完善</a>去完善信息</span>
                }
                {
                    isLocal === 'localFind' && 
                    <span className={styles.houseDetailSpan}>此身份证号已存在本列表中，请尝试换一个身份证号</span>
                }
              </Col>
            </Row>
          </Modal>
        )
    }
}

export default Form.create()(AddModal)