import React from 'react'
import pathToRegexp  from 'path-to-regexp'
import PropsTypes from 'prop-types'
import { Col, Row, Form, Button, Card, Input, Select, Modal } from 'antd'
import { routerRedux } from 'dva/router'
import { connect } from 'dva'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout'
import AutoList from '../../../components/List/List'
import styles from '../index.less'
import AddModal from './addModal'
import { findRecord } from '../../../utils/utils'

const FormItem = Form.Item
const { Option } = Select
const colLayout = {
    span: 6,
}
const colLayoyt1 = {
  span: 8,
}
const FormItemLaylout = {
    labelCol: {
        span: 9,
    },
    wrapperCol: {
        span: 15,
    },
}

const TextAreaFormItemLaylout = {
    labelCol: {
        span: 5,
    },
    wrapperCol: {
        span: 19,
    },
}

@Form.create()
@connect(({ infoHouseDetail, loading }) => ({
  infoHouseDetail,
  loading: loading.models.infoHouseDetail,
}))
export default class HouseDetail extends React.Component {
    componentDidMount () {
      const { location, dispatch } = this.props
      const matchEdit = pathToRegexp('/gridInfoManage/houseEdit/:id').exec(location.pathname)
      const matchAdd = pathToRegexp('/gridInfoManage/houseAdd/:id').exec(location.pathname)
      if (matchEdit) {
          dispatch({
              type: 'infoHouseDetail/clearState',
          })
          dispatch({
              type: 'infoHouseDetail/initBasic',
          })
          dispatch({
              type: 'infoHouseDetail/get',
              payload: {
                  id: matchEdit[1],
              },
          })
          dispatch({
              type: 'infoHouseDetail/getFathersByhouseId',
              payload: {
                  id: matchEdit[1],
              },
          })
          dispatch({
              type: 'infoHouseDetail/updateState',
              payload: {
                  type: 'edit',
              },
          })
      }else if (matchAdd) {
          dispatch({
              type: 'infoHouseDetail/clearState',
          })
          dispatch({
              type: 'infoHouseDetail/initBasic',
          })
          dispatch({
              type: 'infoHouseDetail/updateState',
              payload: {
                  mershId: matchAdd[1],
                  type: 'add',
              },
          })
          dispatch({
              type: 'infoHouseDetail/getFathersByMershId',
              payload: {
                  id: matchAdd[1],
              },
          })
      }
    }
    save = () => {
        const { getFieldsValue, validateFields } = this.props.form
        validateFields((error, values) => {
            if (error) {
                return
            } else {
                this.props.dispatch({
                    type: "infoHouseDetail/updateState",
                    payload: {
                        saveLoading: true,
                    },
                })
                const values = getFieldsValue()
                const { mershId, type, house } = this.props.infoHouseDetail
                if (type === 'add') {
                    this.props.dispatch({
                        type: "infoHouseDetail/save",
                        payload: {
                            ...values,
                            mershId,
                        },
                    })
                } else if (type === 'edit') {
                    this.props.dispatch({
                      type: "infoHouseDetail/edit",
                      payload: {
                          ...house,
                          ...values,
                          mershId,
                      },
                    })
                }
            }
        })

    }
    handleCancle = () => {
      Modal.confirm({
          content: '你确定要取消当前操作吗? 这样除了实有人口信息以外的新增或者修改的信息将不会被保存',
          onOk: () => {
              this.props.history.goBack()
          },
          okText: '确定',
          cancelText: '取消',
      })
    }
    create = () => {
        this.props.dispatch({
            type: 'infoHouseDetail/updateState',
            payload: {
                ModalType: 'create',
                ModalVisable: true,
            },
        })
    }
    delRow = (record) => {
        Modal.confirm({
          title: '删除',
          content: '确定删除此项？',
          cancelText:'取消',
          okText:'确定',
          onOk: () => {
            this.props.dispatch({
                type: 'infoHouseDetail/delPerson',
                payload: {
                  id: record.id,
                },
            })
          },
          onCancel: () => {
          },
        });
    }
    editRow = (record) => {
        const { house } = this.props.infoHouseDetail
        this.props.dispatch(routerRedux.push({
            pathname: `/gridInfoManage/houseEditPerson/${house.id}/${record.id}/${record.baseInfo.id}`,
        }))
    }
    render () {
        const { getFieldDecorator }  = this.props.form
        const { house, selectedRowKeys, department, area, grid, persons, houseType, ModalType, ModalVisable , idCard, isFindPerson, personBase, isLocal, saveLoading, searchLoading } = this.props.infoHouseDetail
      const genderType = ['男', '女'];
        const listProps = {
            dataSource: persons,
            loading: this.props.loading,
            // pagination: {
            //   ...pagination,
            //   onChange (page, pageSize) {
            //     dispatch({
            //       type: 'department/updateState',
            //       payload: {
            //         pagination: {
            //           pageSize,
            //           current: page,
            //         }
            //       }
            //     })
            //   }
            // },
            columsList: {
              columns: [
                {
                  title: '序号',
                  key: 'name1',
                  render: (text, record, index) => {
                    return <span>{index + 1}</span>
                  },
                },{
                  title: '姓名',
                  dataIndex: 'name',
                  key: 'name',
                },{
                  title: '性别',
                  dataIndex: 'gender',
                  key: 'gender',
                  render(record) {
                    return genderType[record];
                  },
                },{
                  title: '出生年月',
                  dataIndex: 'birthday',
                  key: 'birthday',
                },{
                    title: '民族',
                    dataIndex: 'nation',
                    key: 'nation',
                },{
                    title: '身份证件号码',
                    dataIndex: 'idCard',
                    key: 'idCard',
                    render:(text)=>{
                        const data=text.replace(/^(.{4})(.*)(.{4})$/,'$1****$3');
                        return data;
                    },
                },
              ],
              menulistArr : [{
                key:'del',
                name:'删除',
                menuclick:this.delRow,
              },{
                key:'edit',
                name:'编辑',
                menuclick:this.editRow,
              }],
            },
            // rowSelection: {
            //   selectedRowKeys,
            //   onChange: (selectedRowKeys, selectedRows) => {
            //     this.props.dispatch({
            //       type: 'infoHouseDetail/updateState',
            //       payload: {
            //         selectedRowKeys,
            //       },
            //     })
            //   },
            // },
        }
        const AddModalProps = {
            title: ModalType === 'create' ? '添加-实有人口信息' : '编辑-实有人口信息',
            visible: ModalVisable,
            personBase,
            isFindPerson,
            isLocal,
            handleOk: (values) => {
                this.props.dispatch({
                    type: 'infoHouseDetail/updateState',
                    payload: {
                        ModalVisable: false,
                        isFindPerson: 'noFind',
                        isLocal: 'noFind',
                    },
                })
            },
            onCancel: () => {
                this.props.dispatch({
                    type: 'infoHouseDetail/updateState',
                    payload: {
                        ModalVisable: false,
                        isFindPerson: 'noFind',
                        isLocal: 'noFind',
                    },
                })
            },
            onSerach: (values) => {
                const isLocal = findRecord(persons, 'idCard', values);
                this.props.dispatch({
                    type: 'infoHouseDetail/updateState',
                    payload: {
                        isFindPerson: 'noFind',
                        isLocal: 'noFind',
                        idCard: values,
                    },
                });
                if (isLocal.idCard) {
                    this.props.dispatch({
                        type: 'infoHouseDetail/updateState',
                        payload: {
                            isLocal: 'localFind',
                        },
                    })
                } else {
                    this.props.dispatch({
                        type: 'infoHouseDetail/judgePersonHaveHouse',
                        payload: {
                            idCard: values,
                        },
                    })
                    this.props.dispatch({
                        type: 'infoHouseDetail/updateState',
                        payload: {
                            isLocal: 'locaNotFind',
                        },
                    })
                }
            },
            goAdd: () => {
                const { house } = this.props.infoHouseDetail
                this.props.dispatch(routerRedux.push({
                    pathname: `/gridInfoManage/houseAddPerson/${house.id}/${idCard}`,
                }))
            },
        }
        // const saveButton = (<Button type="primary" onClick={this.save}   loading={searchLoading}>保存</Button>)
        const createButton = (<Button type="primary" onClick={this.create} icon="plus">添加</Button>)
        const houseTypeOptions = houseType.map( current => {
            return (
              <Option key={current.k}>{current.val}</Option>
            )
        })
        return (
          <PageHeaderLayout>
            <Card bordered={false} title="房屋录入">
              <Form>
                <Row>
                  <Col {...colLayoyt1}>
                    <FormItem {...FormItemLaylout} label="乡镇">
                      {
                        getFieldDecorator('department', {
                            initialValue: department.name || '',
                            rules: [{ required: true, whitespace: true }],
                        })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayoyt1}>
                    <FormItem {...FormItemLaylout} label="片区">
                      {
                          getFieldDecorator('distract', {
                              initialValue: area.name || '',
                              rules: [{ required: true, whitespace: true }],
                          })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayoyt1}>
                    <FormItem {...FormItemLaylout} label="网格区">
                      {
                        getFieldDecorator('grid', {
                            initialValue: grid.name || '',
                            rules: [{ required: true, whitespace: true }],
                        })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                  <Col {...colLayoyt1}>
                    <FormItem {...FormItemLaylout} label="信息表编号">
                      {
                        getFieldDecorator('infoNumber', {
                            initialValue: house.infoNumber || '',
                            rules: [{ whitespace: true }]
                        })(<Input disabled />)
                      }
                    </FormItem>
                  </Col>
                </Row>
              </Form>
            </Card>
            <Card title="房屋信息">
              <Row>
                <Col {...colLayout}>
                  <FormItem {...FormItemLaylout} label="类别">
                    {
                      getFieldDecorator('houseType', {
                          initialValue: house.houseType  || '',
                          rules: [{ required: true, message: "类别是必填项", whitespace: true }],
                      })(<Select className={styles.select}>{houseTypeOptions}</Select>)
                    }
                  </FormItem>
                </Col>
                <Col {...colLayout}>
                  <FormItem {...FormItemLaylout} label="网格内编号">
                    {
                        getFieldDecorator('gridNumber', {
                            initialValue: house.gridNumber || '',
                        })(<Input disabled />)
                    }
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem {...TextAreaFormItemLaylout} label="房屋地址">
                    {
                        getFieldDecorator('address', {
                            initialValue: house.address || '',
                            rules: [{ required: true, message: "房屋地址是必填项", whitespace: true }],
                        })(<Input.TextArea />)
                    }
                  </FormItem>
                </Col>
              </Row>
            </Card>
            <Card title="产权人">
              <Col {...colLayout}>
                <FormItem {...FormItemLaylout} label="姓名">
                  {
                      getFieldDecorator('name', {
                          initialValue: house.name || '',
                          rules: [{ required: true, message: "姓名是必填项", whitespace: true }],
                      })(<Input />)
                  }
                </FormItem>
              </Col>
              <Col {...colLayout}>
                <FormItem {...FormItemLaylout} label="联系方式">
                  {
                      getFieldDecorator('phone', {
                          initialValue: house.phone || '',
                          rules: [{ required: true, pattern: /^1[3|4|5|7|8][0-9]{9}$/, message: "请填入正确的电话号码", whitespace: true }],
                      })(<Input />)
                  }
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...TextAreaFormItemLaylout} label="工作单位" >
                  {
                      getFieldDecorator('unit', {
                          initialValue: house.unit || '',
                          rules: [{ required: true, message: "工作单位是必填项", whitespace: true }],
                      })(<Input.TextArea />)
                  }
                </FormItem>
              </Col>
            </Card>
            {
              house.name &&
              <Card title="实有人口信息" extra={createButton}>
                <AutoList {...listProps}/>
              </Card>
            }
            <div className={styles.btnBox}>
              <div className={styles.saveAndreturn}>
                <Button icon="close" onClick={this.handleCancle}>返回</Button>
                <Button icon="save" type="primary" onClick={this.save} loading={saveLoading}>保存</Button>
              </div>
            </div>
            <AddModal {...AddModalProps} />
          </PageHeaderLayout>
        )
    }
}

HouseDetail.propsTypes = {

}
