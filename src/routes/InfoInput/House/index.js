import React from 'react'
import pathToRegexp  from 'path-to-regexp'
import { Col, Row, Modal, Card } from 'antd'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import Cards from '../createCards'
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';

@connect(({ infoHouse, loading }) => ({
  infoHouse,
  loading: loading.effects['infoHouse/list'],
})) 
export default class House extends React.Component {

    componentDidMount () {
      const { dispatch, location } = this.props
      dispatch({
          type: 'infoHouse/clearState',
      })
      const match = pathToRegexp('/gridInfoManage/house/:id').exec(location.pathname)
      if (match) {
        dispatch({
            type: 'infoHouse/list',
            payload: {
                mershId: match[1],
            },
        })
        dispatch({
            type: 'infoHouse/updateState',
            payload: {
                history,
                mershId: match[1],
            },
        })
        dispatch({
          type: 'infoHouse/initBasic',
        })
      }
    }
    render () {
        const { dataSource, history, mershId, personInfo, houseInfo, personTypeInfo, name, houseType } = this.props.infoHouse
        const CardsProps = {
          houseType,
          dataSource,
          showedArray: ['编号/gridNumber', '类型/houseType', '产权人/name'],
          actiontypes: ['edit/编辑', 'delete/删除'],
          isShowPreButton: true,
          isShowAddButton: true,
          onClick: (value, type) => {
              switch (type) {
                  case 'edit':
                      this.props.dispatch(routerRedux.push({
                          pathname: `/gridInfoManage/houseEdit/${value.id}`,
                      }))
                      break;
                  case 'delete':
                      Modal.confirm({
                          title: '删除',
                          content: '确认删除这这个房屋吗',
                          cancelText:'取消',
                          okText:'确定',
                          onOk: () => {
                              this.props.dispatch({
                                  type: 'infoHouse/delCard',
                                  payload: {
                                      id: value.id,
                                  },
                              })
                          },
                      })
                      break;
                  default:
                      break;
              }
            },
            addClick: () => {
                this.props.dispatch(routerRedux.push({
                    pathname: `/gridInfoManage/houseAdd/${mershId}`,
                }))
            },
            preClick: () => {
                this.props.history.goBack()
            },
            handleClick: (current) => {
                // this.props.dispatch({
                //     type: 'infoHouse/getBaseInfo',
                //     payload: {
                //         mershId: current.id
                //     }
                // })
                // this.props.dispatch({
                //     type: 'infoHouse/updateState',
                //     payload: {
                //         name: current.name
                //     }
                // })
            },
        }
        const BasicInfoProps = {
            personInfo: personInfo || {}, 
            houseInfo: houseInfo || {}, 
            personTypeInfo: personTypeInfo || {},
            name,
        }
        return (
          <PageHeaderLayout>
            <Card loading={this.props.loading}>
              <Row>
                <Col span={24}> <Cards {...CardsProps} /> </Col>
                {/* <Col span={7}> <BasicInfo {...BasicInfoProps}/> </Col> */}
              </Row>
            </Card>
          </PageHeaderLayout>
        )
    }
}