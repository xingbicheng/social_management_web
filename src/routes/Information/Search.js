import React , {Component} from 'react';
import {  Card, Row, Col, Form, Input, Button } from 'antd';

const FormItem = Form.Item;
@Form.create()

class Search extends Component{
  constructor(props) {
    super(props);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {// 对表单样式进行定义
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 17 },
      },
    };
    return (
      <Card bordered={false}>
        <h3>快速查询</h3>
        <div>
          <Row>
            <Form>
              <Col span={7} style={{textAlign: 'left'}}>
                <FormItem
                  {...formItemLayout}
                  style={{width: '100%'}}
                  label="编号"
                >
                  {getFieldDecorator('password', {
                      rules: [{
                        required: true, message: 'Please input your password!',
                      }, {
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input type="password" />
                    )}
                </FormItem>
              </Col>
              <Col span={7} style={{textAlign: 'left'}}>
                <FormItem
                  {...formItemLayout}
                  style={{width: '100%'}}
                  label="当事人"
                >
                  {getFieldDecorator('password', {
                      rules: [{
                        required: true, message: 'Please input your password!',
                      }, {
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input type="password" />
                    )}
                </FormItem>
              </Col>
              <Col span={7} style={{textAlign: 'left'}}>
                <FormItem
                  {...formItemLayout}
                  style={{width: '100%'}}
                  label="纠纷类别"
                >
                  {getFieldDecorator('password', {
                      rules: [{
                        required: true, message: 'Please input your password!',
                      }, {
                        validator: this.validateToNextPassword,
                      }],
                    })(
                      <Input type="password" />
                    )}
                </FormItem>
              </Col>
            </Form>
            <Col span={3} style={{marginTop: 3}}>
              <Button style={{marginRight: 10}}>查询</Button>
              <Button>重置</Button>
            </Col>
          </Row>
        </div>
      </Card>
    )
  }
}

export default Search
