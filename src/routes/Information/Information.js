import React , {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import {  Card, Row, Col, Form, Input, Button, Menu, Dropdown, Icon, Table, Badge, Pagination } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import Search from './Search'


const ButtonGroup = Button.Group;

@connect(({ citizen, loading }) => ({// 把组件connect以下
  citizen,
  loading: loading.effects['citizen/get'],
}))

class Information extends Component{
  constructor(props){
    super(props);
    this.state = {
      data : [
      { number: 'SCXFW2013001', status: '待办', time: '2013-08-28 08:49:20', party: '马云', type: '代理事项', level: '一般事项', department: '北京公安局', term: '2016-1-1' },
      { number: 'SCXFW20141103', status: '已办', time: '2017-03-21 09:50:19', party: '马化腾', type: '求诀事项', level: '重大事项', department: '美国五角大楼', term: '2016-1-1' },
      { number: 'SCXFW20141104', status: '已办', time: '2018-06-29 09:50:19', party: '李彦宏', type: '其他事项', level: '重大事项', department: '中南海', term: '2016-1-1' },
    ],
    }
  }

  returnAccept = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push('/information/accept'));
  }

  handleDelete = () => {
    const { selectData } = this.state;
    for (var i = 0; i < selectData.length; i++) {// 遍历被选中的数据
      for (var j = 0; j < this.state.data.length; j++) {// 遍历数据
        if (selectData[i].number === this.state.data[j].number) {// 查找并删除被选中数据
          this.state.data.splice(j, 1);
          this.setState({data: this.state.data})
          j--;
        }
      }
    }
  }


  getSelectedData = (selectedData) => {
    this.setState({selectData: selectedData})
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'citizen/get',
      payload:{
        page:'1',
        type:'FundsType',
      },
    })
  }

  render(){
    return(
      <PageHeaderLayout>
        <Card bordered={false} />{/* 描述 */}
        <Search />
        <Card bordered={false} style={{marginTop: 20}}>
          <div>
            <Row>
              <Col span={20} />
              <Col span={4} >
                <Button style={{width: '45%', float: 'left'}} onClick={this.returnAccept}>受理</Button>
                <Button style={{width: '45%', float: 'right'}} type='danger' onClick={this.handleDelete}>删除</Button>
              </Col>
            </Row>
          </div>  {/* 导航 */}
          <div style={{marginTop: 20}}>
            <NestedTable onDelete={this.getSelectedData} msg={this.state.data} />
          </div>  {/* 表格 */}
          <div style={{ marginTop: 30, textAlign: 'right' }}>
            <Pagination showQuickJumper defaultCurrent={2} total={100} style={{ display: 'inline-block', float: 'right'}} />
          </div>  {/* footer分页 */}
        </Card>
      </PageHeaderLayout>
    );
  }
}

class NestedTable extends Component{
  constructor(props) {
    super(props);
    this.data = [];
    for (let i = 0; i < 1; i++) {
      this.data.push({
        key: i,
        name: '刘强东',
        sex: '男',
        birthday: '1980-1-1',
        phone: `110`,
        address: '北京中关村',
      });
    }
  }

  expandedRowRender = () => {
    const columns = [
      { title: '名称', dataIndex: 'name', key: 'name' },
      { title: '性别', dataIndex: 'sex', key: 'sex' },
      { title: '出生年月', dataIndex: 'birthday', key: 'birthday' },
      { title: '电话', dataIndex: 'phone', key: 'phone' },
      {
        title: '住址',
        dataIndex: 'address',
        key: 'address',
      },
    ];

    return (
      <Table
        columns={columns}
        dataSource={this.data}
        pagination={false}
      />
    );
  };

  render(){
    let selectedData = [];

    const menu = (
      <Menu>
        <Menu.Item>
          <span>修改</span>
        </Menu.Item>
        <Menu.Item>
          <span>交办</span>
        </Menu.Item>
        <Menu.Item>
          <span>办结</span>
        </Menu.Item>
      </Menu>
    );

    const columns = [
      { title: '编号', dataIndex: 'number', key: 'number', width: '14%' },
      { title: '状态', dataIndex: 'status', key: 'status', width: '8%' },
      { title: '受理时间', dataIndex: 'time', key: 'time', width: '15%' },
      { title: '当事人', dataIndex: 'party', key: 'party', width: '8%' },
      { title: '事项类别', dataIndex: 'type', key: 'type', width: '11%' },
      { title: '级别', dataIndex: 'level', key: 'level', width: '11%' },
      { title: '办理部门', dataIndex: 'department', key: 'department', width: '11%' },
      { title: '办理期限', width: '11%', key: 'term', dataIndex: 'term' },
      { title: '操作', width: '11%', key: 'operation', dataIndex: 'operation', render: () => (
        <span className="table-operation">
          <Dropdown overlay={menu}>
            <a href="javascript:;" style={{textDecoration: 'none'}}>
                More <Icon type="down" />
            </a>
          </Dropdown>
        </span>
        ) },
    ];

    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        selectedData = selectedRows;
        this.props.onDelete(selectedData);
      },
      onSelect: (record, selected, selectedRows) => {

      },
      onSelectAll: (selected, selectedRows, changeRows) => {
      },
    };

    return (
      <Table
        className="components-table-demo-nested"
        columns={columns}
        rowSelection={rowSelection}
        expandedRowRender={this.expandedRowRender}
        dataSource={this.props.msg}
        pagination={false}
      />
    );
  }
}

export default Information;
