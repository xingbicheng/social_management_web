import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Card, Input, Form, Select, Col, Row, Modal, message, Button } from 'antd';
import ReportForm from './ReportForm';
import { FormItemLayoutPage } from '../../config/config';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ToolBar from '../../components/ToolBar';
import AutoList from '../../components/List/List';
import { EVALUTIONRESULT, RISKTYPE, RISKSTATE } from '../../utils/emenus';
import styles from '../../components/PageInfo/index.less';
import { riskAddState } from '../../utils/seletLocalData';
import { findRecord } from '../../utils/utils';

const FormItem = Form.Item;
const Option = Select.Option;
const confirm = Modal.confirm;

@Form.create()
@connect(({ report, loading }) => ({
  report,
  loading: loading.effects['report/detailPage'],
  loadingUpdate: loading.effects['report/edit'],
  loadingExamine: loading.effects['report/issued'],
  loadingAdd: loading.effects['report/add'],
}))
export default class ReportDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formMethod: 'add',
      handleTitle: '新增',
      states: RISKSTATE.SAVE,
      riskDetailsList: [],
    };
  }

  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'report/getRiskArchiving',
    });
    dispatch({
      type: 'report/getRiskResult',
    });
    dispatch({
      type: 'report/getRiskType',
    });
    dispatch({
      type: 'report/getRiskStatus',
    });
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/risk/detail/:id/:type').exec(this.props.history.location.pathname);
    dispatch({
      type: 'report/updateState',
      payload: {
        type: match[2],
      },
    });
    const { report: { type } } = this.props;
    if (match[2] !== 'add') {
      dispatch({
        type: 'report/detailPage',
        payload: {
          id: match[1],
        },
      });
      if (type === 'check') {
        dispatch({
          type: 'report/updateState',
          payload: {
            HandleMethod: 'watch',
          },
        });
      }
    } else {
      dispatch({
        type: 'report/updateForm',
      });
    }
  };

  handleOk = (values) => {
    const { dispatch, report: { type } } = this.props;
    const { formMethod } = this.state;
    if (type === 'add') {
      const { riskDetailsList: nowDetailList } = this.state;
      if (formMethod === 'add') {
        nowDetailList.unshift(values);
      } else {
        const index = nowDetailList.indexOf(values);
        nowDetailList.splice(index, 1, values);
      }
      this.setState({
        riskDetailsList: nowDetailList,
      });
      dispatch({
        type: 'report/hiddenModal',
      });
      message.success('新增成功');
    } else {
      const { currentRow: { id }, id: riskEvaluationId } = this.props.report;
      dispatch({
        type: `report/${formMethod}Detail`,
        payload: {
          riskEvaluationId,
          id,
          ...values,
        },
      });
    }
  };

  handleCancel = () => {
    this.props.dispatch({
      type: 'report/hiddenModal',
    });
  };

  add = () => {
    const { dispatch, report: { type, id } } = this.props;
    this.setState({
      formMethod: 'add',
      handleTitle: '新增',
    });
    dispatch({
      type: 'report/showModal',
      payload: type === 'add' ? { id } : {},
    });
  };
  update = (record) => {
    const { dispatch, report: { type } } = this.props;
    this.setState({
      formMethod: type === 'edit' ? 'edit' : 'check',
      handleTitle: type === 'edit' ? '编辑' : '查看',
    });
    dispatch({
      type: 'report/showModal',
      payload: {
        ...record,
      },
    });
  };
  delete = (record) => {
    const { dispatch, report: { type, id } } = this.props;
    const than = this;
    confirm({
      title: '删除',
      content: '是否删除当前项目？',
      okText: '确定',
      cancelText: '取消',
      onOk() {
        if (type === 'add') {
          const { riskDetailsList: nowDetailList } = than.state;
          const index = nowDetailList.indexOf(record);
          nowDetailList.splice(index, 1);
          than.setState({
            riskDetailsList: nowDetailList,
          });
          message.success('删除成功');
        }
        else {
          dispatch({
            type: 'report/deleteDetail',
            payload: {
              id: record.id,
              riskEvaluationId: id,
            },
          });
        }
      },
      onCancel() {
      },
    });
  };
  save = () => {
    const than = this;
    const { dispatch, form, report: { detailSource, type, headerSource: { id } } } = this.props;
    const { states, riskDetailsList } = this.state;
    const buttonValue = states === RISKSTATE.SAVE ? '保存' : '提交';
    const formData = form.getFieldsValue();
    form.validateFields((err) => {
      if (!err) {
        if (type === 'add' && riskDetailsList.length === 0) {
          message.error('请至少添加一条报表!');
        } else {
          confirm({
            title: '确认',
            content: `是否${buttonValue}当前报表？`,
            okText: '确定',
            cancelText: '取消',
            onOk() {
              if (type === 'add') {
                dispatch({
                  type: 'report/add',
                  payload: {
                    ...than.state,
                    ...formData,
                  },
                });

              } else {
                dispatch({
                  type: 'report/edit',
                  payload: {
                    ...formData,
                    riskDetailsList: detailSource,
                    id,
                  },
                });
              }
            },
            onCancel() {
            },
          });
        }
      } else {
        message.error('表单验证失败！');
      }
    });
  };
  examine = () => {
    const { id } = this.props.report;
    const { dispatch } = this.props;
    confirm({
      title: '确认',
      content: '确认通过审核当前报表？',
      okText: '确定',
      cancelText: '取消',
      onOk() {
        dispatch({
          type: 'report/issued',
          payload: {
            id,
          },
        });
      },
      onCancel() {
      },
    });
  };
  back = () => {
    this.props.dispatch(routerRedux.push('/risk/report'));
  };
  handleStateChange = () => {
    this.setState({
      states: (this.state.states === RISKSTATE.SAVE) ? RISKSTATE.SUBMIT : RISKSTATE.SAVE,
    });
  };


  render() {
    const {
      report: { detailSource: dataSourceList, headerSource: formInfo, visible, currentRow, risk_evaluation_status, risk_details_type, risk_details_result, risk_details_is_archiving },
      form: { getFieldDecorator }, loading, loadingExamine, loadingUpdate, loadingAdd,
    } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/risk/detail/:id/:type').exec(this.props.history.location.pathname);
    const isReadOnly = !(match[2] === 'edit' || match[2] === 'add');
    const handleTitle = (match[2] === 'edit') ? '编辑' : (match[2] === 'examine') ? '审核' : (match[2] === 'watch') ? '查看' : '新增';
    const modalProps = {
      visible,
      handleOk: this.handleOk,
      handleCancel: this.handleCancel,
      formInfo: currentRow,
      ...this.state,
      isReadOnly,
      risk_details_type,
      risk_details_result,
      risk_details_is_archiving,
    };
    const dataSource = match[2] === 'add' ? this.state.riskDetailsList : dataSourceList;
    const pageProps = {
      dataSource,
      loading,
      columsList: {
        columns: [{
          title: '#',
          dataIndex: 'index',
          key: 'index',
          align: 'center',
          render: (text, record, index) => {
            return <span>{index + 1}</span>;
          },
        },
          {
            title: '项目名称',
            dataIndex: 'name',
            key: 'name',
          },
          {
            title: '类型',
            dataIndex: 'type',
            key: 'type',
            render: (record) => {
              const data = findRecord(this.props.report.risk_details_type, 'k', record);
              return data.val;
            },
          }, {
            title: '开始时间',
            dataIndex: 'startTime',
            key: 'startTime',
          }, {
            title: '结束时间',
            dataIndex: 'endTime',
            key: 'endTime',
          }, {
            title: '责任主体',
            dataIndex: 'responsibleBody',
            key: 'responsibleBody',
          },
          {
            title: '评估结论',
            dataIndex: 'evaluationResult',
            key: 'evaluationResult',
            render: (record) => {
              const data = findRecord(this.props.report.risk_details_result, 'k', record);
              return data.val;
            },
          },
        ],
        menulistArr: isReadOnly ? [{
          key: 'handle-3',
          name: '查看',
          menuclick: this.update,
        },
        ] : [{
          key: 'handle-1',
          name: '编辑',
          menuclick: this.update,
        }, {
          key: 'handle-2',
          name: '删除',
          menuclick: this.delete,
        },
        ],
      },
    };
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.add,
    }];
    const stateOptions = !isReadOnly ? riskAddState.map((current) => {
        return <Option key={current.value}>{current.name}</Option>;
      })
      : risk_evaluation_status.map((current) => {
        return <Option key={current.k}>{current.val}</Option>;
      });

    return (
      <PageHeaderLayout>
        <Card
          title={handleTitle}
          bordered={false}
          style={{ marginBottom: '20px' }}
          loading={loading}
        >
          <Row>
            <Form>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="标题"
                >
                  {getFieldDecorator('title', {
                    initialValue: formInfo.title || '',
                    rules: [{
                      required: true, message: '标题必填项！',
                    }],
                  })(
                    <Input style={{ width: '100%' }} disabled={isReadOnly}/>,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="单位"
                >
                  {getFieldDecorator('organization', {
                    initialValue: formInfo.organization || '',
                    rules: [{
                      required: true, message: '单位必填项！',
                    }],
                  })(
                    <Input style={{ width: '100%' }} disabled={isReadOnly}/>,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="状态"
                >
                  {getFieldDecorator('status', {
                    initialValue: formInfo.status || RISKSTATE.SAVE,
                    rules: [{
                      required: true, message: '状态必填项！',
                    }],
                  })(
                    <Select
                      style={{ width: '100%' }}
                      disabled={isReadOnly}
                      onChange={this.handleStateChange.bind(this)}
                    >
                      {stateOptions}
                    </Select>,
                  )}
                </FormItem>
              </Col>
            </Form>
          </Row>
        </Card>
        <Card
          title='报表详情'
          bordered={false}
        >
          {(
            !isReadOnly && (
              <ToolBar BtnList={BtnList}/>
            )
          )}

          <AutoList {...pageProps} />
          <ReportForm
            {...modalProps}
          />
          {(
            match[2] === 'examine' && (
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button
                    onClick={this.back}
                  >返回
                  </Button>
                  <Button
                    icon='check'
                    type='primary'
                    onClick={this.examine}
                    loading={loadingExamine}
                  >审核通过
                  </Button>
                </div>
              </div>
            ))}
          {(
            !isReadOnly && (
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button
                    onClick={this.back}
                  >返回
                  </Button>
                  <Button
                    icon='check'
                    type='primary'
                    onClick={this.save}
                    loading={loadingUpdate || loadingAdd}
                  >{this.state.states === RISKSTATE.SAVE ? '保存' : '提交'}表单
                  </Button>
                </div>
              </div>
            )
          )}
        </Card>
      </PageHeaderLayout>
    );
  }
};

