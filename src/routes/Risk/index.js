import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Row, Card, Modal, Col, message } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ToolBar from '../../components/ToolBar';
import AutoList from '../../components/List/List';
import SearchForm from './SearchForm';
import { RISKSTATE } from '../../utils/emenus';
import { findRecord } from '../../utils/utils';

const confirm = Modal.confirm;

@connect(({ report, loading }) => ({
  report,
  loading: loading.effects['report/listpage'],
}))

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      selectedRows: [],
      selectedRowKeys: [],
    };
  }

  componentDidMount() {
    const{dispatch} = this.props;
    this.listpage();
    dispatch({
      type: 'report/getRiskStatus',
    });
  }

  watch = (record) => {
    const { dispatch } = this.props;
    const { id } = record;
    dispatch(routerRedux.push({
      pathname: `/risk/detail/${id}/check`,
    }));
  };
  update = (record) => {
    if (record.status === RISKSTATE.SAVE) {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch({
        type: 'report/updateState',
        payload: {
          HandleMethod: 'edit',
          isEdit: (record.status === RISKSTATE.SAVE),
        },
      });
      dispatch(routerRedux.push({
        pathname: `/risk/detail/${id}/edit`,
      }));
    } else {
      message.error('不能编辑当前报表');
    }
  };
  submit = (record) => {
    if (record.status === RISKSTATE.SAVE) {
      const { dispatch } = this.props;
      const { id } = record;
      confirm({
        title: '确认',
        content: '是否提交该消息？',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          dispatch({
            type: 'report/submit',
            payload: {
              id,
            },
          });
        },
        onCancel() {
        },
      });
    }
    else {
      message.error('只能提交保存的报表');
    }
  };
  examine = (record) => {
    if (record.status === RISKSTATE.SUBMIT) {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch({
        type: 'report/updateState',
        payload: {
          HandleMethod: 'examine',
          isEdit: (record.status === RISKSTATE.SAVE),
        },
      });
      dispatch(routerRedux.push({
        pathname: `/risk/detail/${id}/examine`,
      }));
    } else {
      message.error('只能审核提交的报表');
    }
  };
  delete = (record) => {
    const than = this;
    if (record.status === RISKSTATE.SAVE) {
      if (record && record.id) {
        confirm({
          title: '删除',
          content: '是否删除当前报表？',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            than.props.dispatch({
              type: 'report/delete',
              payload: {
                id: record.id,
              },
            });
          },
          onCancel() {
          },
        });
      }
    } else {
      message.error('只能删除保存状态的报表！');
    }
  };

  addReport = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
      pathname: '/risk/detail/0/add',
    }));
  };

  listpage = () => {
    const { dispatch, report: { searchInfo, pagination } } = this.props;
    const { page, pageSize } = pagination;
    dispatch({
      type: 'report/listpage',
      payload: {
        page,
        pageSize,
        ...searchInfo,
      },
    });
  };

  restSearch = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'report/clearAll',
    });
    dispatch({
      type: 'report/listpage',
      payload: {
        page: 1,
        pageSize: 10,
      },
    });
  };
  search = (values) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'report/setSearchInfo',
      payload: { ...values },
    });
    dispatch({
      type: 'report/listpage',
      payload: {
        ...values,
      },
    });
  };
  turnPage = (e) => {
    const { current, pageSize } = e;
    const { dispatch, report: { searchInfo } } = this.props;
    dispatch({
      type: 'report/listpage',
      payload: {
        ...searchInfo,
        page: current,
        pageSize,
      },
    });
  };
  deleteSelects = () => {
    const idList = this.state.selectedRows;
    const than = this;
    const rows = idList.filter(i => i.status !== RISKSTATE.SAVE);
    if (rows.length) {
      message.error('包含不是保存状态的报表');
      return;
    }
    confirm({
      title: '删除',
      content: '是否删除所勾选信息？',
      okText: '确定',
      cancelText: '取消',
      onOk() {
        const { selectedRowKeys } = than.state;
        than.props.dispatch({
          type: 'report/deleteAll',
          payload: {
            ids: selectedRowKeys,
          },
        });
        than.setState({
          selectedRowKeys: [],
        });
      },
    });
  };

  render() {
    const { report: { dataSource, searchInfo, pagination,risk_evaluation_status }, loading, form } = this.props;
    const { selectedRowKeys } = this.state;
    const hasSelected = selectedRowKeys.length > 0;
    const pageProps = {
      dataSource,
      loading,
      columsList: {
        columns: [{
          title: '#',
          key: 'index',
          dataIndex: 'index',
          align: 'center',
          render: (text, record, index) => {
            return <span>{index + 1}</span>;
          },
        },
          {
            title: '标题',
            dataIndex: 'title',
            key: 'title',
          }, {
            title: '单位',
            dataIndex: 'organization',
            key: 'organization',
          }, {
            title: '状态',
            dataIndex: 'status',
            key: 'status',
            render:(record)=>{
              const data= findRecord(this.props.report.risk_evaluation_status,'k',record);
              return data.val;
            },
          },
          {
            title: '创建时间',
            dataIndex: 'createTime',
            key: 'createTime',
          },
        ],
        menulistArr: [
          {
            key: 'handle-1',
            name: '查看',
            menuclick: this.watch,
          }, {
            key: 'handle-2',
            name: '编辑',
            menuclick: this.update,
          },
          {
            key: 'handle-3',
            name: '提交',
            menuclick: this.submit,
          },
          {
            key: 'handle-4',
            name: '审核',
            menuclick: this.examine,
          },
          {
            key: 'handle-5',
            name: '删除',
            menuclick: this.delete,
          },
        ],
      },
      pagination,
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            number: selectedRows.length,
            selectedRows,
            selectedRowKeys,
          });
        },
      },
    };
    const searchProps = {
      parentForm: form,
      searchInfo,
      restForm: this.restSearch,
      search: this.search,
      risk_evaluation_status,
    };
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.addReport,
    }];

    return (
      <PageHeaderLayout>
        <Card
          title='快速查询'
          bordered={false}
          style={{ marginBottom: '20px' }}
        >
          <SearchForm {...searchProps}  />
        </Card>
        <Card
          title='报表信息'
          bordered={false}
        >
          <Row type="flex" justify="end">
            <Col style={{ lineHeight: '30px' }} span={2}>
              {hasSelected && `当前选中${this.state.number}条`}
            </Col>
            <Col>
              {
                hasSelected &&
                <Button type='danger' icon="close" onClick={this.deleteSelects}>删除</Button>
              }
            </Col>
            <Col>
              <ToolBar BtnList={BtnList}/>
            </Col>
          </Row>
          <AutoList {...pageProps} turnPage={this.turnPage}/>
        </Card>
      </PageHeaderLayout>
    );
  }
};

