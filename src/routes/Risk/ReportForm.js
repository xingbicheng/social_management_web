import React, { Component } from 'react';
import * as moment from 'moment';
import { Form, Input, Select, Modal, DatePicker, message } from 'antd';
import { FormItemLayoutModal } from '../../config/config';
import { getNowFormatDate } from '../../utils/utils';

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

@Form.create()
export default class ReportForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startValue: null,
      endValue: null,
      endOpen: false,
    };
  }

  onHandleCancel = () => {
    const { form, handleCancel } = this.props;
    form.resetFields();
    handleCancel();
  };
  onHandleOk = () => {
    const { form, handleOk, isDisplay } = this.props;
    const formData = form.getFieldsValue();
    if (isDisplay) {
      this.onHandleCancel();
      return;
    }
    formData.startTime = getNowFormatDate(formData.startTime);
    formData.endTime = getNowFormatDate(formData.endTime);
    form.validateFields((err) => {
      if (!err) {
        form.resetFields();
        handleOk(formData);
      } else {
        message.error('表单验证失败！');
      }
    });
  };

  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  };

  onStartChange = (value) => {
    this.onChange('startValue', value);
  };

  onEndChange = (value) => {
    this.onChange('endValue', value);
  };

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  };

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  };
  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  };

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  };

  render() {
    let now = new moment();
    const dateFormat = 'YYYY-MM-DD';
    const {
      visible, handleTitle, formInfo = {},
      form: { getFieldDecorator }, isDisplay, risk_details_type, risk_details_result, risk_details_is_archiving,
    } = this.props;
    const { endOpen } = this.state;
    const typeOptions = risk_details_type.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const resultOptions = risk_details_result.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const archivingOptions = risk_details_is_archiving.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    return (
      <Modal
        title={handleTitle}
        visible={visible}
        onOk={this.onHandleOk}
        onCancel={this.onHandleCancel}
      >
        <Form>
          <FormItem
            {...FormItemLayoutModal}
            label="项目名称"
          >
            {getFieldDecorator('name', {
              initialValue: formInfo.name || '',
              rules: [{
                required: true, message: '项目名称是必写的',
              }],
            })(
              <Input disabled={isDisplay}/>,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="类型"
          >
            {getFieldDecorator('type', {
              initialValue: formInfo.type || '',
              rules: [{
                required: true, message: '类型是必填项！',
              }],
            })(
              <Select disabled={isDisplay}>
                {typeOptions}
              </Select>,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="开始时间"
          >
            {getFieldDecorator('startTime', {
              initialValue: moment(formInfo.startTime || now, dateFormat),
              rules: [{
                required: true, message: '开始时间必填项！',
              }],
            })(
              <DatePicker
                style={{ width: '100%' }}
                disabledDate={this.disabledStartDate}
                showTime
                placeholder="开始时间"
                onChange={this.onStartChange}
                onOpenChange={this.handleStartOpenChange}
                disabled={isDisplay}
              />,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="结束时间"
          >{getFieldDecorator('endTime', {
            initialValue: moment(formInfo.endTime || now.add(1, 'd'), dateFormat),
            rules: [{

              required: true, message: '结束时间必须晚于开始时间！',
            }],
          })(
            <DatePicker
              style={{ width: '100%' }}
              disabledDate={this.disabledEndDate}
              showTime
              placeholder="结束时间"
              onChange={this.onEndChange}
              open={endOpen}
              onOpenChange={this.handleEndOpenChange}
              disabled={isDisplay}
            />,
          )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="责任主体"
          >
            {getFieldDecorator('responsibleBody', {
              initialValue: formInfo.responsibleBody || '',
              rules: [{
                required: true, message: '责任主体是必填项！',
              }],
            })(
              <Input disabled={isDisplay}/>,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="评估结论"
          >
            {getFieldDecorator('evaluationResult', {
              initialValue: formInfo.evaluationResult || '',
              rules: [{
                required: true, message: '评估结论是必填项！',
              }],
            })(
              <Select disabled={isDisplay}>
                {resultOptions}
              </Select>,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="是否建档"
          >
            {getFieldDecorator('isArchiving', {
              initialValue: formInfo.isArchiving || '',
              rules: [{
                required: true, message: '清选择是否建档！',
              }],
            })(
              <Select disabled={isDisplay}>
                {archivingOptions}
              </Select>,
            )}
          </FormItem>
          <FormItem
            {...FormItemLayoutModal}
            label="备注"
          >
            {getFieldDecorator('memo', {
              initialValue: formInfo.memo || '',
              rules: [{
                required: true, min: 0, max: 400,
                whitespace: true,
                message: '请不要输入纯空格,并且输入内容不能超出400字',
              }],
            })(
              <TextArea placeholder="字数为1-400" autosize={{ minRows: 4, maxRows: 4 }} disabled={isDisplay}/>,
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

