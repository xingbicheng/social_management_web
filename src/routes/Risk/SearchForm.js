import React, { Component } from 'react';
import { message, Col, Row, Form, Input, Button, Select } from 'antd';
import { FormItemLayoutPage, SearchBtnLayout, SearchFormLayout } from '../../config/config';
import styles from '../../theme/table.less';

const FormItem = Form.Item;
const Option = Select.Option;


@Form.create()
export default class SearchForm extends Component {
  searchList = () => {
    const { form, search } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        for (const valuesName in values) {
          if (values.hasOwnProperty(valuesName)) {
            if (!values[valuesName]) {
              delete values[valuesName];
            }
          }
        }
        search(values);
      } else {
        message.error('表单验证失败！');
      }
    });
  };

  restSearchForm = () => {
    const { form, restForm } = this.props;
    form.resetFields();
    restForm();
  };

  render() {
    const { searchInfo, form: { getFieldDecorator },risk_evaluation_status } = this.props;
    const stateOptions = risk_evaluation_status.map((current) => {
      return <Option key={current.k} >{current.val}</Option>;
    });
    return (
      <Row gutter={24}>
        <Form className={styles.searchBox}>
          <Row>
            <Col {...SearchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                label="标题"
              >
                {getFieldDecorator('title', {
                  initialValue: searchInfo.title || '',
                  rules: [],
                })(
                  <Input placeholder='请输入您要查询的标题' style={{ width: '100%' }}/>,
                )}
              </FormItem>
            </Col>
            <Col {...SearchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                label="状态"
              >
                {getFieldDecorator('status', {
                  initialValue: searchInfo.status || '',
                  rules: [],
                })(
                  <Select style={{ width: '100%' }}>
                    <Option value=''>全部</Option>
                    {stateOptions}
                  </Select>,
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col {...SearchBtnLayout}>
              <div
                style={{ margin: 0 }}
                className={styles.btnBox}
              >
                <Button onClick={this.restSearchForm}>重置</Button>
                <Button onClick={this.searchList} type="primary" icon="search">搜索</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Row>
    );
  }
}
