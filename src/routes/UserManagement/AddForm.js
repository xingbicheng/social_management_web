import { InputNumber, Modal, Form, Input ,Select,Alert,TreeSelect} from 'antd';
import {FormItemLayoutModal} from "../../config/config";
import {deleteSpace} from "../../utils/utils"

const FormItem = Form.Item;
const Option = Select.Option;
@Form.create()
class AddForm extends React.Component {
    constructor(props){
        super(props);
    }
    onHandleCreate = () => {
      const { form,handleOk } = this.props;
      form.validateFields((err, values) => {
          if(!err){
              let list =deleteSpace(values);
              handleOk(list);
          }else{
              return
          }
      });
  };
    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        handleCancel();
    }
    checkAccount(rule, value, callback) {
        const phoneRegular = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        if (value) {
          // react使用正则表达式变量的test方法进行校验，直接使用value.match(regex)显示match未定义
          if (phoneRegular.test(value)) {
            callback();
          } else {
            callback('请输入正确的手机号码！');
          }
        } else {
          callback('请输入正确的手机号码！');
        }
      }
    checkEmail (rule,value, callback) {
      const regex =  /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
      if (value) {
        if (regex.test(value)) {
          callback();
        } else {
          callback('请输入正确的邮箱号码！');
        }
      } else {
        callback();
      }
    }
    checkUserName(rule,value, callback){
      // 用户名正则，4到16位（字母，数字，下划线，减号）
        const uPattern = /^[a-zA-Z0-9_-]{4,11}$/;
        if(value){
          if (uPattern.test(value)) {
            callback();
          } else {
            callback('请输入正确的用户名！');
          }
      } 
      else {
        callback();
      }
    }
    render() {
      const formItemLayout = FormItemLayoutModal;  // 通用样式表
      const { visible, form,title,formInfo,treeData,rolesList,addloading,handleOk } = this.props;
      const options = rolesList.map(d => <Option key={d.id}>{d.name}</Option>);
      const { getFieldDecorator,getFieldsValue,resetFields } = form;
      return (
        <Modal
          visible={visible}
          title={title}
          okText="确定"
          confirmLoading={addloading}
          onCancel={this.onHandleCancel}
          onOk={this.onHandleCreate}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem
              label="昵称"
              {...formItemLayout}
            >
              {getFieldDecorator('nickname', {
                initialValue:formInfo.nickname || '',
                rules: [{max:8, required: true,whitespace:true, message: '请正确填写昵称!' }],
              })(
                <Input  placeholder='昵称为8个字符以内'  />
              )}
            </FormItem>
            <FormItem
              label="用户名"
              hasFeedback
              {...formItemLayout}
            >
              {getFieldDecorator('username', {
                initialValue:formInfo.username || '',
                rules: [{type:'string', required: true, message: '请填写用户名' },
                { validator: this.checkUserName}],
               
              })(
                <Input placeholder='用户名为4-11个字符或数字'  />
              )}
            </FormItem>
          <FormItem
              {...formItemLayout}
              label="手机号："
              hasFeedback
            >
              {getFieldDecorator('telephone', {
                  initialValue:formInfo.telephone || '',
                  rules: [{type:'string', required: true, message: '请输入手机号码!' },{
                    // 这里input内的输入内容进行绑定函数即可，在Input里面无需进行函数绑定开使用验证（红色部分）
                    validator: this.checkAccount,
                  }],
                })(
                  <Input placeholder='请输入手机号码' />
                )}
            </FormItem> 
            <FormItem
              {...formItemLayout}
              label="邮箱："
              hasFeedback
            >
              {getFieldDecorator('email', {
                  initialValue:formInfo.email || '',
                  rules: [{type:'string', required: true, message: '请输入邮箱!' },{
                    // 这里input内的输入内容进行绑定函数即可，在Input里面无需进行函数绑定开使用验证（红色部分）
                    validator: this.checkEmail,
                  }],
                })(
                  <Input placeholder='请输入邮箱'   />
                )}
            </FormItem> 
            <FormItem
              label="部门选择"
              {...formItemLayout}
            >
              {getFieldDecorator('departmentId', {
                initialValue:String(formInfo.departmentId)||'',
                rules: [{ required: true, message: '请选择部门' }],
              })(
                <TreeSelect
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  treeData={treeData}
                  placeholder="Please select"
                />
                )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="是否启用"
            >
              {getFieldDecorator('status', {
                 initialValue:String(formInfo.status)||'',
                                rules: [{required: true, message:'请选择是否启用'}],
                            })(
                              <Select style={{ width:'100%' }}>
                                <Option value="0">不可用</Option>
                                <Option value="1">可用</Option>
                                <Option value="2">锁定</Option>
                              </Select>
                            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="设置角色"
            >
              {getFieldDecorator('Roles', {
                   initialValue:String(formInfo.roleId)||'',
                                rules: [{required: true}],
                            })(
                              <Select style={{ width:'100%' }}>
                                {options}
                              </Select>
                            )}
            </FormItem>
          </Form>
        </Modal>
      );
    }
  }
export default AddForm;
