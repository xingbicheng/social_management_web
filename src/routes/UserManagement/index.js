import { Card,Form, Modal,  } from 'antd';
import React,{Component} from 'react';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import AutoList from '../../components/ListTwoOperation/List';
import AddForm from './AddForm';
import ToolBar from '../../components/ToolBar';

import { status } from '../../utils/seletLocalData';
import SearchForm from './SearchForm';


const confirm = Modal.confirm;

@Form.create()
@connect(({ usercontroller,loading }) => ({
  usercontroller,
  loading: loading.effects['usercontroller/getAllList'],
  addloading: loading.effects['usercontroller/addNewUsers'],
}))
class UserManagement extends Component {
  constructor(props){
    super(props);
    this.state = {
      title:'xxx',
      selectedRowKeys: [], // Check here to configure the default column
      formMethod:'addNewUsers',
      statusValue:status,
    };
  }

  componentDidMount(){
    this.listpage();
    const {dispatch} = this.props;
    dispatch({
      type: 'usercontroller/getDepartmentList',
   });
  }
  listpage = ()=>{
    const { dispatch,usercontroller:{searchInfo,pagination} } = this.props;
    const { pageNum,pageSize } = pagination;
    dispatch({
        type:'usercontroller/getAllList',
        payload:{
            page:pageNum,
            pageSize,
            ...searchInfo,
        },
    });
  }

  add = () => {
    this.props.dispatch({
      type: 'usercontroller/showModal',
      payload:{
          departmentId:'',
          username:0,
          status:'',
          password:0,
          roleId:'',
          rolename:'',
          nickname:0,
          telephone:0,
          departmentName:'',
      }
   });
   this.props.dispatch({
    type: 'usercontroller/getAllRolesList', // 获取所有角色
   });
    this.setState({
      title:'新建',
      formMethod:'addNewUsers',
    })
  }

  handleCancel = () => {
    this.props.dispatch({
      type:'usercontroller/hiddenModal',
  });
    this.props.dispatch({
      type:'usercontroller/isLoading'
    })
  }

  update=(record)=>{
    const { current } = this.props.usercontroller.pagination;
    this.setState({
      title:'编辑',
      formMethod:'edit',
  });
    this.props.dispatch({
      type:'usercontroller/getRoleById',   // 根据ID获取角色
      payload:{
        userId:record.id,
      },
    })
      this.props.dispatch({
      type:'usercontroller/getListById',  //查询当前用户信息
      payload:{
        ...record,
      },
    })
    this.props.dispatch({
      type: 'usercontroller/getAllRolesList',  //获取所有角色
     });
  }
  turnPage = (e)=>{
    const {current,pageSize} = e;
    const { dispatch,searchInfo } = this.props;
    dispatch({
        type:'usercontroller/getAllList',
        payload:{
            ...searchInfo,
            page:current,
            pageSize,
        },
    });
}
  setstatus1=(record) => {
    const { current } = this.props.usercontroller.pagination;
    this.props.dispatch({
      type:'usercontroller/setstatus',
      payload:{
          id:record.id,
          state:1,
          pageNum:current
      },
   });
   this.findRecord(record);
  }
  setstatus2=(record) => {
    const { current } = this.props.usercontroller.pagination;
    this.props.dispatch({
      type:'usercontroller/setstatus',
      payload:{
          id:record.id,
          state:2,
          pageNum:current
      },
   });
   this.findRecord(record);
  }
  setstatus0=(record) => {
    const { current } = this.props.usercontroller.pagination;
    this.props.dispatch({
      type:'usercontroller/setstatus',
      payload:{
          id:record.id,
          state:0,
          pageNum:current
      },
   });
   this.findRecord(record);
  }

  reset = (record) => {
    this.props.dispatch({
      type:'usercontroller/reset',
      payload:{
          id:record.id,
      },
  });
  }
  findRecord=(record)=>
    {
        const {statusValue} =this.state;
        let i=0;
        for(i=0;i<statusValue.length;i++)
        {
            if(statusValue[i].key===record.status)
            {
                return statusValue[i].val
            }
        }
    }
  handleOk = (value) =>{
    const { dispatch } = this.props;
    const { formMethod } = this.state;
    const { currentRow } = this.props.usercontroller;
    const { current } = this.props.usercontroller.pagination;

    dispatch({
      type:`usercontroller/${formMethod}`,
      payload:{
        id:currentRow.id,
        ...value,
        pageNum:current
      },
    })
  }
  restSearch = () => {
    const {dispatch} = this.props;
    dispatch({
        type:'usercontroller/clearSearchInfo',
    });
    dispatch({
        type:'usercontroller/getAllList',
        payload:{
            // page:1,
            // pageSize:10,
        },
    });
  }
  search = (values)=>{
    const {dispatch} = this.props;
    dispatch({
        type:'usercontroller/setSearchInfo',
        payload:{...values},
    });
    dispatch({
        type:'usercontroller/getAllList',
        payload:{
          ...values,
        }
    })
  }

  render() {
    const { usercontroller:{dataSource,visible,pagination,currentRow,treeData,searchInfo,rolesList,userRole} } = this.props;
    const { form,loading,addloading } = this.props;
    const { title } =this.state;
    const { selectedRowKeys } = this.state;
    const searchProps = {
      parentForm:form,
      searchInfo,
      restForm:this.restSearch,
      search:this.search,
    };
    const pageProps = {
      dataSource,
      loading,
      columsList:{
          columns:[{
              title:'#',
              key:'index',
              dataIndex:'index',
              align:'center',
              render: (text,record, index) => {
                  return <span>{index + 1}</span>
              },
          },{
              title: '用户名',
              key: 'username',
              dataIndex:'username',
          },{
              title: '昵称',
              key: 'nickname',
              dataIndex:'nickname',
          }, {
            title : '状态',
            key : 'status',
            dataIndex : 'status',
            render: (text,record) => {
                        const result = this.findRecord(record);
                        return (
                          <span>{result}</span>
                        )
               },
          },
            {
              title : '电话号码',
              key : 'telephone',
              dataIndex : 'telephone',
          //     render:(text,record)=>{
          //     console.log(text,record.telephone)
          //       let data=record.telephone;
          //     const data1=data.replace(/^(.{4})(.*)$/,'$1......');
          //     return data1;
          // },
            },
            {
              title : '邮箱',
              key : 'email',
              dataIndex : 'email',
            },
          ],

          menulistArr:[{
              key:'handle-1',
              name:'修改信息',
              menuclick:this.update,
          },{
          key:'handle-4',
          name:'重置密码',
          menuclick:this.reset,
      }],
         menulistArrz:[{
          key:'handle-4',
          name:'置为可用',
          menuclick:this.setstatus1,
            },{
              key:'handle-5',
              name:'锁定账户',
              menuclick:this.setstatus2,
          },{
            key:'handle-6',
            name:'置为不可用',
            menuclick:this.setstatus0,
        }],
      },
      pagination,
  }
    const modalProps = {
      rolesList,
      addloading,
      userRole,
      visible,
      title,
      treeData,
      handleOk:this.handleOk,
      handleCancel:this.handleCancel,
      formInfo:currentRow,
    }
    const BtnList = [{
      icon:'plus',
      name:'新增',
      type:'',
      key:'1',
      click:this.add,
  },]
    return (
      <PageHeaderLayout>
         <Card title="快速查询" bordered={false} style={{marginBottom:'20px'}}>
         <SearchForm {...searchProps}  />
            </Card>
      <Card bordered={false}>
          <div style={{ marginBottom: 16 }}>
               <ToolBar BtnList={BtnList} />
              <AddForm {...modalProps} />
            </div>
            <AutoList {...pageProps} turnPage={this.turnPage}  />
      </Card>
      </PageHeaderLayout>
    )
  }
}
export default UserManagement;
