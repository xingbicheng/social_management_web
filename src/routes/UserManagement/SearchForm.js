import React,{Component} from 'react';
import { Form,Input,Button,Select,Row,Col,Alert } from 'antd';
import {FormItemLayoutPage,SearchFormLayout,SearchBtnLayout} from "../../config/config";
import styles from '../../theme/table.less';

const FormItem = Form.Item;
const Option = Select.Option;
@Form.create()
class SearchForm extends Component{
    constructor(props){
        super(props);
    }
    checkAccount(rule, value, callback) {
      // //与表单数据进行关联
      //   const form = this.props;
        // 正则用//包起来
        const phoneRegular = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        if (value) {
          // react使用正则表达式变量的test方法进行校验，直接使用value.match(regex)显示match未定义
          if (phoneRegular.test(value)) {
            callback();
          } else {
            callback('请输入正确的手机号码！');
          }
        } else {
          callback();
        }
      }
    searchList=(e)=>{
        const { form,search } = this.props;
        e.preventDefault();
        form.validateFields((err, values) => {
            if(!err){
                for(const valuesName in values){
                    if(values.hasOwnProperty(valuesName)){
                        if(!values[valuesName]){
                            delete values[valuesName];
                        }
                    }
                }
                search(values);
            }else{
                return (
                  <Alert
                    description="表单验证失败"
                    type="error"
                  />
                )
            }
        });
    }
    restSearchForm=()=>{
        const { form,restForm } = this.props;
        form.resetFields();
        restForm();
    }
    render(){
        const { searchInfo,form:{getFieldDecorator} } = this.props;
        const formItemLayout = FormItemLayoutPage;  // 通用样式表
        return(
          <Row>
            <Form 
            
            className={styles.searchBox}>
             
             <Row>
              <Col  {...SearchFormLayout}>
                <FormItem
                  {...formItemLayout}
                  label="用户名"
                >
                  {getFieldDecorator('username', {
                                initialValue:searchInfo.name || '',
                                rules: [{type:'string', message: '请输入用户名!' }],
                            })(
                              <Input placeholder='请输入用户名'  style={{ width:'100%' }} />
                            )}
                </FormItem>
              </Col>
              <Col {...SearchFormLayout}>
                <FormItem
                  {...formItemLayout}
                  label="电话号码"
                >
                  {getFieldDecorator('telephone', {
                                initialValue:searchInfo.abridge || '',
                                rules: [{type:'string', message: '请输入手机号码!' },{
                                  // 这里input内的输入内容进行绑定函数即可，在Input里面无需进行函数绑定开使用验证（红色部分）
                                  validator: this.checkAccount,}],
                            })(
                              <Input placeholder='请输入手机号码'  />
                            )}
                </FormItem>
              </Col>
              </Row>
              <Row>
                <Col  {...SearchBtnLayout}>
                  <div
                    style={{margin:0}}
                    className={styles.btnBox}
                  >
                    <Button onClick={this.restSearchForm}>重置</Button>
                    <Button onClick={this.searchList} type="primary" icon="search">搜索</Button>
                  </div>
                </Col>
              </Row>
            </Form>
          </Row>
        );
    }
}
export default SearchForm;
