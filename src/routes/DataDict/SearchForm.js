import React, { Component } from 'react';
import { Form, Input, Button, Select, Row, Col, Alert } from 'antd';
import { FormItemLayoutPage,SearchFormLayout,SearchBtnLayout } from '../../config/config';
import styles from '../../theme/table.less';

const FormItem = Form.Item;
const Option = Select.Option;

@Form.create()
class SearchForm extends Component {
  constructor(props) {
    super(props);
  }

  searchList = () => {
    const { form, search } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        for (const valuesName in values) {
          if (values.hasOwnProperty(valuesName)) {
            if (!values[valuesName]) {
              delete values[valuesName];
            }
          }
        }
        search(values);
      } else {
        return (
          <Alert
            description="表单验证失败"
            type="error"
          />
        );
      }
    });
  };
  restSearchForm = () => {
    const { form, restForm } = this.props;
    form.resetFields();
    restForm();
  };

  render() {
    const { searchInfo, types ,form: { getFieldDecorator } } = this.props;
    const formItemLayout = FormItemLayoutPage;  // 通用样式表
    const typeData = types.map(d => <Option key={d.type}>{d.type}</Option>)
    return (
      <Row gutter={24}>
        <Form className={styles.searchBox}>
          <Row>
            <Col {...SearchFormLayout}>
            <FormItem
            {...formItemLayout}
                label="类型"
              >
                {getFieldDecorator('type', {
                            rules: [{
                                required: true, message: '类型是必填项！',
                            }],
                        })(<Select showSearch style={{ width: '100%' }}>{typeData}</Select>)
                        }
              </FormItem>
            </Col>       
          </Row>
          <Row >
            <Col {...SearchBtnLayout}>
              <div
                style={{ margin: 0 }}
                className={styles.btnBox}
              >
                <Button onClick={this.restSearchForm}>重置</Button>
                <Button onClick={this.searchList} type="primary" icon="search">搜索</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Row>
    );
  }
}

export default SearchForm;
