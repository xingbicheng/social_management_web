import React,{Component} from 'react';
import {  Card,Form,Modal } from 'antd';
import { connect } from 'dva';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import AutoList from '../../components/List/List';
import ToolBar from '../../components/ToolBar';
import AdvanceForm from './AdvanceForm';
import SearchForm from './SearchForm';

import { formmaterData } from '../../utils/utils';


const confirm = Modal.confirm;

@Form.create()
@connect(({ dictdata, loading }) => ({
     dictdata,
    loading: loading.effects['dictdata/listpage'],
}))
class DataDictIndex extends Component{
    constructor(props){
        super(props);
        this.state = {
            handleTitle:'新增',
            formMethod:'add',
            number:0,
        }
    }
    componentWillMount(){
        this.listpage();
    }
    listpage = ()=>{
        console.log(this.props);
        const { dispatch,dictdata:{searchInfo,pagination} } = this.props;
        const { pageNum,pageSize } = pagination;
        dispatch({
            type:'dictdata/listpage',
            payload:{
                page:pageNum,
                pageSize,
                ...searchInfo,
            },
        });
        dispatch({
            type:'dictdata/getType',

        });
    }

    add =()=>{
        const { dispatch } = this.props;
        this.setState({
            formMethod:'add',
            handleTitle:'新增',
        });
        dispatch({
            type: 'dictdata/showModal',
            payload:{
                createUserId: 0,
                id: 1,
                superiorDepartmentId: 0,
                updateUserId: 0,
            },
        });
    }

    // 编辑
    update=(record)=>{
        this.setState({
            handleTitle:'编辑',
            formMethod:'edit',
        });
        this.props.dispatch({
            type:'dictdata/showModal',
            payload:{
                ...record,
            },
        });
    }
    // 删除
    delete=(record)=>{
        const than = this;
        if(record && record.id){
            confirm({
                title: '删除',
                content: '是否删除当前部门？',
                okText:'确定',
                cancelText:'取消',
                onOk() {
                    than.props.dispatch({
                        type:'dictdata/delete',
                        payload:{
                            id:record.id,
                        },
                    });
                },
                onCancel() {

                },
            });

        }
    }

    turnPage = (e)=>{
        const {current,pageSize} = e;
        const { dispatch,dictdata:{searchInfo} } = this.props;
        dispatch({
            type:'dictdata/listpage',
            payload:{
                ...searchInfo,
                page:current,
                pageSize,
            },
        });
    }

    handleOk = (values) =>{
        const { formMethod } = this.state;
        const { currentRow } = this.props.dictdata;
        this.props.dispatch({
            type:`dictdata/${formMethod}`,
            payload:{
                ...currentRow,
                ...values,
                // id:currentRow.id,
            },
        });
    }

    handleCancel = () =>{
        this.props.dispatch({
            type:'dictdata/hiddenModal',
        });
    }

    restSearch = () => {
        const {dispatch} = this.props;
        dispatch({
            type:'dictdata/clearSearchInfo',
        });
        dispatch({
            type:'dictdata/listpage',
            payload:{
                page:1,
                pageSize:10,
            },
        });
    }

    search = (values)=>{
        const {dispatch} = this.props;
        dispatch({
            type:'dictdata/setSearchInfo',
            payload:{...values},
        });
        dispatch({
            type:'dictdata/listpage',
            payload:{
                page:1,
                pageSize:10,
                ...values,
            },
        })
    }

    render(){
        const { dictdata:{dataSource,visible,currentRow,searchInfo,pagination,types}, loading} = this.props;
        const { form } = this.props;
        const { handleTitle } = this.state;
        const pageProps = {
            dataSource,
            loading,
            columsList:{
                columns:[{
                    title:'#',
                    key:'index',
                    dataIndex:'index',
                    align:'center',
                    render: (text, record, index) => {
                        return <span>{index + 1}</span>
                    },
                },{
                    title: '类型',
                    key: 'type',
                    dataIndex:'type',
                },{
                    title: '键',
                    key: 'k',
                    dataIndex:'k',
                }, {
                  title : '键值',
                  key : 'val',
                  dataIndex : 'val',
                },
                  {
                    title : '备注',
                    key : 'memo',
                    dataIndex : 'memo',
                  },
                ],
                menulistArr:[{
                    key:'handle-1',
                    name:'编辑',
                    menuclick:this.update,
                },{
                    key:'handle-2',
                    name:'删除',
                    menuclick:this.delete,
                }],
            },
            pagination,
        }
        const modalProps = {
            visible,
            types,
            handleOk:this.handleOk,
            handleCancel:this.handleCancel,
            title:`管理员表-${handleTitle}`,
            formInfo:  currentRow,
        }
        const searchProps = {
            parentForm:form,
            types,
            searchInfo,
            restForm:this.restSearch,
            search:this.search,
        }
        const BtnList = [{
            icon:'plus',
            name:'新增',
            type:'',
            key:'1',
            click:this.add,
        },
    ]
        return(
          <PageHeaderLayout>
            <Card title="快速查询" bordered={false} style={{marginBottom:'20px'}} >
              <SearchForm {...searchProps}  />
            </Card>
            <Card bordered={false}>
              <ToolBar BtnList={BtnList} />
              <AutoList {...pageProps} turnPage={this.turnPage} />
              <AdvanceForm {...modalProps}  />
            </Card>
          </PageHeaderLayout>
        );
    }
}
export default DataDictIndex;

