import React,{Component} from 'react';
import {Form,Input,Select,Modal,Alert} from 'antd';
import {FormItemLayoutModal} from "../../config/config";

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;


@Form.create()
class AdvanceForm extends Component{
    constructor(props){
        super(props);
    }

    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        handleCancel();
    }
    onHandleOk = ()=>{
        const { form,handleOk } = this.props;
        form.validateFields((err, values) => {
            if(!err){
                handleOk(values);
            }else{
                return (
                  <Alert
                    description="表单验证失败"
                    type="error"
                  />
                )
            }
        });
        form.resetFields();
    };
    render(){
        const { visible,types,title,formInfo,form:{getFieldDecorator} } = this.props;
        const formItemLayout = FormItemLayoutModal;  // 通用样式表
        const typeData = types.map(d => <Option key={d.type}>{d.type}</Option>)
        return(
          <Modal
            title={title}
            visible={visible}
            onOk={this.onHandleOk}
            onCancel={this.onHandleCancel}
          >
            <Form onSubmit={this.handleSubmit}>
              <FormItem
                {...formItemLayout}
                label="类型"
              >
                {getFieldDecorator('type', {
                            initialValue:formInfo.type || '',
                            rules: [{
                                required: true, message: '类型是必填项！',
                            }],
                        })(<Select showSearch style={{ width: '100%' }}>{typeData}</Select>)
                        }
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="键"
              >
                {getFieldDecorator('k', {
                            initialValue:formInfo.k || '',
                            rules: [{
                                required: true, message: '数据字典键是必填项！',
                            }],
                        })(
                          <Input  placeholder='请填写键名'/>
                        )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="键值"
              >
                {getFieldDecorator('val', {
                            initialValue:formInfo.val || '',
                            rules: [{
                                required: true, message: '键值是必选项！',
                            }],
                        })(
                          <Input placeholder='请填写键值' style={{width:'100%'}} />
                        )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="简介"
              >
                {getFieldDecorator('memo', {
                            initialValue:formInfo.memo || '',
                            rules: [{
                                required: true,min:0,max:500,
                                message:'请填写简介,不能超过500字'
                            }],
                        })(
                          <TextArea placeholder="请填入备注" autosize={{ minRows: 4, maxRows: 6 }} />
                        )}
              </FormItem>
            </Form>
          </Modal>
        );
    }
}
export default AdvanceForm;

