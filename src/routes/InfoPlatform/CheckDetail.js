import React, { Component } from 'react';
import { connect } from 'dva';
import { Button, Card, Modal, Input, Form, Select, Col, Row, message } from 'antd';
import { routerRedux } from 'dva/router';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { SUBMITSTATE } from '../../utils/emenus';
import { FormItemLayoutPage } from '../../config/config';
import styles from '../../components/PageInfo/index.less';

const FormItem = Form.Item;
const Option = Select.Option;
const confirm = Modal.confirm;
const { TextArea } = Input;

@Form.create()
@connect(({ checkPage, loading }) => ({
  checkPage,
  loadingNotice: loading.effects['checkPage/NoticeDetail'],
  loadingSubmit: loading.effects['checkPage/SubmitDetail'],
  loadingExamine: loading.effects['checkPage/examine'],
  loadingPass: loading.effects['checkPage/pass'],
  loadingNotPass: loading.effects['checkPage/notPass'],
}))
export default class CheckDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auditAdvice: '',
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'checkPage/getSubmitCategory',
    });
    dispatch({
      type: 'checkPage/getSubmitShowHomepage',
    });
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/check/:id/:type').exec(this.props.history.location.pathname);
    dispatch({
      type: `checkPage/${match[2]}`,
      payload: {
        id: match[1],
      },
    });
  }

  pass = () => {
    const than = this;
    const { form } = this.props;
    const formData = form.getFieldsValue();
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/check/:id/:type').exec(this.props.history.location.pathname);
    form.validateFields((err) => {
      if (!err) {
        confirm({
          title: '确认',
          content: '是否审核通过',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            than.props.dispatch({
              type: 'checkPage/pass',
              payload: {
                id: match[1],
                ...formData,
                auditAdvice: than.state.auditAdvice,
              },
            });
          },
          onCancel() {
          },
        });
      } else {
        message.error('表单验证失败！');
      }
    });
  };
  notPass = () => {
    const than = this;
    const { form } = this.props;
    const formData = form.getFieldsValue();
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/check/:id/:type').exec(this.props.history.location.pathname);
    form.validateFields((err) => {
      if (!err) {
        confirm({
          title: '确认',
          content: '是否审核不通过',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            than.props.dispatch({
              type: 'checkPage/notPass',
              payload: {
                id: match[1],
                auditAdvice: than.state.auditAdvice,
                ...formData,
              },
            });
          },
          onCancel() {
          },
        });
      } else {
        message.error('表单验证失败！');
      }
    });
  };

  handleAdviceChange(event) {
    this.setState({
      auditAdvice: event.target.value,
    });
  };

  back = () => {
    this.props.dispatch(routerRedux.push('/infoPlatform/submit'));
  };

  render() {
    const {
      loadingNotPass, loadingPass, loadingNotice, loadingExamine, loadingSubmit,
      checkPage: { releaseTime, personName, articleContext, auditFailure, title, state, category, showHomepage, sort,submitted_infor_show,submitted_infor_category },
      form: { getFieldDecorator },
    } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/check/:id/:type').exec(this.props.history.location.pathname);
    const isShowAdvice = (match[2] === 'SubmitDetail'
      && (state === SUBMITSTATE.EXAMINEFAIL
        || state === SUBMITSTATE.EXAMINEPASS)) ? 'block' : 'none';
    const isPublish = releaseTime ? `发布时间:${releaseTime}` : '暂未发布';
    const categoryOptions = submitted_infor_category.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const showOptions = submitted_infor_show.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    return (
      <PageHeaderLayout>
        <Card
          loading={loadingSubmit || loadingNotice || loadingExamine}
        >
          <h1 align="center">{title}</h1>
          <div style={{ textAlign: 'center', marginBottom: 20 }}>
            <span>
                作者:{personName}
              &nbsp;&nbsp;|&nbsp;&nbsp;
              {isPublish}
            </span>
          </div>
          <div
            dangerouslySetInnerHTML={{ __html: articleContext }}
          />
          <div style={{ marginTop: 20, display: isShowAdvice }}>
            <Card title='审核意见'>
              <p>{auditFailure}</p>
            </Card>
          </div>
        </Card>
        {
          match[2] === 'examine' && (
            <Card
              bordered={false}
              title='审核'
              style={{ marginTop: 20 }}
              loading={loadingExamine}
            >
              <Row>
                <Form>
                  <Col span={8}>
                    <FormItem
                      {...FormItemLayoutPage}
                      label="信息类型"
                    >
                      {getFieldDecorator('category', {
                        initialValue: category || '',
                        rules: [{
                          required: true, message: '信息类型必选！',
                        }],
                      })(
                        <Select disabled>
                          {categoryOptions}
                        </Select>,
                      )}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem
                      {...FormItemLayoutPage}
                      label="首页显示"
                    >
                      {getFieldDecorator('showHomepage', {
                        initialValue: showHomepage || '0',
                        required: true, message: '请选择是否首页显示！',
                      })(
                        <Select>
                          {showOptions}
                        </Select>,
                      )}
                    </FormItem>
                  </Col>
                  <Col span={8}>
                    <FormItem
                      {...FormItemLayoutPage}
                      label="排序"
                    >
                      {getFieldDecorator('sort', {
                        initialValue: sort || '0',
                        rules: [{
                          required: true, message: '排序为必填！',
                        }],
                      })(
                        <Input
                          style={{ width: '100%' }}
                        />,
                      )}
                    </FormItem>
                  </Col>
                </Form>
              </Row>
            </Card>
          )}
        {
          match[2] === 'examine' && (
            <Card
              bordered={false}
              loading={loadingExamine}
            >
              <form>
                <FormItem
                  label="审核意见"
                >
                  {getFieldDecorator('auditAdvice', {
                    initialValue: '',
                    rules: [{
                      required: true,
                      whitespace: true,
                      message: '请不要输入纯空格,并且输入内容不能超出800字',
                    }],
                  })(
                    <TextArea
                      placeholder="请填写审核建议(1-800字)"
                      autosize={{ minRows: 6, maxRows: 10 }}
                      onChange={this.handleAdviceChange.bind(this)}
                    />,
                  )}
                </FormItem>
              </form>
              <div className={styles.btnBox}>
                <div className={styles.saveAndreturn}>
                  <Button
                    icon='close'
                    onClick={this.notPass}
                    loading={loadingNotPass}
                  >审核不通过
                  </Button>
                  <Button
                    icon='check'
                    type='primary'
                    onClick={this.pass}
                    loading={loadingPass}
                  >审核通过
                  </Button>
                </div>
              </div>
            </Card>
          )}
      </PageHeaderLayout>
    );
  }
}
