import React, { Component } from 'react';
import { Select, Col, Row, Form, Input, Button, message } from 'antd';
import { FormItemLayoutPage, SearchBtnLayout, SearchFormLayout } from '../../../config/config';
import styles from '../../../theme/table.less';

const FormItem = Form.Item;
const Option = Select.Option;


@Form.create()
export default class SearchForm extends Component {
  constructor(props) {
    super(props);
  }

  searchList = () => {
    const { form, search } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        for (const valuesName in values) {
          if (values.hasOwnProperty(valuesName)) {
            if (!values[valuesName]) {
              delete values[valuesName];
            }
          }
        }
        search(values);
      } else {
        message.error('表单验证失败！');
      }
    });
  };

  restSearchForm = () => {
    const { form, restForm } = this.props;
    form.resetFields();
    restForm();
  };

  render() {
    const {
      searchInfo, form: { getFieldDecorator }, submitted_infor_statues, submitted_infor_category,
    } = this.props;
    const categoryOptions = submitted_infor_category.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    const stateOptions = submitted_infor_statues.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    return (
      <Row gutter={24}>
        <Form className={styles.searchBox}>
          <Row>
            <Col {...SearchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                label="标题"
              >
                {getFieldDecorator('title', {
                  initialValue: searchInfo.title || '',
                  rules: [],
                })(
                  <Input placeholder='请输入您要查询的标题' style={{ width: '100%' }}/>,
                )}
              </FormItem>
            </Col>
            <Col {...SearchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                label="类型"
              >
                {getFieldDecorator('category', {
                  initialValue: searchInfo.category || '',
                  rules: [],
                })(
                  <Select>
                    <Option value=''>全部</Option>
                    {categoryOptions}
                  </Select>,
                )}
              </FormItem>
            </Col>
            <Col {...SearchFormLayout}>
              <FormItem
                {...FormItemLayoutPage}
                label="状态"
              >
                {getFieldDecorator('state', {
                  initialValue: searchInfo.state || '',
                  rules: [],
                })(
                  <Select>
                    <Option value=''>全部</Option>
                    {stateOptions}
                  </Select>,
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col {...SearchBtnLayout}>
              <div
                style={{ margin: 0 }}
                className={styles.btnBox}
              >
                <Button onClick={this.restSearchForm}>重置</Button>
                <Button onClick={this.searchList} type="primary" icon="search">搜索</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Row>
    );
  }
}

