import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Row, Col, Card, Modal, Button, message } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/ListTwoOperation/List';
import ToolBar from '../../../components/ToolBar/index';
import SearchForm from './SearchForm';
import { SUBMITCATEGORY, SUBMITSTATE } from '../../../utils/emenus';
import { findRecord } from '../../../utils/utils';

const confirm = Modal.confirm;

@connect(({ infoSubmit, loading }) => ({
  infoSubmit,
  loading: loading.effects['infoSubmit/SubmitPage'],
}))
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      selectedRows: [],
      selectedRowKeys: [],
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoSubmit/getSubmitCategory',
    });
    dispatch({
      type: 'infoSubmit/getSubmitStatues',
    });
    this.listpage();
  }

  addSubmit = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push({
        pathname: '/infoPlatform/submitDetail/0/add',
      }),
    );
  };
  handleSubmit = (record) => {
    const than = this;
    const { submittedSectionId, id } = record;
    if (record.state === '0') {
      confirm({
        title: '确认',
        content: '是否报送该消息？',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          than.props.dispatch({
            type: 'infoSubmit/submit',
            payload: {
              id,
              submittedSectionId,
            },
          });
        },
        onCancel() {
        },
      });
    } else {
      message.error('只能报送保存状态的信息');
    }
  };
  delete = (record) => {
    const than = this;
    if (record.state === SUBMITSTATE.SAVE) {
      if (record && record.id) {
        confirm({
          title: '删除',
          content: '是否删除？',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            than.props.dispatch({
              type: 'infoSubmit/delete',
              payload: {
                id: record.id,
              },
            });
          },
          onCancel() {
          },
        });
      }
    } else {
      message.error('只能删除保存状态的信息');
    }
  };
  deleteSelects = () => {
    const idList = this.state.selectedRows;
    const than = this;
    const rows = idList.filter(i => i.state !== SUBMITSTATE.SAVE);
    if (rows.length) {
      message.error('包含不是保存状态的信息');
      return;
    }
    confirm({
      title: '删除',
      content: '是否删除所勾选信息？',
      okText: '确定',
      cancelText: '取消',
      onOk() {
        const { selectedRowKeys } = than.state;
        than.props.dispatch({
          type: 'infoSubmit/deleteAll',
          payload: {
            ids: selectedRowKeys,
          },
        });
        than.setState({
          selectedRowKeys: [],
        });
      },
    });

  };

  listpage = () => {
    const { dispatch } = this.props;
    const { searchInfo, pagination } = this.props.infoSubmit;
    const { page, pageSize } = pagination;
    dispatch({
      type: 'infoSubmit/SubmitPage',
      payload: {
        page,
        pageSize,
        ...searchInfo,
      },
    });
  };
  watch = (record) => {
    const { dispatch } = this.props;
    const { id } = record;
    dispatch(routerRedux.push({
      pathname: `/infoPlatform/check/${id}/SubmitDetail`,
    }));
  };
  update = (record) => {
    if (record.state === SUBMITSTATE.SAVE) {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch(routerRedux.push({
        pathname: `/infoPlatform/submitDetail/${id}/edit`,
      }));
    } else {
      message.error('不能编辑当前信息');
    }
  };
  restSearch = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoSubmit/clearAll',
    });
    dispatch({
      type: 'infoSubmit/SubmitPage',
      payload: {
        page: 1,
        pageSize: 10,
      },
    });
  };
  search = (values) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoSubmit/setSearchInfo',
      payload: { ...values },
    });
    dispatch({
      type: 'infoSubmit/SubmitPage',
      payload: {
        ...values,
      },
    });
  };
  turnPage = (e) => {
    const { current, pageSize } = e;
    const { dispatch, infoSubmit: { searchInfo } } = this.props;
    dispatch({
      type: 'infoSubmit/SubmitPage',
      payload: {
        ...searchInfo,
        page: current,
        pageSize,
      },
    });
  };
  examine = (record) => {
    if (record.state === SUBMITSTATE.SUBMIT) {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch(routerRedux.push({
        pathname: `/infoPlatform/check/${id}/examine`,
      }));
    } else {
      message.error('只能审核已报送的信息');
    }
  };
  show = (record) => {
    const { dispatch } = this.props;
    const { id } = record;
    if (record.state === SUBMITSTATE.EXAMINEPASS) {
      confirm({
        title: '确认',
        content: '是否设为首页显示？',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          dispatch({
            type: 'infoSubmit/show',
            payload: {
              id,
            },
          });
        },
        onCancel() {
        },
      });
    } else {
      message.error('只能设置已审核成功的信息');
    }
  };
  notShow = (record) => {
    const { dispatch } = this.props;
    const { id } = record;
    if (record.state === SUBMITSTATE.PUBLISH) {
      confirm({
        title: '确认',
        content: '是否设为首页不显示？',
        okText: '确定',
        cancelText: '取消',
        onOk() {
          dispatch({
            type: 'infoSubmit/notShow',
            payload: {
              id,
            },
          });
        },
        onCancel() {
        },
      });
    } else {
      message.error('只能设置已发布的信息');
    }
  };

  render() {
    const { infoSubmit: { dataSource, pagination, searchInfo, submitted_infor_statues, submitted_infor_category }, loading, form } = this.props;
    const { selectedRowKeys } = this.state;
    const hasSelected = selectedRowKeys.length > 0;
    const pageProps = {
      dataSource,
      loading,
      columsList: {
        columns: [{
          title: '#',
          key: 'index',
          dataIndex: 'index',
          align: 'center',
          render: (text, record, index) => {
            return <span>{index + 1}</span>;
          },
        }, {
          title: '标题',
          key: 'title',
          dataIndex: 'title',
        },
          {
            title: '信息类别',
            key: 'category',
            dataIndex: 'category',
            render: (record) => {
              const data = findRecord(this.props.infoSubmit.submitted_infor_category, 'k', record);
              return data.val;
            },
          },
          {
            title: '部门',
            key: 'submittedSection',
            dataIndex: 'submittedSection',
          },
          {
            title: '时间',
            key: 'updateTime',
            dataIndex: 'updateTime',
          },
          {
            title: '状态',
            key: 'state',
            dataIndex: 'state',
            render: (record) => {
              const data = findRecord(this.props.infoSubmit.submitted_infor_statues, 'k', record);
              return data.val;
            },
          },
        ],
        menulistArr: [{
          key: 'handle-1',
          name: '查看',
          menuclick: this.watch,
        },
          {
            key: 'handle-2',
            name: '编辑',
            menuclick: this.update,
          },
          {
            key: 'handle-3',
            name: '报送',
            menuclick: this.handleSubmit,
          },
          {
            key: 'handle-4',
            name: '审核',
            menuclick: this.examine,
          },
          {
            key: 'handle-5',
            name: '删除',
            menuclick: this.delete,
          }],
        menulistArrz: [{
          key: 'handle-6',
          name: '首页显示',
          menuclick: this.show,
        }, {
          key: 'handle-7',
          name: '取消首页显示',
          menuclick: this.notShow,
        }],
      },
      pagination,
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            number: selectedRows.length,
            selectedRows,
            selectedRowKeys,
          });
        },
      },
    };
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.addSubmit,
    }];
    const searchProps = {
      parentForm: form,
      searchInfo,
      restForm: this.restSearch,
      search: this.search,
      submitted_infor_statues,
      submitted_infor_category,
    };
    return (
      <PageHeaderLayout>
        <Card
          title='快速查询'
          bordered={false}
          style={{ marginBottom: '20px' }}
        >
          <SearchForm {...searchProps}  />
        </Card>
        <Card
          bordered={false}
        >
          <Row type="flex" justify="end">
            <Col style={{ lineHeight: '30px' }} span={2}>
              {hasSelected && `当前选中${this.state.number}条`}
            </Col>
            <Col>
              {
                hasSelected &&
                <Button type='danger' icon="close" onClick={this.deleteSelects}>删除</Button>
              }
            </Col>
            <Col>
              <ToolBar BtnList={BtnList}/>
            </Col>
          </Row>
          <AutoList {...pageProps} turnPage={this.turnPage}/>
        </Card>
      </PageHeaderLayout>
    );
  }
}
