import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Card, Modal, Input, Form, Select, Col, Row, Checkbox, message, TreeSelect } from 'antd';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { FormItemLayoutPage } from '../../../config/config';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../components/PageInfo/index.less';
import { SUBMITSTATE } from '../../../utils/emenus';


const FormItem = Form.Item;
const Option = Select.Option;
const confirm = Modal.confirm;
const { TextArea } = Input;
const TreeNode = TreeSelect.TreeNode;

@Form.create()
@connect(({ infoSubmit, loading }) => ({
  infoSubmit,
  loading: loading.effects['infoSubmit/SubmitDetail'],
  loadingUpdate: loading.effects['infoSubmit/update'],
  loadingAdd: loading.effects['infoSubmit/add'],
}))

export default class SubmitDetail extends Component {
  constructor(props) {
    super(props);
    this.reactQuillRef = null;
    this.state = {
      state: SUBMITSTATE.SAVE,
      checked: 0,
      buttonValue: '保存',
    };
  }

  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoSubmit/getSubmitCategory',
    });
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/submitDetail/:id/:type').exec(this.props.history.location.pathname);
    if (match[2] !== 'add') {
      dispatch({
        type: 'infoSubmit/SubmitDetail',
        payload: {
          id: match[1],
        },
      });
    } else {
      dispatch({
        type: 'infoSubmit/updateForm',
      });
    }
    this.props.dispatch({
      type: 'infoSubmit/getDepartmentList',
    });
    dispatch({
      type: 'infoSubmit/addDepartment',
      payload: {
        submittedSectionId: '请选择部门',
      },
    });
  };

  onChange = (e) => {
    this.setState({
      checked: e.target.checked,
      buttonValue: this.state.checked ? '保存' : '报送',
      state: this.state.checked ? SUBMITSTATE.SAVE : SUBMITSTATE.SUBMIT,
    });
  };
  add = () => {
    const than = this;
    const { form } = this.props;
    const formData = form.getFieldsValue();
    const msg = '是否' + this.state.buttonValue + '?';
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/submitDetail/:id/:type').exec(this.props.history.location.pathname);
    const quillContent = this.reactQuillRef.getEditor().getText();
    form.validateFields((err) => {
      if (!err) {
        confirm({
          title: than.state.buttonValue,
          content: msg,
          okText: '确定',
          cancelText: '取消',
          onOk() {
            if (!quillContent.trim()) {
              message.warning('文章内容不能为空！！！！');
              return;
            }
            if (match[1] === '0') {
              than.props.dispatch({
                type: 'infoSubmit/add',
                payload: {
                  ...than.state,
                  whileTime: than.state.state,
                  ...formData,
                },
              });
            }
            else {
              than.props.dispatch({
                type: 'infoSubmit/update',
                payload: {
                  ...than.state,
                  whileTime: than.state.state,
                  ...formData,
                  id: match[1],
                },
              });
            }
          },
          onCancel() {
          },
        });
      } else {
        message.error('表单验证失败！');
      }
    });
  };
  back = () => {
    this.props.dispatch(routerRedux.push('/infoPlatform/submit'));
  };


  render() {
    const { infoSubmit: { detailSource: formInfo, treeData, submitted_infor_category }, form: { getFieldDecorator }, loadingUpdate, loadingAdd, loading } = this.props;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/submitDetail/:id/:type').exec(this.props.history.location.pathname);
    const ButtonName = this.state.buttonValue;
    const buttonIcon = this.state.checked ? 'check' : 'save';
    const handleTitle = (match[2] === 'edit') ? '编辑' : '新增';
    const FormItemLayoutPages = {
      labelCol: {
        xs: { span: 2 },
        sm: { span: 2 },
        md: { span: 2 },
        lg: { span: 2 },
      },
      wrapperCol: {
        xs: { span: 22 },
        sm: { span: 22 },
        md: { span: 22 },
        lg: { span: 22 },
      },
    };
    const categoryOptions = submitted_infor_category.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });

    return (
      <PageHeaderLayout>
        <Card
          bordered={false}
          title={handleTitle}
          style={{ marginBottom: '20px' }}
          loading={loading}
        >
          <Row>
            <Col span={24}>
              <Form>
                <FormItem
                  label="标题"
                  {...FormItemLayoutPages}
                >
                  {getFieldDecorator('title', {
                    initialValue: formInfo.title || '',
                    rules: [{
                      required: true, message: '标题必填项！',
                    }],
                  })(
                    <Input/>,
                  )}
                </FormItem>
              </Form>
            </Col>
          </Row>
          <Row>
            <Form>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="信息类型"
                >
                  {getFieldDecorator('category', {
                    initialValue: formInfo.category || '',
                    rules: [{
                      required: true, message: '信息类型必选！',
                    }],
                  })(
                    <Select>
                      {categoryOptions}
                    </Select>,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  label="部门选择"
                  {...FormItemLayoutPage}
                >
                  {getFieldDecorator('submittedSectionId', {
                    initialValue: String(formInfo.submittedSectionId) || '',
                    rules: [{ required: true, message: '请选择部门' }],
                  })(
                    <TreeSelect
                      dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                      treeData={treeData}
                      placeholder="请选择部门"
                    />,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="排序"
                >
                  {getFieldDecorator('sort', {
                    initialValue: formInfo.sort || '0',
                    rules: [{
                      required: true, message: '排序为必填！',
                    }],
                  })(
                    <Input
                      style={{ width: '100%' }}
                    />,
                  )}
                </FormItem>
              </Col>
            </Form>
          </Row>
        </Card>
        <Card loading={loading}>
          <FormItem
            label="文章内容"
          >
            {getFieldDecorator('articleContext', {
              initialValue: formInfo.articleContext || '',
              rules: [{
                required: true,
                whitespace: true,
                message: '文章内容必填！',
              }],
            })(
              <ReactQuill
                theme="snow"
                ref={(el) => {
                  this.reactQuillRef = el;
                }}
                placeholder='文章字数为1-99999字(必填)'
              />,
            )}
          </FormItem>
          <Checkbox
            checked={this.state.checked}
            disabled={this.state.disabled}
            onChange={this.onChange}
          >
            是否同时报送
          </Checkbox>
          <div className={styles.btnBox}>
            <div className={styles.saveAndreturn}>
              <Button
                onClick={this.back}
              >返回
              </Button>
              <Button
                icon={buttonIcon}
                type="primary"
                onClick={this.add}
                loading={loadingUpdate || loadingAdd}
              >{ButtonName}
              </Button>
            </div>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
};
