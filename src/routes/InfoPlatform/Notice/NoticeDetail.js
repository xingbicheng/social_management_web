import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button, Card, Checkbox, Input, Form, Select, Col, Row, Modal, message } from 'antd';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { FormItemLayoutPage } from '../../../config/config';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import styles from '../../../components/PageInfo/index.less';
import { NOTICESTATE } from '../../../utils/emenus';
import { noticeCategory } from '../../../utils/seletLocalData';


const FormItem = Form.Item;
const Option = Select.Option;
const confirm = Modal.confirm;

@Form.create()
@connect(({ infoPlatform, loading }) => ({
  infoPlatform,
  loading: loading.effects['infoPlatform/NoticeDetail'],
  loadingUpdate: loading.effects['infoPlatform/update'],
  loadingAdd: loading.effects['infoPlatform/add'],
}))

export default class NoticeDetail extends Component {
  constructor(props) {
    super(props);
    this.reactQuillRef = null;
    this.state = {
      state: NOTICESTATE.SAVE,
      checked: 0,
      buttonValue: '保存',
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoPlatform/getNoticeCategory',
    });
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/noticeDetail/:id/:type').exec(this.props.history.location.pathname);
    if (match[2] !== 'add') {
      dispatch({
        type: 'infoPlatform/NoticeDetail',
        payload: {
          id: match[1],
        },
      });
    } else {
      dispatch({
        type: 'infoPlatform/updateForm',
      });
    }
  }

  onChange = (e) => {
    this.setState({
      checked: e.target.checked,
      buttonValue: this.state.checked ? '保存' : '发布',
      state: this.state.checked ? NOTICESTATE.SAVE : NOTICESTATE.PUBLISH,
    });
  };
  add = () => {
    const than = this;
    const { form, dispatch } = this.props;
    const formData = form.getFieldsValue();
    const msg = `是否${  this.state.buttonValue  }?`;
    const pathToRegexp = require('path-to-regexp');
    const match = pathToRegexp('/infoPlatform/noticeDetail/:id/:type').exec(this.props.history.location.pathname);
    const quillContent = this.reactQuillRef.getEditor().getText();
    form.validateFields((err) => {
      if (!err) {
        confirm({
          title: than.state.buttonValue,
          content: msg,
          okText: '确定',
          cancelText: '取消',
          onOk() {
            if (!quillContent.trim()) {
              message.warning('文章内容不能为空！！！！');
              return;
            }
            if (match[1] === '0') {
              dispatch({
                type: 'infoPlatform/add',
                payload: {
                  ...than.state,
                  ...formData,
                  whileTime: than.state.state,
                },
              });
            } else {
              dispatch({
                type: 'infoPlatform/update',
                payload: {
                  ...than.state,
                  ...formData,
                  whileTime: than.state.state,
                  id: match[1],
                },
              });
            }
          },
          onCancel() {
          },
        });
      } else {
        message.error('表单验证失败！');
      }
    });
  };

  back = () => {
    this.props.dispatch(routerRedux.push('/infoPlatform/notice'));
  };

  render() {
    const { form: { getFieldDecorator }, loadingUpdate, loadingAdd, loading, infoPlatform: { detailSource: formInfo,infor_notice_type } } = this.props;
    const ButtonName = this.state.buttonValue;
    const typeOptions = infor_notice_type.map((current) => {
      return <Option key={current.k}>{current.val}</Option>;
    });
    return (
      <PageHeaderLayout>
        <Card
          bordered={false}
          title='编辑通知'
          style={{ marginBottom: '20px' }}
          loading={loading}
        >
          <Row>
            <Form>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="标题"
                >
                  {getFieldDecorator('title', {
                    initialValue: formInfo.title || '',
                    rules: [{
                      required: true, message: '标题必填项！',
                    }],
                  })(
                    <Input/>,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="信息类型"
                >
                  {getFieldDecorator('category', {
                    initialValue: formInfo.category || '',
                    rules: [{
                      required: true, message: '信息类型必选！',
                    }],
                  })(
                    <Select>
                      {typeOptions}
                    </Select>,
                  )}
                </FormItem>
              </Col>
              <Col span={8}>
                <FormItem
                  {...FormItemLayoutPage}
                  label="排序"
                >
                  {getFieldDecorator('sort', {
                    initialValue: formInfo.sort || '0',
                    rules: [{
                      required: true, message: '排序为必填！',
                    }],
                  })(
                    <Input style={{ width: '100%' }}/>,
                  )}
                </FormItem>
              </Col>
            </Form>
          </Row>
        </Card>
        <Card loading={loading}>
          <FormItem
            label="文章内容"
          >
            {getFieldDecorator('articleContext', {
              initialValue: formInfo.articleContext || '',
              rules: [{
                required: true,
                whitespace: true,
                message: '文章内容必填！',
              }],
            })(
              <ReactQuill
                theme="snow"
                ref={(el) => {
                  this.reactQuillRef = el;
                }}
                placeholder='文章字数为1-99999字(必填)'
              />,
            )}
          </FormItem>
          <Checkbox
            checked={this.state.checked}
            disabled={this.state.disabled}
            onChange={this.onChange}
          >
            是否同时发布
          </Checkbox>
          <div className={styles.btnBox}>
            <div className={styles.saveAndreturn}>
              <Button
                onClick={this.back}
              >返回
              </Button>
              <Button
                icon='save'
                type="primary"
                onClick={this.add}
                loading={loadingUpdate || loadingAdd}
              >{ButtonName}
              </Button>
            </div>
          </div>
        </Card>
      </PageHeaderLayout>
    );
  }
};
