import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Card, Modal, Row, Col, Button, message } from 'antd';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import AutoList from '../../../components/List/List';
import ToolBar from '../../../components/ToolBar/index';
import SearchForm from './SearchForm';
import { NOTICECATEGORY, NOTICESTATE } from '../../../utils/emenus';
import { findRecord } from '../../../utils/utils';

const confirm = Modal.confirm;

@connect(({ infoPlatform, loading }) => ({
  infoPlatform,
  loading: loading.effects['infoPlatform/NoticePage'],
}))
export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      selectedRows: [],
      selectedRowKeys: [],
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoPlatform/getNoticeCategory',
    });
    dispatch({
      type: 'infoPlatform/getNoticeStatues',
    });
    this.listpage();
  }

  addNotice = () => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push(`/infoPlatform/noticeDetail/0/add`));
  };
  delete = (record) => {
    const than = this;
    if (record.state === NOTICESTATE.SAVE) {
      if (record && record.id) {
        confirm({
          title: '删除',
          content: '是否删除？',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            than.props.dispatch({
              type: 'infoPlatform/delete',
              payload: {
                id: record.id,
              },
            });
          },
          onCancel() {
          },
        });
      }
    } else {
      message.error('只能删除保存状态的信息');
    }
  }

  ;
  deleteSelects = () => {
    const idList = this.state.selectedRows;
    const than = this;
    const rows = idList.filter(i => i.state !== NOTICESTATE.SAVE);
    if (rows.length) {
      message.error('包含不是保存状态的信息');
      return;
    }
    confirm({
      title: '删除',
      content: '是否删除所勾选信息？',
      okText: '确定',
      cancelText: '取消',
      onOk() {
        const { selectedRowKeys } = than.state;
        than.props.dispatch({
          type: 'infoPlatform/deleteAll',
          payload: {
            ids: selectedRowKeys,
          },
        });
        than.setState({
          selectedRowKeys: [],
        });
      },
    });

  };

  listpage = () => {
    const { dispatch } = this.props;
    const { searchInfo, pagination } = this.props.infoPlatform;
    const { page, pageSize } = pagination;
    dispatch({
      type: 'infoPlatform/NoticePage',
      payload: {
        page,
        pageSize,
        ...searchInfo,
      },
    });
  };
  watch = (record) => {
    const { dispatch } = this.props;
    const { id } = record;
    dispatch(routerRedux.push(`/infoPlatform/check/${id}/NoticeDetail`));
  };
  update = (record) => {
    if (record.state === NOTICESTATE.SAVE) {
      const { dispatch } = this.props;
      const { id } = record;
      dispatch({
        type: 'infoPlatform/NoticeDetail',
        payload: {
          id,
        },
      });
      dispatch(routerRedux.push(`/infoPlatform/noticeDetail/${id}/edit`));
    } else {
      message.error('不能编辑当前信息');
    }
  };
  restSearch = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoPlatform/clearAll',
    });
    dispatch({
      type: 'infoPlatform/NoticePage',
      payload: {
        page: 1,
        pageSize: 10,
      },
    });
  };
  search = (values) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'infoPlatform/setSearchInfo',
      payload: { ...values },
    });
    dispatch({
      type: 'infoPlatform/NoticePage',
      payload: {
        ...values,
      },
    });
  };
  turnPage = (e) => {
    const { current, pageSize } = e;
    const { dispatch, infoPlatform: { searchInfo } } = this.props;
    dispatch({
      type: 'infoPlatform/NoticePage',
      payload: {
        ...searchInfo,
        page: current,
        pageSize,
      },
    });
  };

  render() {
    const { infoPlatform: { dataSource, pagination, searchInfo, infor_notice_type, infor_notice_statues }, loading, form } = this.props;
    const { selectedRowKeys } = this.state;
    const hasSelected = selectedRowKeys.length > 0;
    const pageProps = {
      dataSource,
      loading,
      columsList: {
        columns: [{
          title: '#',
          key: 'index',
          dataIndex: 'index',
          align: 'center',
          render: (text, record, index) => {
            return <span>{index + 1}</span>;
          },
        }, {
          title: '标题',
          key: 'title',
          dataIndex: 'title',
        },
          {
            title: '信息类别',
            key: 'category',
            dataIndex: 'category',
            render: (record) => {
              const data = findRecord(this.props.infoPlatform.infor_notice_type, 'k', record);
              return data.val;
            },
          },
          {
            title: '状态',
            key: 'state',
            dataIndex: 'state',
            render: (record) => {
              const data = findRecord(this.props.infoPlatform.infor_notice_statues, 'k', record);
              return data.val;
            },
          },
          {
            title: '发布时间',
            key: 'releaseTime',
            dataIndex: 'releaseTime',
            render: (record) => {
              if (record) {
                return record;
              } else {
                return '暂未发布';
              }
            },
          },
        ],
        menulistArr: [{
          key: 'handle-1',
          name: '查看',
          menuclick: this.watch,
        },
          {
            key: 'handle-2',
            name: '编辑',
            menuclick: this.update,
          }, {
            key: 'handle-3',
            name: '删除',
            menuclick: this.delete,
          }],
      },
      pagination,
      rowSelection: {
        selectedRowKeys,
        onChange: (selectedRowKeys, selectedRows) => {
          this.setState({
            number: selectedRows.length,
            selectedRows,
            selectedRowKeys,
          });
        },
      },
    };
    const BtnList = [{
      icon: 'plus',
      name: '新增',
      type: '',
      key: '1',
      click: this.addNotice,
    }];
    const searchProps = {
      parentForm: form,
      searchInfo,
      restForm: this.restSearch,
      search: this.search,
      infor_notice_type,
      infor_notice_statues,
    };
    return (
      <PageHeaderLayout>
        <Card
          title='快速查询'
          bordered={false}
          style={{ marginBottom: '20px' }}
        >
          <SearchForm {...searchProps}  />
        </Card>
        <Card bordered={false}>
          <Row type="flex" justify="end">
            <Col style={{ lineHeight: '30px' }} span={2}>
              {hasSelected && `当前选中${this.state.number}条`}
            </Col>
            <Col>
              {
                hasSelected &&
                <Button type='danger' icon="close" onClick={this.deleteSelects}>删除</Button>
              }
            </Col>
            <Col>
              <ToolBar BtnList={BtnList}/>
            </Col>
          </Row>
          <AutoList {...pageProps} turnPage={this.turnPage}/>
        </Card>
      </PageHeaderLayout>
    );
  }
}
