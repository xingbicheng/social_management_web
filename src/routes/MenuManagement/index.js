import { Card,Table,Modal } from 'antd';
import React,{Component} from 'react';
import { connect } from 'dva';
import DropDown from '../../components/Dropdown/Dropdown'
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ToolBar from '../../components/ToolBar';
import AddForm from './AddForm';


const confirm = Modal.confirm;

@connect(({ menucontroller,loading }) => ({
    menucontroller,
    loading: loading.effects['menucontroller/getAllList'],
  }))
class MenuManagement extends Component {
  state = {
    pagination: {},
    loading: false,
    title:'xxx',
    type:'add',
    formMethod:'addMenu',
    columns:[
    {
        title: '菜单名称',
        key: 'name',
        dataIndex:'name',
    },
    {
        title: '图标',
        key: 'icon',
        width:150,
        dataIndex:'icon',
    }, 
    {
        title: '链接',
        key: 'route',
        dataIndex:'route',
    },    
    ],
    
  };
  
  componentDidMount() {
      this.listpage();
      this.createColums();
  }
  listpage = ()=>{
    const { menucontroller:{pagination} } = this.props;
    const { pageNum,pageSize } = pagination;
    const { dispatch } = this.props;
    dispatch({
        type:'menucontroller/getAllList',
        payload:{
            page:pageNum,
            pageSize,
            // ...searchInfo,
        },
    });
  }
  createColums = () => {
    const {  columns  } = this.state;
    const  menulistArr=[{
        key:'handle-4',
        name:'新增子菜单',
        menuclick:this.addChildrenMenu,
        },{
            key:'handle-5',
            name:'删除菜单',
            menuclick:this.del,
        },{
            key:'handle-6',
            name:'修改菜单',
            menuclick:this.edit,
    }]
    if ( menulistArr.length > 0 ) {
        const handleOptions = {
            title: '操作',
            key: 'handle',
            width: 100,
            render: (text,record,index)=>{
                return <DropDown record={record} index={index} menuOptions={menulistArr} />
            },
        };
        columns.push(handleOptions);
    }
    this.setState({
        columns,
    });
  };
  addFirst= ()=>{
    // const { pageNum,pageSize } = pagination;
      this.setState({
          title:'菜单-新增',
          formMethod:'addMenu',
      })
      this.props.dispatch({
          type:'menucontroller/showModal',
          payload: {
            name:0,
            route:0,
            parentId:0,
            icon:0,
          },
      })
  }
  addChildrenMenu=(record)=>{
    this.setState({
        title:'菜单-新增子菜单',
        type:'addchild',
    })
    this.props.dispatch({
        type:'menucontroller/showModal',
        payload:{
            parentId:record.id,
        },
    })
  }
  del=(record)=>{
    let _this=this;
    if(record && record.id){
        confirm({
            title: '删除',
            content: '是否删除所选信息？',
            okText:'确定',
            cancelText:'取消',
            onOk() {
                _this.props.dispatch({
                    type:'menucontroller/delMenu',
                    payload:{
                        id:record.id,
                    },
                })
            },
        });
    }
   
  }
  edit=(record)=>{
      this.setState({
        title:'菜单-修改菜单',
        formMethod:'editMenu',
      })
    this.props.dispatch({
        type:'menucontroller/showModal',
        payload:{
            ...record,
        },
    })
  }
  handleOk=(values)=>{
    const { formMethod } = this.state;
    const { menucontroller:{ currentRow } } =this.props;
    this.props.dispatch({
        type:`menucontroller/${formMethod}`,
        payload:{
            ...values,
            id:currentRow.id,
        }
    })
    this.props.dispatch({
        type:'menucontroller/hiddenModal',
      })
  }
  handleCancel=()=>{
      this.props.dispatch({
        type:'menucontroller/hiddenModal',
      })
  }
  render() {
    const { menucontroller:{ dataSource,currentRow } }=this.props;
    const { title,type } =this.state;
    const { menucontroller:{ modalVisible,pagination,} } = this.props;
    
    const pageProps = {
        dataSource:dataSource,
        pagination:false,
    }
    const modalProps = {
        modalVisible,
        title,
        type,
        handleOk:this.handleOk,
        handleCancel:this.handleCancel,
        formInfo:currentRow,
      }
    const BtnList = [{
        icon:'plus',
        name:'新增一级菜单',
        type:'',
        key:'1',
        click:this.addFirst,
    },]
    return (
        <PageHeaderLayout>
        <Card bordered={false}>
            <div style={{ marginBottom: 16 }}>
                  <ToolBar BtnList={BtnList} />
                <AddForm {...modalProps} />
              </div>
              <Table {...pageProps} columns={this.state.columns} rowKey={record => record.id}  />
        </Card>
        </PageHeaderLayout>
    );
  }
}
export default MenuManagement;