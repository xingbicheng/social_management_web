import { InputNumber, Modal, Form, Input,Alert,TreeSelect} from 'antd';
import {FormItemLayoutModal,FormItemLayoutModal1} from "../../config/config";
import {deleteSpace} from "../../utils/utils"

const FormItem = Form.Item;
const TreeNode = TreeSelect.TreeNode;
@Form.create()
class AddForm extends React.Component {
    constructor(props){
        super(props);
    }
    onHandleCreate = () => {
        const { form,handleOk } = this.props;
        // const form = this.formRef.props.form;
        form.validateFields((err, values) => {
          if (!err) {
            let list =deleteSpace(values);
            handleOk(list);
          }else{
            return (
                <Alert
                  description="表单验证失败"
                  type="error"
                />
              )
          }
          form.resetFields();
        });
      }
    onHandleCancel = ()=>{
        const { form,handleCancel } = this.props;
        form.resetFields();
        handleCancel();
            }
    render() {
      const formItemLayout = FormItemLayoutModal;  // 通用样式表
      const formItemLayout1 = FormItemLayoutModal1;  // 通用样式表
      const { modalVisible, form,title,formInfo,treeData } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={modalVisible}
          title={title}
          okText="确定"
          onCancel={this.onHandleCancel}
          onOk={this.onHandleCreate}
        >
          <Form layout="vertical">
          
            <FormItem label="菜单名称:"
            {...formItemLayout}>
              {getFieldDecorator('name', {
                initialValue:formInfo.name || '',
                rules: [{ required: true, message: '请填写菜单名称!' }],
              })(
                <Input placeholder='请输入菜单名称' />
              )}
            </FormItem>
            <FormItem label="上级菜单ID"
            {...formItemLayout1}>
              {getFieldDecorator('parentId', {
                initialValue:String(formInfo.parentId) || '',
                rules: [{ required: true, message: '请填写上级部门ID!' }],
              })(
                <span className="ant-form-text">{String(formInfo.parentId) || ''}</span>
              )}
            </FormItem>
            <FormItem label="图标:"
            {...formItemLayout}>
              {getFieldDecorator('icon', {
                initialValue:formInfo.icon || '',
                rules: [{ required: true, message: '请填写图标!' }],
              })(
                <Input  placeholder='请填写图标' />
              )}
            </FormItem>
            <FormItem label="链接:"
            {...formItemLayout}>
              {getFieldDecorator('route', {
                initialValue:formInfo.route || '',
                rules: [{ required: true, message: '请填写链接!' }],
              })(
                <Input placeholder='请填写链接' />
              )}
            </FormItem>
            <FormItem label="序号:"
            {...formItemLayout}>
              {getFieldDecorator('sort', {
                initialValue:formInfo.sort || '',
                rules: [{ required: true, message: '必须是自然数' }],
              })(
                <InputNumber  placeholder='请填写序号' min={0} precision={0} />,
              )}
            </FormItem>
          
          </Form>
        </Modal>
      );
    }
  }
export default AddForm;