import { createElement } from 'react';
import dynamic from 'dva/dynamic';
import pathToRegexp from 'path-to-regexp';
import { getMenuData } from './menu';

let routerDataCache;

const modelNotExisted = (app, model) =>
  // eslint-disable-next-line
  !app._models.some(({ namespace }) => {
    return namespace === model.substring(model.lastIndexOf('/') + 1);
  });

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => {
  // () => require('module')
  // transformed by babel-plugin-dynamic-import-node-sync
  if (component.toString().indexOf('.then(') < 0) {
    models.forEach(model => {
      if (modelNotExisted(app, model)) {
        // eslint-disable-next-line
        app.model(require(`../models/${model}`).default);
      }
    });
    return props => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return createElement(component().default, {
        ...props,
        routerData: routerDataCache,
      });
    };
  }
  // () => import('module')
  return dynamic({
    app,
    models: () =>
      models.filter(model => modelNotExisted(app, model)).map(m => import(`../models/${m}.js`)),
    // add routerData prop
    component: () => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return component().then(raw => {
        const Component = raw.default || raw;
        return props =>
          createElement(Component, {
            ...props,
            routerData: routerDataCache,
          });
      });
    },
  });
};

function getFlatMenuData(menus) {
  let keys = {};
  menus.forEach(item => {
    if (item.children) {
      keys[item.path] = { ...item };
      keys = { ...keys, ...getFlatMenuData(item.children) };
    } else {
      keys[item.path] = { ...item };
    }
  });
  return keys;
}

export const getRouterData = app => {
  const routerConfig = {
    '/': {
      component: dynamicWrapper(app, ['user', 'login'], () => import('../layouts/BasicLayout')),
    },
    '/dashboard': {
      component: dynamicWrapper(app, ['dashboard'], () => import('../routes/DashBoard')),
    },
    '/list/advancelist': {
      component: dynamicWrapper(app, ['department'], () => import('../routes/AdvanceList/AdvanceList')),
    },
    '/accept': {
      component: dynamicWrapper(app, [], () => import('../routes/CommonList/PageForm')),
    },
    '/systemset/datadict': {
      component: dynamicWrapper(app, ['dictdata'], () => import('../routes/DataDict/index')),
    },
    '/systemset/departmentmanage': {
      component: dynamicWrapper(app, ['department'], () => import('../routes/Department')),
    },
    '/systemset/usermanagement': {
      component: dynamicWrapper(app, ['usercontroller'], () => import('../routes/UserManagement/index')),
    },
    '/systemset/rolemanagement': {
      component: dynamicWrapper(app, ['rolecontroller'], () => import('../routes/RoleManagement/index')),
    },
    '/systemset/menumanagement': {
      component: dynamicWrapper(app, ['menucontroller'], () => import('../routes/MenuManagement/index')),
    },
    '/information/citizen': {
      component: dynamicWrapper(app, ['citizen'], () => import('../routes/CitizenService/index')),
    },
    '/information/dispute':{
      component: dynamicWrapper(app, ['dispute'],() => import('../routes/Dispute/index')),
    },
    '/information/pageForm/:id':{
      component: dynamicWrapper(app, ['dispute'],() => import('../routes/Dispute/PageForm')),
    },
    '/information/accept/:id':{
      component: dynamicWrapper(app, ['citizen', 'dispute'],() => import('../routes/CitizenService/Accept')),
    },
    '/information/handover/:type/:id': {
      component: dynamicWrapper(app, ['citizen'], () => import('../routes/CitizenService/Handover')),
    },
    '/information/handoverEdit/:type/:id': {
      component: dynamicWrapper(app, ['citizen'], () => import('../routes/CitizenService/HandoverEdit')),
    },
    '/person/otherInfo/:type/:idCard': {
      component: dynamicWrapper(app, ['otherpeople'], () => import('../routes/Person/PersonOthers/OtherInfo')),
    },
    '/person/personother': {
      component: dynamicWrapper(app, ['otherpeople'], () => import('../routes/Person/PersonOthers')),
    },
    '/person/personbase': {
      component: dynamicWrapper(app, ['personbase'], () => import('../routes/Person/PersonBase')),
    },
    '/person/baseInfo/:id': {
      component: dynamicWrapper(app, ['personbase'], () => import('../routes/Person/PersonBase/BaseInfo')),
    },
    '/person/personDrug': {
      component: dynamicWrapper(app, ['persondrug'], () => import('../routes/Person/PersonDrug')),
    },
    '/person/personRelease': {
      component: dynamicWrapper(app, ['personrelease'], () => import('../routes/Person/PersonRelease')),
    },
    '/person/releaseInfo/:type/:id/:idCard': {
      component: dynamicWrapper(app, ['personrelease'], () => import('../routes/Person/PersonRelease/ReleaseInfo')),
    },
    '/person/druginfo/:type/:idCard':{
      component: dynamicWrapper(app, ['persondrug'], () => import('../routes/Person/PersonDrug/DrugInfo')),
    },
    '/person/releaseHelper/:id/:idCard': {
      component: dynamicWrapper(app, ['releasehelper'], () => import('../routes/Person/PersonRelease/PersonReleaseHelper')),
    },
    '/person/releasePolicy/:id/:idCard': {
      component: dynamicWrapper(app, ['releasepolicy'], () => import('../routes/Person/PersonRelease/PersonReleasePolicy')),
    },
    '/person/policyForm/:id/:id': {
      component: dynamicWrapper(app, ['releasepolicy'], () => import('../routes/Person/PersonRelease/PersonReleasePolicy/PolicyForm')),
    },
    '/person/releaseEdu/:id/:idCard': {
      component: dynamicWrapper(app, ['releaseeducation'], () => import('../routes/Person/PersonRelease/PersonReleaseEdu')),
    },
    '/user': {
      component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    },
    '/user/login': {
      component: dynamicWrapper(app, ['login'], () => import('../routes/User/Login')),
    },
    '/user/register': {
      component: dynamicWrapper(app, ['register'], () => import('../routes/User/Register')),
    },
    '/user/register-result': {
      component: dynamicWrapper(app, [], () => import('../routes/User/RegisterResult')),
    },
    '/gridInfoManage/department': {
      component: dynamicWrapper(app, ['infoDepartment'], () => import('../routes/InfoInput/Department')),
    },

    '/gridInfoManage/area/:id': {
      component: dynamicWrapper(app, ['infoArea'], () => import('../routes/InfoInput/Area')),
    },
    '/gridInfoManage/areaEdit/:id': {
      component: dynamicWrapper(app, ['infoAreaDetail'], () => import('../routes/InfoInput/Area/detail')),
    },
    '/gridInfoManage/areaAdd/:id': {
      component: dynamicWrapper(app, ['infoAreaDetail'], () => import('../routes/InfoInput/Area/detail')),
    },
    '/gridInfoManage/grid/:id': {
      component: dynamicWrapper(app, ['infoGrid'], () => import('../routes/InfoInput/Grid')),
    },
    '/gridInfoManage/gridEdit/:id': {
      component: dynamicWrapper(app, ['infoGridDetail'], () => import('../routes/InfoInput/Grid/detail')),
    },
    '/gridInfoManage/gridAdd/:id': {
      component: dynamicWrapper(app, ['infoGridDetail'], () => import('../routes/InfoInput/Grid/detail')),
    },
    '/gridInfoManage/house/:id': {
      component: dynamicWrapper(app, ['infoHouse'], () => import('../routes/InfoInput/House')),
    },
    '/gridInfoManage/houseEdit/:id': {
      component: dynamicWrapper(app, ['infoHouseDetail'], () => import('../routes/InfoInput/House/detail')),
    },
    '/gridInfoManage/houseAdd/:id': {
      component: dynamicWrapper(app, ['infoHouseDetail'], () => import('../routes/InfoInput/House/detail')),
    },
    '/gridInfoManage/houseAddPerson/:id/:idCard': {
      component: dynamicWrapper(app, ['infoHouseAddPerson'], () => import('../routes/InfoInput/House/addPage')),
    },
    '/gridInfoManage/houseEditPerson/:houseId/:itemId/:personId': {
      component: dynamicWrapper(app, ['infoHouseAddPerson'], () => import('../routes/InfoInput/House/addPage')),
    },
    '/risk/report': {
      component: dynamicWrapper(app, ['report'], () => import('../routes/Risk/index')),
    },
    '/risk/detail/:id/:type': {
      component: dynamicWrapper(app, ['report'], () => import('../routes/Risk/ReportDetail')),
    },
    '/infoPlatform/notice': {
      component: dynamicWrapper(app, ['infoPlatform'], () => import('../routes/InfoPlatform/Notice/index')),
    },
    '/infoPlatform/noticeDetail/:id/:type': {
      component: dynamicWrapper(app, ['infoPlatform'], () => import('../routes/InfoPlatform/Notice/NoticeDetail')),
    },
    '/infoPlatform/submit': {
      component: dynamicWrapper(app, ['infoSubmit'], () => import('../routes/InfoPlatform/Submit/index')),
    },
    '/infoPlatform/submitDetail/:id/:type': {
      component: dynamicWrapper(app, ['infoSubmit'], () => import('../routes/InfoPlatform/Submit/SubmitDetail')),
    },
    '/infoPlatform/check/:id/:type': {
      component: dynamicWrapper(app, ['checkPage'], () => import('../routes/InfoPlatform/CheckDetail')),
    },

    /* '/exception/403': {
        component: dynamicWrapper(app, [], () => import('../routes/Exception/403')),
    },
    '/exception/404': {
        component: dynamicWrapper(app, [], () => import('../routes/Exception/404')),
    },
    '/exception/500': {
        component: dynamicWrapper(app, [], () => import('../routes/Exception/500')),
    }, */
    '/pageinfo': {
      component: dynamicWrapper(app, [], () => import('../components/PageInfo/PageInfo')),
    },
  };
  // Get name from ./menu.js or just set it in the router data.
  const menuData = getFlatMenuData(getMenuData());

  // Route configuration data
  // eg. {name,authority ...routerConfig }
  const routerData = {};
  // The route matches the menu
  Object.keys(routerConfig).forEach(path => {
    // Regular match item name
    // eg.  router /user/:id === /user/chen
    const pathRegexp = pathToRegexp(path);
    const menuKey = Object.keys(menuData).find(key => pathRegexp.test(`${key}`));
    let menuItem = {};
    // If menuKey is not empty
    if (menuKey) {
      menuItem = menuData[menuKey];
    }
    let router = routerConfig[path];
    // If you need to configure complex parameter routing,
    // https://github.com/ant-design/ant-design-pro-site/blob/master/docs/router-and-nav.md#%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E8%B7%AF%E7%94%B1%E8%8F%9C%E5%8D%95
    // eg . /list/:type/user/info/:id
    router = {
      ...router,
      name: router.name || menuItem.name,
      authority: router.authority || menuItem.authority,
      hideInBreadcrumb: router.hideInBreadcrumb || menuItem.hideInBreadcrumb,
    };
    routerData[path] = router;
  });
  return routerData;
};
