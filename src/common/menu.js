import { isUrl } from '../utils/utils';

let menuData = [
  {
    name: '首页',
    icon: 'dashboard',
    path: 'dashboard',
  },
  {
    name: '系统设置',
    icon: 'setting',
    path: 'systemset',
    children: [{
      name: '数据字典',
      path: 'datadict',
    }, {
      name: '部门管理',
      path: 'departmentmanage',
    }, {
      name: '用户管理',
      path: 'usermanagement',
    }, {
      name: '角色管理',
      path: 'rolemanagement',
    }, {
      name: '菜单管理',
      path: 'menumanagement',
    }],
  },

  {
    name: '人员管理信息',
    icon: 'contacts',
    path: 'person',
    children: [
      {
        name: '吸毒人员',
        path: 'persondrug',
      },
      {
        name: '其他人员',
        path: 'personother',
      },
      {
        name: '刑满释放人员',
        path: 'personrelease',
      },
      {
        name: '基本人员信息',
        path: 'personbase',
      },
    ],
  },
  {
    name: '受理信息',
    icon: 'form',
    path: 'information',
    children: [
      {
        name: '公民服务',
        path: 'citizen',
      },
      {
        name: '矛盾纠纷',
        path: 'dispute',
      },

    ],
  }, {
    name: '社会稳定风险评估',
    icon: 'pie-chart',
    path: 'risk',
    children: [
      {
        name: '填报报表',
        path: 'report',
      },
      {
        name: '报表详情',
        path: 'detail',
        hideInMenu: true,
      },
      {
        name: '新增报表',
        path: 'add',
        hideInMenu: true,
      }],
  },
  {
    name: '信息平台',
    icon: 'book',
    path: 'infoPlatform',
    children: [
      {
        name: '通知通报',
        path: 'notice',
      },
      {
        name: '报送信息',
        path: 'submit',
      },
      {
        name: '报送审核',
        path: 'examine',
        hideInMenu: true,
      },
      {
        name: '报送详情',
        path: 'submitDetail',
        hideInMenu: true,
      },
      {
        name: '新增通知',
        path: 'addNotice',
        hideInMenu: true,
      },
      {
        name: '通知详情',
        path: 'noticeDetail',
        hideInMenu: true,
      },
      {
        name: '查看详情',
        path: 'check',
        hideInMenu: true,
      }],
  },
  {
    name: '网格化管理',
    icon: 'table',
    path: 'gridInfoManage',
    children: [
      {
        name: '信息录入',
        path: 'department',
      },
      {
        name: '片区管理',
        path: 'area',
        hideInMenu: true,
      }, {
        name: '网格管理',
        path: 'grid',
        hideInMenu: true,
      }, {
        name: '房屋管理',
        path: 'house',
        hideInMenu: true,
      },
    ],
  }, /* ,{
        name: '页面详情',
        path: 'pageinfo',
    } */
];

function changeMenuData() {
  const data = sessionStorage.getItem('menuData');
  menuData = JSON.parse(data) || menuData;
}

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => {
  // changeMenuData();
  return formatter(menuData);
};
