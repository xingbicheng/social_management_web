import axios from 'axios';
import pathToRegexp from 'path-to-regexp'
import { message } from 'antd';
import { checkStatus, handleError } from './checkStatus';
import tokenHandler from '../tokenHandler';

export default function axiosRequest(url, options) {
    let cloneData = null;
    const { body } = options;

    // 构造请求头
    let config = {};
    if (url.endsWith('/excel')) {
        config = {
            headers: {
                token: tokenHandler.getSessionByKey('token'),
            },
            responseType: 'blob',
        }
    }else if (url.indexOf('getcaptcha') !== -1) {
        config = {
            headers: {
                Accept: 'image/png',
            },
            responseType: 'arraybuffer',
        }
    } else if (url.indexOf('files/upload') !== -1) {
        config = {
            headers: {
                token: tokenHandler.getSessionByKey('token'),
                'Content-Type': 'multipart/form-data',
            },
            responseType: 'json',
        }
    } else if (url.indexOf('getimg') !== -1) {
        config = {
            headers: {
                token: tokenHandler.getSessionByKey('token'),
                Accept: 'image/png',
            },
            responseType: 'arraybuffer',
        }
    } else {
        config = {
            headers: {
                token: tokenHandler.getSessionByKey('token'),
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            responseType: 'json',
        }
    }
    // config.headers.token = '6d40222b-05c6-4508-8be4-b8624a368a81';

    const newOptions = { ...config, ...options };

    newOptions.url = url;
    if ('method' in newOptions) {
        newOptions.method = newOptions.method.toUpperCase();
    } else {
        newOptions.method = 'GET';
    }

    if (newOptions.method !== 'GET') {
        if (!(newOptions.body instanceof FormData)) {
            newOptions.data = JSON.stringify(newOptions.body);
        } else {
            newOptions.data = newOptions.body;
        }
    } else {
        newOptions.params = newOptions.body;
    }

    axios.defaults.withCredentials = true

    return axios(newOptions).then(response => {
        const { statusText, status, data:resultData } = response;
        if (url.indexOf('getcaptcha') !== -1) {
            const base64Str = btoa(new Uint8Array(response.data).reduce((datas, byte) => datas + String.fromCharCode(byte), ''))
            return Promise.resolve({
                code : 0,
                data : `data:image/png;base64,${base64Str}`,
            })
        } else if (url.indexOf('getimg') !== -1) {
            const base64Str = btoa(new Uint8Array(response.data).reduce((datas, byte) => datas + String.fromCharCode(byte), ''))
            return Promise.resolve({
                code:0,
                data:`data:image/png;base64,${base64Str}`,
            })
        } else if (url.endsWith('/excel')) {
            return Promise.resolve({
                success: true,
                message: statusText,
                statusCode: status,
                resultData,
            })
        }
        return Promise.resolve({
            success: true,
            message: statusText,
            statusCode: status,
            ...resultData,
        })
    }).catch(handleError);
}
