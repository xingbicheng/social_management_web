export default function handleResponse(response){
    const { config:{url},status,statusText,data } = response;
    if (url.indexOf('getcaptcha') !== -1) {
        const base64Str = btoa(new Uint8Array(data).reduce((datas, byte) => datas + String.fromCharCode(byte), ''));
        return Promise.resolve({
            code : 0,
            data : `data:image/png;base64,${base64Str}`,
        })
    } else if (url.indexOf('getimg') !== -1) {
        const base64Str = btoa(new Uint8Array(data).reduce((datas, byte) => datas + String.fromCharCode(byte), ''));
        return Promise.resolve({
            code:0,
            data:`data:image/png;base64,${base64Str}`,
        })
    } else if (url.endsWith('/excel')) {
        return Promise.resolve({
            success: true,
            message: statusText,
            statusCode: status,
            data,
        })
    }
    return Promise.resolve({
        success: true,
        message: statusText,
        statusCode: status,
        ...data,
    })
}
