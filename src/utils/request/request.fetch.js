import axios from 'axios';
import pathToRegexp from 'path-to-regexp'
import { message } from 'antd';
import { checkStatus, handleError } from './checkStatus';

export default function fetchRequest(url, options) {
    let config = {};
    if(url.indexOf('/getcaptcha') !== -1){
        config = {
            headers:{
                Accept:"image/png",
            },
        }
    }else{
        config = {
            headers:{
                'token':'6d40222b-05c6-4508-8be4-8624a368a81',
                'Content-Type':'application/json',
                Accept:"application/json",
            },
        }
    }
    let newConfig = {...config,...options};
    newConfig.url = url;
    // 转换method大小写，并设置默认值 GET
    if('method' in newConfig){
        newConfig.method = newConfig.method.toUpperCase();
    }else{
        newConfig.method = 'POST';
    }
    //
    if(newConfig.method !== 'GET'){
        // 判断body的内容是一个数据表单
        if(!(newConfig.body instanceof FormData)){
            newConfig.data = JSON.stringify(newConfig.body);
        }else{
            newConfig.data = newConfig.body;
        }
    }else{
        newConfig.params = newConfig.body;
    }
    axios.defaults.withCredentials = true
    return axios(newConfig)
        .then(checkStatus)
        .then(response =>{
            const { status,statusText } = response;
            // 如果是验证码需要转图片为base64编码
            if(url.indexOf('/getcaptcha') !== -1){

            }
        }).catch(handleError);
}
