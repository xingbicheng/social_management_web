const SEXLIST = {
    MAN:'0',
    WOMEN:'1',
}
const MATTERLEVEN = {
  NORMAL:'0',
  IMPORTANT:'1',
}

const INOTICE = {
  SAVE:'0',
  RELEASE:'1',
}

const ISUBMITTED = {
    SAVE:'0',
    SUBMIT:'1',
    AUDITFIAL:'2',
    AUDITSUCCESS:'3',
    RELEASE:'4',
}

const AceeptInfo = {
    DISPUTE:'0',
    SERVICE:'1',
}
const RISKSTATE = {
  SAVE:'1',
  SUBMIT:'2',
  EXAMINE:'3',
  SIGN:'4',
}
const EVALUTIONRESULT = {
  APPROVE:'1',
  REPRIEVE:'2',
  FORBIT:'3',
}
const RISKTYPE = {
  OTHER:'0',
  REFORM:'1',
  POLICY:'2',
  DECISION:'3',
  ENGINEERING:'4',
}
const SUBMITCATEGORY = {
  WORK:'1',
  EXPERIENCE:'2',
  ARTICLE:'3',
  DEMEANOR:'4',
}
const SUBMITSTATE = {
  SAVE:'0',
  SUBMIT:'1',
  EXAMINEFAIL:'2',
  EXAMINEPASS:'3',
  PUBLISH:'4',
}
const NOTICECATEGORY = {
  NOTICE:'0',
  POLICY:'1',
}
const NOTICESTATE = {
  SAVE:'0',
  PUBLISH:'1',
}


module.exports = {
    SEXLIST,
    MATTERLEVEN,
    ISUBMITTED,
    AceeptInfo,
    RISKSTATE,
    EVALUTIONRESULT,
    RISKTYPE,
    SUBMITCATEGORY,
    SUBMITSTATE,
    NOTICECATEGORY,
    NOTICESTATE,
    INOTICE,
};
