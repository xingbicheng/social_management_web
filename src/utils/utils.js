import moment from 'moment';
import { parse, stringify } from 'qs';
import remoteLinkAddress from '../../public/ip.js';
import selectData, { place } from './seletLocalData';
import lodash from 'lodash'

export function fixedZero(val) {
  return val * 1 < 10 ? `0${val}` : val;
}

export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);

    if (day === 0) {
      day = 6;
    } else {
      day -= 1;
    }

    const beginTime = now.getTime() - day * oneDay;

    return [moment(beginTime), moment(beginTime + (7 * oneDay - 1000))];
  }

  if (type === 'month') {
    const year = now.getFullYear();
    const month = now.getMonth();
    const nextDate = moment(now).add(1, 'months');
    const nextYear = nextDate.year();
    const nextMonth = nextDate.month();

    return [
      moment(`${year}-${fixedZero(month + 1)}-01 00:00:00`),
      moment(moment(`${nextYear}-${fixedZero(nextMonth + 1)}-01 00:00:00`).valueOf() - 1000),
    ];
  }

  if (type === 'year') {
    const year = now.getFullYear();

    return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
  }
}

export function formatterTime(type,time){
    if(time === ''){
        return ;
    }
    let formatter = '';
    if(type === 'date'){
        formatter = 'yyyy-MM-dd';
    }else if(type === 'time'){
        return getNowFormatDate(time);
    }else{
        formatter = type;
    }
    let date = new Date(time);
    return formatter.replace(/yyyy/g,date.getFullYear()).replace(/MM/g,date.getMonth() + 1).replace(/dd/g,date.getDate()).replace(/hh/g,date.getHours()).replace(/mm/g,date.getMinutes()).replace(/ss/g,date.getSeconds());
}

export function getPlainNode(nodeList, parentPath = '') {
  const arr = [];
  nodeList.forEach(node => {
    const item = node;
    item.path = `${parentPath}/${item.path || ''}`.replace(/\/+/g, '/');
    item.exact = true;
    if (item.children && !item.component) {
      arr.push(...getPlainNode(item.children, item.path));
    } else {
      if (item.children && item.component) {
        item.exact = false;
      }
      arr.push(item);
    }
  });
  return arr;
}

function accMul(arg1, arg2) {
  let m = 0;
  const s1 = arg1.toString();
  const s2 = arg2.toString();
  m += s1.split('.').length > 1 ? s1.split('.')[1].length : 0;
  m += s2.split('.').length > 1 ? s2.split('.')[1].length : 0;
  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / 10 ** m;
}

export function digitUppercase(n) {
  const fraction = ['角', '分'];
  const digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
  const unit = [['元', '万', '亿'], ['', '拾', '佰', '仟', '万']];
  let num = Math.abs(n);
  let s = '';
  fraction.forEach((item, index) => {
    s += (digit[Math.floor(accMul(num, 10 * 10 ** index)) % 10] + item).replace(/零./, '');
  });
  s = s || '整';
  num = Math.floor(num);
  for (let i = 0; i < unit[0].length && num > 0; i += 1) {
    let p = '';
    for (let j = 0; j < unit[1].length && num > 0; j += 1) {
      p = digit[num % 10] + unit[1][j] + p;
      num = Math.floor(num / 10);
    }
    s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
  }

  return s
    .replace(/(零.)*零元/, '元')
    .replace(/(零.)+/g, '零')
    .replace(/^整$/, '零元整');
}

function getRelation(str1, str2) {
  if (str1 === str2) {
    console.warn('Two path are equal!'); // eslint-disable-line
  }
  const arr1 = str1.split('/');
  const arr2 = str2.split('/');
  if (arr2.every((item, index) => item === arr1[index])) {
    return 1;
  } else if (arr1.every((item, index) => item === arr2[index])) {
    return 2;
  }
  return 3;
}

function getRenderArr(routes) {
  let renderArr = [];
  renderArr.push(routes[0]);
  for (let i = 1; i < routes.length; i += 1) {
    // 去重
    renderArr = renderArr.filter(item => getRelation(item, routes[i]) !== 1);
    // 是否包含
    const isAdd = renderArr.every(item => getRelation(item, routes[i]) === 3);
    if (isAdd) {
      renderArr.push(routes[i]);
    }
  }
  return renderArr;
}

/**
 * Get router routing configuration
 * { path:{name,...param}}=>Array<{name,path ...param}>
 * @param {string} path
 * @param {routerData} routerData
 */
export function getRoutes(path, routerData) {
  let routes = Object.keys(routerData).filter(
    routePath => routePath.indexOf(path) === 0 && routePath !== path
  );
  // Replace path to '' eg. path='user' /user/name => name
  routes = routes.map(item => item.replace(path, ''));
  // Get the route to be rendered to remove the deep rendering
  const renderArr = getRenderArr(routes);
  // Conversion and stitching parameters
  const renderRoutes = renderArr.map(item => {
    const exact = !routes.some(route => route !== item && getRelation(route, item) === 1);
    return {
      exact,
      ...routerData[`${path}${item}`],
      key: `${path}${item}`,
      path: `${path}${item}`,
    };
  });
  return renderRoutes;
}


export function getQueryPath(path = '', query = {}) {
  const search = stringify(query);
  if (search.length) {
    return `${path}?${search}`;
  }
  return path;
}


/* eslint no-useless-escape:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/g;

export function isUrl(path) {
  return reg.test(path);
}
export function createTheURL(modelAPI, interfaceType) {
    const temp = modelAPI.split('');
    if (temp[temp.length - 1] !== '/') {
        temp.push('/');
    }
    const baseURL = remoteLinkAddress();
    const IP = baseURL + temp.join('') + interfaceType;
    // if (isUrl(IP)) {
    //   return IP;
    // } else {
    //   Toast.fail('非合法的IP地址，请检查传入的地址');
    // }
    return IP;
}

// 利用递归建立树级联数据结构  根据superFlag来明确上下级关系 identify是用来表明自己的位置关系
function createCascader(dataSource, superFlag, identify, ) {
  const CascaderOptions = [];
  for (let i = 0; i < dataSource.length; i++) {
    const hierarchy = dataSource[i].superiorDepartmentId;
    if ( ! hierarchy ) {
      CascaderOptions.push({ value: dataSource[i].name, label: dataSource[i].name, id: dataSource[i].id })
    } else {
      while(true) {

        break;
      }
    }
  }
}

export function constructMenuTreeData (data, superiorDepartmentId = 0){
  const tree = [];
  for (let i = 0; i < data.length; i++) {
      if (data[i].superiorDepartmentId == superiorDepartmentId) {
          let obj = data[i];
          const temp = constructMenuTreeData(data, data[i].value);
          if (temp.length > 0) {
           obj.children = temp;
          }
          tree.push(obj);
      }
  }
  return tree;
}
export function constructMenuTreeData1 (data, parentId = 0){
  const tree = [];
  for (let i = 0; i < data.length; i++) {
      if (data[i].parentId == parentId) {
          let obj = data[i];
          const temp = constructMenuTreeData1(data, data[i].key);
          if (temp.length > 0) {
           obj.children = temp;
          }
          tree.push(obj);
      }
  }
  return tree;
}
export function constructMenuTreeData2 (data, parentId = 0){
  const tree = [];
  for (let i = 0; i < data.length; i++) {
      if (data[i].parentId == parentId) {
          let obj = data[i];
          const temp = constructMenuTreeData2(data, data[i].id);
          if (temp.length > 0) {
           obj.children = temp;
          }
          tree.push(obj);
      }
  }
  return tree;
}
export function replaceKeyData1(data){
  let replaceTreeData = [];
  for(let i= 0;i<data.length;i++){
    let obj={};
    obj.title=data[i].name;
    obj.key=String(data[i].id);
    // obj.createUserId=data[i].createUserId;
    // obj.memo=data[i].memo;
    // obj.status=data[i].status;
    // obj.updateUserId=data[i].updateUserId;
    obj.parentId=data[i].parentId;
    replaceTreeData.push(obj);
  }
  return replaceTreeData;

}

export function replaceKeyData(data){
  let replaceTreeData = [];
  for(let i= 0;i<data.length;i++){
    let obj={};
    obj.label=data[i].name;
    obj.value=String(data[i].id);
    obj.createUserId=data[i].createUserId;
    obj.memo=data[i].memo;
    obj.status=data[i].status;
    obj.updateUserId=data[i].updateUserId;
    obj.superiorDepartmentId=data[i].superiorDepartmentId;
    replaceTreeData.push(obj);
  }
  return replaceTreeData;
}
/**
 * 数组格式转树状结构
 * @param   {array}     array
 * @param   {String}    id
 * @param   {String}    pid
 * @param   {String}    children
 *
 * @return  {Array}
 */
export function arrayToTree(array, id = 'id', pid = 'pid', children = 'children') {
  const data = lodash.cloneDeep(array)
  const result = []
  const hash = {}
  data.forEach((item, index) => {
    hash[data[index][id]] = data[index]
  })

  data.forEach((item) => {
    const hashVP = hash[item[pid]]
    if (hashVP) {
      !hashVP[children] && (hashVP[children] = [])
      hashVP[children].push(item)
    } else {
      result.push(item)
    }
  })
  return result
}

/**
 * 在数状结构当中递归查找children
 * @param {array} array
 * @param {string} id
 * @param {} id的值
 * @return {array}
 */

 export function findChildren(array, id = "id", value, children = "children") {
   const data = lodash.cloneDeep(array)
   for( let i = 0; i < data.length; i++ ) {
    if (data[i][id] == value) {
      return data[i][children]
    } else if (data[i][children]){
      const result = findChildren(data[i][children], id, value, children)
      if (result) {
        return result
      }
    }
   }
 }
// 日期转换,将你的获取到的日期传入函数,将返回一个后台需要的日期格式
 export function getNowFormatDate(time) {
  const date = time ? new Date(time) : new Date();
  const seperator1 = '-';
  const seperator2 = ':';
  let month = date.getMonth() + 1;
  let strDate = date.getDate();
  if (month >= 1 && month <= 9) {
    month = `0${month}`;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = `0${strDate}`;
  }
  const currentdate = `${date.getFullYear() + seperator1 + month + seperator1 + strDate
  } ${date.getHours()}${seperator2}${date.getMinutes()
  }${seperator2}${date.getSeconds()}`;
  return currentdate;
}

 /**
  * 根据id查找记录
  * @param {array} array
  * @param {string} id
  * @param {} id的值
  */
 export function findRecord(array, id = 'id', value) {
   let result = {};
   for( let i = 0; i < array.length; i++) {
     if (array[i][id] == value) {
      result = array[i];
      break;
     }
   }
   return result;
 }

 /**
  *  Select框的可搜索框属性
  */
 export var SelectProps = {
  showSearch: true,
  optionFilterProp: 'children',
  filterOption: (input, option) => {
    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  },
}

/*
* @param {string} type 选择的数据名称
* @param {string} data 需要映射的数据
* */
export function formmaterData(type,data) {
    const dataMap = selectData[type];
    let result = null;
    dataMap.map((item)=> {
        if (item.key === data) {
            result =  item.value;
        }
    });
    return result;
}

/*
* @param {string} vale 需要转换为字符串的数组
*/

export function changeToString (value) {
  const provinceValue = value[0];
  const  cityValue = value[1];
  const  areaValue = value[2];
  for (let i = 0; i < place.length; i++) {
    if (place[i].value === provinceValue) {
      const province = place[i].label;
      const citys = place[i].children;
      for (let j = 0; j < citys.length; j++) {
        if (cityValue === citys[j].value) {
          const city = citys[j].label
          const areas = citys[j].children;
          for (let k = 0; k < areas.length; k++) {
            if (areaValue === areas[k].value) {
              const area = areas[k].label;
              return (province + city + area);
            }
          }
        }
      }
    }
  }
}


/*
* @param {string} vale 需要转换为数组的数组
*/
export function changeToArray (defaultPlace) {
  if(typeof(defaultPlace) === 'number'||typeof(defaultPlace) === 'undefined') return;
  const placeData = [];
  const pr = defaultPlace.slice(0, 3);// 省简写
  for (let i = 0; i < place.length; i++) {
    if (place[i].label.indexOf(pr) > -1) {
      placeData.push(place[i].value);
      const ci = defaultPlace.slice(place[i].label.length, place[i].label.length + 3);
      const citys = place[i].children;
      for (let j = 0; j < citys.length; j++) {
        if (citys[j].label.indexOf(ci) > -1) {
          placeData.push(citys[j].value);
          const ar = defaultPlace.slice(place[i].label.length + citys[j].label.length, place[i].label.length + citys[j].label.length + 2);
          const areas = citys[j].children;
          for (let k = 0; k < areas.length; k++) {
            if (areas[k].label.indexOf(ar) > -1) {
              placeData.push(areas[k].value)
              return placeData;
            }
          }
        }
      }
    }
  }
}

/*
* 查找对象中的字符串属性，并删除其两端空格
*/
export function deleteSpace(data) {
  for (let i = 0; i < Object.keys(data).length; i++) {
    let key = Object.keys(data)[i];
    if (typeof data[key] === 'string') {
      data[key] = data[key].trim();
    }
  }
  return data;
}
/*
* 用于验证富文本框中内容是否为纯空格和换行
*/
export function checkQuillSpace(data) {
  const quillReg = /^(<p>(&nbsp;[ ]?)*|(<br>)*<\/p>)+$/g;
  return quillReg.test(data);
}
